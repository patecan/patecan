const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

const db = admin.firestore();

/*_________________________________ FOLLOW _________________________________*/
exports.onCreateFollower = functions.region("asia-southeast1").firestore
    .document("followers/{userFollowedId}/userFollowers/{followerId}")
    .onCreate(async (snapshot, context) => {
        const userFollowedId = context.params.userFollowedId;
        const followerId = context.params.followerId;

        console.log("userId ${userId} followerId ${followerId}");

        // 1. Get followed user posts
        const followedUserPost = await db
            .collection("posts")
            .doc(userFollowedId)
            .collection("userPosts")
            .get();

        // 2. Get following user posts
        const timelinePost = db
            .collection("timeline")
            .doc(followerId)
            .collection("timelinePosts");

        // 3. Add following Post to follower TimeLine
        followedUserPost.forEach((document) => {
            if (document.exists) {
                const postID = document.id;
                const dataFromPost = document.data();
                timelinePost.doc(postID).set(dataFromPost);
            }
        });
    });

/*_________________________________ UNFOLLOW _________________________________*/
exports.onUnFollower = functions.region("asia-southeast1").firestore
    .document("followers/{userId}/userFollowers/{followerId}")
    .onDelete(async (snapshot, context) => {
        const userId = context.params.userId;
        const followerId = context.params.followerId;

        console.log("deleted");
        // 1. Get followed user posts
        const timelinePosts = db
            .collection("timeline")
            .doc(followerId)
            .collection("timelinePosts")
            .where("ownerID", "==", userId);

        // 2. Delete follower Post from TimeLine
        const querySnapshot = await timelinePosts.get();

        querySnapshot.forEach((doc) => {
            if (doc.exists) {
                console.log("document exist");
                console.log(doc.id);
                console.log(doc.data());
                doc.ref.delete();
            }
        });
    });

/*_________________________________ CREATE POST _________________________________*/
exports.onCreatePost = functions.region("asia-southeast1").firestore
    .document("posts/{userId}/userPosts/{postId}")
    .onCreate(async (snapshot, context) => {
        const postCreated = snapshot.data();
        const userId = context.params.userId;
        const postId = context.params.postId;

        const querySnapshot = await db
            .collection("followers")
            .doc(userId)
            .collection("userFollowers")
            .get();

        querySnapshot.forEach((document) => {
            const followerId = document.id;
            db.collection("timeline")
                .doc(followerId)
                .collection("timelinePosts")
                .doc(postId)
                .set(postCreated);
        });
    });

/*_________________________________ UPDATE POST _________________________________*/
exports.onUpdatePost = functions.region("asia-southeast1").firestore
    .document("posts/{userId}/userPosts/{postId}")
    .onUpdate(async (change, context) => {
        const postUpdated = change.after.data();
        const userId = context.params.userId;
        const postId = context.params.postId;

        const querySnapshot = await db
            .collection("followers")
            .doc(userId)
            .collection("userFollowers")
            .get();

        querySnapshot.forEach((document) => {
            const followerId = document.id;
            db.collection("timeline")
                .doc(followerId)
                .collection("timelinePosts")
                .doc(postId)
                .get()
                .then((document) => {
                    if (document.exists) {
                        document.ref.update(postUpdated);
                    }
                });
        });
    });

/*_________________________________ DELETE POST _________________________________*/
exports.onDeletePost = functions.region("asia-southeast1").firestore
    .document("posts/{userId}/userPosts/{postId}")
    .onDelete(async (snapshot, context) => {
        const userId = context.params.userId;
        const postId = context.params.postId;

        const querySnapshot = await db
            .collection("followers")
            .doc(userId)
            .collection("userFollowers")
            .get();

        querySnapshot.forEach((document) => {
            const followerId = document.id;
            db.collection("timeline")
                .doc(followerId)
                .collection("timelinePosts")
                .doc(postId)
                .get()
                .then((document) => {
                    if (document.exists) {
                        document.ref.delete();
                    }
                });
        });
    });

/*_________________________________ POST NOTIFICATION _________________________________*/
exports.onCreateNotification = functions
    .region("asia-southeast1")
    .firestore.document("notifications/{userId}/postNotifications/{postId}")
    .onCreate(async (snapshot, context) => {
        const userId = context.params.userId;

        const querySnapshot = await db.doc(`users/${userId}`).get();

        const androidNotificationToken =
            querySnapshot.data().androidNotificationToken;

        if (androidNotificationToken != null) {
            sendNotification(androidNotificationToken, snapshot.data());
        } else {
            console.log("no notification");
        }

        function sendNotification(androidNotifyToken, notificationDocument) {
            let body;
            switch (notificationDocument.type) {
                case "comment":
                    body = `${notificationDocument.usernameNotify} comment on your post`;
                    break;
                case "like":
                    body = `${notificationDocument.usernameNotify} like your post`;
                    break;
                case "follow":
                    body = `${notificationDocument.usernameNotify} follow you`;
                    break;
                default:
                    break;
            }

            const message = {
                data: {
                    user: JSON.stringify(querySnapshot),
                },
            };

            admin
                .messaging()
                .sendToDevice(androidNotificationToken, message)
                .then((response) => {
                    console.log("Successfully sent message:", response);
                })
                .catch((error) => {
                    console.log("Error sending message:", error);
                });
        }
    });


/*_________________________________ STORE NOTIFICATION _________________________________*/

exports.onCreateStoreNotifications = functions
    .region("asia-southeast1")
    .firestore.document("notifications/{userId}/storeNotifications/{productId}")
    .onCreate(async (snapshot, context) => {
        const userId = context.params.userId;
        const productId = context.params.productId;

        const userQuerySnapshot = await db.doc(`users/${userId}`).get();
        const productQuerySnapshot = await db.doc(`products/${productId}`).get();

        const notificationToken = userQuerySnapshot.data().androidNotificationToken;

        if (notificationToken != null) {
            sendNotification(notificationToken, snapshot.data());
        } else {
            console.log("no notification");
        }

        function sendNotification(notificationToken, document) {
            let body;
            switch (document.type) {
                case "product_ordered":
                    body = `Product ${productQuerySnapshot.name} have been ordered`;
                    break;
                default:
                    break;
            }
        }
    });



/*_________________________________ SEND MESSAGE _________________________________*/
exports.onCreateConversation = functions.region("asia-southeast1")
    .firestore
    .document("conversations/{conversationID}")
    .onCreate(async (snapshot, context) => {
        const conversationCreated = snapshot.data();
        const conversationID = context.params.conversationID;

        if (conversationCreated != null) {
            const memberIDsArray = conversationCreated.members;
            const lastMessage = conversationCreated.messages[conversationCreated.messages.length - 1];

            const senderDocument = await db.collection("users")
                .doc(lastMessage.senderID).get();

            console.log(senderDocument);

            for (let i = 0; i < memberIDsArray.length; i++) {
                const currUserID = memberIDsArray[i];
                const otherUserIDsArray = memberIDsArray.filter((otherID) => {
                    return otherID !== currUserID;
                });

                for (const otherUserID of otherUserIDsArray) {
                    if (conversationCreated.members.length > 2) {
                        await db.collection("users")
                            .doc(currUserID.toString())
                            .collection("GroupConversations")
                            .doc(conversationID)
                            .set({
                                "conversationID": conversationID,
                                "photoUrl": senderDocument.data().photoUrl,
                                "displayName": senderDocument.data().displayName,
                                "senderID": lastMessage.senderID,
                                "lastMessage_content": lastMessage.message_content,
                                "sentTime": lastMessage.sentTime,
                                "unseenCount": admin.firestore.FieldValue.increment(1),
                                "message_type": lastMessage.message_type,
                            });
                    } else {
                        await db.collection("users")
                            .doc(currUserID.toString())
                            .collection("Conversations")
                            .doc(otherUserID.toString())
                            .create({
                                "conversationID": conversationID,
                                "photoUrl": senderDocument.data().photoUrl,
                                "displayName": senderDocument.data().displayName,
                                "unseenCount": 0,
                                "senderID": lastMessage.senderID,
                                "lastMessage_content": lastMessage.message_content,
                                "sentTime": lastMessage.sentTime,
                                "message_type": lastMessage.message_type,
                            });
                    }
                }
            }
        }
    },

    );


exports.onNewMessageInConversation = functions.region("asia-southeast1")
    .firestore
    .document("conversations/{conversationID}")
    .onUpdate(async (change, context) => {
        const conversationUpdated = change.after.data();
        const conversationID = context.params.conversationID;


        const memberIDsArray = conversationUpdated.members;
        const lastMessage = conversationUpdated.messages[conversationUpdated.messages.length - 1];

        const senderDocument = await db.collection("users")
            .doc(lastMessage.senderID).get();

        for (const currUserID of memberIDsArray) {
            const otherUserIDsArray = memberIDsArray.filter((otherID) => {
                return otherID !== currUserID;
            });

            for (const otherUserID of otherUserIDsArray) {
                if (conversationUpdated.members.length > 2) {
                    await db.collection("users")
                        .doc(currUserID.toString())
                        .collection("GroupConversations")
                        .doc(conversationID)
                        .set({
                            "conversationID": conversationID,
                            "photoUrl": senderDocument.data().photoUrl,
                            "displayName": senderDocument.data().displayName,
                            "senderID": lastMessage.senderID,
                            "lastMessage_content": lastMessage.message_content,
                            "sentTime": lastMessage.sentTime,
                            "unseenCount": admin.firestore.FieldValue.increment(1),
                            "message_type": lastMessage.message_type,
                        });

                } else {
                    await db.collection("users")
                        .doc(currUserID.toString())
                        .collection("Conversations")
                        .doc(otherUserID)
                        .set({
                            "conversationID": conversationID,
                            "photoUrl": senderDocument.data().photoUrl,
                            "displayName": senderDocument.data().displayName,
                            "senderID": lastMessage.senderID,
                            "lastMessage_content": lastMessage.message_content,
                            "sentTime": lastMessage.sentTime,
                            "unseenCount": admin.firestore.FieldValue.increment(1),
                            "message_type": lastMessage.message_type,
                        });
                }

            }
        }
    });


