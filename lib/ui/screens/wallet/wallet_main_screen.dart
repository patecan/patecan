import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card_new/credit_card_widget.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/payment/payment_main_screen.dart';

class WalletMainScreen extends StatefulWidget {
  WalletMainScreen();

  @override
  _WalletMainScreenState createState() => _WalletMainScreenState();
}

class _WalletMainScreenState extends State<WalletMainScreen> {
  @override
  initState() {
    super.initState();
  }

  List cardsMapList = [
    {
      'cardNumber': '42424242424242',
      'expiryDate': '03/23',
      'cardHolderName': 'Thien Trong',
      'cvvCode': '123',
      'showBackView': false,
    },
    {
      'cardNumber': '5555555555554444',
      'expiryDate': '04/24',
      'cardHolderName': 'Tracer',
      'cvvCode': '123',
      'showBackView': false,
    },
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: Color(0xff3A4276),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "e Wallet",
          style: GoogleFonts.poppins(
            fontSize: 22,
            color: Color(0xff3A4276),
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('users')
            .doc(SignedAccount.instance.id)
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          double _balance = SignedAccount.instance.balance;

          if (snapshot.hasData) {
            _balance = double.parse(snapshot.data['balance'].toString());
          }

          return Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.only(left: 18, right: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: size.height * 0.25,
                    padding: EdgeInsets.only(
                        left: 18, right: 18, top: 22, bottom: 22),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color(0xffF1F3F6)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              _balance.toString(),
                              style: GoogleFonts.poppins(
                                fontSize: 24,
                                color: Color(0xff171822),
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Text(
                              "Current Balance",
                              style: GoogleFonts.poppins(
                                  fontSize: 15,
                                  color: AppColors.darkBlueText.withOpacity(.4),
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        RaisedButton(
                          onPressed: () async {
                            final GlobalKey<FormState> _addMoneyFormKey =
                                GlobalKey<FormState>();
                            TextEditingController _amountNeedController =
                                TextEditingController();

                            await showDialog(
                              context: context,
                              builder: (context) {
                                return StatefulBuilder(
                                  builder: (context, setState) {
                                    return AlertDialog(
                                      content: Container(
                                        child: Form(
                                          key: _addMoneyFormKey,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextFormField(
                                                controller:
                                                    _amountNeedController,
                                                keyboardType:
                                                    TextInputType.number,
                                                validator: (value) {
                                                  if (value != null &&
                                                      double.parse(value) < 1) {
                                                    return 'Invalid amount';
                                                  }
                                                  return null;
                                                },
                                                decoration: InputDecoration(
                                                    hintText: "Amount"),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      title: Text('Amount'),
                                      actions: <Widget>[
                                        InkWell(
                                          child: Text('OK'),
                                          onTap: () async {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PaymentMainScreen(
                                                          from: 'wallet',
                                                          amountNeed: double.parse(
                                                              _amountNeedController
                                                                  .value.text),
                                                        )));
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            );
                          },
                          elevation: 0,
                          padding: EdgeInsets.all(12),
                          child: Text(
                            "+",
                            style: TextStyle(
                                color: Color(0xff1B1D28), fontSize: 22),
                          ),
                          shape: CircleBorder(),
                          color: Color(0xffFFAC30),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 34,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Payment Method",
                        style: GoogleFonts.poppins(
                          fontSize: 15,
                          color: Color(0xff3A4276),
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: InkWell(
                            onTap: () async {},
                            child: GestureDetector(
                              onTap: () async {
                                setState(() {
                                  cardsMapList[index]['showBackView'] =
                                      cardsMapList[index]['showBackView'] ==
                                          false;
                                });
                              },
                              child: CreditCardWidget(
                                cardNumber: cardsMapList[index]['cardNumber'],
                                expiryDate: cardsMapList[index]['expiryDate'],
                                cardHolderName: cardsMapList[index]
                                    ['cardHolderName'],
                                cvvCode: cardsMapList[index]['cvvCode'],
                                showBackView: cardsMapList[index]
                                    ['showBackView'],
                                isHolderNameVisible: true,
                                isChipVisible: true,
                                isSwipeGestureEnabled: true,
                                cardBgColor: Colors.black,
                                onCreditCardWidgetChange: (CreditCardBrand) {
                                  print(CreditCardBrand);
                                }, //true when you want to show cvv(back) view
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: cardsMapList.length,
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

Widget _contentOverView(double height) {
  return Container(
    height: height * 0.25,
    padding: EdgeInsets.only(left: 18, right: 18, top: 22, bottom: 22),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10), color: Color(0xffF1F3F6)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "00,000",
              style: GoogleFonts.poppins(
                fontSize: 24,
                color: Color(0xff171822),
                fontWeight: FontWeight.w800,
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Current Balance",
              style: GoogleFonts.poppins(
                  fontSize: 15,
                  color: Color(0xff3A4276).withOpacity(.4),
                  fontWeight: FontWeight.w500),
            ),
          ],
        ),
        RaisedButton(
          onPressed: () {},
          elevation: 0,
          padding: EdgeInsets.all(12),
          child: Text(
            "+",
            style: TextStyle(color: Color(0xff1B1D28), fontSize: 22),
          ),
          shape: CircleBorder(),
          color: Color(0xffFFAC30),
        )
      ],
    ),
  );
}
