import 'dart:isolate';

import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlarmNotification extends StatefulWidget {
  const AlarmNotification({Key? key}) : super(key: key);

  @override
  _AlarmNotificationState createState() => _AlarmNotificationState();
}

void printHello() {
  final DateTime now = DateTime.now();
  final int isolateId = Isolate.current.hashCode;
  print("[$now] Hello, world! isolate=${isolateId} function='$printHello'");
}

class _AlarmNotificationState extends State<AlarmNotification> {
  @override
  void didChangeDependencies() async {
    await AndroidAlarmManager.initialize();
    final int helloAlarmID = 0;
    await AndroidAlarmManager.periodic(
        const Duration(seconds: 4), helloAlarmID, printHello);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Alarm")),
      body: Container(),
    );
  }
}
