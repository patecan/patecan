import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:social_media/services/messages/meeting_service.dart';
import 'package:social_media/ui/screens/meeting/test_alarm_notification.dart';

import '../../widgets/messages/meeting/ongoing_meeting_room_item.dart';
import '../../widgets/social/progress.dart';
import 'create_meeting_room_screen.dart';
import 'meeting_room_detail_screen.dart';
import 'meeting_schedule_screen.dart';

class MeetingHomeScreen extends StatefulWidget {
  MeetingHomeScreen({Key? key}) : super(key: key);

  @override
  _MeetingHomeScreenState createState() => _MeetingHomeScreenState();
}

class _MeetingHomeScreenState extends State<MeetingHomeScreen> {
  GlobalKey<FormState> inviteFormKey = new GlobalKey<FormState>();
  MeetingRoomService inviteService = new MeetingRoomService();

  bool isLoading = false;

  TextEditingController inviteController = new TextEditingController();
  TextEditingController totalInviteController = new TextEditingController();

  dispose() {
    inviteController.clear();
    inviteController.dispose();
    super.dispose();
  }

  void submit() async {
    if (inviteFormKey.currentState!.validate()) {
      inviteFormKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      await inviteService.inviteUser(inviteController.text, 1);
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double _deviceHeight = MediaQuery.of(context).size.height;
    double _deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: isLoading == true
          ? CircularProgress()
          : Container(
              width: _deviceWidth,
              height: _deviceHeight,
              padding: EdgeInsets.symmetric(horizontal: _deviceWidth * 0.075),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 247, 224, 0.25),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                          child: Container(
                            height: 55,
                            width: 55,
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: <Color>[
                                  Colors.deepPurple.withOpacity(1),
                                  Colors.lightBlue.withOpacity(1)
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 5.0,
                                  spreadRadius: 0.025,
                                  offset: Offset(0.5, 0.5),
                                ),
                              ],
                            ),
                            child: Icon(Icons.video_call, color: Colors.white),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    CreateMeetingRoomScreen()));
                          }),
                      GestureDetector(
                          child: Container(
                            height: 55,
                            width: 55,
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: <Color>[
                                  Colors.deepPurple.withOpacity(1),
                                  Colors.lightBlue.withOpacity(1)
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 5.0,
                                  spreadRadius: 0.025,
                                  offset: Offset(0.5, 0.5),
                                ),
                              ],
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: FaIcon(FontAwesomeIcons.calendarAlt,
                                  color: Colors.white),
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => MeetingScheduleScreen()));
                          }),
                      GestureDetector(
                          child: Container(
                            height: 55,
                            width: 55,
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: <Color>[
                                  Colors.deepPurple.withOpacity(1),
                                  Colors.lightBlue.withOpacity(1)
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 5.0,
                                  spreadRadius: 0.025,
                                  offset: Offset(0.5, 0.5),
                                ),
                              ],
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Icon(Icons.alarm_on, color: Colors.white),
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => AlarmNotification()));
                          }),
                    ],
                  ),
                  Container(
                    child: Form(
                      onChanged: () {
                        inviteFormKey.currentState!.save();
                      },
                      key: inviteFormKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: TextFormField(
                                controller: inviteController,
                                style: TextStyle(color: Colors.indigo),
                                decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: 'Username or phone number',
                                  contentPadding:
                                      const EdgeInsets.only(left: 10.0),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.lightBlueAccent),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.white),
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                  ),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter username or phone number';
                                  }
                                  return null;
                                },
                                onSaved: (value) {},
                                cursorColor: Colors.indigo,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: _deviceWidth,
                    padding:
                        const EdgeInsets.only(top: 10.0, right: 5.0, left: 5.0),
                    child: MaterialButton(
                      onPressed: submit,
                      color: Colors.deepPurpleAccent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.call, color: Colors.white),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "CALL",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                                child: Text("Ongoing meeting room",
                                    style: TextStyle(color: Colors.green))),
                          ),
                          Divider(thickness: 1.0),
                          StreamBuilder(
                            stream: FirebaseFirestore.instance
                                .collection('meeting_room')
                                .where('status', isEqualTo: 'onGoing')
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.hasData) {
                                QuerySnapshot querySnapshot = snapshot.data;
                                List<DocumentSnapshot> onGoingMeetingRoomList =
                                    querySnapshot.docs;
                                return Container(
                                  height: onGoingMeetingRoomList.length * 160,
                                  child: ListView.builder(
                                    itemCount: onGoingMeetingRoomList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return GestureDetector(
                                        child: OnGoingMeetingRoom(
                                          meetingRoomId:
                                              onGoingMeetingRoomList[index].id,
                                        ),
                                        onTap: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  MeetingRoomDetailScreen(
                                                meetingId:
                                                    onGoingMeetingRoomList[
                                                            index]
                                                        .id,
                                                meetingStatus:
                                                    onGoingMeetingRoomList[
                                                        index]['status'],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  ),
                                );
                              } else {
                                return Text('');
                              }
                            },
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                                child: Text("Upcoming meeting room",
                                    style: TextStyle(color: Colors.black))),
                          ),
                          Divider(thickness: 1.0),
                          StreamBuilder(
                            stream: FirebaseFirestore.instance
                                .collection('meeting_room')
                                .where('status', isEqualTo: 'new')
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.hasData) {
                                QuerySnapshot querySnapshot = snapshot.data;
                                List<DocumentSnapshot> onGoingMeetingRoomList =
                                    querySnapshot.docs;
                                return Container(
                                  height: onGoingMeetingRoomList.length * 150,
                                  child: ListView.builder(
                                    itemCount: onGoingMeetingRoomList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return GestureDetector(
                                        child: OnGoingMeetingRoom(
                                          meetingRoomId:
                                              onGoingMeetingRoomList[index].id,
                                        ),
                                        onTap: () async {
                                          await getPermissions();
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  MeetingRoomDetailScreen(
                                                meetingId:
                                                    onGoingMeetingRoomList[
                                                            index]
                                                        .id,
                                                meetingStatus:
                                                    onGoingMeetingRoomList[
                                                        index]['status'],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  ),
                                );
                              } else {
                                return Text('');
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  Future<void> getPermissions() async {
    PermissionStatus cameraPermissionStatus = await Permission.camera.status;
    PermissionStatus microphonePermissionStatus =
        await Permission.microphone.status;

    if (cameraPermissionStatus != Permission.camera.isGranted) {
      await Permission.camera.request();
    }
    if (microphonePermissionStatus != Permission.microphone.isGranted) {
      await Permission.camera.request();
    }
  }
}
