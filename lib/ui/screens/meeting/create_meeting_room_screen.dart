import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dropdown_below/dropdown_below.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'package:social_media/models/clubhouse/meeting_room.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/messages/meeting_service.dart';
import 'package:social_media/ui/widgets/messages/contact_user_item.dart';
import 'package:social_media/ui/widgets/rounded_loading_button.dart';

enum MeetingRoomTypes { private, public }

class CreateMeetingRoomScreen extends StatefulWidget {
  CreateMeetingRoomScreen({Key? key}) : super(key: key);

  @override
  _CreateMeetingRoomScreenState createState() =>
      _CreateMeetingRoomScreenState();
}

class _CreateMeetingRoomScreenState extends State<CreateMeetingRoomScreen> {
  /*___________________________ FirebaseInviteService ____________________________*/
  MeetingRoomService inviteService = new MeetingRoomService();

  /*___________________________ Form Attributes ____________________________*/
  /*___________________________ Form Attributes ____________________________*/
  final GlobalKey<FormState> createMeetingRoomFormKey = GlobalKey<FormState>();
  late RoundedLoadingButtonController roundedLoadingButton;

  /*_________________________ Meeting Attributes __________________________*/
  TextEditingController _meetingTitleController = TextEditingController();
  TextEditingController _participantsController = TextEditingController();
  List<MyUser> _participantsList = [];
  DateTime meetingDateTime = DateTime.now();
  MeetingRoomTypes meetingRoomType = MeetingRoomTypes.private;

  late List<DropdownMenuItem> _dropdownMeetingTypes;
  var _selectedMeetingType;

  @override
  void initState() {
    roundedLoadingButton = RoundedLoadingButtonController();
    _dropdownMeetingTypes = MeetingTypes.values.map(
      (e) {
        return DropdownMenuItem(
          value: e,
          child: Text(EnumToString.convertToString(e)),
        );
      },
    ).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Create meeting")),
      body: Container(
        color: Color.fromRGBO(255, 247, 224, 0.25),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Form(
                    key: createMeetingRoomFormKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                              controller: _meetingTitleController,
                              style: TextStyle(color: Colors.indigo),
                              decoration: new InputDecoration(
                                filled: true,
                                fillColor: Theme.of(context).backgroundColor,
                                hintText: 'Meeting title',
                                contentPadding:
                                    EdgeInsets.only(top: 17, left: 17.0),
                                focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.blue),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(19)),
                                  borderSide: BorderSide(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter meeting title';
                                }
                                return null;
                              },
                              onSaved: (value) {},
                              cursorColor: Colors.indigo,
                            ),
                          ),
                        ),
                        Container(
                          height: 120,
                          child: CupertinoDatePicker(
                            initialDateTime: DateTime.now(),
                            mode: CupertinoDatePickerMode.dateAndTime,
                            onDateTimeChanged: (DateTime dateTimeValue) {
                              print(dateTimeValue);
                              meetingDateTime = dateTimeValue;
                            },
                          ),
                        ),
                        SizedBox(height: 20),
                        DropdownBelow(
                          itemWidth: 200,
                          itemTextstyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Colors.black),
                          boxTextstyle: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0XFFbbbbbb)),
                          boxPadding: EdgeInsets.fromLTRB(13, 12, 0, 12),
                          boxHeight: 45,
                          boxWidth: 450,
                          boxDecoration: BoxDecoration(
                            border: Border.all(color: Colors.blue),
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          hint: Text('choose item'),
                          value: _selectedMeetingType,
                          items: _dropdownMeetingTypes,
                          onChanged: (value) {
                            setState(() {
                              _selectedMeetingType = value;
                            });
                          },
                        ),
                        GroupButton(
                          mainGroupAlignment: MainGroupAlignment.start,
                          isRadio: true,
                          spacing: 10,
                          selectedButton:
                              MeetingRoomTypes.values.indexOf(meetingRoomType),
                          direction: Axis.horizontal,
                          onSelected: (index, isSelected) {
                            List<MeetingRoomTypes> meetingRoomTypesList =
                                MeetingRoomTypes.values;
                            meetingRoomType = meetingRoomTypesList[index];
                          },
                          buttons: MeetingRoomTypes.values.map((element) {
                            return EnumToString.convertToString(element);
                          }).toList(),
                          unselectedBorderColor: Colors.lightBlue,
                          unselectedColor: Theme.of(context).backgroundColor,
                          selectedColor: Colors.blue,
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Theme.of(context).backgroundColor,
                                  hintText: 'Participant username',
                                  contentPadding: EdgeInsets.only(
                                      left: 17.0, top: 8, bottom: 8),
                                  isDense: true,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                                controller: _participantsController,
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('users')
                                    .where('username',
                                        isEqualTo: _participantsController.text)
                                    .get()
                                    .then((querySnapshot) {
                                  querySnapshot.docs.forEach((document) {
                                    setState(() {
                                      _participantsList.add(
                                          MyUser.fromDocumentSnapshot(
                                              document));
                                    });
                                  });
                                  _participantsController.clear();
                                });
                              },
                              child: Text('Add'),
                            ),
                          ],
                        ),
                        if (_participantsList.length > 0)
                          SingleChildScrollView(
                            child: Container(
                              height: 200,
                              child: ListView.builder(
                                itemCount: _participantsList.length,
                                itemBuilder: (context, index) {
                                  return ContactUserItem(
                                    user: _participantsList[index],
                                    from: 'message',
                                  );
                                },
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RoundedLoadingButton(
                  valueColor: Colors.white,
                  width: 300,
                  controller: roundedLoadingButton,
                  height: 40,
                  borderColor: Colors.white,
                  color: Colors.deepPurpleAccent,
                  successColor: Colors.green,
                  child: Text("Create"),
                  onPressed: () async {
                    roundedLoadingButton.start();
                    if (createMeetingRoomFormKey.currentState!.validate()) {
                      await inviteService.createMeetingRoom(
                          title: _meetingTitleController.text,
                          meetingType: _selectedMeetingType,
                          invitedParticipants: _participantsList,
                          meetingDateTime: meetingDateTime);

                      roundedLoadingButton.success();
                    } else {
                      roundedLoadingButton.reset();
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
