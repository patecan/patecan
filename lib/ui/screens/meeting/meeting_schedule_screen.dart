import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class MeetingScheduleScreen extends StatefulWidget {
  const MeetingScheduleScreen({Key? key}) : super(key: key);

  @override
  MeetingScheduleScreenState createState() => MeetingScheduleScreenState();
}

class MeetingScheduleScreenState extends State<MeetingScheduleScreen> {
  CalendarController _controller = CalendarController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SfCalendar(
            view: CalendarView.schedule,
            dataSource: MeetingDataSource(_getDataSource()),
            scheduleViewMonthHeaderBuilder: (BuildContext buildContext,
                ScheduleViewMonthHeaderDetails details) {
              final String monthName = details.date.month.toString();
              return Stack(
                children: [
                  Container(
                    height: 50,
                    child: Image(
                        image: AssetImage('assets/images/vet_background.jpg'),
                        fit: BoxFit.cover,
                        width: details.bounds.width,
                        height: details.bounds.height),
                  ),
                  Positioned(
                    left: 55,
                    right: 0,
                    top: 15,
                    bottom: 25,
                    child: Text(
                      monthName + ' ' + details.date.year.toString(),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              );
            },
            showDatePickerButton: true),
      ),
    );
  }
}

List<Meeting> _getDataSource() {
  final List<Meeting> meetings = <Meeting>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
      DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 2));
  meetings.add(Meeting(
      'Conference', startTime, endTime, const Color(0xFF0F8644), false));
  return meetings;
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}

class Meeting {
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  String eventName;
  DateTime from;
  DateTime to;
  Color background;
  bool isAllDay;
}
