import 'dart:math' as math; // import this

import 'package:avatar_view/avatar_view.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/ui/screens/pet_adopt/pet_adopt_main_screen.dart';
import 'package:social_media/ui/screens/pet_park/pet_park_main_screen.dart';
import 'package:social_media/ui/screens/social/timeline_screen.dart';
import 'package:social_media/ui/screens/users/user_main_preferences_screen.dart';
import 'package:social_media/ui/screens/vet/vet_main_screen.dart';

import '../../constant/user_roles.dart';
import '../../models/users/singned_account.dart';
import '../../services/authentication/account_authentication_service.dart';
import 'homepage_screen.dart';

class MenuItem {
  final Text title;
  final Icon? icon;
  final FaIcon? faIcon;

  const MenuItem(this.title, {this.icon, this.faIcon});
}

class MenuItems {
  static const timeline = MenuItem(
      Text(
        'Timeline',
        style: TextStyle(color: Colors.white),
      ),
      icon: Icon(Icons.home, color: Colors.white));
  static const short = MenuItem(
      Text('Shorts', style: TextStyle(color: Colors.white)),
      icon: Icon(Icons.video_library, color: Colors.white));
  static const message = MenuItem(
      Text('Message', style: TextStyle(color: Colors.white)),
      icon: Icon(Icons.message, color: Colors.white));
  static const store = MenuItem(
      Text('Store', style: TextStyle(color: Colors.white)),
      icon: Icon(Icons.store, color: Colors.white));
  static const place = MenuItem(
      Text('Place', style: TextStyle(color: Colors.white)),
      icon: Icon(Icons.map, color: Colors.white));
  static const petPark = MenuItem(
      Text('Pet Park', style: TextStyle(color: Colors.white)),
      icon: Icon(Icons.local_florist, color: Colors.white));
  static const petAdopt = MenuItem(
      Text('Pet Adopt', style: TextStyle(color: Colors.white)),
      faIcon: FaIcon(FontAwesomeIcons.clinicMedical, color: Colors.white));
  static const vet = MenuItem(
      Text('Vet', style: TextStyle(color: Colors.white)),
      faIcon: FaIcon(FontAwesomeIcons.capsules, color: Colors.white));
  static const all = [short, message, store, place, petPark, petAdopt, vet];
}

class MenuPage extends StatelessWidget {
  final MenuItem currentItem;
  final ValueChanged<MenuItem> onSelectedItem;

  const MenuPage(
      {Key? key, required this.currentItem, required this.onSelectedItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Theme(
      data: ThemeData.dark(),
      child: Scaffold(
        backgroundColor: AppColors.pinkBackground,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: size.width * 0.5,
                padding: EdgeInsets.only(left: 25),
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        AvatarView(
                          radius: 30,
                          borderColor: Colors.transparent,
                          avatarType: AvatarType.RECTANGLE,
                          backgroundColor: Colors.transparent,
                          imagePath: SignedAccount.instance.photoUrl!,
                          placeHolder: Container(
                            child: Icon(
                              Icons.person,
                              size: 20,
                            ),
                          ),
                          errorWidget: Container(
                            child: Icon(
                              Icons.person,
                              size: 20,
                            ),
                          ),
                        ),
                        SizedBox(width: 5),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(SignedAccount.instance.username!,
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontFamily: 'Nunito',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15)),
                            Text(
                              EnumToString.convertToString(
                                  SignedAccount.instance.userRoles),
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontFamily: 'Nunito',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Container(
                      width: 130,
                      height: 40,
                      margin: EdgeInsets.only(top: 7),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  UserMainPreferenceScreen()));
                        },
                        style: ElevatedButton.styleFrom(
                          primary: AppColors.pinkBackground,
                          side: BorderSide(
                              width: 2,
                              color: AppColors.darkRedText.withOpacity(0.7)),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 7.0, bottom: 7.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'My Profile',
                                style: TextStyle(
                                  fontSize: 15,
                                  color: AppColors.darkRedText,
                                  fontFamily: 'Nunito',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              FaIcon(FontAwesomeIcons.pencilAlt,
                                  size: 14, color: AppColors.darkRedText),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Container(
                margin: EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                  color: AppColors.pink2Background,
                  borderRadius: BorderRadius.all(
                    Radius.circular(25),
                  ),
                ),
                child: Column(
                  children: [
                    ...MenuItems.all
                        .map((item) => Container(
                              margin: EdgeInsets.only(left: 5, top: 2.5),
                              child: ListTileTheme(
                                selectedColor: AppColors.pink2Background,
                                child: ListTile(
                                    selectedTileColor: Colors.black26,
                                    minLeadingWidth: 20,
                                    leading: item.faIcon == null
                                        ? item.icon
                                        : item.faIcon,
                                    title: item.title,
                                    selected: currentItem == item,
                                    onTap: () {
                                      switch (item) {
                                        case MenuItems.short:
                                          Navigator.of(context)
                                              .pushNamed('/short_video_screen');
                                          break;
                                        case MenuItems.message:
                                          Navigator.of(context).pushNamed(
                                              '/messaging_main_screen');
                                          break;
                                        case MenuItems.store:
                                          Navigator.of(context)
                                              .pushNamed('/store_main_screen');
                                          break;
                                        case MenuItems.place:
                                          if (SignedAccount
                                                  .instance.userRoles ==
                                              UserRoles.user) {
                                            Navigator.of(context).pushNamed(
                                                '/user_places_main_screen');
                                          } else if (SignedAccount
                                                  .instance.userRoles ==
                                              UserRoles.foster) {
                                            Navigator.of(context).pushNamed(
                                                '/foster_places_main_screen');
                                          }
                                          break;
                                        case MenuItems.petPark:
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PetParkMainScreen()));
                                          break;
                                        case MenuItems.petAdopt:
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PetAdoptMainScreen()));
                                          break;
                                        case MenuItems.vet:
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      VetMainScreen()));
                                          break;
                                        default:
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      TimelineScreen()));
                                          break;
                                      }
                                    }),
                              ),
                            ))
                        .toList(),
                  ],
                ),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                width: 130,
                height: 40,
                margin: EdgeInsets.only(bottom: 30, left: 15),
                child: ElevatedButton(
                  onPressed: () async {
                    if (FirebaseAuth.instance.currentUser != null) {
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(SignedAccount.instance.id)
                          .update({'isOnline': false});
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(SignedAccount.instance.id)
                          .update({'lastOnline': DateTime.now()});
                      await FirebaseAuth.instance.signOut();
                    } else {
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(SignedAccount.instance.id)
                          .update({'isOnline': false});
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(SignedAccount.instance.id)
                          .update({'lastOnline': DateTime.now()});

                      await AccountAuthenticationService.signOutWithGoogle();
                    }
                    HomepageScreen.isLoggedIn.value = false;
                  },
                  style: ElevatedButton.styleFrom(
                    primary: AppColors.pink2Background,
                    side: BorderSide(
                        width: 2,
                        color: AppColors.pink2Background.withOpacity(0.7)),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Transform(
                          alignment: Alignment.center,
                          transform: Matrix4.rotationY(math.pi),
                          child: FaIcon(
                            FontAwesomeIcons.running,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          'Logout',
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
