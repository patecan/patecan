import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/screens/nfc/pet_nfc_main_screen.dart';
import 'package:social_media/ui/widgets/social/user_searched_item.dart';

import '../../../constant/app_colors.dart';

class SocialSearchScreen extends StatefulWidget {
  @override
  _SocialSearchScreenState createState() => _SocialSearchScreenState();
}

class _SocialSearchScreenState extends State<SocialSearchScreen> {
  late double width;
  late double height;
  late EdgeInsets padding;

  FirebaseUserHelper firebaseHelper = new FirebaseUserHelper();
  List<UserSearchedItem> listSearchResult = [];
  TextEditingController searchController = TextEditingController();

  static ValueNotifier<QuerySnapshot?> searchResult =
      ValueNotifier<QuerySnapshot?>(null);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xffF2F7FF),
            leadingWidth: 30,
            leading: Container(
              margin: EdgeInsets.only(left: 10),
              child: IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.angleLeft,
                  color: AppColors.pinkGradient,
                  size: 27.5,
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ),
            title: TextFormField(
              decoration: InputDecoration(
                hintText: 'Search',
                filled: true,
                prefixIcon: Icon(Icons.search, size: 28.0),
                suffixIcon: IconButton(
                  icon: Icon(Icons.clear),
                  onPressed: () {},
                ),
              ),
              onFieldSubmitted: handleSearch,
            ),
            actions: [
              IconButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => PetNFCScreen()));
                  },
                  icon: Icon(Icons.nfc, color: Color(0xffFFADAD), size: 30.0)),
              IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.book, color: Color(0xffFFADAD), size: 30.0)),
            ],
          ),
          body: Container(
            color: Color(0xffF2F7FF),
            child: ValueListenableBuilder<QuerySnapshot?>(
              valueListenable: searchResult,
              builder: (context, valueListenable, child) {
                return (searchResult.value == null)
                    ? buildNoContent()
                    : (searchResult.value!.docs.length < 1 ||
                            searchResult.value!.docs.isEmpty)
                        ? buildNoContent()
                        : Container(
                            child: ListView(
                              children: searchResult.value!.docs
                                  .map((documentSnapshot) {
                                MyUser user = MyUser.fromDocumentSnapshot(
                                    documentSnapshot);
                                print(user.toString());
                                return GestureDetector(
                                    child: UserSearchedItem(user));
                              }).toList(),
                            ),
                          );
              },
              child: null,
            ),
          )),
    );
  }

  buildNoContent() {
    listSearchResult = [];
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    padding = MediaQuery.of(context).padding;
    double height3 = height - padding.top - kToolbarHeight;
    double width3 = width - padding.top - kToolbarHeight;

    Orientation orientation = MediaQuery.of(context).orientation;
    return Container(
      child: Center(
        child: SvgPicture.asset(
          'assets/images/search.svg',
          height: orientation == Orientation.portrait ? height3 : width3,
        ),
      ),
    );
  }

  List<UserSearchedItem> list() {
    listSearchResult = searchResult.value!.docs.map((documentSnapshot) {
      MyUser user = MyUser.fromDocumentSnapshot(documentSnapshot);
      return UserSearchedItem(user);
    }).toList();

    return listSearchResult;
  }

  void handleSearch(String value) async {
    searchResult.value = null;
    if (value == "") {
      searchResult.value = null;
    } else {
      QuerySnapshot foundUser =
          await firebaseHelper.getUserSnapshotByDisplayName(value);
      if (foundUser.docs.length > 0) {
        searchResult.value = foundUser;
      }
    }
  }
}
