import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/notification/my_post_notification.dart';
import 'package:social_media/models/notification/my_store_notification.dart';
import 'package:social_media/services/notifications/firebase_social_notify_service.dart';
import 'package:social_media/ui/widgets/header.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

import '../../widgets/notifications/post_notification_item.dart';
import '../../widgets/notifications/store_notification_item.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  void initState() {
    super.initState();
  }

  SocialMediaNotificationService firebaseNotificationHelper =
      new SocialMediaNotificationService();

  @override
  Widget build(context) {
    return Scaffold(
      appBar: Header("Notification"),
      body: Container(
        child: FutureBuilder(
          future:
              firebaseNotificationHelper.getAllPostNotificationForCurrentUser(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> postSnapshot) {
            if (postSnapshot.hasData) {
              List<Widget> notificationList = [];
              List<DocumentSnapshot> documentList = postSnapshot.data;
              documentList.forEach((element) {
                if (element['postID'] != null) {
                  MyPostNotification myNotification =
                      MyPostNotification.fromQueryDocument(element);
                  notificationList.add(PostNotificationItem(myNotification));
                }
              });

              return FutureBuilder(
                future: firebaseNotificationHelper
                    .getAllStoreNotificationForCurrentUser(),
                builder: (BuildContext context,
                    AsyncSnapshot<dynamic> storeSnapshot) {
                  if (storeSnapshot.hasData) {
                    List<DocumentSnapshot> documentList = storeSnapshot.data;
                    documentList.forEach((element) {
                      MyStoreNotification myNotification =
                          MyStoreNotification.fromQueryDocument(element);
                      notificationList
                          .add(StoreNotificationItem(myNotification));
                    });

                    return ListView(
                      children: notificationList,
                    );
                  } else {
                    return ListView(
                      children: notificationList,
                    );
                  }
                },
              );
            } else {
              return LinearProgress();
            }
          },
        ),
      ),
    );
  }
}
