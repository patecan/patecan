import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:just_audio/just_audio.dart';
import 'package:social_media/models/places/trip.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/places/map_service.dart';
import 'package:social_media/services/social_media/firebase_timeline_service.dart';
import 'package:social_media/ui/screens/social/social_search_screen.dart';
import 'package:social_media/ui/screens/stories/stories_preview_section.dart';
import 'package:social_media/ui/widgets/social/post_item.dart';
import 'package:social_media/ui/widgets/social/progress.dart';
import 'package:social_media/ui/widgets/social/recommend_follow.dart';

import '../../../constant/app_colors.dart';
import '../../widgets/places/request_detail_dialog.dart';
import '../shorts/shorts_preview_section.dart';

class TimelineScreen extends StatefulWidget {
  const TimelineScreen({Key? key}) : super(key: key);

  @override
  _TimelineScreenState createState() => _TimelineScreenState();
}

class _TimelineScreenState extends State<TimelineScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  FirebaseTimelineHelper firebaseTimeline = new FirebaseTimelineHelper();

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  final AudioPlayer _player = AudioPlayer();

  @override
  void initState() {
    configurePushNotification();
    tabController = TabController(length: 2, initialIndex: 0, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      leading: IconButton(
          onPressed: () => ZoomDrawer.of(context)!.toggle(),
          icon: Icon(
            Icons.menu,
            color: Color(0xff53AFEE),
          )),
      title: Text(
        'PATECAN',
        style: TextStyle(
            color: Color(0xff53AFEE),
            fontFamily: "Nunito",
            fontWeight: FontWeight.bold,
            fontSize: 16.5),
      ),
      backgroundColor: AppColors.pinkBackground,
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SocialSearchScreen()));
            },
            icon: Icon(
              Icons.search,
              color: Color(0xff53AFEE),
              size: 30,
            ),
          ),
        )
      ],
    );

    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: appBar,
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('timeline')
            .doc(SignedAccount.instance.id)
            .collection('timelinePosts')
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            QuerySnapshot querySnapshot = snapshot.data;
            List<PostItem> postList = querySnapshot.docs.map((document) {
              return PostItem.fromDocumentSnapshot(document);
            }).toList();

            if (postList.length == 0) {
              return RecommendFollow(
                  MyUser.fromSignedInAccount(SignedAccount.instance));
            } else {
              return Container(
                decoration: BoxDecoration(
                  color: AppColors.pinkBackground,
                ),
                height: size.height * 0.9,
                width: size.width,
                child: ListView(
                  children: [
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            constraints: BoxConstraints.expand(height: 50),
                            child: TabBar(
                                controller: tabController,
                                labelColor: Colors.white,
                                labelPadding:
                                    EdgeInsets.symmetric(vertical: 10),
                                unselectedLabelColor: Colors.grey,
                                indicator: BubbleTabIndicator(
                                  indicatorHeight: 30,
                                  indicatorColor: AppColors.tabViewButtonColor,
                                  tabBarIndicatorSize:
                                      TabBarIndicatorSize.label,
                                  indicatorRadius: 25,
                                  insets: EdgeInsets.all(0),
                                ),
                                tabs: [
                                  Tab(text: "Stories"),
                                  Tab(text: "Shorts"),
                                ]),
                          ),
                          Container(
                            height: size.height * 0.22,
                            child: TabBarView(
                              controller: tabController,
                              children: [
                                Container(
                                    width: size.width,
                                    padding: EdgeInsets.all(8),
                                    child: StoriesPreviewSection()),
                                Container(
                                    width: size.width,
                                    padding: EdgeInsets.all(8),
                                    child: ShortsPreviewSection()),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    Column(
                      children: postList,
                    )
                    //postList,
                  ],
                ),
              );
            }
          } else {
            return CircularProgress();
          }
        },
      ),
    );
  }

  configurePushNotification() async {
    if (Platform.isIOS) {
      NotificationSettings settings =
          await FirebaseMessaging.instance.requestPermission(
        alert: true,
        announcement: true,
        badge: true,
        carPlay: true,
        criticalAlert: true,
        provisional: true,
        sound: true,
      );
      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        print('User granted permission');
      } else if (settings.authorizationStatus ==
          AuthorizationStatus.provisional) {
        print('User granted provisional permission');
      } else {
        print('User declined or has not accepted permission');
      }
    }

    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    String? token = await FirebaseMessaging.instance.getToken();
    SignedAccount.instance.token = token;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(SignedAccount.instance.id)
        .update({'androidNotificationToken': token!});

    await FirebaseMessaging.instance.subscribeToTopic('alldriders');
    await FirebaseMessaging.instance.subscribeToTopic('allusers');

    Future<void> firebaseMessagingBackgroundHandler(
        RemoteMessage message) async {
      print("Handling a background message");
    }

    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) async {
        print('Got a message in the foreground!');
        print('Message data: ${message.data}');
        print('Message data: ${message.notification}');
        _player.play();
        await _player.seek(Duration(seconds: 2));
        await _player.pause();
        RemoteNotification notification = message.notification!;
        AndroidNotification android = message.notification!.android!;

        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              icon: '@mipmap/ic_launcher',
              // other properties...
            ),
          ),
        );

        if (await Vibrate.canVibrate) {
          Vibrate.vibrate();
        }
        if (message.data['ride_request'] != null) {
          var rideRequest = json.decode(message.data['ride_request']);

          MapService mapService = new MapService();
          TripDetail? tripDetail =
              await mapService.getRequestDetails(context, rideRequest);

          if (tripDetail != null) {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return RequestDetailDialog(tripDetail: tripDetail);
              },
            );
          }
        } else if (message.data['invite'] != null) {
          String inviterDisplayName = message.data['invite'];

          AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.SCALE,
            title: 'New meeting invite',
            desc: "${inviterDisplayName} is calling you",
            btnOkOnPress: () {
              Navigator.pop(context);
            },
          )..show();
        }
      },
    );
  }
}
