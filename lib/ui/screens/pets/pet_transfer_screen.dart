import 'package:flutter/material.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/pets/pet_service.dart';

class PetTransferScreen extends StatefulWidget {
  late MyUser sender;
  late MyUser receiver;
  late Pet pet;

  PetTransferScreen(
      {Key? key,
      required this.sender,
      required this.pet,
      required this.receiver})
      : super(key: key);

  @override
  _PetTransferScreenState createState() => _PetTransferScreenState();
}

class _PetTransferScreenState extends State<PetTransferScreen> {
  PetService petService = new PetService();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        height: size.height * 0.9,
        width: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FutureBuilder(
              future: petService.transferPet(
                  widget.sender.id!, widget.pet.id!, widget.receiver.id!),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return Center(
                    child: Image.asset("assets/images/confirm_mark.png",
                        height: 200, width: 200),
                  );
                } else {
                  return Container();
                }
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(widget.sender.username!,
                    style: TextStyle(fontSize: 20, color: Colors.blue)),
                Icon(Icons.arrow_forward),
                Text(widget.pet.petUniqueName!,
                    style: TextStyle(fontSize: 20, color: Colors.black)),
                Icon(Icons.arrow_forward),
                Text(widget.receiver.username!,
                    style: TextStyle(fontSize: 20, color: Colors.blue)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
