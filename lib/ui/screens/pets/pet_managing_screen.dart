import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/models/users/singned_account.dart';

import '../../widgets/pets/my_pet_item.dart';
import '../../widgets/social/progress.dart';

class PetManagingMainScreen extends StatefulWidget {
  PetManagingMainScreen({Key? key}) : super(key: key);

  @override
  _PetManagingMainScreenState createState() => _PetManagingMainScreenState();
}

class _PetManagingMainScreenState extends State<PetManagingMainScreen> {
  List<Pet> myPetList = [];

  double deviceHeight = 0.0;
  double deviceWidth = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        height: deviceHeight * 0.9,
        width: deviceWidth,
        child: Stack(
          children: [
            StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('pets')
                  .doc(SignedAccount.instance.id!)
                  .collection('petOwned')
                  .snapshots(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  myPetList = List<Pet>.from(snapshot.data.docs.map((document) {
                    return Pet.fromDocumentSnapshot(document);
                  }));
                  return Positioned(
                    child: Container(
                      margin: EdgeInsets.only(top: 60, left: 5, right: 5),
                      child: ListView.builder(
                        itemCount: myPetList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return MyPetItem(myPetList[index]);
                        },
                      ),
                    ),
                  );
                } else {
                  return CircularProgress();
                }
              },
            ),
            Positioned(
              left: 0,
              top: 32,
              child: Container(
                margin: EdgeInsets.only(left: 12, right: 12),
                width: deviceWidth * 0.95,
                height: deviceHeight * 0.05,
                child: TextField(
                  autocorrect: false,
                  style: TextStyle(color: Colors.blue),
                  decoration: new InputDecoration(
                    filled: true,
                    fillColor: Theme.of(context).backgroundColor,
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.blue,
                    ),
                    hintText: 'Title',
                    contentPadding: const EdgeInsets.only(left: 17.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.blue),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(
                        color: Colors.blue,
                        width: 1.0,
                      ),
                    ),
                  ),
                  onSubmitted: (value) {
                    setState(() {
                      // searchName = value;
                      // print(searchName);
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
