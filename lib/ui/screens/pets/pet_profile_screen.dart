import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/models/pets/pet_event_types.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/social_media/firebase_follower_service.dart';
import 'package:social_media/services/social_media/firebase_following_service.dart';
import 'package:social_media/services/social_media/firebase_post_service.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../widgets/header.dart';
import '../../widgets/social/profile_button.dart';
import '../users/users_select_screen.dart';

class PetProfileScreen extends StatefulWidget {
  String petId;
  String ownerId;

  PetProfileScreen({required this.ownerId, required this.petId});

  @override
  _PetProfileScreenState createState() => _PetProfileScreenState();
}

class _PetProfileScreenState extends State<PetProfileScreen> {
  FirebaseUserHelper firebaseUserService = new FirebaseUserHelper();
  FirebasePostHelper firebasePostHelper = new FirebasePostHelper();
  FirebaseFollowerHelper firebaseFollowers = new FirebaseFollowerHelper();
  FirebaseFollowingHelper firebaseFollowing = new FirebaseFollowingHelper();

  String postView = "grid";
  bool isLoading = false;
  bool isFollowing = false;
  int postCount = 0;
  int followersCount = 0;
  int followingCount = 0;

  setFollow(bool isFollow) {
    setState(() {
      this.isFollowing = isFollow;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(context) {
    Size size = MediaQuery.of(context).size;

    Pet? pet;

    return Scaffold(
      appBar: Header('PET'),
      body: StreamBuilder<Object>(
          stream: FirebaseFirestore.instance
              .collection('pets')
              .doc(widget.ownerId)
              .collection('petOwned')
              .doc(widget.petId)
              .snapshots(),
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              pet = Pet.fromDocumentSnapshot(snapshot.data);
              return ListView(
                children: [
                  Container(
                    height: size.height * 0.33,
                    child: Stack(
                      children: [
                        Positioned(
                          child: Container(
                            height: size.height * (0.35 / 2.5),
                            width: size.width,
                            color: Colors.transparent,
                            child: Image.network(
                              "https://firebasestorage.googleapis.com/v0/b/flutter-social-media-7fb3e.appspot.com/o/pets%2F242595057_571906867383933_17474311130699103_n.jpg?alt=media&token=54f708d5-0ec7-4711-837d-dc390eba2f6b",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 50,
                          right: 108,
                          child: Container(
                            width: 65,
                            height: 65,
                            child: FutureBuilder(
                              future: firebaseUserService
                                  .getImageOfUser(pet!.ownerId!),
                              builder:
                                  (context, AsyncSnapshot<dynamic> snapshot) {
                                if (snapshot.hasData) {
                                  return CircularProfileAvatar(
                                    snapshot.data,
                                    radius: 100,
                                    backgroundColor: Colors.transparent,
                                    borderWidth: 5,
                                    borderColor: Colors.white,
                                    cacheImage: true,
                                    imageFit: BoxFit.cover,
                                    onTap: () {},
                                  );
                                } else {
                                  return CircularProfileAvatar(
                                    'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
                                    radius: 100,
                                    backgroundColor: Colors.transparent,
                                    borderWidth: 10,
                                    borderColor: Colors.blue,
                                    cacheImage: true,
                                    imageFit: BoxFit.cover,
                                    onTap: () {},
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            padding: EdgeInsets.only(left: 16, right: 16),
                            child: Column(
                              children: [
                                Container(
                                  height: size.height * 0.18,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: 100,
                                        height: 100,
                                        child: CircularProfileAvatar(
                                          pet!.photoUrl!,
                                          radius: 100,
                                          borderWidth: 5,
                                          borderColor: Colors.white,
                                          cacheImage: true,
                                          imageFit: BoxFit.cover,
                                          onTap: () {},
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "@${pet!.petUniqueName!}",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                if (pet!.bio!.isNotEmpty)
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(top: 2.0),
                                    child: Text(
                                      pet!.bio!,
                                      style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                Container(
                                  width: double.infinity,
                                  child: ProfileButton(pet!.id!, setFollow),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(left: 12, right: 12),
                    child: StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection('pets')
                          .doc(pet!.ownerId)
                          .collection('petOwned')
                          .doc(pet!.id)
                          .collection('EventsHistory')
                          .snapshots(),
                      builder: (context, AsyncSnapshot<dynamic> snapshot) {
                        if (snapshot.hasData) {
                          QuerySnapshot querySnapshot = snapshot.data;
                          List<DocumentSnapshot> documentList =
                              querySnapshot.docs;
                          List<PetEvent> petEventsList = documentList
                              .map((doc) => PetEvent.fromDocumentSnapshot(doc))
                              .toList();
                          return Container(
                            height: (60 * petEventsList.length).toDouble(),
                            child: ListView.builder(
                              itemBuilder: (context, index) {
                                return ListTile(
                                  dense: true,
                                  leading: Icon(
                                    Icons.pin_drop,
                                    size: 20,
                                  ),
                                  iconColor: Colors.redAccent,
                                  title: Text(
                                      "${PetEvent.getEventTypeString(petEventsList[index].eventType)}",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.blue)),
                                  subtitle: Text("${petEventsList[index].name}",
                                      style: TextStyle(
                                          fontSize: 13, color: Colors.black)),
                                  trailing: Text(timeago
                                      .format(petEventsList[index].time)),
                                );
                              },
                              itemCount: petEventsList.length,
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ),
                  Divider(),
                  if (pet!.ownerId == SignedAccount.instance.id)
                    Container(
                      padding: EdgeInsets.only(left: 15, right: 15),
                      width: double.infinity,
                      child: ElevatedButton.icon(
                        label: Text('Tranfer'),
                        icon: Icon(Icons.arrow_forward),
                        style: ElevatedButton.styleFrom(
                            primary: Colors.amber,
                            minimumSize: Size(double.infinity, 30)),
                        onPressed: () {
                          MyUser sender = MyUser.fromSignedInAccount(
                              SignedAccount.instance);

                          print(" Sender ${sender}, Pet ${pet}");

                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) =>
                                  UsersSelectScreen(sender: sender, pet: pet!),
                            ),
                          );
                        },
                      ),
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              postView = "grid";
                            });
                          },
                          color:
                              postView == "grid" ? Colors.blue : Colors.black,
                          icon: Icon(Icons.grid_on)),
                      IconButton(
                          onPressed: () {
                            setState(() {
                              postView = "list";
                            });
                          },
                          color:
                              postView == "list" ? Colors.blue : Colors.black,
                          icon: Icon(Icons.list)),
                    ],
                  ),
                ],
              );
            }

            return Container();
          }),
    );
  }

/* _________________________________ HELPER FUNCTION _________________________________ */

}
