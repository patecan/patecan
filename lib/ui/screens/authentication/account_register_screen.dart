import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountRegisterScreen extends StatefulWidget {
  const AccountRegisterScreen({Key? key}) : super(key: key);

  @override
  _AccountRegisterScreenState createState() => _AccountRegisterScreenState();
}

class _AccountRegisterScreenState extends State<AccountRegisterScreen> {
  final _accountRegisterFormKey = GlobalKey<FormState>();

  String _email = '';
  String _password = '';
  String _confirmPassword = '';

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordConfirmController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Form(
          key: _accountRegisterFormKey,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 60, horizontal: 20),
            child: ListView(
              children: [
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: TextFormField(
                    controller: _emailController,
                    autofocus: false,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: TextStyle(fontSize: 17),
                      border: OutlineInputBorder(),
                      errorStyle:
                          TextStyle(color: Colors.black26, fontSize: 13),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please ";
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
