import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;
import 'package:video_player/video_player.dart';

class RegisterPetFaceIdScreen extends StatefulWidget {
  @override
  State<RegisterPetFaceIdScreen> createState() =>
      _RegisterPetFaceIdScreenState();
}

class _RegisterPetFaceIdScreenState extends State<RegisterPetFaceIdScreen> {
  final ImagePicker _picker = ImagePicker();
  VideoPlayerController? _controller;
  bool isCaptured = false;
  XFile? videoFile;

  @override
  void dispose() {
    _disposeVideoController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Video Capture"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () async {
              videoFile = await _picker.pickVideo(
                  source: ImageSource.camera,
                  maxDuration: const Duration(seconds: 20));
              setState(() {
                isCaptured = true;
              });
              _playVideo(videoFile);
            },
            icon: Icon(Icons.video_call_rounded, color: Colors.green, size: 50),
          ),
          Text(
            "Capture Video",
            style: TextStyle(color: Colors.black),
          ),
          Container(
            height: size.height * 0.7,
            width: size.width,
            child: _previewVideo(),
          ),
          (isCaptured)
              ? MaterialButton(
                  child: Text(
                    "Submit Video to BE",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  onPressed: () {
                    _sendVideoToBackend(videoFile!);
                  },
                  color: Colors.deepOrangeAccent,
                )
              : SizedBox.shrink(),
        ],
      ),
    );
  }

  void _sendVideoToBackend(XFile videoFile) async {
    File _videoFile = File(videoFile.path);

    String fileName = p.basenameWithoutExtension(_videoFile.path);

    FormData formData = FormData.fromMap({
      "video": await MultipartFile.fromFile(
        _videoFile.path,
        filename: fileName,
        contentType: new MediaType("video/mp4", "mp4"),
      )
    });

    var response = await Dio().post(
        'http://192.168.2.3:8000/videos?fileName=$fileName',
        data: formData);

    print(response);
  }

  Widget _previewVideo() {
    if (_controller == null) {
      return const Text(
        'You have not yet picked a video',
        textAlign: TextAlign.center,
      );
    }
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: AspectRatioVideo(_controller),
    );
  }

  Future<void> _playVideo(XFile? file) async {
    if (file != null && mounted) {
      print("Loading Video");
      await _disposeVideoController();
      late VideoPlayerController controller;

      controller = VideoPlayerController.file(File(file.path));

      _controller = controller;
      await controller.initialize();
      await controller.setLooping(true);
      await controller.play();
      setState(() {});
    } else {
      print("Loading Video error");
    }
  }

  Future<void> _disposeVideoController() async {
    /*  if (_toBeDisposed != null) {
      await _toBeDisposed!.dispose();
    }
    _toBeDisposed = _controller;*/
    if (_controller != null) {
      await _controller!.dispose();
    }
    _controller = null;
  }
}

class AspectRatioVideo extends StatefulWidget {
  AspectRatioVideo(this.controller);

  final VideoPlayerController? controller;

  @override
  AspectRatioVideoState createState() => AspectRatioVideoState();
}

class AspectRatioVideoState extends State<AspectRatioVideo> {
  VideoPlayerController? get controller => widget.controller;
  bool initialized = false;

  void _onVideoControllerUpdate() {
    if (!mounted) {
      return;
    }
    if (initialized != controller!.value.isInitialized) {
      initialized = controller!.value.isInitialized;
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    controller!.addListener(_onVideoControllerUpdate);
  }

  @override
  void dispose() {
    controller!.removeListener(_onVideoControllerUpdate);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (initialized) {
      return Center(
        child: AspectRatio(
          aspectRatio: controller!.value.aspectRatio,
          child: VideoPlayer(controller!),
        ),
      );
    } else {
      return Container();
    }
  }
}
