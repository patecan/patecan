import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/authentication/account_authentication_service.dart';
import 'package:social_media/services/authentication/local_auth_service.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/screens/authentication/account_register_screen.dart';

import '../../../constant/app_colors.dart';
import '../../widgets/social/progress.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final storage = FlutterSecureStorage();

  /* Responsive Design */
  double _deviceWidth = 0;
  double _deviceHeight = 0;

  /* Login helper */
  FirebaseUserHelper _firebaseHelper = new FirebaseUserHelper();
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  late AccountAuthenticationService authProvider;

  /* User */
  GoogleSignInAccount? _currentUser;
  MyUser? signedUser;
  SignedAccount signedAccount = SignedAccount.instance;

  /* Other attributes */
  GlobalKey<FormState> _loginFormKey = new GlobalKey<FormState>();
  String? email;
  String? password;
  bool isAuthenticating = false;

  @override
  void initState() {
    authProvider = AccountAuthenticationService(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    _deviceHeight = size.height;
    _deviceWidth = size.width;

    return Scaffold(
      body: isAuthenticating == true
          ? CircularProgress()
          : Container(
              width: _deviceWidth,
              height: _deviceHeight,
              padding: EdgeInsets.symmetric(horizontal: _deviceWidth * 0.15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                gradient: SweepGradient(
                  center: FractionalOffset.center,
                  startAngle: 0.0,
                  endAngle: 3.14 * 2,
                  colors: <Color>[
                    Color(0xFFBEFAF5),
                    Color(0xFFB0B6EA), // blue
                    Color(0xFFFFF1E8), // green// yellow
                    Color(0xFFFFD7BE),
                    Color(
                        0xFFBEFAF5), // red,// blue again to seamlessly transition to the start
                  ],
                  stops: <double>[0.0, 0.25, 0.5, 0.75, 1.0],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Form(
                      onChanged: () {
                        _loginFormKey.currentState!.save();
                      },
                      key: _loginFormKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: buildTextFormField("email"),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: buildTextFormField("password"),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: _deviceWidth,
                    padding:
                        const EdgeInsets.only(top: 10.0, right: 5.0, left: 5.0),
                    child: MaterialButton(
                      onPressed: () async {
                        if (await Connectivity().checkConnectivity() ==
                            ConnectivityResult.none) {
                          return;
                        }

                        if (_loginFormKey.currentState!.validate()) {
                          _loginFormKey.currentState!.save();
                          setState(() {
                            isAuthenticating = true;
                          });
                          await authProvider.loginWithEmailAndPassword(
                              email: email!, password: password!);
                          isAuthenticating = false;
                        }
                      },
                      color: Colors.lightBlue,
                      child: Text(
                        "LOGIN",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                    width: _deviceWidth,
                    padding: EdgeInsets.all(5.0),
                    child: SignInButton(
                      Buttons.Google,
                      elevation: 0.0,
                      text: "Sign in with Google",
                      onPressed: () async {
                        setState(() {
                          isAuthenticating = true;
                        });
                        await authProvider.loginWithGoogle();
                        isAuthenticating = false;
                      },
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      setState(() {
                        isAuthenticating = true;
                      });
                      var isValidUser = await LocalAuthService.authenticate();

                      if (isValidUser) {
                        var email = await storage.read(key: 'email');
                        var password = await storage.read(key: 'password');

                        print("EMAIL ${email} PASS ${password}");

                        await authProvider.loginWithEmailAndPassword(
                            email: email!, password: password!);
                        isAuthenticating = false;
                      }

                      isAuthenticating = false;
                    },
                    child: Container(
                      height: size.height * 0.05,
                      width: size.width,
                      margin: EdgeInsets.only(top: 5),
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                        border: Border.all(color: AppColors.blueGradient),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(0),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 1,
                            spreadRadius: 0.02,
                            offset: Offset(0.25, 0.25),
                          ),
                        ],
                      ),
                      child: Container(
                        margin: EdgeInsets.all(0),
                        padding: EdgeInsets.all(0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: size.height * 0.03,
                              height: size.height * 0.03,
                              child: Image.asset(
                                  'assets/images/auth/fingerprint.png'),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'Face ID & Fingerprint',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: AppColors.blueGradient,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: _deviceWidth,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MaterialButton(
                          padding: EdgeInsets.all(0),
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => AccountRegisterScreen(),
                              ),
                            );
                          },
                          child: Text(
                            "REGISTER",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        MaterialButton(
                          padding: EdgeInsets.all(0),
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed('/recover_password_screen');
                          },
                          child: Text(
                            "FORGOT PASSWORD",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  /* ___________________________ Widget Builder ___________________________ */
  Widget buildTextFormField(String fieldType) {
    if (fieldType == "email") {
      return TextFormField(
        style: TextStyle(color: Colors.indigo),
        decoration: new InputDecoration(
          filled: true,
          fillColor: Colors.white,
          hintText: 'Email',
          contentPadding: const EdgeInsets.only(left: 10.0),
          focusedBorder: OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.lightBlueAccent),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: new BorderSide(color: Colors.white),
            borderRadius: new BorderRadius.circular(5.0),
          ),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter Email';
          } else if (!RegExp(
                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(value)) {
            return 'Email not valid';
          }
          return null;
        },
        onSaved: (value) {
          setState(() {
            email = value;
          });
        },
        cursorColor: Colors.indigo,
      );
    } else {
      return TextFormField(
        style: TextStyle(color: Colors.indigo),
        obscureText: true,
        decoration: new InputDecoration(
          filled: true,
          fillColor: Colors.white,
          hintText: 'Password',
          contentPadding: const EdgeInsets.only(left: 10.0),
          focusedBorder: OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.lightBlueAccent),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: new BorderSide(color: Colors.white),
            borderRadius: new BorderRadius.circular(5.0),
          ),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter Password';
          } else if (value.length < 6) {
            return 'Password too short';
          }
          return null;
        },
        onSaved: (value) {
          setState(
            () {
              password = value;
            },
          );
        },
        cursorColor: Colors.indigo,
      );
    }
  }
}
