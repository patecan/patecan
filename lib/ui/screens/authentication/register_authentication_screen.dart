import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:social_media/services/authentication/local_auth_service.dart';
import 'package:social_media/ui/screens/authentication/register_pet_face_id_screen.dart';
import 'package:social_media/ui/screens/nfc/pet_write_nfc_screen.dart';

import '../../../constant/app_colors.dart';

class RegisterAuthenticationScreen extends StatefulWidget {
  const RegisterAuthenticationScreen({Key? key}) : super(key: key);

  @override
  State<RegisterAuthenticationScreen> createState() =>
      _RegisterAuthenticationScreenState();
}

class _RegisterAuthenticationScreenState
    extends State<RegisterAuthenticationScreen> {
  var finalSecureStorage = FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Account Authentication'),
        centerTitle: false,
      ),
      body: Padding(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildAvailability(context),
              GestureDetector(
                onTap: () async {
                  await finalSecureStorage.write(
                      key: 'email',
                      value: FirebaseAuth.instance.currentUser!.email);

                  TextEditingController _passwordController =
                      new TextEditingController();

                  final GlobalKey<FormState> _registerBioPasswordFormKey =
                      GlobalKey<FormState>();

                  return await showDialog(
                    context: context,
                    builder: (context) {
                      return StatefulBuilder(
                        builder: (context, setState) {
                          return AlertDialog(
                            content: Container(
                              child: Form(
                                key: _registerBioPasswordFormKey,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    TextFormField(
                                      controller: _passwordController,
                                      validator: (value) {
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                          hintText: "Your Password"),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            title:
                                Text('Type you password to register Face ID'),
                            actions: <Widget>[
                              InkWell(
                                child: Text('OK'),
                                onTap: () async {
                                  await finalSecureStorage.write(
                                      key: 'password',
                                      value: _passwordController.value.text);

                                  await finalSecureStorage.write(
                                      key: 'email',
                                      value: FirebaseAuth
                                          .instance.currentUser!.email);

                                  print(
                                      "EMAIL ${await finalSecureStorage.read(key: 'email')} PASS ${await finalSecureStorage.read(key: 'password')}");
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                  );
                },
                child: Container(
                  height: size.height * 0.05,
                  width: size.width,
                  margin: EdgeInsets.only(top: 5),
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.blue2),
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        spreadRadius: 0.02,
                        offset: Offset(0.25, 0.25),
                      ),
                    ],
                  ),
                  child: Container(
                    margin: EdgeInsets.all(0),
                    padding: EdgeInsets.all(0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: size.height * 0.03,
                          height: size.height * 0.03,
                          margin: EdgeInsets.only(left: 20),
                          child: FaIcon(FontAwesomeIcons.fingerprint,
                              color: AppColors.pink2Background),
                        ),
                        Spacer(),
                        Container(
                          child: Text(
                            'Register Face ID & Fingerprint',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: AppColors.blue2,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => RegisterPetFaceIdScreen()),
                  );
                },
                child: Container(
                  height: size.height * 0.05,
                  width: size.width,
                  margin: EdgeInsets.only(top: 5),
                  padding: EdgeInsets.only(
                      top: 5.0, bottom: 5.0, right: 25, left: 25),
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.blue2),
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        spreadRadius: 0.02,
                        offset: Offset(0.25, 0.25),
                      ),
                    ],
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: size.height * 0.03,
                        height: size.height * 0.03,
                        child:
                            Image.asset('assets/images/auth/fingerprint.png'),
                      ),
                      Spacer(),
                      Container(
                        child: Text(
                          'Register Pet Face ID',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: AppColors.blue2,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => PetWriteNFCScreen()),
                  );
                },
                child: Container(
                  height: size.height * 0.05,
                  width: size.width,
                  margin: EdgeInsets.only(top: 5),
                  padding: EdgeInsets.only(
                      top: 5.0, bottom: 5.0, right: 25, left: 25),
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.blue2),
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        spreadRadius: 0.02,
                        offset: Offset(0.25, 0.25),
                      ),
                    ],
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: size.height * 0.03,
                        height: size.height * 0.03,
                        child: Image.asset('assets/images/nfc/pet_tag.png'),
                      ),
                      Spacer(),
                      Container(
                        margin: EdgeInsets.only(left: 0),
                        child: Text(
                          'Register Pet Tag',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: AppColors.blue2,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildAvailability(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        final isAvailable = await LocalAuthService.hasBiometrics();
        final biometrics = await LocalAuthService.getBiometrics();

        final hasFingerprint = biometrics.contains(BiometricType.fingerprint);

        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Availability'),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Biometrics $isAvailable"),
                Text("Fingerprint $hasFingerprint"),
              ],
            ),
          ),
        );
      },
      child: Row(
        children: [Text('Check Availability'), Icon(Icons.event_available)],
      ),
    );
  }
}
