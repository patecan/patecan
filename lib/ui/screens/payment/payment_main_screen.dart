import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/payment/payment_service.dart';
import 'package:social_media/ui/screens/payment/existing_card_screen.dart';

class PaymentMainScreen extends StatefulWidget {
  late String from;
  double amountNeed;

  PaymentMainScreen({required this.from, required this.amountNeed, Key? key})
      : super(key: key);

  @override
  State<PaymentMainScreen> createState() => _PaymentMainScreenState();
}

class _PaymentMainScreenState extends State<PaymentMainScreen> {
  PaymentService paymentService = new PaymentService();

  @override
  void initState() {
    PaymentService.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: ListView.separated(
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () async {
                  if (index == 0) {
                    var response = await PaymentService.payWithNewCard(
                        amount: widget.amountNeed.toString());

                    if (widget.from == 'wallet') {
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(SignedAccount.instance.id)
                          .update(
                        {'balance': FieldValue.increment(response['amount'])},
                      );
                    }
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ExistingCardScreen(
                                from: widget.from,
                                amount: widget.amountNeed.toString())));
                  }
                },
                child: ListTile(
                  title: index == 0
                      ? Text('Pay with new card ')
                      : Text('Pay with existing card'),
                  leading: index == 0
                      ? Icon(Icons.add_circle, color: Colors.blue)
                      : Icon(Icons.credit_card, color: Colors.blue),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider(
                color: Colors.black,
              );
            },
            itemCount: 2),
      ),
    );
  }
}
