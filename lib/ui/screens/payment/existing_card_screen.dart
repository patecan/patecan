import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card_new/credit_card_widget.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/services/payment/payment_service.dart';

import '../../../models/users/singned_account.dart';

class ExistingCardScreen extends StatefulWidget {
  String from;
  String amount;

  ExistingCardScreen({required this.from, required this.amount, Key? key})
      : super(key: key);

  @override
  State<ExistingCardScreen> createState() => _ExistingCardScreenState();
}

class _ExistingCardScreenState extends State<ExistingCardScreen> {
  List cardsMapList = [
    {
      'cardNumber': '42424242424242',
      'expiryDate': '03/23',
      'cardHolderName': 'Thien Trong',
      'cvvCode': '123',
      'showBackView': false,
    },
    {
      'cardNumber': '5555555555554444',
      'expiryDate': '04/24',
      'cardHolderName': 'Tracer',
      'cvvCode': '123',
      'showBackView': false,
    },
  ];

  @override
  Widget build(BuildContext context) {
    List<CardDetails> cardList = cardsMapList
        .map((card) => CardDetails(
              number: card['cardNumber'],
              expirationMonth: int.parse(card['expiryDate'].split('/')[0]),
              expirationYear: int.parse(card['expiryDate'].split('/')[1]),
              cvc: card['cvvCode'],
            ))
        .toList();

    return Scaffold(
      appBar: AppBar(
        title: Text('Choose existing card'),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        color: AppColors.pinkBackground,
        child: ListView.builder(
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: InkWell(
                onTap: () async {},
                child: GestureDetector(
                  onTap: () async {
                    var response = await PaymentService.payWithExistingCard(
                        context: context,
                        amount: widget.amount,
                        cardDetails: cardList[index]);

                    print(response);

                    if (widget.from == 'wallet') {
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(SignedAccount.instance.id)
                          .update(
                        {
                          'balance':
                              FieldValue.increment(double.parse(widget.amount))
                        },
                      );
                    }
                  },
                  child: CreditCardWidget(
                    cardNumber: cardsMapList[index]['cardNumber'],
                    expiryDate: cardsMapList[index]['expiryDate'],
                    cardHolderName: cardsMapList[index]['cardHolderName'],
                    cvvCode: cardsMapList[index]['cvvCode'],
                    showBackView: cardsMapList[index]['showBackView'],
                    isHolderNameVisible: true,
                    isChipVisible: true,
                    isSwipeGestureEnabled: true,
                    cardBgColor: Colors.black,
                    onCreditCardWidgetChange: (CreditCardBrand) {
                      print(CreditCardBrand);
                    }, //true when you want to show cvv(back) view
                  ),
                ),
              ),
            );
          },
          itemCount: cardsMapList.length,
        ),
      ),
    );
  }
}
