import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tflite/tflite.dart';

class VideoScreen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<VideoScreen> {
  List<CameraDescription>? cameras;
  CameraImage? cameraImage;
  CameraController? controller;
  bool isBusy = false;
  String result = "";
  String recognizedPetUrl = "question-mark.png";

  @override
  initState() {
    loadModel();
    initCamera();
    super.initState();
  }

  @override
  Future<void> dispose() async {
    await controller?.dispose();
    super.dispose();
  }

  //TODO intialize camera controller and get frames of live camera footage one by one
  Future<void> initCamera() async {
    cameras = await availableCameras();

    controller = CameraController(cameras![0], ResolutionPreset.max);
    final lensDirection = controller?.description.lensDirection;
    CameraDescription newDescription;

    newDescription = cameras!.firstWhere((description) =>
        description.lensDirection == CameraLensDirection.front);

    controller = CameraController(newDescription, ResolutionPreset.max);

    controller!.initialize().then((_) {
      if (!mounted) {
        return;
      } else {
        setState(() {
          controller!.startImageStream((image) => {
                if (!isBusy)
                  {isBusy = true, cameraImage = image, startImageRecognition()}
              });
        });
      }
    });
  }

//TODO load model and label file
  loadModel() async {
    String? res = await Tflite.loadModel(
        model: "assets/trained_model/model_unquant.tflite",
        labels: "assets/trained_model/labels.txt",
        numThreads: 1,
        // defaults to 1
        isAsset: true,
        // defaults to true, set to false to load resources outside assets
        useGpuDelegate:
            false // defaults to false, set to true to use GPU delegate
        );
  }

//TODO perform image classification with loaded model
  startImageRecognition() async {
    var recognitionsList = await Tflite.runModelOnFrame(
        bytesList: cameraImage!.planes.map((plane) {
          return plane.bytes;
        }).toList(),
        imageHeight: cameraImage!.height,
        imageWidth: cameraImage!.width,
        rotation: 90,
        numResults: 2,
        imageMean: 256,
        imageStd: 256,
        threshold: double.negativeInfinity,
        asynch: true);

    result = "";
    if (recognitionsList != null && recognitionsList.length > 0) {
      setState(
        () {
          result +=
              "${recognitionsList[0]['label']} : ${recognitionsList[0]['confidence']}%\n";
          recognizedPetUrl = "${recognitionsList.first['label']}.jpg";
        },
      );
      print(
          "${recognitionsList[0]['label']} : ${recognitionsList[0]['confidence']}%\n");
    }
    isBusy = false;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: Container(
            color: Colors.black,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 60),
                          child: controller == null
                              ? Center(
                                  child: Container(
                                    width: 140,
                                    height: 150,
                                    child: Icon(
                                      Icons.videocam,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              : AspectRatio(
                                  aspectRatio: 9.0 / 13.0,
                                  child: CameraPreview(controller!),
                                ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 130),
                          child: Image.asset(
                              'assets/images/pet_dictionary/rec.png')),
                      Center(
                          child: Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text(
                                'PET SCANNER',
                                style: TextStyle(
                                    color: Colors.orange.shade700,
                                    fontSize: 25),
                              ))),
                    ],
                  ),
                  Center(
                    child: Container(
                      width: 300,
                      margin: EdgeInsets.only(
                        top: 15,
                      ),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 15, left: 25, right: 25, bottom: 15),
                          child: Row(
                            children: [
                              Container(
                                width: 70,
                                height: 70,
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(35.0),
                                    child: Container(
                                        color: Colors.black,
                                        child: Image.asset(
                                            'assets/images/pet_dictionary/$recognizedPetUrl'))),
                              ),
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(top: 8, left: 10),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '$result',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ],
                            crossAxisAlignment: CrossAxisAlignment.center,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
