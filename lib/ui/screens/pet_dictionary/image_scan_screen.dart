import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:image_picker/image_picker.dart';
import 'package:tflite/tflite.dart';

class ImagesScreen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ImagesScreen> {
  File? _image;
  img.Image? _byteImage;
  late ImagePicker imagePicker;
  String result = 'SCANNER';

  @override
  void initState() {
    super.initState();
    imagePicker = ImagePicker();
    loadModelFiles();
  }

  @override
  dispose() {
    super.dispose();
    Tflite.close();
  }

  Future<void> imageFromGallery() async {
    XFile? file = await imagePicker.pickImage(source: ImageSource.gallery);
    setState(
      () {
        if (file != null) {
          _image = File(file.path);

          //_byteImage = img.decodeImage(_image!.readAsBytesSync());
          // img.Image resized_img =
          //     img.copyResize(image_temp!, width: 96, height: 96);
          //
          // new io.File(file.path)..writeAsBytesSync(img.encodeJpg(resized_img));

          if (_image != null) {
            startImageRecognition();
          }
        }
      },
    );
  }

  Future<void> imageFromCamera() async {
    XFile? file = await imagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      if (file != null) {
        _image = File(file.path);

        //_byteImage = img.decodeImage(_image!.readAsBytesSync());
        // img.Image resized_img =
        //     img.copyResize(image_temp!, width: 96, height: 96);
        //
        // new io.File(file.path)..writeAsBytesSync(img.encodeJpg(resized_img));

        if (_image != null) {
          startImageRecognition();
        }
      }
    });
  }

  loadModelFiles() async {
    await Tflite.loadModel(
        model: "assets/trained_model/model_unquant.tflite",
        labels: "assets/trained_model/labels.txt",
        numThreads: 1,
        isAsset: true,
        // defaults to true, set to false to load resources outside assets
        useGpuDelegate:
            true // defaults to false, set to true to use GPU delegate
        );
  }

  // Uint8List imageToByteListFloat32(
  //     img.Image image, int inputSize, double mean, double std) {
  //   var convertedBytes = Float32List(1 * inputSize * inputSize * 3);
  //   var buffer = Float32List.view(convertedBytes.buffer);
  //   int pixelIndex = 0;
  //   for (var i = 0; i < inputSize; i++) {
  //     for (var j = 0; j < inputSize; j++) {
  //       var pixel = image.getPixel(j, i);
  //       buffer[pixelIndex++] = (img.getRed(pixel) - mean) / std;
  //       buffer[pixelIndex++] = (img.getGreen(pixel) - mean) / std;
  //       buffer[pixelIndex++] = (img.getBlue(pixel) - mean) / std;
  //     }
  //   }
  //   return convertedBytes.buffer.asUint8List();
  // }

  //TODO perform image recognition with images
  startImageRecognition() async {
    List<dynamic>? recognitionsList = await Tflite.runModelOnImage(
        path: _image!.path,
        numResults: 2,
        imageMean: 255,
        imageStd: 255,
        threshold: double.negativeInfinity,
        asynch: true);

    // List<dynamic>? recognitionsList = await Tflite.runModelOnBinary(
    //     binary: imageToByteListFloat32(_byteImage!, 224, 127.5, 127.5),
    //     numResults: 2, // get this value to be the number of classes you have
    //     threshold: 0.05, // defaults to 0.1, or put whatever you want here
    //     asynch: true // defaults to true
    //     );

    print("RECOGNITE ${recognitionsList}");

    result = "";
    if (recognitionsList != null) {
      recognitionsList.forEach(
        (recognition) {
          setState(
            () {
              result +=
                  "${recognition['label']}: ${recognition['confidence']}%\n";
            },
          );

          print(
              "label: ${recognition['label']}: ${(recognition['confidence'])}\n");
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            // Image.file(),
            Center(
              child: Container(
                margin: EdgeInsets.only(top: 25, bottom: 25),
                child: Text(
                  '$result',
                  style:
                      TextStyle(color: Colors.orange.shade700, fontSize: 17.5),
                ),
              ),
            ),

            Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: _image != null
                    ? Container(
                        child: Image.file(
                          _image!,
                          width: 300,
                          height: 300,
                        ),
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                      )
                    : Image.asset(
                        'assets/images/auth/fingerprint.png',
                        width: 300,
                        height: 300,
                      )),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    imageFromGallery();
                  },
                  child: Column(
                    children: [
                      Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(90.0),
                        ),
                        color: Colors.white,
                        child: Container(
                          width: 100,
                          height: 100,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  child: Icon(
                                Icons.image,
                                size: 50,
                                color: Colors.orange.shade700,
                              )),
                            ],
                          ),
                        ),
                      ),
                      Center(
                          child: Text(
                        'Gallery',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'Nunito',
                          color: Colors.orange.shade700,
                        ),
                        textAlign: TextAlign.center,
                      )),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(90.0),
                      ),
                      color: Colors.white,
                      child: InkWell(
                        onTap: () {
                          imageFromCamera();
                        },
                        child: Container(
                          width: 100,
                          height: 100,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  child: Icon(
                                Icons.camera,
                                size: 50,
                                color: Colors.orange.shade700,
                              )),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Text(
                      'Camera',
                      style: TextStyle(
                        fontSize: 18,
                        fontFamily: 'urdu_font',
                        color: Colors.orange.shade700,
                      ),
                      textAlign: TextAlign.center,
                    )),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
