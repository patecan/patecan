import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_geofire/flutter_geofire.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/places/direction.dart';
import 'package:social_media/models/places/place.dart';
import 'package:social_media/models/places/trip.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/places_configuration.dart';
import 'package:social_media/providers/place_provider.dart';
import 'package:social_media/services/places/MapsKitHelper.dart';
import 'package:social_media/services/places/map_more_service.dart';
import 'package:social_media/services/places/map_service.dart';
import 'package:social_media/ui/widgets/places/finish_trip_dialog.dart';
import 'package:social_media/ui/widgets/places/places_drawer.dart';

class TripDetailScreen extends StatefulWidget {
  TripDetail? tripDetail;

  TripDetailScreen(this.tripDetail, {Key? key}) : super(key: key);

  TripDetailScreen.empty();

  @override
  _TripDetailScreenState createState() => _TripDetailScreenState();
}

class _TripDetailScreenState extends State<TripDetailScreen> {
  /* Controller */
  late GoogleMapController rideMapController;
  Completer<GoogleMapController> googleMapCompleter = Completer();
  GlobalKey<ScaffoldState> placesHomepageScreenKey =
      new GlobalKey<ScaffoldState>();

  /* Database and Service */
  MapService mapService = new MapService();
  MapMoreService mapMoreService = new MapMoreService();
  late PlaceProvider placeProvider;
  late DatabaseReference placeRealTimeDB =
      FirebaseDatabase.instance.reference();

  /* Draw on map */
  Set<Polyline> polyLinesSet = {};
  Set<Marker> markersSet = {};
  Set<Circle> circlesSet = {};

  /* For listen feature */
  var keysEntered = false;
  Set keysRetrieved = new Set();

  Direction? direction;
  String stage = 'accepted';
  bool isSelectedDestination = false;

  /* Đây là biến để báo rằng không nên có nhiều request cùng thực hiện một lúc */
  bool isRequesting = false;

  /* Button Stage */
  Color buttonColor = Colors.orangeAccent;
  Text buttonText = Text('ACCEPTED');

  /* Design */
  double deviceHeight = 0.0;
  double deviceWidth = 0.0;
  double mapBottomPadding = 0.0;

  @override
  initState() {
    placeProvider = Provider.of<PlaceProvider>(context, listen: false);
    placeRealTimeDB = FirebaseDatabase.instance.reference();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    deviceHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: placesHomepageScreenKey,
      drawer: PlacesDrawer(deviceHeight, deviceWidth),
      body: Container(
        width: deviceWidth,
        child: FutureBuilder(
          future: setDirection(),
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  GoogleMap(
                    padding: EdgeInsets.only(
                        bottom: mapBottomPadding,
                        top: MediaQuery.of(context).padding.top),
                    mapType: MapType.normal,
                    polylines: polyLinesSet,
                    markers: markersSet,
                    circles: circlesSet,
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: true,
                    initialCameraPosition:
                        CameraPosition(target: LatLng(1, 1), zoom: 0),
                    onMapCreated: (GoogleMapController controller) async {
                      googleMapCompleter.complete(controller);

                      rideMapController = controller;

                      CameraPosition cameraPosition = new CameraPosition(
                          target: LatLng(currentPosition!.latitude,
                              currentPosition!.longitude),
                          zoom: 17);

                      await rideMapController.animateCamera(
                          CameraUpdate.newCameraPosition(cameraPosition));

                      await mapService.setupCurrentPositionLocation(
                          currentPosition!, context);

                      await getLocationUpdate();

                      setState(() {
                        mapBottomPadding = deviceHeight * 0.33;
                      });
                      await drawDirectionPolyline();
                    },
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      child: Container(
                        height: deviceHeight * 0.33,
                        width: deviceWidth,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 5.0,
                              spreadRadius: 0.025,
                              offset: Offset(0.5, 0.5),
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                child: Text(
                                    "Distance: ${direction!.distanceText}"),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                child: Text(
                                    "User: ${widget.tripDetail!.requester.displayName!}"),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Expanded(
                                child: Container(
                                  child: ListTile(
                                    minVerticalPadding: 0,
                                    minLeadingWidth: 5,
                                    dense: true,
                                    leading: Image.asset(
                                        'assets/images/places/desticon.png',
                                        height: 20,
                                        width: 20),
                                    title: Text(
                                        widget.tripDetail!.pickupPlace
                                            .placeFormattedAddress!,
                                        style: TextStyle(fontSize: 15)),
                                  ),
                                ),
                              ),
                              Container(
                                height: deviceHeight * 0.08,
                                child: ListTile(
                                  leading: Image.asset(
                                      'assets/images/places/pickicon.png',
                                      height: 20,
                                      width: 20),
                                  minLeadingWidth: 10,
                                  minVerticalPadding: 0,
                                  dense: true,
                                  contentPadding: EdgeInsets.only(
                                    top: 0,
                                    bottom: 0,
                                    left: 6.0,
                                    right: 0.0,
                                  ),
                                  title: Center(
                                    child: Text(
                                      direction!.distanceText!,
                                      style: TextStyle(
                                          fontSize: 14, fontFamily: 'bolt'),
                                    ),
                                  ),
                                  trailing: Image.asset(
                                      'assets/images/places/desticon.png',
                                      height: 20,
                                      width: 20),
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: buttonColor,
                                    minimumSize: Size(double.infinity, 50)),
                                child: buttonText,
                                onPressed: () async {
                                  if (stage == 'accepted') {
                                    stage = 'arrived';
                                    await placeRealTimeDB
                                        .child("ride_request")
                                        .child(widget.tripDetail!.rideID)
                                        .child('status')
                                        .set(stage);

                                    setState(() {
                                      buttonText = Text("START TRIP");
                                      buttonColor = Colors.blue;
                                    });

                                    await mapService.getDirectionDetail(
                                        LatLng(
                                            (await getCurrentPosition())
                                                .latitude,
                                            (await getCurrentPosition())
                                                .longitude),
                                        LatLng(
                                            widget.tripDetail!.destPlace
                                                .latitude!,
                                            widget.tripDetail!.destPlace
                                                .longitude!));
                                  } else if (stage == 'arrived') {
                                    stage = "on trip";
                                    await placeRealTimeDB
                                        .child("ride_request")
                                        .child(widget.tripDetail!.rideID)
                                        .child('status')
                                        .set(stage);

                                    setState(() {
                                      buttonText = Text("END TRIP");
                                      buttonColor = Colors.redAccent;
                                    });

                                    // Bắt đầu đếm giờ
                                    startTimer();
                                  } else if (stage == "on trip") {
                                    stage = "end trip";

                                    await placeRealTimeDB
                                        .child("ride_request")
                                        .child(widget.tripDetail!.rideID)
                                        .child('status')
                                        .set(stage);

                                    setState(() {
                                      buttonText = Text("ENDED");
                                      buttonColor = Colors.grey;
                                    });

                                    await endTrip();
                                  }
                                },
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return Text('loading...');
            }
          },
        ),
      ),
    );
  }

  /*_________________________________   HELPER FUNCTIONS _________________________________ */

  Future<void> getPermission() async {
    await Permission.location.request();
  }

  Future<void> getLocationUpdate() async {
    LatLng prevPosition = LatLng(0, 0);

    fosterPositionStream = Geolocator.getPositionStream(
            locationSettings: LocationSettings(
                accuracy: LocationAccuracy.bestForNavigation,
                distanceFilter: 4))
        .listen(
      (Position position) async {
        currentPosition = position;

        await Geofire.setLocation(
            SignedAccount.instance.id!, position.latitude, position.longitude);

        double rotationDegree = MapsKitHelper.getMarkerRotation(
            prevPosition.latitude,
            prevPosition.longitude,
            position.latitude,
            position.longitude);

        Marker newCurrentPositionMarker = Marker(
          markerId: new MarkerId(SignedAccount.instance.id!),
          position: new LatLng(position.latitude, position.longitude),
          icon: fosterMarkerIcon!,
          rotation: rotationDegree,
          infoWindow: InfoWindow(title: "Current Position"),
        );

        CameraPosition cameraPosition = new CameraPosition(
            target: LatLng(position.latitude, position.longitude), zoom: 17);
        await rideMapController
            .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

        setState(
          () {
            markersSet.removeWhere((marker) =>
                marker.markerId.value == SignedAccount.instance.id!);
            markersSet.add(newCurrentPositionMarker);
          },
        );

        prevPosition = LatLng(position.latitude, position.longitude);

        updateTripDetail();

        placeRealTimeDB
            .child('ride_request')
            .child(widget.tripDetail!.rideID)
            .child('driver_location')
            .set({
          'latitude': position.latitude.toString(),
          'longitude': position.longitude.toString(),
        });
      },
    );
  }

  void updateTripDetail() async {
    if (!isRequesting) {
      isRequesting = true;
      LatLng currentLatLng = LatLng((await getCurrentPosition()).latitude,
          (await getCurrentPosition()).longitude);

      LatLng? destinationLatLngAtStage;

      switch (stage) {
        case 'accepted':
          destinationLatLngAtStage = LatLng(
              widget.tripDetail!.pickupPlace.latitude!,
              widget.tripDetail!.pickupPlace.longitude!);
          break;
        default:
          destinationLatLngAtStage = LatLng(
              widget.tripDetail!.destPlace.latitude!,
              widget.tripDetail!.destPlace.longitude!);
          break;
      }

      Direction? directionAtStage = await mapService.getDirectionDetail(
          currentLatLng, destinationLatLngAtStage);

      if (directionAtStage != null) {
        if (mounted) {
          setState(() {
            this.direction = directionAtStage;
          });
        }
      }

      isRequesting = false;
    }
  }

  Future<void> drawDirectionPolyline() async {
    polyLinesSet.clear();
    markersSet.clear();
    circlesSet.clear();

    var placeProvider = Provider.of<PlaceProvider>(context, listen: false);

    Place pickupPlace = widget.tripDetail!.pickupPlace;

    LatLng currentLatLng = LatLng((await getCurrentPosition()).latitude,
        (await getCurrentPosition()).longitude);
    LatLng pickupLatLng =
        LatLng(pickupPlace.latitude! + 0.005, pickupPlace.longitude! + 0.005);

    if (direction != null) {
      PolylinePoints polylinePoints = new PolylinePoints();
      List<PointLatLng> pointsList =
          polylinePoints.decodePolyline(direction!.encodedPoints!);

      List<LatLng> latLngList = pointsList.map((PointLatLng point) {
        return LatLng(point.latitude, point.longitude);
      }).toList();

      /*_________________________________ Set bound to fit the view_________________________________ */
      LatLngBounds latLngBounds;

      if (pickupLatLng.latitude > currentPosition!.latitude &&
          pickupLatLng.longitude > currentPosition!.longitude) {
        latLngBounds = LatLngBounds(
          southwest: currentLatLng,
          northeast: pickupLatLng,
        );
      } else if (pickupLatLng.longitude > currentLatLng.longitude) {
        latLngBounds = LatLngBounds(
          southwest: LatLng(pickupLatLng.latitude, currentLatLng.longitude),
          northeast: LatLng(currentLatLng.latitude, pickupLatLng.longitude),
        );
      } else if (pickupLatLng.latitude > currentLatLng.latitude) {
        latLngBounds = LatLngBounds(
          southwest: LatLng(currentLatLng.latitude, pickupLatLng.longitude),
          northeast: LatLng(pickupLatLng.latitude, currentLatLng.longitude),
        );
      } else {
        latLngBounds =
            LatLngBounds(southwest: pickupLatLng, northeast: currentLatLng);
      }

      rideMapController
          .animateCamera(CameraUpdate.newLatLngBounds(latLngBounds, 70));

      /*_________________________________ Set Polyline, Marker and Circle_________________________________ */

      Polyline polyline = Polyline(
          polylineId: PolylineId(SignedAccount.instance.id!),
          color: Color.fromARGB(255, 95, 100, 237),
          points: latLngList,
          jointType: JointType.round,
          width: 4,
          startCap: Cap.roundCap,
          endCap: Cap.roundCap,
          geodesic: true);

      Marker pickupMarker = Marker(
        markerId: MarkerId('pickup'),
        position: pickupLatLng,
        icon: userMarkerIcon!,
        infoWindow: InfoWindow(
            title: pickupPlace.placeFormattedAddress, snippet: 'My Location'),
      );

      Marker currentMarker = Marker(
        markerId: MarkerId(SignedAccount.instance.id!),
        position: currentLatLng,
        icon: fosterMarkerIcon!,
        infoWindow:
            InfoWindow(title: 'Current Destination', snippet: 'My Destination'),
      );

      Circle pickupCircle = Circle(
          circleId: CircleId('pickup'),
          strokeColor: Colors.green,
          strokeWidth: 2,
          radius: 5,
          center: pickupLatLng,
          fillColor: Colors.green);

      Circle currentCircle = Circle(
        circleId: CircleId('current'),
        strokeColor: Colors.red,
        strokeWidth: 2,
        radius: 5,
        center: currentLatLng,
        fillColor: Colors.orange,
      );

      setState(() {
        polyLinesSet.add(polyline);
        markersSet.addAll({pickupMarker, currentMarker});
        circlesSet.addAll({pickupCircle, currentCircle});
        isSelectedDestination = true;
      });
    }
  }

  void resetMap() {
    setState(() {
      polyLinesSet.clear();
      circlesSet.clear();
      markersSet.clear();
      isSelectedDestination = false;
      direction = null;
      placeProvider.destinationPlace = null;
    });
  }

  void cancelRequest() {
    placeRealTimeDB
        .child('ride_request')
        .child("${widget.tripDetail!.rideID}")
        .remove();
  }

  Future<Direction> setDirection() async {
    await createFosterMaker(this.context);
    await createUserMaker(this.context);

    placeProvider = Provider.of<PlaceProvider>(context, listen: false);
    placeProvider.pickupPlace = widget.tripDetail!.pickupPlace;
    placeProvider.destinationPlace = widget.tripDetail!.destPlace;
    Place pickupPlace = placeProvider.pickupPlace!;

    LatLng currentLatLng =
        LatLng(currentPosition!.latitude, currentPosition!.longitude);
    LatLng pickupLatLng =
        LatLng(pickupPlace.latitude! + 0.005, pickupPlace.longitude! + 0.005);

    direction =
        await mapService.getDirectionDetail(currentLatLng, pickupLatLng);

    return direction!;
  }

  void selectLocation(LatLng location) async {
    setState(() {
      Place place =
          new Place(latitude: location.latitude, longitude: location.longitude);
      placeProvider.setNewDestinationPlace(place);
      markersSet.add(new Marker(
          markerId: MarkerId(SignedAccount.instance.id!), position: location));

      CameraPosition cameraPosition = CameraPosition(target: location);
      rideMapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    });
    await drawDirectionPolyline();
  }

  Future<void> endTrip() async {
    timer!.cancel();

    Direction? directionWhenTripStart = await mapService.getDirectionDetail(
        LatLng(widget.tripDetail!.pickupPlace.latitude!,
            widget.tripDetail!.pickupPlace.longitude!),
        LatLng((await getCurrentPosition()).latitude,
            (await getCurrentPosition()).longitude));

    if (directionWhenTripStart != null) {
      directionWhenTripStart.durationValue = secondCount;
      directionWhenTripStart.durationText = secondCount.toString();

      int totalCost = (directionWhenTripStart.estimateCost()).floor();
      await placeRealTimeDB
          .child('ride_request')
          .child("${widget.tripDetail!.rideID}")
          .child('cost')
          .set(totalCost);

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return FinishTripDialog(cost: totalCost);
          });
    }
  }
}
