import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/ui/screens/places/regular_user/user_places_homepage_screen.dart';

class UserPlacesMainScreen extends StatefulWidget {
  const UserPlacesMainScreen({Key? key}) : super(key: key);

  @override
  _UserPlacesMainScreenState createState() => _UserPlacesMainScreenState();
}

class _UserPlacesMainScreenState extends State<UserPlacesMainScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return UserPlacesHomepageScreen();
  }
}
