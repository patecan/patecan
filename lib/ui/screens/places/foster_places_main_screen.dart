import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:social_media/services/notifications/push_notification_service.dart';

import 'foster/foster_homepage_screen.dart';

class FosterPlacesMainScreen extends StatefulWidget {
  const FosterPlacesMainScreen({Key? key}) : super(key: key);

  @override
  _FosterPlacesMainScreenState createState() => _FosterPlacesMainScreenState();
}

class _FosterPlacesMainScreenState extends State<FosterPlacesMainScreen> {
  late PageController pageController;
  int pageIndex = 0;

  @override
  void initState() {
    pageController = new PageController(
        initialPage: pageIndex, keepPage: true, viewportFraction: 1);

    super.initState();
  }

  void changePage(int pageIndex) {
    pageController.animateToPage(
      pageIndex,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeInCubic,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: PageView(
          controller: pageController,
          physics: new NeverScrollableScrollPhysics(),
          children: <Widget>[
            FosterHomepageScreen(),
            Container(),
            Container(),
            Container(),
          ],
          onPageChanged: (index) {
            setState(() => this.pageIndex = index);
          },
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this.pageIndex,
        onTap: changePage,
        elevation: 0,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.credit_card), label: 'Earning'),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Rating'),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined), label: 'Account'),
        ],
      ),
    );
  }
}
