import 'package:flutter/material.dart';

class VehicleInfoScreen extends StatefulWidget {
  VehicleInfoScreen({Key? key}) : super(key: key);

  @override
  _VehicleInfoScreenState createState() => _VehicleInfoScreenState();
}

class _VehicleInfoScreenState extends State<VehicleInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SafeArea(child: SingleChildScrollView(

        child: Column(children: [
            SizedBox(height: 20),
            Image.asset('images/logo.png', height: 110, width: 110),
          Column(children: [
            Text('Enter vehicle details'),
            TextField(
              decoration: InputDecoration(
                labelText: 'Car model',
                hintStyle:TextStyle(
                  color: Colors.grey,
                  fontSize: 10.0,
                ),
              ),
              style: TextStyle(fontSize: 14),
            ),
          ])
        ],),
      ),
        
      ),
    );
  }
}
