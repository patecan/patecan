import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_geofire/flutter_geofire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/place_provider.dart';

import '../../../../places_configuration.dart';
import '../../../widgets/social/progress.dart';

class FosterHomepageScreen extends StatefulWidget {
  @override
  _FosterHomepageScreenState createState() => _FosterHomepageScreenState();
}

class _FosterHomepageScreenState extends State<FosterHomepageScreen> {
  GoogleMapController? googleMapController;
  Completer<GoogleMapController> googleMapCompleter = Completer();
  late DatabaseReference placeDBRef = FirebaseDatabase.instance.reference();

  /* For listen feature */
  var keysEntered = false;
  Set keysRetrieved = new Set();
  Set<Marker> markersSet = new Set();

  /* For setting current position on map*/
  late PlaceProvider placeProvider;

  /* Responsive */
  double deviceHeight = 0.0;
  double deviceWidth = 0.0;

  /* For listen buttons */
  bool isListening = false;

  @override
  void initState() {
    getPermission();
    /* Hàm để lắng nghe xung quanh */
    initPlatformState();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    /* Vì mỗi lần các điểm đang lắng nghe thay đổi
    ( move, enter, exite, query ) chúng ta đều setState()
    nên mỗi lần như thế ta cần đăng ký nghe lại lần nữa*/
    initPlatformState();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    deviceWidth = MediaQuery.of(context).size.width;

    keysRetrieved.forEach(
      (element) {
        if (element == null) {
          keysRetrieved.remove(element);
        }
        print(
            '======================= key: ${element.toString()} =======================');
      },
    );

    return Scaffold(
      body: Container(
        height: deviceHeight,
        width: deviceWidth,
        child: FutureBuilder(
          future: getCurrentPosition(),
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              currentPosition = snapshot.data;

              return Stack(
                children: [
                  Container(
                    width: deviceWidth,
                    child: GoogleMap(
                      markers: markersSet,
                      mapType: MapType.normal,
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top),
                      myLocationButtonEnabled: false,
                      myLocationEnabled: false,
                      zoomGesturesEnabled: true,
                      zoomControlsEnabled: true,
                      initialCameraPosition:
                          CameraPosition(target: LatLng(1, 1), zoom: 0),
                      onMapCreated: (GoogleMapController controller) async {
                        googleMapCompleter.complete(controller);
                        googleMapController = controller;

                        /* Đây là hàm cập nhật lên database khi current postition thay đổi vị trí */
                        await getLocationUpdate();
                        CameraPosition cameraPosition = new CameraPosition(
                            target: LatLng(currentPosition!.latitude,
                                currentPosition!.longitude),
                            zoom: 15);
                        await googleMapController!.animateCamera(
                            CameraUpdate.newCameraPosition(cameraPosition));
                      },
                    ),
                  ),
                  Positioned(
                    top: 0,
                    child: Container(
                      height: deviceHeight * 0.2,
                      width: deviceWidth,
                      decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(7.5),
                          bottomRight: Radius.circular(7.5),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5,
                            spreadRadius: 1,
                            offset: Offset(1, 1),
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          Container(
                            height: deviceHeight * 0.1,
                            alignment: Alignment.topLeft,
                            child: ListView(
                              children: keysRetrieved.map(
                                (key) {
                                  return FutureBuilder(
                                    future: getLocation(key),
                                    builder: (context, result) {
                                      if (result.hasData) {
                                        return Column(
                                          children: [
                                            Text(key),
                                            Text(result.data.toString()),
                                          ],
                                        );
                                      } else {
                                        return Text('waiting...');
                                      }
                                    },
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                          Center(
                            child: Text("Total Keys " +
                                keysRetrieved.length.toString()),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Center(
                                child: RaisedButton(
                                  onPressed: () async {
                                    await setTestLocations();
                                    await goOnline(SignedAccount.instance.id!);
                                  },
                                  color: Colors.blueAccent,
                                  child: Text(
                                    "+ Test",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                              Center(
                                child: RaisedButton(
                                  onPressed: () {
                                    removeTestLocation();
                                  },
                                  color: Colors.blueAccent,
                                  child: Text(
                                    "- Test",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                              Center(
                                child: RaisedButton(
                                  onPressed: () {
                                    initPlatformState();
                                  },
                                  color: Colors.blueAccent,
                                  child: Text(
                                    "Listen",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                              Center(
                                child: RaisedButton(
                                  onPressed: () {
                                    removeQueryListener();
                                    goOffline("trongkamicute");
                                    goOffline(SignedAccount.instance.id!);
                                  },
                                  color: Colors.blueAccent,
                                  child: Text(
                                    "Un-Listen",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return CircularProgress();
            }
          },
        ),
      ),
    );
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    await getCurrentPosition();
    await createFosterMaker(this.context);

    String pathToListen = "driverAvailable";
    await Geofire.initialize(pathToListen);

    try {
      Geofire.setLocation(SignedAccount.instance.id!, currentPosition!.latitude,
          currentPosition!.longitude);

      Geofire.queryAtLocation(
              currentPosition!.latitude, currentPosition!.longitude, 50)!
          .listen((location) {
        print(location);
        if (location != null) {
          var callBack = location['callBack'];
          switch (callBack) {
            case Geofire.onKeyEntered:
              if (keysEntered = true) {
                keysRetrieved.removeWhere((key) => key == null);
                keysRetrieved.add(location["key"].toString());
                if (mounted) {
                  setState(() {});
                }
              }
              break;

            case Geofire.onKeyExited:
              keysRetrieved.remove(location["key"]);
              if (mounted) {
                setState(() {});
              }
              break;

            case Geofire.onKeyMoved:
              print(
                  'key ${location["key"]} moved to: ${location["latitude"]} ${location["latitude"]}');
              if (mounted) {
                setState(() {});
              }
              break;

            case Geofire.onGeoQueryReady:
              // All initial Data is loaded
              keysEntered = true;
              if (location['result'] == null ||
                  location['key'] == null ||
                  location['latitude'] == null ||
                  location['longitude'] == null) {
                print('yes it null');
              } else {
                keysRetrieved.add(location["key"].toString());
              }
              if (mounted) {
                setState(() {});
              }
              break;
          }
        }
        if (mounted) {
          setState(() {});
        }
      }).onError((error) {
        print(error);
      });
    } on PlatformException {
      print("Platform Exception");
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  /* ___________________________ Helper functions ___________________________*/
  Future<void> goOnline(String fosterId) async {
    placeDBRef.child('driver').child(fosterId).set({'trip': 'waiting'});
    print("${fosterId} GO ONLINE");
  }

  Future<void> setTestLocations() async {
    bool? responseA = await Geofire.setLocation("trongkamicute",
        currentPosition!.latitude + 0.005, currentPosition!.longitude);

    goOnline("trongkamicute");
    print(responseA);
  }

  Future<void> goOffline(String fosterId) async {
    await Geofire.removeLocation(fosterId);
    placeDBRef.child('driver').child(fosterId).set({'trip': 'waiting'});
    print("${fosterId} GO OFFLINE");
  }

  void removeTestLocation() async {
    bool? response = await Geofire.removeLocation("trongkamicute");

    markersSet.removeWhere(
        (Marker marker) => marker.markerId == MarkerId("trongkamicute"));

    keysRetrieved.forEach((element) {
      if (element == null) {
        keysRetrieved.remove(element);
      }
    });

    print(response);
    if (mounted) {
      setState(() {});
    }
  }

  void removeQueryListener() async {
    bool? response = await Geofire.stopListener();
    keysEntered = false;
    markersSet.clear();
    keysRetrieved = new Set();
    if (mounted) {
      setState(() {});
    }
    print(response);
  }

  Future<Map> getLocation(String key) async {
    print('key $key');
    Map<String, dynamic> response = await Geofire.getLocation('$key');
    print('get Location: $response');

    LatLng latLng = LatLng(response['lat'], response['lng']);
    await createFosterMaker(this.context);

    Marker thisMarker = Marker(
      markerId: MarkerId(key),
      position: latLng,
      icon: SignedAccount.instance.id == key
          ? fosterMarkerIcon!
          : BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
    );

    markersSet.add(thisMarker);
    markersSet.forEach((thisMarker) {
      print('markers { ${response['lat']},${response['lng']}  }');
    });

    return response;
  }

  Future<void> getLocationUpdate() async {
    fosterPositionStream = Geolocator.getPositionStream(
      locationSettings: LocationSettings(
          accuracy: LocationAccuracy.bestForNavigation, distanceFilter: 4),
    ).listen((Position position) async {
      currentPosition = position;

      await Geofire.setLocation(
          SignedAccount.instance.id!, position.latitude, position.longitude);

      Marker newCurrentPositionMarker = Marker(
        markerId: new MarkerId(SignedAccount.instance.id!),
        position: new LatLng(position.latitude, position.longitude),
        icon: fosterMarkerIcon!,
        infoWindow: InfoWindow(title: "Current Position"),
      );

      CameraPosition cameraPosition = new CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: 17);
      await googleMapController!
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      setState(
        () {
          markersSet.removeWhere(
              (marker) => marker.markerId.value == SignedAccount.instance.id!);
          markersSet.add(newCurrentPositionMarker);
        },
      );
    });
  }

  Future<void> getPermission() async {
    await Permission.location.request();
  }
}
