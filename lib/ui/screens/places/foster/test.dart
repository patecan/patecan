import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_geofire/flutter_geofire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/place_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Set keysRetrieved = new Set();
  late Position currentPosition;
  var keysEntered = false;
  late GoogleMapController googleMapController;
  late PlaceProvider placeProvider;
  Set<Marker> markersSet = new Set();

  setCurrentPosition() async {
    currentPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.bestForNavigation);
    return currentPosition;
  }

  @override
  void initState() {
    setCurrentPosition();
    initPlatformState();
    updateYourOwnLocation();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    initPlatformState();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    print('_______________________________________');
    keysRetrieved.forEach((element) {
      if (element == null) {
        keysRetrieved.remove(element);
      }
      print(element.toString());
    });
    print('_______________________________________');
    return MaterialApp(
      home: FutureBuilder(
          future: setCurrentPosition(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Scaffold(
                body: Column(
                  children: <Widget>[
                    Container(
                      height: 100,
                      width: 500,
                      child: ListView(
                          children: keysRetrieved.map((e) {
                        return FutureBuilder(
                            future: getLocation(e),
                            builder: (context, result) {
                              if (result.hasData) {
                                return Column(children: [
                                  Text(e),
                                  Text(result.data.toString()),
                                ]);
                              } else {
                                return Text('waiting...');
                              }
                            });
                      }).toList()),
                    ),
                    Center(
                      child: Text(
                          "\nTotal Keys " + keysRetrieved.length.toString()),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: RaisedButton(
                            onPressed: () {
                              setTwoLocations();
                            },
                            color: Colors.blueAccent,
                            child: Text(
                              "Set 2 Location",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        Center(
                          child: RaisedButton(
                            onPressed: () {
                              removeLocation();
                            },
                            color: Colors.blueAccent,
                            child: Text(
                              "Remove 2 Locations",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: RaisedButton(
                            onPressed: () {
                              initPlatformState();
                            },
                            color: Colors.blueAccent,
                            child: Text(
                              "Register Listener",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        Center(
                          child: RaisedButton(
                            onPressed: () {
                              removeQueryListener();
                            },
                            color: Colors.blueAccent,
                            child: Text(
                              "Remove Query Listener",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: 400,
                      height: 500,
                      child: GoogleMap(
                        markers: markersSet,
                        mapType: MapType.normal,
                        myLocationButtonEnabled: false,
                        myLocationEnabled: false,
                        zoomGesturesEnabled: true,
                        zoomControlsEnabled: true,
                        initialCameraPosition:
                            CameraPosition(target: LatLng(1, 1), zoom: 0),
                        onMapCreated: (GoogleMapController controller) async {
                          googleMapController = controller;
                          CameraPosition cameraPosition = new CameraPosition(
                              target: LatLng(currentPosition.latitude,
                                  currentPosition.longitude),
                              zoom: 15);
                          googleMapController.animateCamera(
                              CameraUpdate.newCameraPosition(cameraPosition));
                        },
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Text('waiting');
            }
          }),
    );
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String pathToReference = "Sites";

    Geofire.initialize(pathToReference);

    try {
      currentPosition = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.bestForNavigation);
      Geofire.queryAtLocation(
              currentPosition.latitude, currentPosition.longitude, 50)!
          .listen((map) {
        print(map);
        if (map != null) {
          var callBack = map['callBack'];
          switch (callBack) {
            case Geofire.onKeyEntered:
              if (keysEntered = true) {
                keysRetrieved.removeWhere((key) => key == null);
                keysRetrieved.add(map["key"].toString());
              }
              break;

            case Geofire.onKeyExited:
              keysRetrieved.remove(map["key"]);
              break;

            case Geofire.onKeyMoved:
              print(
                  'key ${map["key"]} moved to: ${map["latitude"]} ${map["latitude"]}');
              setState(() {});
              break;

            case Geofire.onGeoQueryReady:
              // All initial Data is loaded
              keysEntered = true;
              if (map['result'] == null ||
                  map['key'] == null ||
                  map['latitude'] == null ||
                  map['longitude'] == null) {
                print('yes it null');
              } else {
                keysRetrieved.add(map["key"].toString());
              }

              setState(() {});
              break;
          }
        }
        setState(() {});
      }).onError((error) {
        print(error);
      });
    } on PlatformException {
//      response = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  /* ___________________________ Helper functions ___________________________*/
  void setTwoLocations() async {
    if (keysRetrieved.contains("trongkamicute") ||
        keysRetrieved.contains("trongkamicute2")) {
      return;
    }

    bool? responseA = await Geofire.setLocation("trongkamicute",
        currentPosition.latitude + 0.05, currentPosition.longitude);

    bool? responseB = await Geofire.setLocation("trongkamicute2",
        currentPosition.latitude + 0.03, currentPosition.longitude);

    print(responseA);
    print(responseB);
  }

  void removeLocation() async {
    bool? response = await Geofire.removeLocation("trongkamicute");
    bool? response2 = await Geofire.removeLocation("trongkamicute2");

    keysRetrieved.forEach((element) {
      if (element == null) {
        keysRetrieved.remove(element);
      }
    });

    print(response);
    print(response2);
    setState(() {});
  }

  void removeQueryListener() async {
    bool? response = await Geofire.stopListener();
    keysEntered = false;

    keysRetrieved = new Set();
    setState(() {});

    print(response);
  }

  Future<Map> getLocation(String key) async {
    print('key $key');
    Map<String, dynamic> response = await Geofire.getLocation('$key');
    print('get Location: $response');

    LatLng latLng = LatLng(response['lat'], response['lng']);
    Marker thisMarker = Marker(
      markerId: MarkerId(key),
      position: latLng,
      icon: BitmapDescriptor.defaultMarkerWithHue(key == 'trongkami'
          ? BitmapDescriptor.hueRed
          : BitmapDescriptor.hueGreen),
    );

    markersSet.add(thisMarker);
    markersSet.forEach((thisMarker) {
      print('markers { ${response['lat']},${response['lng']}  }');
    });

    return response;
  }

  void updateYourOwnLocation() {
    Geolocator.getPositionStream(
            locationSettings: LocationSettings(
                accuracy: LocationAccuracy.bestForNavigation,
                distanceFilter: 4))
        .listen((Position position) async {
      currentPosition = position;

      await Geofire.setLocation(
          'trongkami', position.latitude, position.longitude);
      print('get location done');

      CameraPosition cameraPosition = new CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: 15);
      googleMapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    });
  }
}
