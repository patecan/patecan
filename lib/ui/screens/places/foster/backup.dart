// import 'dart:async';
// import 'dart:math';
//
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_database/firebase_database.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:geoflutterfire/geoflutterfire.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:permission_handler/permission_handler.dart';
// import 'package:provider/provider.dart';
// import 'package:social_media/models/places/direction.dart';
// import 'package:social_media/models/places/nearby_driver.dart';
// import 'package:social_media/models/users/singned_account.dart';
// import 'package:social_media/providers/place_provider.dart';
// import 'package:social_media/services/places/map_service.dart';
// import 'package:social_media/widgets/places/places_drawer.dart';
// import 'package:social_media/widgets/rounded_loading_button.dart';
// import 'package:flutter_geofire/flutter_geofire.dart';
//
// import '../../../models/places/nearby_driver.dart';
//
// class FosterHomeScreen1 extends StatefulWidget {
//   const FosterHomeScreen1({Key? key}) : super(key: key);
//
//   @override
//   _FosterHomeScreen1State createState() => _FosterHomeScreen1State();
// }
//
// class _FosterHomeScreen1State extends State<FosterHomeScreen1> {
//   late GoogleMapController googleMapController;
//   GlobalKey<ScaffoldState> placesHomepageScreenKey =
//       new GlobalKey<ScaffoldState>();
//   MapService mapService = new MapService();
//   late PlaceProvider placeProvider;
//   Direction? direction;
//   List<String> keysRetrieved = [];
//
//   Position? currentPosition;
//   late RoundedLoadingButtonController _requestController;
//
//   late DatabaseReference placeDBRef = FirebaseDatabase.instance.reference();
//   StreamSubscription? streamSubscription;
//
//   Set<Polyline> polylinesSet = {};
//   Set<Marker> markersSet = {};
//   Set<Circle> circlesSet = {};
//
//   bool isSelectedDestination = false;
//   bool isGoOnline = false;
//   bool nearbyDriversKeysLoaded = false;
//
//   var geoLocator = Geolocator();
//   var locationOptions = LocationOptions(
//       accuracy: LocationAccuracy.bestForNavigation, distanceFilter: 4);
//
//   double deviceHeight = 0.0;
//   double deviceWidth = 0.0;
//   double mapBottomPadding = 0.0;
//
//   @override
//   initState() {
//     placeProvider = Provider.of<PlaceProvider>(context, listen: false);
//     placeDBRef = FirebaseDatabase.instance.reference();
//     _requestController = RoundedLoadingButtonController();
//     startGeofireListener();
//     getPermission();
//     super.initState();
//   }
//
//   didChangeDependencies() {
//     super.didChangeDependencies();
//   }
//
//   @override
//   void setState(fn) {
//     super.setState(fn);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     deviceHeight = (MediaQuery.of(context).size.height -
//         MediaQuery.of(context).padding.top);
//     deviceWidth = MediaQuery.of(context).size.width;
//
//     return Scaffold(
//       key: placesHomepageScreenKey,
//       drawer: PlacesDrawer(deviceHeight, deviceWidth),
//       body: Stack(
//         children: [
//           GoogleMap(
//             polylines: polylinesSet,
//             markers: markersSet,
//             circles: circlesSet,
//             padding: EdgeInsets.only(
//                 bottom: mapBottomPadding,
//                 top: MediaQuery.of(context).padding.top),
//             mapType: MapType.normal,
//             myLocationButtonEnabled: true,
//             myLocationEnabled: true,
//             zoomGesturesEnabled: true,
//             zoomControlsEnabled: true,
//             initialCameraPosition:
//                 CameraPosition(target: LatLng(1, 1), zoom: 0),
//             onMapCreated: (GoogleMapController controller) async {
//               googleMapController = controller;
//               setState(() {
//                 mapBottomPadding = deviceHeight * 0.33;
//               });
//               // currentPosition = await mapService.setupCurrentPositionLocation(
//               //     googleMapController, context);
//             },
//           ),
//           Positioned(
//             top: 0,
//             child: Container(
//               height: deviceHeight * 0.10,
//               width: deviceWidth,
//               padding: EdgeInsets.symmetric(horizontal: 20),
//               alignment: Alignment.center,
//               decoration: BoxDecoration(
//                 color: isGoOnline
//                     ? Colors.lightGreen
//                     : Theme.of(context).backgroundColor,
//                 borderRadius: BorderRadius.only(
//                   bottomLeft: Radius.circular(7.5),
//                   bottomRight: Radius.circular(7.5),
//                 ),
//                 boxShadow: [
//                   BoxShadow(
//                     color: Colors.grey,
//                     blurRadius: 5,
//                     spreadRadius: 1,
//                     offset: Offset(1, 1),
//                   ),
//                 ],
//               ),
//               child: isGoOnline == false
//                   ? RoundedLoadingButton(
//                       color: Colors.white,
//                       borderColor: Colors.lightGreen,
//                       successColor: Colors.green,
//                       valueColor: Colors.green,
//                       width: deviceWidth,
//                       height: deviceHeight * 0.05,
//                       elevation: 0,
//                       onPressed: () {
//                         _requestController.start();
//                         setState(() {
//                           goOnline();
//                           _requestController.success();
//                           isGoOnline = true;
//                           _requestController.stop();
//                         });
//                       },
//                       controller: _requestController,
//                       child: Text(
//                         'GO ONLINE',
//                         style: TextStyle(
//                             color: Colors.lightGreen,
//                             fontWeight: FontWeight.bold,
//                             fontSize: 15),
//                       ),
//                     )
//                   : Align(
//                       alignment: Alignment.centerRight,
//                       child: IconButton(
//                         icon: Icon(
//                           Icons.cancel,
//                           color: Colors.white,
//                           size: 30,
//                         ),
//                         onPressed: () {
//                           setState(() {
//                             isGoOnline = false;
//                             goOffline();
//                           });
//                         },
//                       ),
//                     ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
//
//   /*_________________________________   HELPER FUNCTIONS _________________________________ */
//   Future<void> getPermission() async {
//     await Permission.location.request();
//   }
//
//   /*_________________________________  GO ON/OFFLINE _________________________________ */
//
//   void goOnline() async {
//     Geofire.initialize('driverAvailable');
//     Geofire.setLocation(SignedAccount.instance.id!,
//         placeProvider.place!.latitude!, placeProvider.place!.longitude!);
//
//     Geofire.setLocation(SignedAccount.instance.id! + '1',
//         placeProvider.place!.latitude! + 0.05, placeProvider.place!.longitude!);
//     placeDBRef
//         .child('driver/${SignedAccount.instance.id}/newtrip')
//         .set('waiting');
//   }
//
//   void goOffline() async {
//     placeDBRef.child('drivers/${SignedAccount.instance.id}').onDisconnect();
//     //FirebaseDatabase.instance.reference().child('driverAvailable').child(SignedAccount.instance.id!).remove();
//     Geofire.removeLocation(SignedAccount.instance.id!);
//   }
//
//   /*_________________________________ NEARBY FUNCTIONS  _________________________________ */
//
//   Future<void> startGeofireListener() async {
//     Position currentPosition = await Geolocator.getCurrentPosition(
//         desiredAccuracy: LocationAccuracy.bestForNavigation);
//
//     Geofire.initialize('driversAvailable');
//
//     try {
//       Geofire.queryAtLocation(
//               currentPosition.latitude, currentPosition.longitude, 20)!
//           .listen(
//         (map) {
//           if (map != null) {
//             var callBack = map['callBack'];
//
//             switch (callBack) {
//               case Geofire.onKeyEntered:
//                 NearbyDriver nearbyDriver = NearbyDriver(
//                     key: map['key'],
//                     latitude: map['latitude'],
//                     longitude: map['longitude']);
//
//                 NearbyDriver.nearbyDriverList.add(nearbyDriver);
//
//                 if (nearbyDriversKeysLoaded) {
//                   updateDriversOnMap();
//                 }
//                 break;
//
//               case Geofire.onKeyExited:
//                 NearbyDriver.removeFromList(map['key']);
//                 updateDriversOnMap();
//                 break;
//
//               case Geofire.onKeyMoved:
//                 // Update your key's location
//
//                 NearbyDriver nearbyDriver = NearbyDriver(
//                     key: map['key'],
//                     latitude: map['latitude'],
//                     longitude: map['longitude']);
//
//                 NearbyDriver.updateNearbyLocation(nearbyDriver);
//                 updateDriversOnMap();
//                 break;
//
//               case Geofire.onGeoQueryReady:
//                 nearbyDriversKeysLoaded = true;
//                 updateDriversOnMap();
//                 break;
//             }
//           }
//           setState(() {});
//         },
//       ).onError((error) {
//         print(error);
//       });
//     } on PlatformException {
// //      response = 'Failed to get platform version.';
//     }
//
//     // If the widget was removed from the tree while the asynchronous platform
//     // message was in flight, we want to discard the reply rather than calling
//     // setState to update our non-existent appearance.
//     if (!mounted) return;
//   }
//
//   void updateDriversOnMap() {
//     setState(() {
//       markersSet.clear();
//     });
//
//     Set<Marker> tempMarkers = Set<Marker>();
//
//     for (NearbyDriver driver in NearbyDriver.nearbyDriverList) {
//       LatLng driverPosition = LatLng(driver.latitude, driver.longitude);
//       Marker thisMarker = Marker(
//         markerId: MarkerId('driver${driver.key}'),
//         position: driverPosition,
//         icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
//       );
//
//       tempMarkers.add(thisMarker);
//     }
//
//     setState(() {
//       markersSet = tempMarkers;
//     });
//   }
// }
