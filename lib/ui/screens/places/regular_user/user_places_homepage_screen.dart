import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_geofire/flutter_geofire.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/places/direction.dart';
import 'package:social_media/models/places/place.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/place_provider.dart';
import 'package:social_media/services/notifications/push_notification_service.dart';
import 'package:social_media/services/places/MapsKitHelper.dart';
import 'package:social_media/services/places/map_more_service.dart';
import 'package:social_media/services/places/map_service.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

import '../../../../places_configuration.dart';
import '../../../widgets/places/places_drawer.dart';
import '../../../widgets/places/search_slivers.dart';
import '../../../widgets/places/user_places_dashboard.dart';

class UserPlacesHomepageScreen extends StatefulWidget {
  UserPlacesHomepageScreen({Key? key}) : super(key: key);

  @override
  _UserPlacesHomepageScreenState createState() =>
      _UserPlacesHomepageScreenState();
}

class _UserPlacesHomepageScreenState extends State<UserPlacesHomepageScreen> {
  GoogleMapController? googleMapController;
  Completer<GoogleMapController> googleMapCompleter = Completer();

  GlobalKey<ScaffoldState> placesHomepageScreenKey =
      new GlobalKey<ScaffoldState>();

  MapService mapService = new MapService();
  MapMoreService mapMoreService = new MapMoreService();

  PushNotificationService pushNotificationService =
      new PushNotificationService();

  late PlaceProvider placeProvider;
  Direction? direction;

  late DatabaseReference placeRealTimeDB =
      FirebaseDatabase.instance.reference();

  Set<Polyline> polyLinesSet = {};
  Set<Marker> markersSet = {};
  Set<Circle> circlesSet = {};

  /* For listen feature */
  var keysEntered = false;
  Set keysRetrieved = new Set();

  bool isSelectedDestination = false;

  double deviceHeight = 0.0;
  double deviceWidth = 0.0;
  double mapBottomPadding = 0.0;

  @override
  initState() {
    getPermission();
    placeProvider = Provider.of<PlaceProvider>(context, listen: false);
    placeRealTimeDB = FirebaseDatabase.instance.reference();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    createUserMaker(context);
    deviceHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: placesHomepageScreenKey,
      drawer: PlacesDrawer(deviceHeight, deviceWidth),
      body: Container(
        width: deviceWidth,
        child: FutureBuilder(
          future: getCurrentPosition(),
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  GoogleMap(
                    polylines: polyLinesSet,
                    markers: markersSet,
                    circles: circlesSet,
                    padding: EdgeInsets.only(
                        bottom: mapBottomPadding,
                        top: MediaQuery.of(context).padding.top),
                    mapType: MapType.normal,
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: true,
                    onTap: selectLocation,
                    initialCameraPosition:
                        CameraPosition(target: LatLng(1, 1), zoom: 0),
                    onMapCreated: (GoogleMapController controller) async {
                      googleMapCompleter.complete(controller);
                      googleMapController = controller;

                      await createUserMaker(this.context);
                      CameraPosition cameraPosition = new CameraPosition(
                          target: LatLng(currentPosition!.latitude,
                              currentPosition!.longitude),
                          zoom: 15);

                      await googleMapController!.animateCamera(
                          CameraUpdate.newCameraPosition(cameraPosition));
                      await mapService.setupCurrentPositionLocation(
                          currentPosition!, context);

                      await getLocationUpdate();
                      await startListenOtherDrivers();
                      setState(() {
                        mapBottomPadding = deviceHeight * 0.33;
                      });
                    },
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      child: isSelectedDestination
                          ? UserPlacesDashboard(
                              deviceHeight: deviceHeight,
                              deviceWidth: deviceWidth,
                              direction: direction!,
                              createRideRequest: createRideRequest,
                              cancelRequest: cancelRequest,
                            )
                          : SearchSlivers(
                              deviceHeight: deviceHeight,
                              deviceWidth: deviceWidth,
                              drawDirectionPolyline: drawDirectionPolyline,
                            ),
                    ),
                  ),
                  Positioned(
                    top: MediaQuery.of(context).padding.top +
                        deviceHeight * 0.01,
                    left: deviceHeight * 0.01,
                    child: GestureDetector(
                      onTap: () =>
                          placesHomepageScreenKey.currentState!.openDrawer(),
                      child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black,
                                blurRadius: 5,
                                spreadRadius: 0.1,
                                offset: Offset(0.7, 0.7),
                              ),
                            ],
                          ),
                          child: CircleAvatar(
                            backgroundColor: Theme.of(context).backgroundColor,
                            child: Icon(
                              Icons.menu_rounded,
                              color: Colors.blueAccent,
                            ),
                          )),
                    ),
                  ),
                ],
              );
            } else {
              return CircularProgress();
            }
          },
        ),
      ),
    );
  }

  /*_________________________________   HELPER FUNCTIONS _________________________________ */
  Future<void> getPermission() async {
    await Permission.location.request();
  }

  Future<void> drawDirectionPolyline() async {
    polyLinesSet.clear();
    //markersSet.clear();
    circlesSet.clear();

    var placeProvider = Provider.of<PlaceProvider>(context, listen: false);

    Place pickupPlace = placeProvider.pickupPlace!;
    Place destinationPlace = placeProvider.destinationPlace!;

    LatLng pickupLatLng = LatLng(pickupPlace.latitude!, pickupPlace.longitude!);
    LatLng destinationLatLng =
        LatLng(destinationPlace.latitude!, destinationPlace.longitude!);

    direction =
        await mapService.getDirectionDetail(pickupLatLng, destinationLatLng);

    if (direction != null) {
      PolylinePoints polylinePoints = new PolylinePoints();
      List<PointLatLng> pointsList =
          polylinePoints.decodePolyline(direction!.encodedPoints!);

      List<LatLng> latLngList = pointsList.map((PointLatLng point) {
        return LatLng(point.latitude, point.longitude);
      }).toList();

      setState(
        () {
          Polyline polyline = Polyline(
              polylineId: PolylineId(SignedAccount.instance.id!),
              color: Color.fromARGB(255, 95, 100, 237),
              points: latLngList,
              jointType: JointType.round,
              width: 4,
              startCap: Cap.roundCap,
              endCap: Cap.roundCap,
              geodesic: true);

          polyLinesSet.add(polyline);
        },
      );

      /*_________________________________ Set bound to fit the view_________________________________ */
      LatLngBounds latLngBounds;

      if (pickupLatLng.latitude > destinationLatLng.latitude &&
          pickupLatLng.longitude > destinationLatLng.longitude) {
        latLngBounds = LatLngBounds(
          southwest: destinationLatLng,
          northeast: pickupLatLng,
        );
      } else if (pickupLatLng.longitude > destinationLatLng.longitude) {
        latLngBounds = LatLngBounds(
          southwest: LatLng(pickupLatLng.latitude, destinationLatLng.longitude),
          northeast: LatLng(destinationLatLng.latitude, pickupLatLng.longitude),
        );
      } else if (pickupLatLng.latitude > destinationLatLng.latitude) {
        latLngBounds = LatLngBounds(
          southwest: LatLng(destinationLatLng.latitude, pickupLatLng.longitude),
          northeast: LatLng(pickupLatLng.latitude, destinationLatLng.longitude),
        );
      } else {
        latLngBounds =
            LatLngBounds(southwest: pickupLatLng, northeast: destinationLatLng);
      }

      googleMapController!
          .animateCamera(CameraUpdate.newLatLngBounds(latLngBounds, 70));

      /*_________________________________ Set Marker and Circle_________________________________ */
      Marker pickupMarker = Marker(
        markerId: MarkerId(SignedAccount.instance.id!),
        position: pickupLatLng,
        icon: userMarkerIcon!,
        infoWindow: InfoWindow(
            title: pickupPlace.placeFormattedAddress, snippet: 'My Location'),
      );

      Marker destinationMarker = Marker(
        markerId: MarkerId('destination'),
        position: destinationLatLng,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
        infoWindow: InfoWindow(
            title: destinationPlace.placeFormattedAddress,
            snippet: 'My Destination'),
      );

      Circle pickupCircle = Circle(
          circleId: CircleId('pickup'),
          strokeColor: Colors.green,
          strokeWidth: 2,
          radius: 5,
          center: pickupLatLng,
          fillColor: Colors.green);

      Circle destinationCircle = Circle(
        circleId: CircleId('destination'),
        strokeColor: Colors.orange,
        strokeWidth: 2,
        radius: 5,
        center: destinationLatLng,
        fillColor: Colors.orange,
      );

      setState(
        () {
          markersSet.addAll({pickupMarker, destinationMarker});
          circlesSet.addAll({pickupCircle, destinationCircle});
          isSelectedDestination = true;
        },
      );
    }
  }

  createRideRequest() async {
    Place pickupPlace = placeProvider.pickupPlace!;
    Place destinationPlace = placeProvider.destinationPlace!;

    Map<String, dynamic> pickupMap = {
      'address': pickupPlace.placeFormattedAddress,
      'latitude': pickupPlace.latitude.toString(),
      'longitude': pickupPlace.longitude.toString(),
    };

    Map<String, dynamic> destinationMap = {
      'address': destinationPlace.placeFormattedAddress,
      'latitude': destinationPlace.latitude.toString(),
      'longitude': destinationPlace.longitude.toString(),
    };

    Map<String, dynamic> rideRequestMap = {
      'ride_id': SignedAccount.instance.id!,
      'create_time': DateTime.now().toString(),
      'requester_id': SignedAccount.instance.id,
      'passenger_name': SignedAccount.instance.displayName,
      'pickup_location': pickupMap,
      'destination_location': destinationMap,
      'payment_method': 'cash',
      'driver_id': 'waiting',
      'driver_name': '',
      'driver_phone': '',
    };

    await placeRealTimeDB
        .child('ride_request')
        .child(SignedAccount.instance.id!)
        .set(rideRequestMap);

    List<String> availableDriverIdList =
        await mapMoreService.getIdOfAvailableDrivers();

    availableDriverIdList.forEach(
      (driverId) async {
        await pushNotificationService.sendRideRequestNotification(
            toUserId: driverId,
            rideRequestMap: rideRequestMap,
            notifyTitle:
                'New ride request from ${SignedAccount.instance.displayName}',
            notifyBody:
                '${SignedAccount.instance.displayName} requesting for ride ');
      },
    );
  }

  void cancelRequest() {
    placeRealTimeDB.child('ride_request').remove();
  }

  Future<void> getLocationUpdate() async {
    LatLng oldPosition = LatLng(0, 0);

    userPositionStream = Geolocator.getPositionStream(
            locationSettings: LocationSettings(
                accuracy: LocationAccuracy.bestForNavigation,
                distanceFilter: 4))
        .listen(
      (Position position) async {
        currentPosition = position;

        // await Geofire.setLocation(
        //     SignedAccount.instance.id!, position.latitude, position.longitude);

        var markerRotation = MapsKitHelper.getMarkerRotation(
            oldPosition.latitude,
            oldPosition.longitude,
            position.latitude,
            position.longitude);

        Marker newCurrentPositionMarker = Marker(
          markerId: new MarkerId(SignedAccount.instance.id!),
          position: new LatLng(position.latitude, position.longitude),
          icon: userMarkerIcon!,
          rotation: markerRotation,
          infoWindow: InfoWindow(title: "Current Position"),
        );

        CameraPosition cameraPosition = new CameraPosition(
            target: LatLng(position.latitude, position.longitude), zoom: 17);
        await googleMapController!
            .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

        setState(
          () {
            markersSet.removeWhere((marker) =>
                marker.markerId.value == SignedAccount.instance.id!);
            markersSet.add(newCurrentPositionMarker);
          },
        );

        oldPosition = LatLng(position.latitude, position.longitude);
      },
    );
  }

  Future<void> getOtherDriversLocationUpdate(String key,
      {double? lat, double? long}) async {
    LatLng latLng;
    if (lat != null && long != null) {
      latLng = LatLng(lat, long);
    } else {
      Map<String, dynamic> response = await Geofire.getLocation('$key');
      latLng = LatLng(response['lat'], response['lng']);
    }

    Marker otherDriverMarker;

    print("nearby list: $nearbyDriverMap");

    if (nearbyDriverMap.containsKey(key)) {
      otherDriverMarker = Marker(
          markerId: MarkerId(key),
          position: latLng,
          icon: fosterAcceptedMarkerIcon!);
    } else {
      otherDriverMarker = Marker(
          markerId: MarkerId(key), position: latLng, icon: fosterMarkerIcon!);
    }
    print("KEY LatLng: $latLng");
    setState(() {
      markersSet.removeWhere((marker) => marker.markerId.value == key);
      markersSet.add(otherDriverMarker);
      print("KEY Change Marker");
    });
  }

  void selectLocation(LatLng location) async {
    CameraPosition? cameraPosition;
    setState(
      () {
        Place place = new Place(
            latitude: location.latitude, longitude: location.longitude);
        placeProvider.setNewDestinationPlace(place);

        markersSet.add(
          new Marker(
              markerId: MarkerId(SignedAccount.instance.id!),
              position: location),
        );

        cameraPosition = CameraPosition(target: location);
      },
    );
    if (cameraPosition != null) {
      await googleMapController!.animateCamera(
        CameraUpdate.newCameraPosition(cameraPosition!),
      );
    }
    await drawDirectionPolyline();
  }

  Future<void> startListenOtherDrivers() async {
    await getCurrentPosition();
    await createFosterMaker(this.context);
    await createFosterAcceptedMaker(this.context);

    String pathToListen = "driverAvailable";
    await Geofire.initialize(pathToListen);
    print("START LISTEN");
    try {
      Geofire.queryAtLocation(
              currentPosition!.latitude, currentPosition!.longitude, 999999)!
          .listen((location) async {
        if (location != null) {
          print("LOCATION EVENT: ${location}");

          var callBack = location['callBack'];
          switch (callBack) {
            case Geofire.onKeyEntered:
              print("KEY ENTER: ${location["key"]}");
              if (keysEntered = true) {
                keysRetrieved.removeWhere((key) => key == null);
                keysRetrieved.add(location["key"].toString());
                if (mounted) {
                  getOtherDriversLocationUpdate(location["key"]);
                }
              }
              break;

            case Geofire.onKeyExited:
              print("KEY EXIT: ${location["key"]}");
              keysRetrieved.remove(location["key"]);
              if (mounted) {
                getOtherDriversLocationUpdate(location["key"]);
              }
              break;

            case Geofire.onKeyMoved:
              print(
                  "KEY MOVE: ${location["key"]} --> [ ${location["latitude"]}, ${location["longitude"]} ]");
              if (mounted) {
                getOtherDriversLocationUpdate(location["key"],
                    lat: location["latitude"], long: location["longitude"]);
              }
              break;

            case Geofire.onGeoQueryReady:
              // All initial Data is loaded
              print("KEY READY: ${location["key"]}");
              keysEntered = true;
              if (location['result'] == null ||
                  location['key'] == null ||
                  location['latitude'] == null ||
                  location['longitude'] == null) {
                print('yes it null');
              } else {
                keysRetrieved.add(location["key"].toString());
              }
              if (mounted) {
                setState(() {});
              }
              break;
          }
        }
        if (mounted) {
          setState(() {});
        }
      }).onError((error) {
        print(error);
      });
    } on PlatformException {
      print("Platform Exception");
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  void findDriver() {
    // NearbyDriver nearbyDriver = nearbyDriverMap.get[0];
  }

  void resetMap() {
    setState(() {
      polyLinesSet.clear();
      circlesSet.clear();
      markersSet.clear();
      isSelectedDestination = false;
      direction = null;
      placeProvider.destinationPlace = null;
    });
  }
}
