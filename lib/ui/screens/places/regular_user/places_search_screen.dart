import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/constant/credentials.dart';
import 'package:social_media/providers/place_provider.dart';
import 'package:social_media/services/places/map_service.dart';

class PlacesSearchScreen extends StatefulWidget {
  const PlacesSearchScreen({Key? key}) : super(key: key);

  @override
  _PlacesSearchScreenState createState() => _PlacesSearchScreenState();
}

class _PlacesSearchScreenState extends State<PlacesSearchScreen> {
  TextEditingController pickUpController = new TextEditingController();
  TextEditingController destinationController = new TextEditingController();

  MapService mapService = new MapService();
  FocusNode focusNode = new FocusNode();

  List predictedPlacesList = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    FocusScope.of(context).requestFocus(focusNode);
    super.didChangeDependencies();
  }

  void searchPlace(String placeName) async {
    if (placeName.trim().isNotEmpty) {
      Uri autoCompleteUrl = Uri.parse(
          'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$placeName&key=${Credentials.GOOGLE_CLOUD_API_KEY}&language=vi&components=country:vn');

      http.Response response = await http.get(autoCompleteUrl);

      if (response.statusCode >= 200 && response.statusCode < 300) {
        if (mounted) {
          setState(() {
            predictedPlacesList = mapService.getPredictedPlacesList(response);
          });
        }
      } else {
        return null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    double deviceWidth = MediaQuery.of(context).size.width;

    String pickUpAddress = '';
    if (Provider.of<PlaceProvider>(context, listen: false).pickupPlace !=
        null) {
      pickUpAddress = Provider.of<PlaceProvider>(context, listen: false)
              .pickupPlace!
              .placeName ??
          'nothings';
    }

    pickUpController.text = pickUpAddress;

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top + deviceHeight * 0.02,
        ),
        height: deviceHeight,
        width: deviceWidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: [
                Positioned(
                  left: deviceWidth * 0.03,
                  child: GestureDetector(
                    child: Icon(Icons.arrow_back),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
                Center(
                  child: Text(
                    'Set Destination',
                    style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'bolt',
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            SizedBox(height: deviceHeight * 0.03),
            ListTile(
              leading: Image.asset('assets/images/places/pickicon.png',
                  height: 17, width: 17),
              minLeadingWidth: 0,
              title: TextField(
                controller: pickUpController,
                decoration: InputDecoration(
                  hintText: 'Pickup location',
                  fillColor: AppColors.colorLightGrayFair,
                  filled: true,
                  isDense: true,
                  contentPadding: EdgeInsets.only(left: 10, top: 7, bottom: 7),
                  border: InputBorder.none,
                ),
              ),
            ),
            ListTile(
              leading: Image.asset('assets/images/places/desticon.png',
                  height: 17, width: 17),
              minLeadingWidth: 0,
              title: TextField(
                focusNode: focusNode,
                controller: destinationController,
                decoration: InputDecoration(
                  hintText: 'Go to',
                  fillColor: AppColors.colorLightGrayFair,
                  filled: true,
                  isDense: true,
                  contentPadding: EdgeInsets.only(left: 10, top: 7, bottom: 7),
                  border: InputBorder.none,
                ),
                onChanged: (value) {
                  searchPlace(value);
                },
              ),
            ),
            Container(
              height: deviceHeight * 0.6,
              child: ListView.builder(
                itemCount: predictedPlacesList.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: ListTile(
                        leading: Icon(
                          Icons.place_outlined,
                          size: 17,
                        ),
                        minLeadingWidth: 0,
                        title: Text(predictedPlacesList[index].mainText),
                        subtitle:
                            Text(predictedPlacesList[index].secondaryText),
                      ),
                    ),
                    onTap: () {
                      mapService.getPlaceDetail(
                          predictedPlacesList[index].placeID, context);
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
