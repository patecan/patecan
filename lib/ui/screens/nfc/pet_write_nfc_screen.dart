import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:simple_ripple_animation/simple_ripple_animation.dart';
import 'package:social_media/services/nfc/nfc_service.dart';

class PetWriteNFCScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PetWriteNFCScreenState();
}

class PetWriteNFCScreenState extends State<PetWriteNFCScreen> {
  ValueNotifier<dynamic> result = ValueNotifier(null);

  NfcService nfcService = NfcService();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    NfcManager.instance.stopSession();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(title: Text('Write Pet Tag')),
      body: SafeArea(
        child: FutureBuilder<bool>(
          future: NfcManager.instance.isAvailable(),
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data != true) {
                return Center(child: Text('NFC Unavailable'));
              } else {
                _tagWrite();
                return RippleAnimation(
                    repeat: true,
                    color: Colors.blue,
                    minRadius: 150,
                    ripplesCount: 7,
                    child: Center(
                      child: Container(
                          width: size.width * 0.4,
                          child: Image.asset('assets/images/nfc/pet_tag.png')),
                    ));
              }
            } else {
              return Center(child: Text('NFC Unavailable'));
            }
          },
        ),
      ),
    );
  }

  void _tagWrite() {
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      var ndef = Ndef.from(tag);
      if (ndef == null || !ndef.isWritable) {
        result.value = 'This NFC tag is not writable';
        NfcManager.instance.stopSession(errorMessage: result.value);
        return;
      }

      NdefMessage message = NdefMessage([
        NdefRecord.createExternal('com.example', 'pet_id',
            Uint8List.fromList('4xoqCRgj7zUsPz86bku9'.codeUnits)),
      ]);
      showDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
                title: new Text("Writing..."),
                content: new Text("Please take a moment while we writing..."),
              ));
      try {
        await ndef.write(message);
        result.value = 'Write NFC Success';
        NfcManager.instance.stopSession();
        Navigator.of(context).pop();
      } catch (e) {
        result.value = e;
        NfcManager.instance.stopSession(errorMessage: result.value.toString());
        Navigator.of(context).pop();
      }
    });
  }
}
