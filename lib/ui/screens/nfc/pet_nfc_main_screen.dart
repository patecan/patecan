import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:simple_ripple_animation/simple_ripple_animation.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/services/nfc/nfc_service.dart';
import 'package:social_media/ui/screens/nfc/pet_write_nfc_screen.dart';
import 'package:social_media/ui/screens/pets/pet_profile_screen.dart';

class PetNFCScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PetNFCScreenState();
}

class PetNFCScreenState extends State<PetNFCScreen> {
  ValueNotifier<dynamic> result = ValueNotifier(null);

  NfcService nfcService = NfcService();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    NfcManager.instance.stopSession();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Find Pet by Tag'),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => PetWriteNFCScreen()));
            },
            icon: Icon(
              Icons.edit,
              color: Colors.black,
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          color: AppColors.veryLightBlue,
          child: FutureBuilder<bool>(
            future: NfcManager.instance.isAvailable(),
            builder: (context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data != true) {
                  return Center(child: Text('NFC Unavailable'));
                } else {
                  _tagRead();
                  return RippleAnimation(
                    repeat: true,
                    color: AppColors.pink2Background,
                    minRadius: 150,
                    ripplesCount: 7,
                    child: Center(
                      child: Container(
                          width: size.width * 0.4,
                          child: Image.asset('assets/images/nfc/pet_tag.png')),
                    ),
                  );
                }
              } else {
                return Center(child: Text('NFC Unavailable'));
              }
            },
          ),
        ),
      ),
    );
  }

  void _tagRead() {
    NfcManager.instance.startSession(
      onDiscovered: (NfcTag tag) async {
        String recordContent = "";
        int recordType = -1;

        tag.data['ndef']['cachedMessage']['records'].forEach(
          (record) async {
            if (record != null) {
              Map<String, dynamic> recordsMap =
                  Map<String, dynamic>.from(record);

              print("RECORD MAP $recordsMap");

              String payload = new String.fromCharCodes(
                  Uint8List.fromList(recordsMap['payload']));
              print(payload);
              recordType = recordsMap['typeNameFormat'];

              if (recordType == 4) {
                recordContent += "${record.toString()}\n\n";
                DocumentSnapshot? foundPet =
                    await nfcService.searchPetByID(payload);

                if (foundPet != null) {
                  Pet pet = Pet.fromDocumentSnapshot(foundPet);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => PetProfileScreen(
                          ownerId: pet.ownerId!, petId: pet.id!),
                    ),
                  );
                } else {}
              }
            }
          },
        );
      },
    );
  }

  void _tagWrite() {
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      var ndef = Ndef.from(tag);
      if (ndef == null || !ndef.isWritable) {
        result.value = 'This NFC tag is not writable';
        NfcManager.instance.stopSession(errorMessage: result.value);
        return;
      }

      NdefMessage message = NdefMessage([
        NdefRecord.createExternal('com.example', 'pet_id',
            Uint8List.fromList('4xoqCRgj7zUsPz86bku9'.codeUnits)),
      ]);
      showDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
                title: new Text("Writing..."),
                content: new Text("Please take a moment while we writing..."),
              ));
      try {
        await ndef.write(message);
        result.value = 'Write NFC Success';
        NfcManager.instance.stopSession();
      } catch (e) {
        result.value = e;
        NfcManager.instance.stopSession(errorMessage: result.value.toString());
        return;
      }
    });
  }
}
