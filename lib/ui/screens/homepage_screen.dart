import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/authentication/login_screen.dart';
import 'package:social_media/ui/screens/menu_page.dart';
import 'package:social_media/ui/screens/pet_dictionary/scan_main_screen.dart';
import 'package:social_media/ui/screens/social/notification_screen.dart';
import 'package:social_media/ui/screens/social/timeline_screen.dart';
import 'package:social_media/ui/screens/social/upload_screen.dart';
import 'package:social_media/ui/screens/users/user_profile_screen.dart';

import '../../constant/app_colors.dart';

class HomepageScreen extends StatefulWidget {
  static ValueNotifier<bool> isLoggedIn = ValueNotifier<bool>(false);

  @override
  _HomepageScreenState createState() => _HomepageScreenState();
}

class _HomepageScreenState extends State<HomepageScreen> {
  int pageIndex = 0;
  late PageController pageController = new PageController(
      initialPage: pageIndex, keepPage: true, viewportFraction: 1);

  var _homepageScaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

  @override
  void initState() {
    final CurvedNavigationBarState? navBarState =
        _bottomNavigationKey.currentState;
    navBarState?.setPage(1);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void changePage(int index) {
    pageIndex = index;
    pageController.animateToPage(
      index,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeInCubic,
    );
  }

  MenuItem currentItem = MenuItems.timeline;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: HomepageScreen.isLoggedIn,
      builder: (context, valueListenable, child) {
        return HomepageScreen.isLoggedIn.value != true
            ? LoginScreen()
            : Scaffold(
                key: _homepageScaffoldKey,
                body: ZoomDrawer(
                    style: DrawerStyle.Style1,
                    borderRadius: 40,
                    angle: -10,
                    slideWidth: 300,
                    showShadow: true,
                    backgroundColor: Colors.orangeAccent.shade100,
                    menuScreen: Builder(builder: (context) {
                      return MenuPage(
                          currentItem: currentItem,
                          onSelectedItem: (item) {
                            setState(() => currentItem = item);
                            ZoomDrawer.of(context)!.close();
                          });
                    }),
                    mainScreen: PageView(
                      scrollDirection: Axis.horizontal,
                      controller: pageController,
                      children: <Widget>[
                        TimelineScreen(),
                        NotificationScreen(),
                        UploadScreen(),
                        UserProfileScreen(SignedAccount.instance.id!),
                        PetDictionaryScreen(),
                      ],
                    )),
                bottomNavigationBar: Container(
                  height: 53,
                  child: CurvedNavigationBar(
                    key: _bottomNavigationKey,
                    color: AppColors.bottomNavbarBackground,
                    buttonBackgroundColor: AppColors.whiteScreenBackground,
                    backgroundColor: AppColors.whiteScreenBackground,
                    height: 50,
                    onTap: changePage,
                    items: <Widget>[
                      Icon(Icons.whatshot, color: Color(0xFF7E85F9)),
                      Icon(Icons.notifications_active,
                          color: Color(0xFF7E85F9)),
                      Icon(Icons.photo_camera, color: Color(0xFF7E85F9)),
                      Icon(Icons.account_circle, color: Color(0xFF7E85F9)),
                      Icon(Icons.book, color: Color(0xFF7E85F9)),
                    ],
                  ),
                ),
              );
      },
      child: null,
    );
  }
}
