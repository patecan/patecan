import 'package:flutter/material.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/pet_adopt/pet_adopt.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/pet_adopt/proof_service.dart';
import 'package:social_media/ui/screens/pet_adopt/proof_message_detail_screen.dart';

class PetAdoptDetailScreen extends StatefulWidget {
  PetAdopt petAdopt;

  PetAdoptDetailScreen(this.petAdopt, {Key? key}) : super(key: key);

  @override
  State<PetAdoptDetailScreen> createState() => _PetAdoptDetailScreenState();
}

class _PetAdoptDetailScreenState extends State<PetAdoptDetailScreen> {
  ProofService proofYouAreDeservedService = ProofService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(),
            ElevatedButton(
                onPressed: () async {
                  Message message = await proofYouAreDeservedService
                      .getMessageIfNewConversation(
                    you: MyUser.fromSignedInAccount(SignedAccount.instance),
                    peerId: widget.petAdopt.owner['_id'],
                  );

                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ProofDetailScreen(
                          MyUser.fromSignedInAccount(SignedAccount.instance),
                          widget.petAdopt.owner['_id'],
                          message),
                    ),
                  );
                },
                child: Text('Adopt Me')),
          ],
        ),
      ),
    );
  }
}
