import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/pet_adopt/pet_adopt.dart';
import 'package:social_media/services/pet_adopt/pet_adopt_service.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/upload_pet_adopt_screen.dart';
import 'package:social_media/ui/widgets/pet_adopt/pet_adopt_item.dart';

import 'owner/pet_adopt_management_screen.dart';

class PetAdoptMainScreen extends StatefulWidget {
  const PetAdoptMainScreen({Key? key}) : super(key: key);

  @override
  State<PetAdoptMainScreen> createState() => _PetAdoptMainScreenState();
}

class _PetAdoptMainScreenState extends State<PetAdoptMainScreen> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  PetAdoptService _petAdoptService = PetAdoptService();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    AppBar appBar = AppBar(
      leading: Center(
        child: IconButton(
          icon: FaIcon(FontAwesomeIcons.angleLeft, color: Colors.white),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[Color(0xffFE97B5), Color(0xffFAC0D1)],
          ),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurStyle: BlurStyle.outer,
              blurRadius: 5,
              offset: Offset(0, 0),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.transparent,
      toolbarHeight: 60,
      title: GradientText(
        'Pet Adopt',
        style: TextStyle(
          fontSize: 17.5,
          fontWeight: FontWeight.bold,
        ),
        colors: [
          Colors.white,
          Colors.white,
        ],
      ),
      centerTitle: false,
      actions: [],
    );

    return SafeArea(
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        controller: _refreshController,
        onRefresh: () async {
          await Future.delayed(Duration(milliseconds: 500));
          _refreshController.refreshCompleted();
        },
        child: Scaffold(
          appBar: appBar,
          body: Container(
            color: AppColors.lightGreyBackground,
            padding: EdgeInsets.all(5),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 60,
                            width: 60,
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurStyle: BlurStyle.outer,
                                  blurRadius: 5,
                                  offset: Offset(0, 0),
                                ),
                              ],
                            ),
                            child:
                                Image.asset('assets/icons/pet_adopt/cat.jpg'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 60,
                            width: 60,
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurStyle: BlurStyle.outer,
                                  blurRadius: 5,
                                  offset: Offset(0, 0),
                                ),
                              ],
                            ),
                            child:
                                Image.asset('assets/icons/pet_adopt/dog.jpg'),
                          ),
                        ],
                      ),
                      Container(
                        width: size.width,
                        height: size.height * 0.75,
                        color: Colors.transparent,
                        child: FutureBuilder(
                          future: _petAdoptService.getAllPetAdopts(),
                          builder: (BuildContext context,
                              AsyncSnapshot<dynamic> snapshot) {
                            if (snapshot.hasData) {
                              Response response = snapshot.data;

                              print("RESPONSE $response");

                              List<PetAdoptItem> petAdoptItemList =
                                  List<PetAdoptItem>.from(response.data
                                      .map((document) => PetAdoptItem(
                                          PetAdopt.fromDocument(document)))
                                      .toList());

                              return Container(
                                child: ListView(
                                  children: petAdoptItemList,
                                ),
                              );
                            } else {
                              return Container();
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => UploadPetAdoptScreen(),
                        ),
                      );
                    },
                    child: Container(
                      width: size.width * 0.5,
                      height: size.width * 0.25,
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          topLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurStyle: BlurStyle.outer,
                            blurRadius: 5,
                            offset: Offset(0.5, 0.5),
                          ),
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50)),
                            ),
                            child: Row(
                              children: [
                                FaIcon(FontAwesomeIcons.cat,
                                    size: 25, color: AppColors.blueHeartButton),
                                FaIcon(FontAwesomeIcons.plus,
                                    size: 12, color: AppColors.blueHeartButton),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) =>
                                        PetAdoptManagementScreen())),
                            child: Container(
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50))),
                              child: Column(
                                children: [
                                  FaIcon(FontAwesomeIcons.cat,
                                      size: 25,
                                      color: AppColors.blueHeartButton),
                                  FaIcon(FontAwesomeIcons.boxOpen,
                                      size: 25, color: Colors.brown),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
