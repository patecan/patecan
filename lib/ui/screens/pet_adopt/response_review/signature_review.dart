import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import '../../../../models/messages/message.dart';
import '../../../widgets/gradient_button.dart';

class SignatureResponseReview extends StatefulWidget {
  Message message;
  String requestId;
  Map<String, dynamic> proofResponse;

  SignatureResponseReview(
      {required this.proofResponse,
      required this.requestId,
      required this.message,
      Key? key})
      : super(key: key);

  @override
  State<SignatureResponseReview> createState() =>
      _SignatureResponseReviewState();
}

class _SignatureResponseReviewState extends State<SignatureResponseReview> {
  Position? position;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    print(widget.proofResponse['uri']);
    return Container(
      child: Column(
        children: [
          Expanded(
            child: Center(
                child: Container(
              child: Image.network(widget.proofResponse['uri']),
            )),
          ),
          Row(
            children: [
              Container(
                height: 50,
                width: size.width / 2,
                padding: EdgeInsets.only(left: 10, right: 10),
                margin: EdgeInsets.only(bottom: 15),
                child: GradientButton(
                  color1: Color(0xffFFADAD),
                  color2: Color(0xffFFADAD),
                  child: Text('REJECT'),
                  onPress: () async {
                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .collection('proof_response')
                        .doc(widget.requestId)
                        .delete();
                  },
                ),
              ),
              Container(
                height: 50,
                width: size.width / 2,
                padding: EdgeInsets.only(left: 10, right: 10),
                margin: EdgeInsets.only(bottom: 15),
                child: GradientButton(
                  color1: Color(0xff8D9F5E),
                  color2: Color(0xff8D9F5E),
                  child: Text(
                    'ACCEPT',
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  onPress: () async {
                    print(widget.proofResponse['requestPoint']);

                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .update({
                      'requestPoint':
                          FieldValue.increment(widget.message.requestPoint!)
                    });

                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .collection('proof_response')
                        .doc(widget.requestId)
                        .update({'accepted': true});

                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
