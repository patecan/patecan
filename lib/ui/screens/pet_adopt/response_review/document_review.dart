import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_icon/file_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../models/messages/message.dart';
import '../../../widgets/gradient_button.dart';

class DocumentResponseReview extends StatefulWidget {
  Message message;
  String requestId;
  Map<String, dynamic> proofResponse;

  DocumentResponseReview(
      {required this.proofResponse,
      required this.requestId,
      required this.message,
      Key? key})
      : super(key: key);

  @override
  State<DocumentResponseReview> createState() => _DocumentResponseReviewState();
}

class _DocumentResponseReviewState extends State<DocumentResponseReview> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: Column(
        children: [
          Expanded(
            child: Center(
              child: Container(
                height: 100,
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7.5),
                  border: Border.all(width: 0, color: Colors.white),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                      leading: FileIcon(
                        '${widget.proofResponse['filename']}',
                        size: 50,
                      ),
                      title: Text('${widget.proofResponse['filename']}'),
                    ),
                    Container(
                      height: 30,
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: GradientButton(
                        color1: Color(0xff7E85F9),
                        color2: Color(0xff7E85F9),
                        child: Text(
                          'OPEN',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        onPress: () async {
                          //OpenFile.open(file.path);

                          if (!await launch(widget.proofResponse['uri']))
                            throw 'Could not launch ${widget.proofResponse['uri']}';
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Row(
            children: [
              Container(
                height: 50,
                width: size.width / 2,
                padding: EdgeInsets.only(left: 10, right: 10),
                margin: EdgeInsets.only(bottom: 15),
                child: GradientButton(
                  color1: Color(0xffFFADAD),
                  color2: Color(0xffFFADAD),
                  child: Text('REJECT'),
                  onPress: () async {
                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .collection('proof_response')
                        .doc(widget.requestId)
                        .delete();
                  },
                ),
              ),
              Container(
                height: 50,
                width: size.width / 2,
                padding: EdgeInsets.only(left: 10, right: 10),
                margin: EdgeInsets.only(bottom: 15),
                child: GradientButton(
                  color1: Color(0xff8D9F5E),
                  color2: Color(0xff8D9F5E),
                  child: Text(
                    'ACCEPT',
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  onPress: () async {
                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .update({
                      'requestPoint':
                          FieldValue.increment(widget.message.requestPoint!)
                    });

                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .collection('proof_response')
                        .doc(widget.requestId)
                        .update({'accepted': true});

                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<File?> downloadFile(String uri) async {
    final http.Response downloadData = await http.get(Uri.parse(uri));

    final directory =
        (await getExternalStorageDirectories(type: StorageDirectory.documents))
            ?.first;

    if (downloadData.statusCode == 200) {
      print(directory);
      print(downloadData.bodyBytes);
      await File(directory!.path + '${widget.proofResponse['filename']}')
          .writeAsBytes(downloadData.bodyBytes.buffer.asUint8List(
              downloadData.bodyBytes.offsetInBytes,
              downloadData.bodyBytes.lengthInBytes));

      File file = File(directory.path + '${widget.proofResponse['filename']}');
      return file;
    }
  }
}
