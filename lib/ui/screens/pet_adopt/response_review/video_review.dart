import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:video_player/video_player.dart';

import '../../../widgets/gradient_button.dart';

class VideoResponseReview extends StatefulWidget {
  Message message;
  String requestId;
  Map<String, dynamic> proofResponse;
  VideoPlayerController? _controller;

  VideoResponseReview(
      {required this.proofResponse,
      required this.requestId,
      required this.message,
      Key? key})
      : super(key: key);

  @override
  State<VideoResponseReview> createState() => _VideoResponseReviewState();
}

class _VideoResponseReviewState extends State<VideoResponseReview> {
  VideoPlayerController? _videoPlayerController;

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: FutureBuilder(
        future: initialize(widget.proofResponse['uri']),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: AspectRatio(
                      aspectRatio: _videoPlayerController!.value.aspectRatio,
                      child: VideoPlayer(_videoPlayerController!),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      height: 50,
                      width: size.width / 2,
                      padding: EdgeInsets.only(left: 10, right: 10),
                      margin: EdgeInsets.only(bottom: 15),
                      child: GradientButton(
                        color1: Color(0xffFFADAD),
                        color2: Color(0xffFFADAD),
                        child: Text('REJECT'),
                        onPress: () async {
                          await FirebaseFirestore.instance
                              .collection('proof_you_are_deserved')
                              .doc(widget.message.conversationID)
                              .collection('proof_response')
                              .doc(widget.requestId)
                              .delete();
                        },
                      ),
                    ),
                    Container(
                      height: 50,
                      width: size.width / 2,
                      padding: EdgeInsets.only(left: 10, right: 10),
                      margin: EdgeInsets.only(bottom: 15),
                      child: GradientButton(
                        color1: Color(0xff8D9F5E),
                        color2: Color(0xff8D9F5E),
                        child: Text(
                          'ACCEPT',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        onPress: () async {
                          print(widget.proofResponse['requestPoint']);

                          await FirebaseFirestore.instance
                              .collection('proof_you_are_deserved')
                              .doc(widget.message.conversationID)
                              .update({
                            'requestPoint': FieldValue.increment(
                                widget.message.requestPoint!)
                          });

                          await FirebaseFirestore.instance
                              .collection('proof_you_are_deserved')
                              .doc(widget.message.conversationID)
                              .collection('proof_response')
                              .doc(widget.requestId)
                              .update({'accepted': true});

                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ],
                ),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }

  Future<void> _disposeVideoController() async {
    if (_videoPlayerController != null) {
      await _videoPlayerController!.dispose();
    }
    _videoPlayerController = null;
  }

  Future<bool> initialize(String videoUrl) async {
    await _disposeVideoController();

    _videoPlayerController = VideoPlayerController.network(videoUrl);
    await _videoPlayerController!.initialize();
    await _videoPlayerController!.setLooping(true);
    await _videoPlayerController!.play();

    return true;
  }
}
