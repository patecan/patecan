import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/create_proof_request_form_screen.dart';
import 'package:social_media/ui/screens/pet_adopt/response_review/document_review.dart';
import 'package:social_media/ui/screens/pet_adopt/response_review/location_review.dart';
import 'package:social_media/ui/screens/pet_adopt/response_review/signature_review.dart';
import 'package:social_media/ui/screens/pet_adopt/response_review/video_review.dart';

class ProofResponseReview extends StatefulWidget {
  Map<String, dynamic> proofResponse;
  Message message;
  RequestAttachment requestAttachment;
  String requestId;

  ProofResponseReview(
      {required this.proofResponse,
      required this.requestId,
      required this.message,
      required this.requestAttachment,
      Key? key})
      : super(key: key);

  @override
  State<ProofResponseReview> createState() => _ProofResponseReviewState();
}

class _ProofResponseReviewState extends State<ProofResponseReview> {
  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      leading: Center(
        child: IconButton(
          icon: FaIcon(FontAwesomeIcons.angleLeft,
              color: AppColors.whiteScreenBackground),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      backgroundColor: Color(0xffFEC868),
      toolbarHeight: 60,
      title: GradientText(
        'Review Response',
        style: TextStyle(
          fontSize: 17.5,
          fontWeight: FontWeight.bold,
        ),
        colors: [
          AppColors.whiteScreenBackground,
          AppColors.whiteScreenBackground,
        ],
      ),
      centerTitle: false,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
        ),
      ),
      actions: [],
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        color: Color(0xffFEC868),
        child: Column(
          children: [
            Expanded(child: createCorrespondResponse()),
          ],
        ),
      ),
    );
  }

  Widget createCorrespondResponse() {
    switch (widget.requestAttachment) {
      case RequestAttachment.location:
        return LocationResponseReview(
          requestId: widget.requestId,
          message: widget.message,
          proofResponse: widget.proofResponse,
        );
      case RequestAttachment.film:
        return VideoResponseReview(
          requestId: widget.requestId,
          message: widget.message,
          proofResponse: widget.proofResponse,
        );
      case RequestAttachment.sign:
        return SignatureResponseReview(
          requestId: widget.requestId,
          message: widget.message,
          proofResponse: widget.proofResponse,
        );
      case RequestAttachment.file:
        return DocumentResponseReview(
          requestId: widget.requestId,
          message: widget.message,
          proofResponse: widget.proofResponse,
        );
      default:
        return Container();
    }
  }
}
