import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:uuid/uuid.dart';

import '../../../../constant/app_colors.dart';
import '../../../../models/messages/message.dart';
import '../../../widgets/gradient_button.dart';
import '../../../widgets/social/progress.dart';

class LocationResponse extends StatefulWidget {
  Message message;
  String requestId;

  LocationResponse({required this.requestId, required this.message, Key? key})
      : super(key: key);

  @override
  State<LocationResponse> createState() => _LocationResponseState();
}

class _LocationResponseState extends State<LocationResponse> {
  Position? position;

  @override
  void initState() {
    getPermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: FutureBuilder(
              future: Geolocator.getCurrentPosition(
                  desiredAccuracy: LocationAccuracy.high),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  position = snapshot.data;
                  return Center(
                    child: Column(children: [
                      Text(
                        position!.latitude.toString(),
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      Text(
                        position!.longitude.toString(),
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ]),
                  );
                } else {
                  return CircularProgress();
                }
              },
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            margin: EdgeInsets.only(bottom: 15),
            child: GradientButton(
              color1: AppColors.pink2Background,
              color2: AppColors.pink2Background,
              child: Text('Submit'),
              onPress: () async {
                Map<String, dynamic> _proofResponse = {
                  '_id': Uuid().v4(),
                  'latitude': position!.latitude,
                  'longitude': position!.longitude,
                  'accepted': false,
                };

                await FirebaseFirestore.instance
                    .collection('proof_you_are_deserved')
                    .doc(widget.message.conversationID)
                    .collection('proof_response')
                    .doc(widget.requestId)
                    .set(_proofResponse);
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> getPermission() async {
    if (await Permission.locationWhenInUse.serviceStatus.isDisabled) {
      await Permission.location.request();
      setState(() {});
    }
  }
}
