import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/services/pet_adopt/proof_service.dart';
import 'package:social_media/ui/screens/pet_adopt/create_response/create_signature_response.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/create_proof_request_form_screen.dart';
import 'package:social_media/ui/widgets/gradient_button.dart';
import 'package:uuid/uuid.dart';
import 'package:video_player/video_player.dart';

import '../../../../models/messages/message.dart';
import 'create_document_response.dart';
import 'create_location_response.dart';

class ProofResponse extends StatefulWidget {
  Message message;
  RequestAttachment requestAttachment;
  String requestId;

  ProofResponse(
      {required this.requestId,
      required this.message,
      required this.requestAttachment,
      Key? key})
      : super(key: key);

  @override
  State<ProofResponse> createState() => _ProofResponseState();
}

class _ProofResponseState extends State<ProofResponse> {
  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      leading: Center(
        child: IconButton(
          icon: FaIcon(FontAwesomeIcons.angleLeft,
              color: AppColors.whiteScreenBackground),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      backgroundColor: AppColors.veryLightBlue,
      toolbarHeight: 60,
      title: GradientText(
        'Your Response',
        style: TextStyle(
          fontSize: 17.5,
          fontWeight: FontWeight.bold,
        ),
        colors: [
          AppColors.whiteScreenBackground,
          AppColors.whiteScreenBackground,
        ],
      ),
      centerTitle: false,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
        ),
      ),
      actions: [],
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        color: AppColors.veryLightBlue,
        child: Column(
          children: [
            Expanded(child: createCorrespondResponse()),
          ],
        ),
      ),
    );
  }

  Widget createCorrespondResponse() {
    switch (widget.requestAttachment) {
      case RequestAttachment.location:
        return LocationResponse(
            requestId: widget.requestId, message: widget.message);
      case RequestAttachment.film:
        return VideoResponse(
            requestId: widget.requestId, message: widget.message);
      case RequestAttachment.sign:
        return CreateSignatureResponse(
            requestId: widget.requestId, message: widget.message);
      case RequestAttachment.file:
        return CreateDocumentResponse(
            requestId: widget.requestId, message: widget.message);
      default:
        return Container();
    }
  }
}

class VideoResponse extends StatefulWidget {
  Message message;
  String requestId;

  VideoResponse({required this.requestId, required this.message, Key? key})
      : super(key: key);

  @override
  State<VideoResponse> createState() => _VideoResponseState();
}

class _VideoResponseState extends State<VideoResponse> {
  ProofService proofService = new ProofService();
  String? source;
  DateTime? takenTime;
  bool isCaptured = false;
  XFile? videoFile;
  final _picker = ImagePicker();
  VideoPlayerController? _videoPlayerController;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () async {
            videoFile = await _picker.pickVideo(
                source: ImageSource.camera,
                maxDuration: const Duration(seconds: 20));

            if (videoFile != null) {
              setState(() {
                isCaptured = true;
                source = 'camera';
                takenTime = DateTime.now();
              });
            }
            _playVideo(videoFile);
          },
          icon: Icon(Icons.video_call_rounded, color: Colors.green, size: 50),
        ),
        Text(
          "Capture Video",
          style: TextStyle(color: Colors.black),
        ),
        Container(
          height: size.height * 0.7,
          width: size.width,
          child: _previewVideo(),
        ),
        (isCaptured)
            ? Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                margin: EdgeInsets.only(bottom: 15),
                child: GradientButton(
                  color1: AppColors.pink2Background,
                  color2: AppColors.pink2Background,
                  child: Text('Submit'),
                  onPress: () async {
                    String? uri = await proofService.uploadVideo(
                        widget.requestId, File(videoFile!.path));

                    Map<String, dynamic> _proofResponse = {
                      '_id': Uuid().v4(),
                      'uri': uri,
                      'source': source,
                      'takeTime': takenTime,
                      'accepted': false,
                    };

                    await FirebaseFirestore.instance
                        .collection('proof_you_are_deserved')
                        .doc(widget.message.conversationID)
                        .collection('proof_response')
                        .doc(widget.requestId)
                        .set(_proofResponse);
                  },
                ),
              )
            : SizedBox.shrink(),
      ],
    );
  }

  /* ------------------------------------------------ SELECT VIDEO FROM FILE --------------------------------------- */
  Future<dynamic> pickVideo() {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("Create Post"),
          children: [
            SimpleDialogOption(
              child: Text("Open Camera"),
              onPressed: () async {
                await handleCameraVideo();
                Navigator.pop(context);
              },
            ),
            SimpleDialogOption(
              child: Text("Open Gallery"),
              onPressed: () async {
                await handleGalleryVideo();
                Navigator.pop(context);
              },
            ),
            SimpleDialogOption(
              child: Text("Cancel"),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  Future<void> handleCameraVideo() async {
    videoFile = await _picker.pickVideo(
        source: ImageSource.camera, maxDuration: const Duration(seconds: 20));
    setState(() {
      isCaptured = true;
    });
    _playVideo(videoFile);
  }

  Future<void> handleGalleryVideo() async {
    videoFile = await _picker.pickVideo(
        source: ImageSource.camera, maxDuration: const Duration(seconds: 20));
    setState(() {
      isCaptured = true;
    });
    _playVideo(videoFile);
  }

  Widget _previewVideo() {
    if (_videoPlayerController == null) {
      return const Text(
        'You have not yet picked a video',
        textAlign: TextAlign.center,
      );
    }
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: AspectRatioVideo(_videoPlayerController),
    );
  }

  Future<void> _disposeVideoController() async {
    if (_videoPlayerController != null) {
      await _videoPlayerController!.dispose();
    }
    _videoPlayerController = null;
  }

  Future<void> _playVideo(XFile? file) async {
    if (file != null && mounted) {
      print("Loading Video");
      await _disposeVideoController();
      late VideoPlayerController controller;

      controller = VideoPlayerController.file(File(file.path));

      _videoPlayerController = controller;
      await controller.initialize();
      await controller.setLooping(true);
      await controller.play();
      setState(() {});
    } else {
      print("Loading Video error");
    }
  }
}

class AspectRatioVideo extends StatefulWidget {
  AspectRatioVideo(this.controller);

  final VideoPlayerController? controller;

  @override
  AspectRatioVideoState createState() => AspectRatioVideoState();
}

class AspectRatioVideoState extends State<AspectRatioVideo> {
  VideoPlayerController? get controller => widget.controller;
  bool initialized = false;

  void _onVideoControllerUpdate() {
    if (!mounted) {
      return;
    }
    if (initialized != controller!.value.isInitialized) {
      initialized = controller!.value.isInitialized;
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    controller!.addListener(_onVideoControllerUpdate);
  }

  @override
  void dispose() {
    controller!.removeListener(_onVideoControllerUpdate);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (initialized) {
      return Center(
        child: AspectRatio(
          aspectRatio: controller!.value.aspectRatio,
          child: VideoPlayer(controller!),
        ),
      );
    } else {
      return Container();
    }
  }
}
