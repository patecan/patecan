import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:uuid/uuid.dart';

import '../../../../constant/app_colors.dart';
import '../../../../models/messages/message.dart';
import '../../../widgets/gradient_button.dart';

class CreateDocumentResponse extends StatefulWidget {
  Message message;
  String requestId;

  CreateDocumentResponse(
      {required this.requestId, required this.message, Key? key})
      : super(key: key);

  @override
  State<CreateDocumentResponse> createState() => _CreateDocumentResponseState();
}

class _CreateDocumentResponseState extends State<CreateDocumentResponse> {
  File? file;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final fileName = file != null ? basename(file!.path) : 'No File Selected';

    return Container(
      child: Column(
        children: [
          Text(
            fileName,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            margin: EdgeInsets.only(bottom: 15),
            child: GradientButton(
              color1: AppColors.pink2Background,
              color2: AppColors.pink2Background,
              child: Text('Select File'),
              onPress: selectFile,
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            margin: EdgeInsets.only(bottom: 15),
            child: GradientButton(
              color1: AppColors.pink2Background,
              color2: AppColors.pink2Background,
              child: Text('Submit'),
              onPress: () async {
                String uri = await uploadFile(file);

                final fileName = basename(file!.path);
                final fileExtension = extension(file!.path);

                Map<String, dynamic> _proofResponse = {
                  '_id': Uuid().v4(),
                  'uri': uri,
                  'filename': fileName,
                  'extension': fileExtension,
                  'accepted': false,
                };

                await FirebaseFirestore.instance
                    .collection('proof_you_are_deserved')
                    .doc(widget.message.conversationID)
                    .collection('proof_response')
                    .doc(widget.requestId)
                    .set(_proofResponse);
              },
            ),
          ),
        ],
      ),
    );
  }

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(allowMultiple: false);

    if (result == null) return;
    final path = result.files.single.path!;

    setState(() => file = File(path));
  }

  Future? uploadFile(File? file) async {
    if (file == null) return;

    String? downloadUrl;

    final fileName = basename(file.path);
    final destination = 'files/$fileName';

    final ref = FirebaseStorage.instance.ref(destination);

    await ref.putFile(file);

    downloadUrl = await ref.getDownloadURL();
    return downloadUrl;
  }
}
