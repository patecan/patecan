import 'dart:io';
import 'dart:ui' as ui;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:social_media/services/pet_adopt/proof_service.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'package:uuid/uuid.dart';

import '../../../../constant/app_colors.dart';
import '../../../../models/messages/message.dart';

class CreateSignatureResponse extends StatefulWidget {
  Message message;
  String requestId;

  CreateSignatureResponse(
      {required this.requestId, required this.message, Key? key})
      : super(key: key);

  @override
  State<CreateSignatureResponse> createState() =>
      _CreateSignatureResponseState();
}

class _CreateSignatureResponseState extends State<CreateSignatureResponse> {
  GlobalKey<SfSignaturePadState> _signaturePadKey = GlobalKey();

  ProofService _proofService = new ProofService();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        child: Column(
          children: [
            Expanded(
              child: SfSignaturePad(
                key: _signaturePadKey,
                minimumStrokeWidth: 1,
                maximumStrokeWidth: 3,
                strokeColor: Colors.blue,
                backgroundColor: Colors.white,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    width: 130,
                    height: 40,
                    margin:
                        EdgeInsets.only(bottom: 15, top: 10, left: 5, right: 5),
                    child: ElevatedButton(
                      onPressed: () async {
                        ui.Image image =
                            await _signaturePadKey.currentState!.toImage();

                        var pngBytes = await image.toByteData(
                            format: ui.ImageByteFormat.png);

                        Directory tempDir = await getTemporaryDirectory();
                        String tempPath = tempDir.path;
                        File('$tempPath/signature.png')
                            .writeAsBytesSync(pngBytes!.buffer.asInt8List());

                        String? url = await _proofService.uploadSignature(
                            widget.requestId, File('$tempPath/signature.png'));

                        Map<String, dynamic> _proofResponse = {
                          '_id': Uuid().v4(),
                          'uri': url,
                          'accepted': false,
                        };

                        await FirebaseFirestore.instance
                            .collection('proof_you_are_deserved')
                            .doc(widget.message.conversationID)
                            .collection('proof_response')
                            .doc(widget.requestId)
                            .set(_proofResponse);
                      },
                      style: ElevatedButton.styleFrom(
                        primary: AppColors.pink2Background,
                        side: BorderSide(
                            width: 2,
                            color: AppColors.pink2Background.withOpacity(0.7)),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Submit',
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                                fontFamily: 'Nunito',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    width: 130,
                    height: 40,
                    margin:
                        EdgeInsets.only(bottom: 15, top: 10, left: 5, right: 5),
                    child: ElevatedButton(
                      onPressed: () async {
                        _signaturePadKey.currentState!.clear();
                      },
                      style: ElevatedButton.styleFrom(
                        primary: AppColors.pink2Background,
                        side: BorderSide(
                            width: 2,
                            color: AppColors.pink2Background.withOpacity(0.7)),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Discard',
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                                fontFamily: 'Nunito',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        height: size.height,
        width: size.width,
      ),
    );
  }
}
