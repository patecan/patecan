import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:group_button/group_button.dart';
import 'package:image/image.dart' as imagePackage;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:social_media/models/pet_adopt/pet_adopt.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/pet_adopt/pet_adopt_service.dart';
import 'package:uuid/uuid.dart';

import '../../../../../models/store/my_address.dart';
import '../../../widgets/store/ContainerPatternPainter.dart';
import '../../address_book/address_book_screen.dart';

class UploadPetAdoptScreen extends StatefulWidget {
  PetAdopt? petAdopt;

  UploadPetAdoptScreen({this.petAdopt});

  @override
  State<StatefulWidget> createState() {
    return _UploadPetAdoptScreenState();
  }
}

class _UploadPetAdoptScreenState extends State<UploadPetAdoptScreen> {
  late String uuid;
  String? imageUrl;
  ImagePicker _imagePicker = ImagePicker();
  PetAdoptService _petAdoptService = PetAdoptService();

  initState() {
    super.initState();
  }

  @override
  didChangeDependencies() {
    if (widget.petAdopt != null) {
      uuid = widget.petAdopt!.id;
      setUpExistedPetAdopt();
    } else {
      uuid = Uuid().v4();
      widget.petAdopt = PetAdopt.empty(id: uuid);
    }
    super.didChangeDependencies();
  }

  setLocation(String location) {
    setState(() {
      this._locationController.text = location;
    });
  }

  /* __________________________ Form Value _____________________ */
  GlobalKey<FormState> _uploadPetAdoptFormKey = GlobalKey<FormState>();

  File? imageFile;

  Species? _species;
  TextEditingController _descController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _ageController = TextEditingController();
  TextEditingController _conditionController = TextEditingController();
  TextEditingController _locationController = TextEditingController();

  /* __________________________ Field Attributes _____________________ */
  bool isLoading = false;

  /* __________________________ Firebase Attributes _____________________ */

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Add Pet'),
        backgroundColor: Colors.orange,
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: setPetAdopt,
          ),
        ],
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Container(
                child: Form(
                  key: _uploadPetAdoptFormKey,
                  onChanged: () {
                    _uploadPetAdoptFormKey.currentState!.save();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        child: Container(
                          height: deviceHeight * 0.13,
                          width: deviceWidth,
                          padding: EdgeInsets.only(left: 0, right: 0, top: 5),
                          margin: EdgeInsets.only(bottom: 5),
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          child: Stack(
                            children: [
                              MyAddress.currentAddress != null
                                  ? Positioned(
                                      top: 0,
                                      left: 10,
                                      child: Container(
                                        width: deviceWidth * 0.9,
                                        child: ListTile(
                                          leading: Icon(Icons.place,
                                              color: Colors.redAccent),
                                          minLeadingWidth: 0,
                                          contentPadding: EdgeInsets.all(0),
                                          title: Text(MyAddress
                                              .currentAddress!.name
                                              .toString()),
                                          subtitle: Text(
                                              MyAddress.currentAddress!.address
                                                  .toString(),
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15)),
                                        ),
                                      ),
                                    )
                                  : Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Text(
                                        'Please select your address',
                                        style: TextStyle(color: Colors.red),
                                      ),
                                    ),
                              Positioned(
                                right: 0,
                                top: ((deviceHeight * 0.12) / 2) / 2,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_forward_ios, size: 16),
                                  onPressed: () {},
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                left: 0,
                                right: 0,
                                child: Container(
                                  width: deviceWidth,
                                  height: 4.0,
                                  child: CustomPaint(
                                    painter: ContainerPatternPainter(),
                                    child: Container(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        onTap: () async {
                          dynamic result = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddressBookScreen()));
                          if (result == 'setAddress') {
                            setState(() {});
                          }
                        },
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                        child: TextFormField(
                          controller: _nameController,
                          style: TextStyle(color: Colors.indigo),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Theme.of(context).backgroundColor,
                            hintText: 'Name',
                            contentPadding: const EdgeInsets.only(left: 17.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.blue),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(19)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                          ),
                          cursorColor: Colors.indigo,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter name';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {});
                          },
                          textInputAction: TextInputAction.next,
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: 20.0, right: 20.0, top: 20),
                        child: TextFormField(
                          controller: _descController,
                          style: TextStyle(color: Colors.indigo),
                          keyboardType: TextInputType.multiline,
                          minLines: 4,
                          maxLines: 100,
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Theme.of(context).backgroundColor,
                            hintText: 'Description',
                            contentPadding:
                                EdgeInsets.only(top: 17, left: 17.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.blue),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(19)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                          ),
                          cursorColor: Colors.indigo,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter description';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {});
                          },
                          textInputAction: TextInputAction.next,
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                        child: TextFormField(
                          controller: _conditionController,
                          keyboardType: TextInputType.multiline,
                          style: TextStyle(color: Colors.indigo),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Theme.of(context).backgroundColor,
                            hintText: 'Condition',
                            contentPadding: const EdgeInsets.only(left: 17.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.blue),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(19)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                          ),
                          cursorColor: Colors.indigo,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter condition';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {});
                          },
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {},
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                        child: TextFormField(
                          controller: _ageController,
                          keyboardType: TextInputType.number,
                          style: TextStyle(color: Colors.indigo),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Theme.of(context).backgroundColor,
                            hintText: 'Age',
                            contentPadding: const EdgeInsets.only(left: 17.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.blue),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(19)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                          ),
                          cursorColor: Colors.indigo,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter age';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {});
                          },
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {},
                        ),
                      ),
                      GestureDetector(
                        onTap: selectImage,
                        child: Container(
                          margin: EdgeInsets.only(
                              left: 20.0, right: 20.0, top: 20.0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            border: Border.all(color: Colors.blue),
                            borderRadius: BorderRadius.all(
                              Radius.circular(19),
                            ),
                          ),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                            child: GridTile(
                              child: widget.petAdopt!.imageUrl!.isEmpty
                                  ? (imageFile == null
                                      ? Icon(Icons.add_box,
                                          size: 30, color: Colors.blue)
                                      : Container(
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: FileImage(imageFile!),
                                            ),
                                          ),
                                        ))
                                  : Container(
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              widget.petAdopt!.imageUrl![0]),
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                        child: Text(
                          "Species",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueAccent,
                              fontSize: 15),
                        ),
                      ),
                      Divider(),
                      Container(
                        margin: EdgeInsets.only(left: 20.0, right: 20.0),
                        child: GroupButton(
                          mainGroupAlignment: MainGroupAlignment.start,
                          isRadio: true,
                          spacing: 10,
                          selectedButton: widget.petAdopt != null
                              ? Species.values
                                  .indexOf(widget.petAdopt!.species!)
                              : -1,
                          direction: Axis.horizontal,
                          onSelected: (index, isSelected) {
                            List<Species> speciesList = Species.values;
                            _species = speciesList[index];
                          },
                          buttons: Species.values.map((element) {
                            return EnumToString.convertToString(element);
                          }).toList(),
                          unselectedBorderColor: Colors.lightBlue,
                          unselectedColor: Theme.of(context).backgroundColor,
                          selectedColor: Colors.blue,
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  /* ------------------------------------------------ SELECT IMAGE FROM FILE --------------------------------------- */
  Future<dynamic> selectImage() {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text("Create Post"),
          children: [
            SimpleDialogOption(
              child: Text("Open Camera"),
              onPressed: () async {
                await handleCameraPicture();
                Navigator.of(context).pop();
              },
            ),
            SimpleDialogOption(
              child: Text("Open Gallery"),
              onPressed: () async {
                await handleGalleryPicture();
                Navigator.of(context).pop();
              },
            ),
            SimpleDialogOption(
              child: Text("Cancel"),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  Future handleCameraPicture() async {
    XFile? file = await _imagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 650, maxWidth: 800);

    if (file != null) {
      setState(() {
        imageFile = File(file.path);
      });
    }
  }

  Future handleGalleryPicture() async {
    XFile? file = await _imagePicker.pickImage(source: ImageSource.gallery);
    if (file != null) {
      setState(() {
        imageFile = File(file.path);
      });
    }
  }

  /* ------------------------------------------------ UPLOAD IMAGE TO FIREBASE --------------------------------------- */

  Future<void> compressImage() async {
    Directory tempDir = await getTemporaryDirectory();
    String tempDirPath = tempDir.path;

    imagePackage.Image decodedImage =
        imagePackage.decodeImage(imageFile!.readAsBytesSync())!;
    final compressedImage = File("${tempDirPath}/img_$uuid}")
      ..writeAsBytesSync(imagePackage.encodeJpg(decodedImage, quality: 100));

    setState(() {
      imageFile = compressedImage;
    });
  }

  void setUpExistedPetAdopt() {
    _nameController.text = widget.petAdopt!.name!;
    _descController.text = widget.petAdopt!.desc!;
    _ageController.text = widget.petAdopt!.age.toString();
    _conditionController.text = widget.petAdopt!.condition!;
    _species = widget.petAdopt!.species;
    imageUrl = widget.petAdopt!.imageUrl![0];
  }

  Future<void> setPetAdopt() async {
    if (_uploadPetAdoptFormKey.currentState!.validate()) {
      imageUrl = (widget.petAdopt!.imageUrl!.isEmpty
          ? await _petAdoptService.uploadImage(imageFile!)
          : widget.petAdopt!.imageUrl![0]);

      if (imageUrl == null) {
        AwesomeDialog(
          context: context,
          animType: AnimType.SCALE,
          dialogType: DialogType.WARNING,
          body: Center(
            child: Text(
              'Please choose image for your property!',
            ),
          ),
          btnOkOnPress: () {},
        )..show();
      } else if (MyAddress.currentAddress == null) {
        AwesomeDialog(
          context: context,
          animType: AnimType.SCALE,
          dialogType: DialogType.WARNING,
          body: Center(
            child: Text(
              'Please specify your property Location!',
            ),
          ),
          btnOkOnPress: () {},
        )..show();
      } else if (_species == null) {
        AwesomeDialog(
          context: context,
          animType: AnimType.SCALE,
          dialogType: DialogType.WARNING,
          body: Center(
            child: Text(
              'Please specify your species type!',
            ),
          ),
          btnOkOnPress: () {},
        )..show();
      } else {
        print('UPLOADING ...');
        setState(() => isLoading = true);
        widget.petAdopt!.name = _nameController.text.toString();
        widget.petAdopt!.desc = _descController.text.toString();
        widget.petAdopt!.age = double.parse(_ageController.value.text);
        widget.petAdopt!.condition = _conditionController.text.toString();
        widget.petAdopt!.owner = {
          '_id': SignedAccount.instance.id,
          'phone': null,
          'email': null,
        };
        widget.petAdopt!.imageUrl!.add(imageUrl!);
        widget.petAdopt!.postTime = DateTime.now();
        widget.petAdopt!.address = MyAddress.currentAddress;
        widget.petAdopt!.species = _species;
        _uploadPetAdoptFormKey.currentState!.save();

        Response response =
            await _petAdoptService.createPetAdopt(widget.petAdopt!);
        setState(() => isLoading = false);

        if (response.statusCode! >= 200 && response.statusCode! < 300) {
          print('SUCCESS UPLOAD ${widget.petAdopt}');
          Navigator.of(context).pop();
        }
      }
    }
  }
}
