import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/services/users/firebase_user_service.dart';

import '../../../../models/users/my_user.dart';
import '../../../../models/users/singned_account.dart';
import '../proof_message_detail_screen.dart';

class ProofMessageItem extends StatefulWidget {
  String candidateId;
  double currentRequestPoint;
  Message message;

  ProofMessageItem(this.candidateId, this.currentRequestPoint, this.message,
      {Key? key})
      : super(key: key);

  @override
  _ProofMessageItemState createState() => _ProofMessageItemState();
}

class _ProofMessageItemState extends State<ProofMessageItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ProofDetailScreen(
                  MyUser.fromSignedInAccount(SignedAccount.instance),
                  SignedAccount.instance.id!,
                  widget.message)));
        },
        child: ListTile(
          title: Text(
            widget.message.displayName!,
            style: TextStyle(fontSize: 17),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.message.messageType == MessageType.Request
                    ? widget.message.requestContent!
                    : widget.message.lastMessageContent!,
                style: TextStyle(fontSize: 15),
              ),
              Text(
                DateTime.now().difference(widget.message.sentTime!).inDays > 0
                    ? DateFormat('EEE, H:m', 'en_US')
                        .format(widget.message.sentTime!)
                    : DateFormat('Hm').format(widget.message.sentTime!),
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
          leading: FutureBuilder(
              future:
                  new FirebaseUserHelper().getImageOfUser(widget.candidateId),
              builder: (context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return CircleAvatar(
                    backgroundImage: NetworkImage(snapshot.data),
                  );
                }
                return CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://4xucy2kyby51ggkud2tadg3d-wpengine.netdna-ssl.com/wp-content/uploads/sites/37/2017/02/IAFOR-Blank-Avatar-Image.jpg'),
                );
              }),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                height: 55,
                width: 55,
                decoration: BoxDecoration(
                  color: Color(0xffD7F2B9),
                  borderRadius: BorderRadius.all(Radius.circular(7.5)),
                ),
                child: Center(
                  child: Text(
                    widget.currentRequestPoint.toStringAsFixed(1),
                    style: TextStyle(
                        fontSize: 17.5,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff758F58)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
