import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/messages/conversation.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/messages/message_service.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/proof_message_thumnail_item.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

class RecentCandidateMessageScreen extends StatefulWidget {
  RecentCandidateMessageScreen({Key? key}) : super(key: key);

  @override
  _RecentCandidateMessageScreenState createState() =>
      _RecentCandidateMessageScreenState();
}

class _RecentCandidateMessageScreenState
    extends State<RecentCandidateMessageScreen> {
  MessageService messageService = new MessageService();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      child: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('proof_you_are_deserved')
              .where('members', arrayContains: SignedAccount.instance.id)
              .snapshots(),
          builder: (context, AsyncSnapshot<dynamic> snapshotConversations) {
            if (snapshotConversations.hasData) {
              QuerySnapshot conversationSnapshot = snapshotConversations.data;

              print("CURRENT CONVERSATIONS: ${conversationSnapshot.docs}");

              List<Map<String, dynamic>> listMessage =
                  conversationSnapshot.docs.map((document) {
                Map<String, dynamic> data =
                    document.data() as Map<String, dynamic>;

                Conversation conversation = Conversation.fromDocument(document);

                Message lastMessage = conversation.messages.last;

                return {
                  'candidate': data['members']
                      .where(
                          (memberId) => memberId != SignedAccount.instance.id!)
                      .toList()
                      .first,
                  'requestPoint': data['requestPoint'],
                  'message': lastMessage
                };
              }).toList();

              return Container(
                child: ListView.builder(
                    itemCount: listMessage.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: ProofMessageItem(
                            listMessage[index]['candidate'],
                            listMessage[index]['requestPoint'],
                            listMessage[index]['message']),
                      );
                    }),
              );
            }
            return LinearProgress();
          }),
    );
  }
}
