import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/services/pet_adopt/proof_service.dart';
import 'package:social_media/ui/widgets/gradient_button.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

enum RequestAttachment {
  location,
  film,
  sign,
  images,
  file,
}

dynamic convertRequestAttachment(
    {int? i, RequestAttachment? requestAttachment}) {
  if (i != null) {
    switch (i) {
      case 1:
        return RequestAttachment.location;
        break;
      case 2:
        return RequestAttachment.film;
        break;
      case 3:
        return RequestAttachment.sign;
        break;
      case 4:
        return RequestAttachment.images;
        break;
      case 5:
        return RequestAttachment.file;
        break;
      default:
        return RequestAttachment.file;
        break;
    }
  } else {
    switch (requestAttachment) {
      case RequestAttachment.location:
        return 1;
        break;
      case RequestAttachment.film:
        return 2;
        break;
      case RequestAttachment.sign:
        return 3;
        break;
      case RequestAttachment.images:
        return 4;
        break;
      case RequestAttachment.file:
        return 5;
        break;
      default:
        return 5;
        break;
    }
  }
}

class CreateProofRequestFormScreen extends StatefulWidget {
  String conversationId;

  CreateProofRequestFormScreen({required this.conversationId, Key? key})
      : super(key: key);

  @override
  State<CreateProofRequestFormScreen> createState() =>
      _CreateProofRequestFormScreenState();
}

class _CreateProofRequestFormScreenState
    extends State<CreateProofRequestFormScreen> {
  ProofService _proofYouAreDeservedService = new ProofService();

  final GlobalKey<FormState> _proofRequestFormKey = GlobalKey<FormState>();

  RequestAttachment? requestAttachment;
  double requestPoint = 0.0;
  int requestAttachSelected = -1;
  TextEditingController _requestContentController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      leading: Center(
        child: IconButton(
          icon:
              FaIcon(FontAwesomeIcons.angleLeft, color: AppColors.pinkGradient),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      backgroundColor: Color(0xffF3FFE8),
      toolbarHeight: 60,
      title: GradientText(
        'Create Request',
        style: TextStyle(
          fontSize: 17.5,
          fontWeight: FontWeight.bold,
        ),
        colors: [
          AppColors.pinkGradient,
          AppColors.blueGradient,
        ],
      ),
      centerTitle: false,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
        ),
      ),
      actions: [],
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        color: Color(0xffF3FFE8),
        child: Form(
          key: _proofRequestFormKey,
          onChanged: () {
            _proofRequestFormKey.currentState!.save();
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Text(
                  'Request',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: AppColors.pinkGradient),
                ),
              ),
              Container(
                child: TextFormField(
                  controller: _requestContentController,
                  keyboardType: TextInputType.multiline,
                  minLines: 7,
                  maxLines: 10,
                  style: TextStyle(color: Colors.indigo),
                  decoration: new InputDecoration(
                    filled: true,
                    fillColor: Theme.of(context).backgroundColor,
                    contentPadding: const EdgeInsets.only(left: 17.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.blue),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 1.0,
                      ),
                    ),
                  ),
                  cursorColor: Colors.indigo,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter Request';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    setState(() {});
                  },
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_) {},
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50, bottom: 10),
                child: Text(
                  'Candidate must send',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: AppColors.blueGradient),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          requestAttachSelected = 1;
                        });
                      },
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          color: requestAttachSelected != 1
                              ? Colors.grey
                              : AppColors.blueGradient,
                          borderRadius: BorderRadius.all(
                            Radius.circular(13),
                          ),
                        ),
                        child: Center(
                            child: FaIcon(
                          FontAwesomeIcons.mapMarked,
                          color: Colors.white,
                        )),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          requestAttachSelected = 2;
                        });
                      },
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          color: requestAttachSelected != 2
                              ? Colors.grey
                              : AppColors.blueGradient,
                          borderRadius: BorderRadius.all(
                            Radius.circular(13),
                          ),
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.film,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          requestAttachSelected = 3;
                        });
                      },
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          color: requestAttachSelected != 3
                              ? Colors.grey
                              : AppColors.blueGradient,
                          borderRadius: BorderRadius.all(
                            Radius.circular(13),
                          ),
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.signature,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          requestAttachSelected = 4;
                        });
                      },
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          color: requestAttachSelected != 4
                              ? Colors.grey
                              : AppColors.blueGradient,
                          borderRadius: BorderRadius.all(
                            Radius.circular(13),
                          ),
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.solidImages,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          requestAttachSelected = 5;
                        });
                      },
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          color: requestAttachSelected != 5
                              ? Colors.grey
                              : AppColors.blueGradient,
                          borderRadius: BorderRadius.all(
                            Radius.circular(13),
                          ),
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.fileInvoice,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50, bottom: 50),
                child: Text(
                  'Request Point',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: AppColors.pinkGradient),
                ),
              ),
              SfSliderTheme(
                data: SfSliderThemeData(
                  tooltipBackgroundColor: AppColors.blueGradient,
                  activeTickColor: AppColors.blueGradient,
                  inactiveTickColor: AppColors.blueGradient,
                  inactiveTrackHeight: 15.0,
                  activeTrackHeight: 15.0,
                  thumbRadius: 10,
                  inactiveTrackColor: AppColors.pinkGradient,
                  activeTrackColor: AppColors.blueGradient,
                  thumbColor: AppColors.blueGradient,
                ),
                child: SfSlider(
                  min: 0.0,
                  max: 10.0,
                  interval: 2,
                  showTicks: true,
                  showLabels: true,
                  enableTooltip: true,
                  value: requestPoint,
                  onChanged: (dynamic newValue) {
                    setState(() {
                      requestPoint = newValue;
                    });
                  },
                ),
              ),
              Spacer(),
              Container(
                margin: EdgeInsets.only(bottom: 10),
                child: GradientButton(
                  color1: AppColors.pinkGradient,
                  color2: AppColors.blueGradient,
                  child: Text(
                    'CREATE REQUEST',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                  onPress: () {
                    _proofYouAreDeservedService.sendRequestMessage(
                        conversationId: widget.conversationId,
                        requestContent: _requestContentController.value.text,
                        requestAttachment: EnumToString.convertToString(
                            convertRequestAttachment(i: requestAttachSelected)),
                        requestPoint: requestPoint);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
