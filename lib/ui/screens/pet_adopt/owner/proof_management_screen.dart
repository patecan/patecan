import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/pet_adopt/pet_adopt.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/proof_actions_screen.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/recent_candidate_message_screen.dart';

class ProofManagementScreen extends StatefulWidget {
  PetAdopt petAdopt;
  ProofManagementScreen(this.petAdopt, {Key? key}) : super(key: key);

  @override
  State<ProofManagementScreen> createState() => _ProofManagementScreenState();
}

class _ProofManagementScreenState extends State<ProofManagementScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this, initialIndex: 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    AppBar appBar = AppBar(
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              Colors.deepPurple.withOpacity(0.75),
              Colors.blue.withOpacity(0.75)
            ],
          ),
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      title: Text(
        'Message',
        style:
            TextStyle(color: Colors.white, fontFamily: "Nunito", fontSize: 20),
      ),
      centerTitle: true,
      bottom: TabBar(
        controller: _tabController,
        unselectedLabelColor: Colors.white,
        indicatorColor: Colors.orange,
        labelColor: Colors.orange,
        indicatorWeight: 3.0,
        tabs: [
          Tab(
            icon: Icon(
              Icons.people_outlined,
            ),
          ),
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.replyAll,
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        height: size.height,
        width: size.width,
        child: TabBarView(
          controller: _tabController,
          children: [
            RecentCandidateMessageScreen(),
            ProofActionsManagementScreen(),
          ],
        ),
      ),
    );
  }
}
