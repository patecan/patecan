import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:group_button/group_button.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/places/place.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/place_provider.dart';
import 'package:social_media/services/places/map_service.dart';
import 'package:social_media/ui/screens/address_book/places_store_search_screen.dart';

import '../../../../models/store/my_address.dart';
import '../../widgets/rounded_loading_button.dart';
import '../../widgets/social/progress.dart';

class PlacesStoreHomepageScreen extends StatefulWidget {
  PlacesStoreHomepageScreen({Key? key}) : super(key: key);

  @override
  _PlacesStoreHomepageScreenState createState() =>
      _PlacesStoreHomepageScreenState();
}

class _PlacesStoreHomepageScreenState extends State<PlacesStoreHomepageScreen> {
  late GoogleMapController googleMapController;
  GlobalKey<ScaffoldState> placesHomepageScreenKey =
      new GlobalKey<ScaffoldState>();

  MapService mapService = new MapService();
  late PlaceProvider placeProvider;

  late Position currentPosition;

  late DatabaseReference placeDBRef = FirebaseDatabase.instance.reference();

  late Stream<Position> _positionStream;
  late StreamSubscription<Position> _positionStreamSubscription;

  Set<Polyline> polyLinesSet = {};
  Set<Marker> markersSet = {};
  Set<Circle> circlesSet = {};

  bool isSelectedDestination = false;

  double deviceHeight = 0.0;
  double deviceWidth = 0.0;
  double mapBottomPadding = 0.0;

  setCurrentPosition() async {
    currentPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.bestForNavigation);
    return currentPosition;
  }

  @override
  initState() {
    getPermission();
    setCurrentPosition();
    placeProvider = Provider.of<PlaceProvider>(context, listen: false);
    _positionStream = Geolocator.getPositionStream(
        locationSettings: LocationSettings(
            accuracy: LocationAccuracy.bestForNavigation, distanceFilter: 4));

    placeDBRef = FirebaseDatabase.instance.reference();
    super.initState();
  }

  @override
  void dispose() {
    _positionStreamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    deviceHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: placesHomepageScreenKey,
      body: Container(
        width: deviceWidth,
        child: FutureBuilder(
            future: setCurrentPosition(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Stack(
                  children: [
                    GoogleMap(
                      polylines: polyLinesSet,
                      markers: markersSet,
                      circles: circlesSet,
                      padding: EdgeInsets.only(
                          bottom: mapBottomPadding,
                          top: MediaQuery.of(context).padding.top),
                      mapType: MapType.normal,
                      myLocationButtonEnabled: true,
                      myLocationEnabled: true,
                      zoomGesturesEnabled: true,
                      zoomControlsEnabled: true,
                      onTap: selectLocation,
                      initialCameraPosition:
                          CameraPosition(target: LatLng(1, 1), zoom: 0),
                      onMapCreated: (GoogleMapController controller) async {
                        googleMapController = controller;
                        CameraPosition cameraPosition = new CameraPosition(
                            target: LatLng(currentPosition.latitude,
                                currentPosition.longitude),
                            zoom: 15);
                        googleMapController.animateCamera(
                            CameraUpdate.newCameraPosition(cameraPosition));
                        await mapService.setupCurrentPositionLocation(
                            currentPosition, context);

                        updateYourOwnLocation();
                        setState(() {
                          mapBottomPadding = deviceHeight * 0.33;
                        });
                      },
                    ),
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: 0,
                      child: AnimatedSwitcher(
                        duration: Duration(milliseconds: 300),
                        child: SearchStoreSlivers(
                          deviceHeight: deviceHeight,
                          deviceWidth: deviceWidth,
                          getDirection: drawDirectionPolyline,
                          setNewAddress: setNewAddress,
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return CircularProgress();
              }
            }),
      ),
    );
  }

  /*_________________________________   HELPER FUNCTIONS _________________________________ */
  Future<void> getPermission() async {
    await Permission.location.request();
  }

  Future<void> drawDirectionPolyline() async {
    polyLinesSet.clear();
    markersSet.clear();
    circlesSet.clear();

    var placeProvider = Provider.of<PlaceProvider>(context, listen: false);

    Place destinationPlace = placeProvider.storePlace!;

    LatLng destinationLatLng =
        LatLng(destinationPlace.latitude!, destinationPlace.longitude!);

    if (destinationLatLng != null) {
      /*_________________________________ Set Marker and Circle_________________________________ */

      Marker destinationMarker = Marker(
        markerId: MarkerId('Address'),
        position: destinationLatLng,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        infoWindow: InfoWindow(
            title: destinationPlace.placeFormattedAddress,
            snippet: 'New Address'),
      );

      setState(() {
        markersSet.add(destinationMarker);
        isSelectedDestination = true;
        CameraPosition cameraPosition =
            CameraPosition(target: destinationLatLng, zoom: 15);
        googleMapController
            .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      });
    }
  }

  void resetMap() {
    setState(() {
      polyLinesSet.clear();
      circlesSet.clear();
      markersSet.clear();
      isSelectedDestination = false;
      placeProvider.storePlace = null;
    });
  }

  void updateYourOwnLocation() {
    _positionStreamSubscription =
        _positionStream.listen((Position position) async {
      currentPosition = position;
      mapService.setupCurrentPositionLocation(position, context);
      CameraPosition cameraPosition = new CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: 15);
      googleMapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    });
  }

  void selectLocation(LatLng location) async {
    setState(() {
      Place newPlace =
          new Place(latitude: location.latitude, longitude: location.longitude);
      placeProvider.setNewStorePlace(newPlace);
      markersSet.add(new Marker(
          markerId: MarkerId(SignedAccount.instance.id!), position: location));
      CameraPosition cameraPosition =
          CameraPosition(target: location, zoom: 15);
      googleMapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    });
    await drawDirectionPolyline();
  }

  void setNewAddress(MyAddress newAddress) async {
    MyAddress.currentAddress = newAddress;
    MyAddress.currentAddress!.address =
        placeProvider.storePlace!.placeFormattedAddress!;
    newAddress.latitude = placeProvider.storePlace!.latitude!;
    newAddress.longitude = placeProvider.storePlace!.longitude!;

    await FirebaseFirestore.instance
        .collection('stores')
        .doc(SignedAccount.instance.id)
        .update(
      {
        'addressBook': FieldValue.arrayUnion(
          [
            {
              'id': newAddress.id,
              'address': newAddress.address,
              'name': newAddress.name,
              'ownerID': newAddress.ownerId,
              'latitude': newAddress.latitude,
              'longitude': newAddress.longitude,
            }
          ],
        ),
      },
    );
    Navigator.of(context).pop();
  }
}

class SearchStoreSlivers extends StatefulWidget {
  double deviceWidth = 0.0;
  double deviceHeight = 0.0;

  Function getDirection;
  Function setNewAddress;

  SearchStoreSlivers(
      {required this.deviceHeight,
      required this.deviceWidth,
      required this.getDirection,
      required this.setNewAddress,
      Key? key})
      : super(key: key);

  @override
  State<SearchStoreSlivers> createState() => _SearchStoreSliversState();
}

class _SearchStoreSliversState extends State<SearchStoreSlivers> {
  RoundedLoadingButtonController roundedLoadingButtonController =
      new RoundedLoadingButtonController();

  TextEditingController newAddressNameController = TextEditingController();

  MyAddress newAddress = MyAddress.empty();
  late AddressTypes newAddressType;
  late PlaceProvider placeProvider;

  @override
  void initState() {
    newAddressType = newAddress.placeType!;
    roundedLoadingButtonController = new RoundedLoadingButtonController();
    placeProvider = Provider.of<PlaceProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.deviceHeight * 0.23,
      width: widget.deviceWidth,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(0.5, 0.5),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 15,
            ),
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.circular(4),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 5.0,
                    spreadRadius: 0.05,
                    offset: Offset(0.7, 0.7),
                  ),
                ],
              ),
              child: GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Row(
                      children: [
                        Icon(
                          Icons.search,
                          color: Colors.lightBlueAccent,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text('search destination'),
                      ],
                    ),
                  ),
                  onTap: () async {
                    dynamic result = await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PlacesStoreSearchScreen(),
                        ));
                    switch (result) {
                      case 'getDirection':
                        await widget.getDirection();
                        break;
                      default:
                        break;
                    }
                  }),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      GroupButton(
                        mainGroupAlignment: MainGroupAlignment.start,
                        isRadio: true,
                        spacing: 0,
                        buttonHeight: widget.deviceHeight * 0.05,
                        buttonWidth: 80,
                        selectedButton:
                            AddressTypes.values.indexOf(AddressTypes.HOME),
                        direction: Axis.horizontal,
                        onSelected: (index, isSelected) {
                          List<AddressTypes> addressTypesList =
                              AddressTypes.values;
                          newAddress.placeType = addressTypesList[index];
                        },
                        buttons: List<String>.from(AddressTypes.values.map(
                          (element) {
                            return EnumToString.convertToString(element);
                          },
                        ).toList()),
                        unselectedBorderColor: Colors.lightBlue,
                        unselectedColor: Theme.of(context).backgroundColor,
                        selectedColor: Colors.blue,
                      ),
                      Container(
                        height: widget.deviceHeight * 0.05,
                        width: widget.deviceWidth * 0.51,
                        margin: EdgeInsets.only(left: 5),
                        child: TextFormField(
                          controller: newAddressNameController,
                          style: TextStyle(color: Colors.indigo),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Theme.of(context).backgroundColor,
                            hintText: 'Address Name',
                            contentPadding: const EdgeInsets.only(left: 17.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.blue),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                          ),
                          cursorColor: Colors.indigo,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter price';
                            } else if (double.parse(value) < 0.0) {
                              return 'Invalid Price';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {});
                          },
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {},
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: RoundedLoadingButton(
                      height: widget.deviceHeight * 0.05,
                      width: widget.deviceWidth * 0.45,
                      onPressed: () async {
                        roundedLoadingButtonController.start();
                        newAddress.placeType = newAddressType;
                        newAddress.name = newAddressNameController.text
                                .toString()
                                .trim()
                                .isEmpty
                            ? 'Untitled'
                            : newAddressNameController.text;
                        MyAddress.currentAddress = newAddress;
                        roundedLoadingButtonController.success();
                        widget.setNewAddress(newAddress);
                        roundedLoadingButtonController.stop();
                      },
                      borderColor: Colors.white,
                      color: Colors.orange,
                      controller: roundedLoadingButtonController,
                      valueColor: Colors.white,
                      child: Text('Set New Address'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
