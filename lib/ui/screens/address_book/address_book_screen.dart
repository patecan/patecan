import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/address_book/places_store_homepage_screen.dart';
import 'package:uuid/uuid.dart';

import '../../../../models/store/my_address.dart';
import '../../widgets/social/progress.dart';

class AddressBookScreen extends StatefulWidget {
  AddressBookScreen({Key? key}) : super(key: key);

  @override
  _AddressBookScreenState createState() => _AddressBookScreenState();
}

class _AddressBookScreenState extends State<AddressBookScreen> {
  Uuid uuid = new Uuid();

  didChangeDependencies() {}

  @override
  Widget build(BuildContext context) {
    double deviceHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    double deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Address Book'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.pop(context, 'setAddress'),
        ),
      ),
      body: Column(
        children: [
          FutureBuilder(
            future: FirebaseFirestore.instance
                .collection('stores')
                .doc(SignedAccount.instance.id!)
                .get(),
            builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (snapshot.hasData) {
                DocumentSnapshot documentSnapshot = snapshot.data!;
                List<MyAddress> addressList = [];
                if (documentSnapshot['addressBook'] != null &&
                    documentSnapshot['addressBook'].length != 0) {
                  addressList =
                      List<MyAddress>.from(documentSnapshot['addressBook'].map(
                    (address) {
                      return MyAddress.fromJson(address);
                    },
                  ).toList());

                  print(addressList);
                  return SingleChildScrollView(
                    child: AddressRadioButtonList(addressList),
                  );
                }
                return Container();
              } else {
                return CircularProgress();
              }
            },
          ),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Container(
            margin: EdgeInsets.only(left: 7, right: 7, bottom: 7),
            child: DottedBorder(
              borderType: BorderType.RRect,
              radius: Radius.circular(7),
              strokeWidth: 2,
              color: Colors.blue,
              padding: EdgeInsets.all(6),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                child: GestureDetector(
                  child: Container(
                    height: deviceHeight * 0.075,
                    width: deviceWidth,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.add_location, color: Colors.blue),
                        Text('Add Address',
                            style: TextStyle(color: Colors.blue))
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => PlacesStoreHomepageScreen(),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AddressRadioButtonList extends StatefulWidget {
  List<MyAddress> addressList;

  AddressRadioButtonList(this.addressList, {Key? key}) : super(key: key);

  @override
  _AddressRadioButtonListState createState() => _AddressRadioButtonListState();
}

class _AddressRadioButtonListState extends State<AddressRadioButtonList> {
  late MyAddress? currentAddress = MyAddress.currentAddress;
  late MyAddress addressProvider;

  @override
  void initState() {
    addressProvider = Provider.of<MyAddress>(context, listen: false);
    if (widget.addressList.length > 0) {
      if (MyAddress.currentAddress == null) {
        currentAddress = widget.addressList[0];
        MyAddress.currentAddress = currentAddress;
      } else {
        currentAddress = widget.addressList.firstWhere(
            (address) => address.id == MyAddress.currentAddress!.id);
      }
    }
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (widget.addressList.length > 0) {
      if (MyAddress.currentAddress == null) {
        currentAddress = widget.addressList[0];
        MyAddress.currentAddress = currentAddress;
      } else {
        currentAddress = widget.addressList.firstWhere(
            (address) => address.id == MyAddress.currentAddress!.id);
      }
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    double deviceWidth = MediaQuery.of(context).size.width;

    return Column(
      children: [
        Container(
          height: deviceHeight * 0.79,
          child: ListView.builder(
            itemCount: widget.addressList.length,
            itemBuilder: (context, index) {
              return Container(
                width: deviceWidth,
                margin: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                padding: EdgeInsets.only(top: 5, bottom: 5, left: 0, right: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 5.0,
                      spreadRadius: 0.025,
                      offset: Offset(0.5, 0.5),
                    ),
                  ],
                ),
                child: RadioListTile<MyAddress>(
                  title: Text(widget.addressList[index].name!),
                  subtitle:
                      Text(widget.addressList[index].address ?? 'Unknown'),
                  value: widget.addressList[index],
                  groupValue: currentAddress,
                  onChanged: (MyAddress? value) async {
                    setState(() {
                      currentAddress = value!;
                      addressProvider.setCurrentAddress(value);
                    });
                  },
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
