import 'dart:async';

import 'package:animator/animator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/stories/story.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/messages/message_service.dart';
import 'package:social_media/services/stories/firebase_stories_service.dart';
import 'package:social_media/ui/screens/shorts/upload_short_video_screen.dart';
import 'package:social_media/ui/widgets/shorts/rotating_circle.dart';

import '../../widgets/shorts/short_video_player_item.dart';

class StoryScreen extends StatefulWidget {
  Story story;

  StoryScreen({required this.story, Key? key}) : super(key: key);

  @override
  _StoryScreenState createState() => _StoryScreenState();
}

class _StoryScreenState extends State<StoryScreen> {
  FirebaseStoriesService storiesService = new FirebaseStoriesService();

  TextEditingController messageController = new TextEditingController();
  MessageService messageService = new MessageService();

  /*______________ Like Feature _____________*/
  bool isLiked = false;
  bool isHearted = false;

  /*______________ Comment Feature _____________*/

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    isLiked = widget.story.likes!.contains(SignedAccount.instance.id);

    return Scaffold(
      body: StreamBuilder<Object>(
        stream: FirebaseFirestore.instance
            .collection('users')
            .doc(widget.story.ownerID)
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            MyUser myUser = MyUser.fromDocumentSnapshot(snapshot.data!);
            return Stack(
              children: [
                ShortVideoPlayerItem(
                  videoURL: widget.story.videoURL!,
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 25, left: 10),
                      height: 50,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        UploadShortVideoScreen()));
                              },
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(25),
                                child: Image.network(
                                  myUser.photoUrl!,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            myUser.displayName!,
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    widget.story.caption!,
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: size.width * 0.75,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10),
                                        decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              232, 234, 235, 1.0),
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 15.0),
                                                child: TextFormField(
                                                  controller: messageController,
                                                  onChanged: (value) {},
                                                  onSaved: (value) {
                                                    setState(() {});
                                                  },
                                                  decoration: InputDecoration(
                                                      border: InputBorder.none,
                                                      hintText: "Type message"),
                                                  cursorColor: Colors.black,
                                                ),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.centerRight,
                                              child: GestureDetector(
                                                child: SizedBox(
                                                  width: size.width * 0.1,
                                                  height: size.width * 0.1,
                                                  child: SvgPicture.asset(
                                                      'assets/icons/paw.svg'),
                                                ),
                                                onTap: sendMessage,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        child: Column(
                                          children: [
                                            GestureDetector(
                                              child: isLiked == true
                                                  ? Icon(Icons.favorite,
                                                      size: 30,
                                                      color: Colors.pink)
                                                  : Icon(Icons.favorite,
                                                      size: 30,
                                                      color: Colors.white),
                                              onTap: () async {
                                                await likeStory(
                                                    widget.story.id!,
                                                    widget.story.likes!);
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Positioned(
                  top: 70,
                  left: 10,
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 8, right: 5, top: 2, bottom: 2),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              widget.story.songName!,
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              widget.story.songName!,
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        RotatingCircle(
                          child: Container(
                            width: 35,
                            height: 35,
                            child: Column(
                              children: [
                                Container(
                                  width: 35,
                                  height: 35,
                                  padding: EdgeInsets.all(5.0),
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Colors.grey.shade700,
                                        Colors.grey
                                      ],
                                    ),
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: GestureDetector(
                                    onTap: () {},
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(25),
                                      child: Image.network(
                                        SignedAccount.instance.photoUrl!,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            );
          } else {
            return Container(color: Colors.black);
          }
        },
      ),
    );
  }

  createHeartAnimation() {
    Animator(
      duration: Duration(milliseconds: 300),
      tween: Tween(begin: 0.8, end: 1.4),
      curve: Curves.elasticOut,
      cycles: 0,
      builder: (BuildContext context, AnimatorState<dynamic> animatorState,
          Widget? child) {
        return Transform.scale(
            scale: animatorState.value,
            child: Icon(Icons.favorite, size: 80.0, color: Colors.red));
      },
    );

    setState(() {
      isHearted = true;
    });

    Timer(Duration(milliseconds: 500), () {
      setState(() {
        isHearted = false;
      });
    });
  }

  Future likeStory(String storyID, List<String> likesList) async {
    print(storyID);

    bool isUserLikedBefore = likesList.contains(SignedAccount.instance.id);

    if (isUserLikedBefore) {
      setState(
        () {
          this.isLiked = false;
          likesList.remove(SignedAccount.instance.id);
        },
      );
      await storiesService.removeLike(storyID, SignedAccount.instance.id!);
    } else if (!isUserLikedBefore) {
      setState(() {
        createHeartAnimation();
        this.isLiked = true;
        likesList.add(SignedAccount.instance.id!);
      });
      await storiesService.addLike(storyID, SignedAccount.instance.id!);
    }
  }

  sendMessage() async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(SignedAccount.instance.id)
        .collection('Conversations')
        .doc(widget.story.ownerID)
        .get();

    if (documentSnapshot.data() == null) {
      // messageService.sendNewMessage(
      //     widget.story.ownerID!, null, messageController.text);
    } else {
      Message message = Message.fromDocumentSnapshot(documentSnapshot);

      if (message.conversationID == null) {
        // messageService.sendNewMessage(
        //     widget.story.ownerID!, null, messageController.text);
        //
        //
        //
      } else {
        late Message newMessage;

        newMessage = new Message(
          messageType: MessageType.Text,
          lastMessageContent: messageController.text,
          senderID: SignedAccount.instance.id,
          sentTime: DateTime.now(),
        );

        messageService.sendMessage(
          message.conversationID!,
          newMessage,
        );
      }

      setState(
        () {
          messageController.clear();
          FocusScope.of(context).unfocus();
        },
      );
    }
  }
}
