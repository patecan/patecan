import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/stories/story.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/widgets/social/progress.dart';
import 'package:social_media/ui/widgets/social/stories/create_your_story_thumbnail_item.dart';

import '../../widgets/social/stories/story_thumbnail_item.dart';

class StoriesPreviewSection extends StatefulWidget {
  StoriesPreviewSection({Key? key}) : super(key: key);

  @override
  _StoriesPreviewSectionState createState() => _StoriesPreviewSectionState();
}

class _StoriesPreviewSectionState extends State<StoriesPreviewSection> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: FutureBuilder(
        future: getFollowingStories(),
        builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
          if (snapshot.hasData) {
            List<Widget> followingStoriesList = snapshot.data!;

            return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: followingStoriesList.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  width: size.width * 0.27,
                  margin: EdgeInsets.only(right: 5),
                  child: followingStoriesList[index],
                );
              },
            );
          } else {
            return LinearProgress();
          }
        },
      ),
    );
  }

  Future<List<String>> getFollowingId() async {
    QuerySnapshot querySnapshot = await FirebaseFirestore.instance
        .collection('following')
        .doc(SignedAccount.instance.id)
        .collection('userFollowing')
        .get();

    List<String> followingIDList = List<String>.from(
      querySnapshot.docs.map(
        (document) {
          return document.id;
        },
      ),
    );
    return followingIDList;
  }

  Future<List<Widget>> getFollowingStories() async {
    List<String> followingIDList = await getFollowingId();

    QuerySnapshot querySnapshot = await FirebaseFirestore.instance
        .collection('stories')
        .where('ownerID', whereIn: followingIDList)
        .get();

    List<Widget> followingStoriesList = [CreateYourStoryThumbnailItem()];

    followingStoriesList.addAll(
      querySnapshot.docs.map((document) {
        return StoryThumbnailItem(story: Story.fromDocument(document));
      }),
    );

    return followingStoriesList;
  }
}
