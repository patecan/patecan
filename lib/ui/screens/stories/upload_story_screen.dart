import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_media/ui/screens/stories/upload_story_confirming_screen.dart';

class UploadStoryScreen extends StatefulWidget {
  UploadStoryScreen({Key? key}) : super(key: key);

  @override
  _UploadStoryScreenState createState() => _UploadStoryScreenState();
}

class _UploadStoryScreenState extends State<UploadStoryScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        child: Align(
          alignment: Alignment.center,
          child: GestureDetector(
            child: Container(
              height: 100,
              child: Column(
                children: [
                  Icon(Icons.videocam, size: 50),
                  Text('Add Story'),
                ],
              ),
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    children: [
                      SimpleDialogOption(
                          child: Text(
                            'Gallery',
                            style: TextStyle(),
                          ),
                          onPressed: () {
                            pickVideo(ImageSource.gallery);
                          }),
                      SimpleDialogOption(
                          child: Text(
                            'Camera',
                            style: TextStyle(),
                          ),
                          onPressed: () {
                            pickVideo(ImageSource.camera);
                          }),
                      SimpleDialogOption(
                        child: Text(
                          'Cancel',
                          style: TextStyle(),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }

  pickVideo(ImageSource source) async {
    Navigator.pop(context);
    var video = await ImagePicker().pickVideo(source: source);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UploadStoryConfirmingScreen(
          videoFile: File(video!.path),
          videoPath: video.path,
          videoSource: source,
        ),
      ),
    );
  }
}
