import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:light_compressor/light_compressor.dart';
import 'package:path_provider/path_provider.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:uuid/uuid.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class UploadStoryConfirmingScreen extends StatefulWidget {
  File videoFile;
  String videoPath;
  ImageSource videoSource;

  UploadStoryConfirmingScreen(
      {required this.videoSource,
      required this.videoPath,
      required this.videoFile,
      Key? key})
      : super(key: key);

  @override
  _UploadStoryConfirmingScreenState createState() =>
      _UploadStoryConfirmingScreenState();
}

class _UploadStoryConfirmingScreenState
    extends State<UploadStoryConfirmingScreen> {
  late VideoPlayerController videoPlayerController;

  TextEditingController songNameController = TextEditingController();
  TextEditingController captionController = TextEditingController();

  LightCompressor _lightCompressor = LightCompressor();

  Size? size;

  @override
  void initState() {
    setState(() {
      videoPlayerController = VideoPlayerController.file(widget.videoFile);
    });

    videoPlayerController.initialize();
    videoPlayerController.play();
    videoPlayerController.setVolume(10);
    videoPlayerController.setLooping(true);
    super.initState();
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    songNameController.dispose();
    captionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: size!.width,
              height: size!.height / 1.35,
              child: VideoPlayer(videoPlayerController),
            ),
            SizedBox(height: 20),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: size!.width / 2,
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: TextFormField(
                      controller: songNameController,
                      decoration: InputDecoration(
                        isDense: true,
                        filled: true,
                        fillColor: Colors.white,
                        labelText: 'Song name',
                        labelStyle: TextStyle(fontSize: 10),
                        prefixIcon: Icon(Icons.music_note),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    width: size!.width,
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: TextFormField(
                      controller: captionController,
                      decoration: InputDecoration(
                        filled: true,
                        isDense: true,
                        fillColor: Colors.white,
                        labelText: 'Caption',
                        labelStyle: TextStyle(fontSize: 10),
                        prefixIcon: Icon(Icons.wb_iridescent),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: size!.width * 0.25,
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text("Discard",
                                style: TextStyle(
                                    fontSize: 15, color: Colors.white)),
                            color: Colors.red,
                          ),
                        ),
                        Container(
                          width: size!.width * 0.65,
                          child: MaterialButton(
                            onPressed: () async {
                              CoolAlert.show(
                                context: context,
                                type: CoolAlertType.loading,
                                text: "uploading...",
                              );
                              await uploadStory();
                              Navigator.of(context).pop();
                            },
                            child: Text("Upload",
                                style: TextStyle(
                                    fontSize: 15, color: Colors.white)),
                            color: Colors.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future uploadStory() async {
    String storyID = Uuid().v4();

    QuerySnapshot querySnapshot =
        await FirebaseFirestore.instance.collection('stories').get();

    String videoURL = await uploadVideoToStorage(storyID);
    String videoThumbnailURL =
        await uploadThumbnailImageToStorage(storyID, videoURL);

    await FirebaseFirestore.instance.collection('stories').doc(storyID).set(
      {
        'ID': '$storyID',
        'ownerID': SignedAccount.instance.id,
        'songName': songNameController.text,
        'caption': captionController.text,
        'videoURL': videoURL,
        'thumbnailURL': videoThumbnailURL,
        'likes': [],
        'comments': [],
        'timeCreated': DateTime.now(),
      },
    );
  }

  Future<String> uploadVideoToStorage(String storyID) async {
    Reference videoStorageRef = FirebaseStorage.instance.ref().child('stories');
    UploadTask uploadTask =
        videoStorageRef.child('$storyID').putFile(await compressVideo());
    TaskSnapshot taskSnapshot = await uploadTask;
    String videoURL = await taskSnapshot.ref.getDownloadURL();
    return videoURL;
  }

  Future<File> compressVideo() async {
    if (widget.videoSource == ImageSource.gallery) {
      return File(widget.videoPath);
    } else {
      final dynamic response = await _lightCompressor.compressVideo(
          path: widget.videoPath,
          destinationPath: widget.videoPath,
          videoQuality: VideoQuality.medium,
          isMinBitrateCheckEnabled: false,
          frameRate: 60);

      String outputFile = response.destinationPath;
      return File(outputFile);
    }
  }

  getVideoThumbnail(String videoURL) async {
    final filePath = await VideoThumbnail.thumbnailFile(
      video: videoURL,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.WEBP,
      maxHeight: (size!.height * 0.2).floor(),
      // specify the height of the thumbnail, let the width auto-scaled to keep the source aspect ratio
      quality: 75,
    );
    return File(filePath!);
  }

  Future<String> uploadThumbnailImageToStorage(
      String storyID, String videoURL) async {
    Reference videoStorageRef =
        FirebaseStorage.instance.ref().child('stories_thumbnail');
    UploadTask uploadTask = videoStorageRef
        .child('$storyID')
        .putFile(await getVideoThumbnail(videoURL));
    TaskSnapshot taskSnapshot = await uploadTask;
    String imageURL = await taskSnapshot.ref.getDownloadURL();
    return imageURL;
  }
}
