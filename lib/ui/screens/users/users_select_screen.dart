import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/widgets/social/progress.dart';
import 'package:social_media/ui/widgets/user_item.dart';

import '../pets/pet_transfer_screen.dart';

class UsersSelectScreen extends StatefulWidget {
  MyUser sender;
  Pet pet;

  UsersSelectScreen({required this.sender, required this.pet});

  @override
  _UsersSelectScreenState createState() => _UsersSelectScreenState();
}

class _UsersSelectScreenState extends State<UsersSelectScreen> {
  String? searchName;
  FirebaseUserHelper userService = new FirebaseUserHelper();
  late Size size;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text("The heirs"),
      ),
      body: Container(
        child: Column(
          children: [
            contactSearchBar(),
            (searchName == null || searchName!.isEmpty)
                ? StreamBuilder(
                    stream: FirebaseFirestore.instance
                        .collection('users')
                        .where('id', isNotEqualTo: SignedAccount.instance.id)
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        QuerySnapshot querySnapshot = snapshot.data;
                        List<MyUser> allUserList = querySnapshot.docs
                            .map((document) =>
                                MyUser.fromDocumentSnapshot(document))
                            .toList();
                        return usersContactListView(allUserList);
                      } else {
                        return CircularProgress();
                      }
                    },
                  )
                : StreamBuilder(
                    stream: userService.getUserByDisplayName(searchName!),
                    builder: (BuildContext context,
                        AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        QuerySnapshot querySnapshot = snapshot.data;
                        List<MyUser> searchedUserList = querySnapshot.docs
                            .map((document) =>
                                MyUser.fromDocumentSnapshot(document))
                            .toList();
                        return usersContactListView(searchedUserList);
                      } else {
                        return CircularProgress();
                      }
                    },
                  ),
          ],
        ),
      ),
    );
  }

  Widget contactSearchBar() {
    return Container(
      height: size.height * 0.08,
      padding: EdgeInsets.symmetric(vertical: size.height * 0.02),
      child: TextField(
        autocorrect: false,
        style: TextStyle(color: Colors.blue),
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.search,
            color: Colors.blue,
          ),
          labelText: "Search",
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
        onSubmitted: (value) {
          setState(() {
            searchName = value;
            print(searchName);
          });
        },
      ),
    );
  }

  Widget usersContactListView(List<MyUser> userList) {
    return Container(
      height: size.height - (size.height * 0.2),
      child: ListView.builder(
        itemCount: userList.length,
        itemBuilder: (context, index) {
          MyUser user = userList[index];

          return UserItem(
              user: user,
              onClick: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => PetTransferScreen(
                        sender: widget.sender, pet: widget.pet, receiver: user),
                  ),
                );
              });
        },
      ),
    );
  }
}
