import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:list_tile_switch/list_tile_switch.dart';
import 'package:provider/provider.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/dark_theme_provider.dart';

import '../authentication/register_authentication_screen.dart';
import '../wallet/wallet_main_screen.dart';

class UserMainPreferenceScreen extends StatefulWidget {
  const UserMainPreferenceScreen({Key? key}) : super(key: key);

  @override
  _UserMainPreferenceScreenState createState() =>
      _UserMainPreferenceScreenState();
}

class _UserMainPreferenceScreenState extends State<UserMainPreferenceScreen> {
  ScrollController scrollController = new ScrollController();

  @override
  void initState() {
    scrollController = new ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    double deviceWidth = MediaQuery.of(context).size.width;
    DarkThemeProvider darkThemeProvider =
        Provider.of<DarkThemeProvider>(context);
    return Scaffold(
      body: CustomScrollView(
        controller: scrollController,
        slivers: [
          SliverAppBar(
            automaticallyImplyLeading: false,
            pinned: true,
            centerTitle: true,
            expandedHeight: 100,
            backgroundColor: Colors.redAccent,
            flexibleSpace: LayoutBuilder(
              builder: (context, constraints) {
                return Container(
                  color: AppColors.pinkBackground,
                  child: FlexibleSpaceBar(
                    collapseMode: CollapseMode.parallax,
                    centerTitle: true,
                    title: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        IconButton(
                          padding: EdgeInsets.all(0),
                          visualDensity: VisualDensity.compact,
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: FaIcon(
                            FontAwesomeIcons.chevronLeft,
                            color: AppColors.darkRedText,
                            size: 14,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 0.0),
                          child: Row(
                            children: [
                              Container(
                                height: kToolbarHeight / 1.5,
                                width: kToolbarHeight / 1.5,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.white, blurRadius: 1.0),
                                  ],
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(
                                        SignedAccount.instance.photoUrl!),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Text(
                                  SignedAccount.instance.displayName!,
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.darkBlueText),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: deviceHeight,
              padding: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                color: AppColors.pinkBackground,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 15, bottom: 10),
                    child: Text(
                      'Information',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                        color: AppColors.darkRedText,
                      ),
                    ),
                  ),
                  Container(
                    height: deviceHeight * 0.2,
                    width: deviceWidth,
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 1,
                          spreadRadius: 0.02,
                          offset: Offset(0.25, 0.25),
                        ),
                      ],
                    ),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15, right: 15, top: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: deviceHeight * 0.08,
                          width: deviceWidth * 0.45,
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 1,
                                spreadRadius: 0.02,
                                offset: Offset(0.25, 0.25),
                              ),
                            ],
                          ),
                          child: Container(
                            margin: EdgeInsets.all(0),
                            padding: EdgeInsets.all(0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.my_library_books_outlined,
                                      color: Colors.pinkAccent),
                                  onPressed: () {},
                                ),
                                Container(
                                  child: Text(
                                    'my order',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          height: deviceHeight * 0.08,
                          width: deviceWidth * 0.45,
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 1,
                                spreadRadius: 0.02,
                                offset: Offset(0.25, 0.25),
                              ),
                            ],
                          ),
                          child: Container(
                            margin: EdgeInsets.all(0),
                            padding: EdgeInsets.all(0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.favorite,
                                      color: Colors.pinkAccent),
                                  onPressed: () {},
                                ),
                                Container(
                                  child: Text(
                                    'my wishlist',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              RegisterAuthenticationScreen()));
                    },
                    child: Container(
                      height: deviceHeight * 0.08,
                      width: deviceWidth,
                      margin: EdgeInsets.only(top: 5, left: 15, right: 15),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: AppColors.veryFreshBlue, width: 2),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        boxShadow: [],
                      ),
                      child: Container(
                        margin: EdgeInsets.all(0),
                        padding: EdgeInsets.all(0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FaIcon(FontAwesomeIcons.userShield,
                                color: AppColors.veryFreshBlue),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'Authentication',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: AppColors.veryFreshBlue,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => WalletMainScreen(),
                        ),
                      );
                    },
                    child: Container(
                      height: deviceHeight * 0.08,
                      width: deviceWidth,
                      margin: EdgeInsets.only(top: 5, left: 15, right: 15),
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff7E85F9), width: 2),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        boxShadow: [],
                      ),
                      child: Container(
                        margin: EdgeInsets.all(0),
                        padding: EdgeInsets.all(0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FaIcon(FontAwesomeIcons.solidCreditCard,
                                color: Color(0xff7E85F9)),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'My Wallet',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Color(0xff7E85F9),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  ListTileSwitch(
                    value: darkThemeProvider.isDarkTheme,
                    leading: Icon(Icons.dark_mode_rounded),
                    onChanged: (value) {
                      setState(() {
                        darkThemeProvider.setDarkTheme(value);
                      });
                    },
                    visualDensity: VisualDensity.comfortable,
                    switchType: SwitchType.cupertino,
                    switchActiveColor: Colors.indigo,
                    title: Text('Theme'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
