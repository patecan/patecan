import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/notification/my_store_notification.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/notifications/store_notification_service.dart';

import '../../widgets/notifications/store_notification_item.dart';
import '../../widgets/social/progress.dart';

class StoreNotificationScreen extends StatefulWidget {
  @override
  _StoreNotificationScreenState createState() =>
      _StoreNotificationScreenState();
}

class _StoreNotificationScreenState extends State<StoreNotificationScreen> {
  @override
  void initState() {
    super.initState();
  }

  NotificationService storeNotificationService = new NotificationService();

  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(title: Text("Notification")),
      body: Container(
        child: FutureBuilder(
          future: FirebaseFirestore.instance
              .collection("notifications")
              .doc(SignedAccount.instance.id)
              .collection('storeNotifications')
              .get(),
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              QuerySnapshot querySnapshot = snapshot.data;
              List<Widget> notificationList = [];
              List<DocumentSnapshot> documentList = querySnapshot.docs;
              documentList.forEach((element) {
                MyStoreNotification myNotification =
                    MyStoreNotification.fromQueryDocument(element);

                print(myNotification);
                notificationList.add(StoreNotificationItem(myNotification));
              });

              return ListView(
                children: notificationList,
              );
            } else {
              print("snapshot data: ${snapshot.data}");
              return CircularProgress();
            }
          },
        ),
      ),
    );
  }
}
