import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/store/property_in_rent_list.dart';
import 'package:social_media/providers/rentlist_provider.dart';
import 'package:social_media/services/notifications/push_notification_service.dart';
import 'package:social_media/services/notifications/store_notification_service.dart';
import 'package:social_media/services/stores/order_service.dart';
import 'package:social_media/services/stores/properties_service.dart';
import 'package:social_media/ui/screens/stores/store_payment_selection_screen.dart';
import 'package:social_media/ui/widgets/gradient_button.dart';
import 'package:uuid/uuid.dart';

import '../../../models/store/my_address.dart';
import '../../../models/store/property.dart';
import '../../widgets/rounded_loading_button.dart';
import '../../widgets/store/ContainerPatternPainter.dart';
import '../../widgets/store/property_item/rent_list_item.dart';
import '../address_book/address_book_screen.dart';

class CheckoutScreen extends StatefulWidget {
  CheckoutScreen({Key? key}) : super(key: key);

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  Uuid uuid = new Uuid();
  RoundedLoadingButtonController controller =
      new RoundedLoadingButtonController();

  OrderService orderService = OrderService();
  PushNotificationService pushNotificationService = PushNotificationService();
  NotificationService storeNotificationService = NotificationService();

  late RentListProvider rentListProvider;
  late Map<String, PropertyInRentList> rentListList;
  late List<PropertyInRentList> productsInRentListList;

  initState() {
    super.initState();
  }

  didChangeDependencies() {
    rentListProvider = Provider.of<RentListProvider>(context);
    rentListList = rentListProvider.rentListItemList;
    productsInRentListList = rentListList.values.toList();
    super.didChangeDependencies();
  }

  FirebasePropertyService productService = FirebasePropertyService();

  late Size size;
  String paymentMethod = 'none';

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    MyAddress addressProvider = Provider.of<MyAddress>(context);

    AppBar appbar = AppBar(
      title: Text("Checkout"),
    );

    double deviceHeight = (size.height - MediaQuery.of(context).padding.top) -
        appbar.preferredSize.height;
    double deviceWidth = size.width;

    return Scaffold(
      appBar: appbar,
      body: Column(
        children: [
          GestureDetector(
            child: Container(
              height: deviceHeight * 0.14,
              width: deviceWidth,
              padding: EdgeInsets.only(left: 0, right: 0, top: 5),
              margin: EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  MyAddress.currentAddress != null
                      ? Positioned(
                          top: 0,
                          left: 10,
                          child: Container(
                            width: deviceWidth * 0.9,
                            child: ListTile(
                              leading:
                                  Icon(Icons.place, color: Colors.redAccent),
                              minLeadingWidth: 0,
                              contentPadding: EdgeInsets.all(0),
                              title: Text(
                                  MyAddress.currentAddress!.name.toString()),
                              subtitle: Text(
                                  MyAddress.currentAddress!.address.toString(),
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15)),
                            ),
                          ),
                        )
                      : Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            'Please select your address',
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                  Positioned(
                    right: 0,
                    top: ((deviceHeight * 0.12) / 2) / 2,
                    child: IconButton(
                      icon: Icon(Icons.arrow_forward_ios, size: 16),
                      onPressed: () {},
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      width: deviceWidth,
                      height: 4.0,
                      child: CustomPaint(
                        painter: ContainerPatternPainter(),
                        child: Container(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            onTap: () async {
              dynamic result = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AddressBookScreen()));
              if (result == 'setAddress') {
                setState(() {});
              }
            },
          ),
          Container(
            height: deviceHeight * (0.8),
            child: ListView.builder(
              itemCount: productsInRentListList.length,
              itemBuilder: (context, index) {
                return RentListItem(productsInRentListList[index],
                    productStages: ProductStages.IN_CHECKOUT);
              },
            ),
          ),
        ],
      ),
      bottomSheet: Container(
        width: deviceWidth,
        height: deviceHeight * 0.2,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15)),
          boxShadow: [
            BoxShadow(
              color: Colors.black,
              blurRadius: 3,
              spreadRadius: 0.005,
              offset: Offset(0.5, 0.5),
            ),
          ],
        ),
        child: Column(
          children: [
            GestureDetector(
              onTap: () async {
                this.paymentMethod = await Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => StorePaymentSelectionScreen(
                      totalPrice: rentListProvider.totalPrice,
                    ),
                  ),
                );

                if (this.paymentMethod != 'none') {
                  setState(() {});
                }
              },
              child: getPaymentMethod(this.paymentMethod),
            ),
            Spacer(),
            ListTile(
              minLeadingWidth: 0,
              contentPadding: EdgeInsets.all(0),
              leading: Container(
                height: 50,
                width: 250,
                margin: EdgeInsets.only(left: 15.0),
                child: GradientButton(
                  color1: Color(0xffFE97B5),
                  color2: Color(0xffFE97B5),
                  onPress: () async {
                    bool isProcessed = await orderService.setMyOrder(
                        productsInRentListList, rentListProvider,
                        isPaid: paymentMethod != 'COD');

                    if (isProcessed == true) {
                      rentListProvider.clear();
                      Navigator.pop(context);
                    }
                  },
                  child: Text(
                    "ORDER",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              trailing: Chip(
                  padding: EdgeInsets.only(right: 15),
                  label: Text(
                    "\$${rentListProvider.totalPrice}",
                    style: TextStyle(
                        fontSize: 25,
                        fontFamily: 'Nunito',
                        fontWeight: FontWeight.w600,
                        color: Color(0xffFE97B5)),
                  ),
                  backgroundColor: Theme.of(context).backgroundColor),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget getPaymentMethod(String paymentMethod) {
    switch (paymentMethod) {
      case 'card':
        return Container(
          height: size.height * 0.08,
          width: size.width,
          margin: EdgeInsets.only(top: 5, left: 15, right: 15),
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xff7E85F9), width: 2),
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            boxShadow: [],
          ),
          child: Container(
            margin: EdgeInsets.all(0),
            padding: EdgeInsets.all(0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FaIcon(FontAwesomeIcons.solidCreditCard,
                    color: Color(0xff7E85F9)),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Payment Method',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Color(0xff7E85F9),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      case 'e-wallet':
        return Container(
          height: size.height * 0.08,
          width: size.width,
          margin: EdgeInsets.only(top: 5, left: 15, right: 15),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.lightGreen, width: 2),
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            boxShadow: [],
          ),
          child: Container(
            margin: EdgeInsets.all(0),
            padding: EdgeInsets.all(0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FaIcon(
                  FontAwesomeIcons.checkCircle,
                  color: Colors.lightGreen,
                ),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Paid with E-Wallet',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.lightGreen,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      default:
        return Container(
          height: size.height * 0.08,
          width: size.width,
          margin: EdgeInsets.only(top: 5, left: 15, right: 15),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blueGrey, width: 2),
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            boxShadow: [],
          ),
          child: Container(
            margin: EdgeInsets.all(0),
            padding: EdgeInsets.all(0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FaIcon(FontAwesomeIcons.moneyBillWaveAlt,
                    color: Colors.blueGrey),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'No payment selected',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
    }
  }
}
