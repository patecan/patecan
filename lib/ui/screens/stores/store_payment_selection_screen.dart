import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/payment/payment_main_screen.dart';

class StorePaymentSelectionScreen extends StatefulWidget {
  double totalPrice;
  List<String> paymentMethods = ['COD', 'card', 'e-wallet'];

  StorePaymentSelectionScreen({required this.totalPrice, Key? key})
      : super(key: key);

  @override
  State<StorePaymentSelectionScreen> createState() =>
      _StorePaymentSelectionScreenState();
}

class _StorePaymentSelectionScreenState
    extends State<StorePaymentSelectionScreen> {
  late Size size;

  String? selectedPaymentMethod;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        height: size.height * 0.79,
        child: ListView.builder(
          itemCount: 3,
          itemBuilder: (context, index) {
            FaIcon icon;

            if (index == 0) {
              icon = FaIcon(FontAwesomeIcons.moneyBillWave);
            } else if (index == 1) {
              icon = FaIcon(FontAwesomeIcons.solidCreditCard);
            } else {
              icon = FaIcon(FontAwesomeIcons.wallet);
            }

            return Container(
              width: size.width,
              margin: EdgeInsets.only(top: 5, left: 10, right: 10),
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 0, right: 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(7.5)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 2.5,
                    spreadRadius: 0.025,
                    offset: Offset(1, 1),
                  ),
                ],
              ),
              child: RadioListTile<String>(
                title: icon,
                value: widget.paymentMethods[index],
                secondary: Text(widget.paymentMethods[index].toUpperCase()),
                groupValue: selectedPaymentMethod,
                onChanged: (String? value) async {
                  if (index == 0) {
                    setState(() {
                      selectedPaymentMethod = value!;
                    });
                  } else if (index == 1) {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => PaymentMainScreen(
                              from: 'checkout',
                              amountNeed: widget.totalPrice,
                            )));
                  } else if (index == 2) {
                    if (SignedAccount.instance.balance >= widget.totalPrice) {
                      AwesomeDialog(
                        context: context,
                        animType: AnimType.SCALE,
                        dialogType: DialogType.INFO,
                        body: Center(
                          child: Text(
                            'Pay ${widget.totalPrice}',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        title: 'Pay ${widget.totalPrice}',
                        desc: 'Pay ${widget.totalPrice}',
                        btnOkOnPress: () async {
                          await FirebaseFirestore.instance
                              .collection('users')
                              .doc(SignedAccount.instance.id)
                              .update({
                            'balance': FieldValue.increment(-widget.totalPrice)
                          });
                          setState(() {
                            selectedPaymentMethod = value!;
                          });

                          Navigator.of(context).pop('e-wallet');
                        },
                      )..show();
                    }
                  }
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
