import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/store/property.dart';
import 'package:social_media/models/store/property_in_rent_list.dart';
import 'package:social_media/providers/rentlist_provider.dart';
import 'package:social_media/services/stores/properties_service.dart';

import '../../widgets/rounded_loading_button.dart';
import '../../widgets/store/property_item/rent_list_item.dart';

class CartScreen extends StatefulWidget {
  CartScreen({Key? key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  RoundedLoadingButtonController controller =
      new RoundedLoadingButtonController();

  initState() {
    super.initState();
  }

  FirebasePropertyService productService = FirebasePropertyService();

  @override
  Widget build(BuildContext context) {
    RentListProvider rentListProvider = Provider.of<RentListProvider>(context);

    Map<String, PropertyInRentList> RentListList =
        rentListProvider.rentListItemList;
    List<PropertyInRentList> productInRentListList =
        RentListList.values.toList();

    AppBar appbar = AppBar(
      title: Text("Cart"),
    );

    double deviceHeight = (MediaQuery.of(context).size.height -
            MediaQuery.of(context).padding.top) -
        appbar.preferredSize.height;
    double deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: appbar,
      body: Container(
        height: deviceHeight * (0.8),
        child: ListView.builder(
          itemCount: productInRentListList.length,
          itemBuilder: (context, index) {
            return RentListItem(productInRentListList[index],
                productStages: ProductStages.IN_RentList);
          },
        ),
      ),
      bottomSheet: Container(
        width: deviceWidth,
        height: deviceHeight * 0.15,
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15)),
          boxShadow: [
            BoxShadow(
              color: Colors.black,
              blurRadius: 2.5,
              spreadRadius: 0.025,
              offset: Offset(0.5, 0.5),
            ),
          ],
        ),
        child: ListTile(
          minLeadingWidth: 0,
          contentPadding: EdgeInsets.all(0),
          leading: Container(
            height: 35,
            width: 200,
            child: RoundedLoadingButton(
              controller: controller,
              onPressed: () {
                controller.start();
                Navigator.of(context).pushNamed('/checkout_screen');
                controller.stop();
              },
              height: 35,
              color: Colors.redAccent,
              valueColor: Colors.white,
              width: 200,
              borderColor: Colors.redAccent,
              child: Text("CHECK OUT"),
            ),
          ),
          trailing: Chip(
              padding: EdgeInsets.only(right: 15),
              label: Text(
                "\$${rentListProvider.totalPrice}",
                style: TextStyle(fontSize: 18),
              ),
              labelStyle: TextStyle(color: Colors.redAccent),
              backgroundColor: Theme.of(context).backgroundColor),
        ),
      ),
    );
  }
}
