import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/providers/property_list_provider.dart';

import '../../../../models/store/property.dart';
import '../../../../models/users/singned_account.dart';
import '../../../../services/stores/properties_service.dart';
import '../../../widgets/social/progress.dart';
import '../../../widgets/store/property_item/my_property_item.dart';
import '../properties/upload_property_screen.dart';

class LessorMainScreen extends StatefulWidget {
  LessorMainScreen({Key? key}) : super(key: key);

  @override
  _LessorMainScreenState createState() => _LessorMainScreenState();
}

class _LessorMainScreenState extends State<LessorMainScreen> {
  FirebasePropertyService productService = FirebasePropertyService();
  List<Property> myProductsList = [];

  double deviceHeight = 0.0;
  double deviceWidth = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    PropertiesListProvider productsListProvider =
        Provider.of<PropertiesListProvider>(context);
    List<Property> myProductsList =
        productsListProvider.getPropertyList().where(
      (element) {
        if (element.ownerId != SignedAccount.instance.id) {
          return false;
        } else {
          return true;
        }
      },
    ).toList();

    deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    deviceWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            height: deviceHeight * 0.05,
            child: TextField(
              autocorrect: false,
              style: TextStyle(color: Colors.blue),
              decoration: new InputDecoration(
                filled: true,
                fillColor: Theme.of(context).backgroundColor,
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.blue,
                ),
                hintText: 'Product name...',
                contentPadding: const EdgeInsets.only(left: 17.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.transparent),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  borderSide: BorderSide(
                    color: Colors.transparent,
                    width: 1.0,
                  ),
                ),
              ),
              onSubmitted: (value) {
                setState(() {
                  // searchName = value;
                });
              },
            ),
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.add_business,
                  size: deviceWidth * 0.07, color: Colors.white),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UploadPropertyScreen(),
                  ),
                );
              },
            ),
          ],
        ),
        body: Container(
          height: deviceHeight * 0.9,
          width: deviceWidth,
          child: FutureBuilder(
            future: productService.findUserProductsList(),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData) {
                print(snapshot.data);
                productsListProvider.propertyList =
                    List<Property>.from(snapshot.data!);
                return Container(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: ListView.builder(
                    itemCount: productsListProvider.propertyList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: MyPropertyItem(
                              productsListProvider.propertyList[index]));
                    },
                  ),
                );
              }
              return CircularProgress();
            },
          ),
        ),
      ),
    );
  }
}
