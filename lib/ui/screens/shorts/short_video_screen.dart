import 'dart:async';
import 'dart:io';

import 'package:animator/animator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/short_videos/firebase_short_video_service.dart';
import 'package:social_media/ui/screens/shorts/short_video_comments_screen.dart';
import 'package:social_media/ui/screens/shorts/upload_short_video_screen.dart';
import 'package:social_media/ui/widgets/shorts/rotating_circle.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

import '../../widgets/shorts/short_video_player_item.dart';

class ShortVideoScreen extends StatefulWidget {
  const ShortVideoScreen({Key? key}) : super(key: key);

  @override
  _ShortVideoScreenState createState() => _ShortVideoScreenState();
}

class _ShortVideoScreenState extends State<ShortVideoScreen> {
  FirebaseShortVideoService shortVideoService = new FirebaseShortVideoService();

  /*______________ Like Feature _____________*/
  int likesCount = 0;
  bool isLiked = false;
  bool isHearted = false;

  /*______________ Comment Feature _____________*/
  int commentCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: StreamBuilder(
        stream:
            FirebaseFirestore.instance.collection('short_video').snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            QuerySnapshot querySnapshot = snapshot.data!;

            return PageView.builder(
              controller: PageController(initialPage: 0, viewportFraction: 1),
              itemCount: querySnapshot.docs.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                likesCount = querySnapshot.docs[index]['likes'].length;
                isLiked = querySnapshot.docs[index]['likes']
                    .contains(SignedAccount.instance.id);

                return Stack(
                  children: [
                    ShortVideoPlayerItem(
                      videoURL: querySnapshot.docs[index]['videoURL'],
                    ),
                    Column(
                      children: [
                        Container(
                          height: 50,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "Following",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        querySnapshot.docs[index]['caption'],
                                        style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 40,
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 8),
                                        child: Text(
                                          querySnapshot.docs[index]['userID'],
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.2,
                                height: size.height * 0.7,
                                margin: EdgeInsets.only(bottom: 70),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 60,
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            left: 5,
                                            child: Container(
                                              width: 50,
                                              height: 50,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(25),
                                              ),
                                              child: GestureDetector(
                                                onTap: () {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              UploadShortVideoScreen()));
                                                },
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  child: Image.network(
                                                    SignedAccount
                                                        .instance.photoUrl!,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                color: Colors.blue,
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                              ),
                                              child: Icon(Icons.add,
                                                  color: Colors.white,
                                                  size: 20),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 25),
                                    Container(
                                      child: Column(
                                        children: [
                                          GestureDetector(
                                            child: isLiked == true
                                                ? Icon(Icons.favorite,
                                                    size: 30,
                                                    color: Colors.pink)
                                                : Icon(Icons.favorite,
                                                    size: 30,
                                                    color: Colors.white),
                                            onTap: () async {
                                              await likeShortVideo(
                                                SignedAccount.instance.id!,
                                                querySnapshot.docs[index]['ID'],
                                                List<String>.from(querySnapshot
                                                    .docs[index]['likes']),
                                              );
                                            },
                                          ),
                                          Text(
                                            likesCount.toString(),
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 25),
                                    Container(
                                      child: Column(
                                        children: [
                                          IconButton(
                                            icon: Icon(Icons.comment,
                                                size: 30, color: Colors.white),
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ShortVideoCommentsScreen(
                                                    shortVideoID: querySnapshot
                                                        .docs[index]['ID'],
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                          StreamBuilder(
                                            stream: FirebaseFirestore.instance
                                                .collection('shorts')
                                                .doc(querySnapshot.docs[index]
                                                    ['ID'])
                                                .collection('comments')
                                                .snapshots(),
                                            builder: (context,
                                                AsyncSnapshot<dynamic>
                                                    snapshots) {
                                              if (snapshots.hasData) {
                                                commentCount =
                                                    snapshots.data!.docs.length;

                                                return Text(
                                                  commentCount.toString(),
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white),
                                                );
                                              } else {
                                                return Text(
                                                  commentCount.toString(),
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white),
                                                );
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 25),
                                    Container(
                                      child: Column(
                                        children: [
                                          IconButton(
                                            icon: Icon(Icons.reply,
                                                size: 30, color: Colors.white),
                                            onPressed: () async {
                                              CoolAlert.show(
                                                context: context,
                                                type: CoolAlertType.loading,
                                                text: "loading...",
                                              );

                                              await shareShortVideo(
                                                  querySnapshot.docs[index]
                                                      ['videoURL'],
                                                  SignedAccount.instance.id!);

                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      bottom: 5,
                      right: 17,
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 8, right: 5, top: 2, bottom: 2),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(3),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "Baby",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                ),
                                Text(
                                  querySnapshot.docs[index]['songName'],
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            RotatingCircle(
                              child: Container(
                                width: 35,
                                height: 35,
                                child: Column(
                                  children: [
                                    Container(
                                      width: 35,
                                      height: 35,
                                      padding: EdgeInsets.all(5.0),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          colors: [
                                            Colors.grey.shade700,
                                            Colors.grey
                                          ],
                                        ),
                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                      child: GestureDetector(
                                        onTap: () {},
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: Image.network(
                                            SignedAccount.instance.photoUrl!,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                );
              },
            );
          } else {
            return CircularProgress();
          }
        },
      ),
    );
  }

  createHeartAnimation() {
    Animator(
      duration: Duration(milliseconds: 300),
      tween: Tween(begin: 0.8, end: 1.4),
      curve: Curves.elasticOut,
      cycles: 0,
      builder: (BuildContext context, AnimatorState<dynamic> animatorState,
          Widget? child) {
        return Transform.scale(
            scale: animatorState.value,
            child: Icon(Icons.favorite, size: 80.0, color: Colors.red));
      },
    );

    setState(() {
      isHearted = true;
    });

    Timer(Duration(milliseconds: 500), () {
      setState(() {
        isHearted = false;
      });
    });
  }

  likeShortVideo(String likerID, String videoID, List<String> likesList) async {
    bool isUserLikedBefore = likesList.contains(SignedAccount.instance.id);

    if (isUserLikedBefore) {
      setState(
        () {
          likesCount--;
          this.isLiked = false;
          likesList.remove(likerID);
        },
      );
      await shortVideoService.removeLike(videoID, SignedAccount.instance.id!);
    } else if (!isUserLikedBefore) {
      setState(() {
        createHeartAnimation();
        likesCount++;
        this.isLiked = true;
        likesList.remove(likerID);
      });

      await shortVideoService.addLike(videoID, SignedAccount.instance.id!);
    }
  }

  shareShortVideo(String videoURL, String sharerID) async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = new File('$tempPath' + '.mp4');

    http.Response response = await http.get(Uri.parse(videoURL));

    if (response.statusCode >= 200 && response.statusCode < 300) {
      file.writeAsBytes(response.bodyBytes);
      Share.shareFiles([file.path],
          text: 'Short video', mimeTypes: ['video/mp4']);
    }
  }
}
