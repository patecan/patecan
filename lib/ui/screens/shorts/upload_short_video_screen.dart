import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_media/ui/screens/shorts/upload_short_video_confirming_screen.dart';

class UploadShortVideoScreen extends StatefulWidget {
  UploadShortVideoScreen({Key? key}) : super(key: key);

  @override
  _UploadShortVideoScreenState createState() => _UploadShortVideoScreenState();
}

class _UploadShortVideoScreenState extends State<UploadShortVideoScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        child: Align(
          alignment: Alignment.center,
          child: GestureDetector(
            child: Container(
              height: 100,
              child: Column(
                children: [
                  Icon(Icons.videocam, size: 50),
                  Text('Add video'),
                ],
              ),
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    children: [
                      SimpleDialogOption(
                          child: Text(
                            'Gallery',
                            style: TextStyle(),
                          ),
                          onPressed: () async {
                            await pickVideo(ImageSource.gallery);
                            Navigator.pop(context);
                          }),
                      SimpleDialogOption(
                          child: Text(
                            'Camera',
                            style: TextStyle(),
                          ),
                          onPressed: () async {
                            await pickVideo(ImageSource.camera);
                            Navigator.pop(context);
                          }),
                      SimpleDialogOption(
                        child: Text(
                          'Cancel',
                          style: TextStyle(),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }

  Future pickVideo(ImageSource source) async {
    var video = await ImagePicker().pickVideo(source: source);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UploadShortVideoConfirmingScreen(
          videoFile: File(video!.path),
          videoPath: video.path,
          videoSource: source,
        ),
      ),
    );
  }
}
