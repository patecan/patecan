import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/services/social_media/firebase_comment_service.dart';
import 'package:social_media/ui/widgets/header.dart';
import 'package:social_media/ui/widgets/social/comment_item.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

class ShortVideoCommentsScreen extends StatefulWidget {
  String shortVideoID;

  ShortVideoCommentsScreen({required this.shortVideoID, Key? key})
      : super(key: key);

  @override
  _ShortVideoCommentsScreenState createState() =>
      _ShortVideoCommentsScreenState();
}

class _ShortVideoCommentsScreenState extends State<ShortVideoCommentsScreen> {
  TextEditingController commentController = new TextEditingController();

  FirebaseCommentHelper commentService = new FirebaseCommentHelper();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header("Comments"),
      body: Column(
        children: [
          Expanded(child: CommentWidget(widget.shortVideoID)),
          Divider(),
          ListTile(
            title: TextFormField(
              controller: commentController,
              decoration: InputDecoration(
                labelText: "Write comment...",
              ),
            ),
            trailing: IconButton(
              icon: Icon(Icons.send),
              onPressed: () async {
                await commentService.addShortVideoComment(
                    widget.shortVideoID, commentController.text);
                commentController.clear();
              },
            ),
          ),
        ],
      ),
    );
  }
}

class CommentWidget extends StatelessWidget {
  String shortVideoID;

  CommentWidget(this.shortVideoID);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('shorts')
          .doc(shortVideoID)
          .collection('comments')
          .snapshots(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.docs.length > 0) {
            QuerySnapshot querySnapshot = snapshot.data;
            List<CommentItem> commentList = [];
            querySnapshot.docs.map(
              (comment) {
                commentList.add(CommentItem.fromDocumentSnapshot(comment));
              },
            ).toList();
            return ListView(
              children: commentList,
            );
          } else {
            return Container();
          }
        } else {
          return LinearProgress();
        }
      },
    );
  }
}
