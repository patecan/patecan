import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/pet_park/joypad.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/pet_park_chat_provider.dart';
import 'package:social_media/ui/screens/pet_park/pet_park_game.dart';

import '../../../models/pet_park/game_direction.dart';
import '../../../models/pet_park/pet_park_room.dart';
import '../../../models/pet_park/player.dart';
import '../pets/pet_profile_screen.dart';
import '../users/user_profile_screen.dart';

class PetParkMainScreen extends StatefulWidget {
  @override
  State<PetParkMainScreen> createState() => _PetParkMainScreenState();
}

class _PetParkMainScreenState extends State<PetParkMainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("pet park")),
      body: Container(child: PetParkGameWidget()),
    );
  }
}

class PetParkGameWidget extends StatefulWidget {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  State<PetParkGameWidget> createState() => _PetParkGameWidgetState();
}

class _PetParkGameWidgetState extends State<PetParkGameWidget> {
  late PetParkGame petParkGame;

  @override
  void initState() {
    petParkGame = PetParkGame(tap);
    super.initState();
  }

  void tap({required String ownerId, String? petId}) {
    if (petId == null) {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => UserProfileScreen(ownerId)));
    } else {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) =>
              PetProfileScreen(petId: petId, ownerId: ownerId)));
    }
  }

  @override
  Widget build(BuildContext context) {
    var recentJobsRef = FirebaseDatabase.instance.reference().child('pet_park');

    Player otherPlayer = Player(tap, 'testerid');

    petParkGame.otherPlayer = otherPlayer;
    petParkGame.otherPlayer!.petParkRoomUserMainPlayer = PetParkRoomUser(
      petParkRoomId: '123',
      petParkRoomName: 'Patecan',
      userId: 'testerid',
      timeJoined: DateTime.now(),
      userData: {},
    );

    if (SignedAccount.instance.id! != 'testerid') {
      return StreamBuilder(
        stream: recentJobsRef.onValue,
        builder: (BuildContext context, AsyncSnapshot<Event> snapshot) {
          if (snapshot.hasData) {
            Map<dynamic, dynamic> data = snapshot.data!.snapshot.value;

            var otherPlayersEntries = data.entries
                .where((element) => element.key != SignedAccount.instance.id!)
                .toList();

            Map<String, dynamic> otherPlayersMap = {};

            otherPlayersEntries.forEach((entry) {
              otherPlayersMap[entry.key as String] = entry.value;
            });

            print(otherPlayersMap);

            // List<Map<String, dynamic>> otherPlayersMap =
            //     List<Map<String, dynamic>>.from(otherPlayersEntries
            //         .map((entry) => {entry.key as String: entry.value})
            //         .toList());

            Map<String, dynamic> testerData =
                Map<String, dynamic>.from(otherPlayersMap['testerid']);

            petParkGame.otherPlayer!.petParkRoomUserMainPlayer!.userData =
                testerData;

            petParkGame.otherPlayer!.direction = EnumToString.fromString(
                GameDirection.values,
                testerData['direction'] == null
                    ? 'none'
                    : testerData['direction']!)!;

            petParkGame.otherPlayer!.position =
                Vector2(testerData['x'], testerData['y']);

            if (petParkGame.otherPetInGameList.isNotEmpty) {
              petParkGame.otherPetInGameList.forEach((petInGame) {
                petInGame.direction = petParkGame.otherPlayer!.direction;
                petInGame.position = new Vector2(
                    (petParkGame.otherPetInGameList.indexOf(petInGame) == 0
                            ? petParkGame.otherPlayer!.position.x - 10
                            : petParkGame.otherPlayer!.position.x - 5) -
                        (30 *
                            (petParkGame.otherPetInGameList.indexOf(petInGame) +
                                1)),
                    petParkGame.otherPlayer!.position.y);
              });
            }
          }

          return Stack(
            children: [
              GameWidget(
                game: petParkGame,
                overlayBuilderMap: {
                  TEXT_FIELD: (_, PetParkGame game) => _chatBoxTextFormField()
                },
                initialActiveOverlays: [TEXT_FIELD],
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.all(32.0),
                  child: Joypad(
                      onDirectionChanged: petParkGame.onJoyPadDirectionChanged),
                ),
              ),
            ],
          );
        },
      );
    } else {
      return Stack(
        children: [
          GameWidget(
            game: petParkGame,
            overlayBuilderMap: {
              TEXT_FIELD: (_, PetParkGame game) => _chatBoxTextFormField()
            },
            initialActiveOverlays: [TEXT_FIELD],
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Joypad(
                  onDirectionChanged: petParkGame.onJoyPadDirectionChanged),
            ),
          ),
        ],
      );
    }

    // return StreamBuilder(
    //   stream: recentJobsRef.onValue,
    //   builder: (BuildContext context, AsyncSnapshot<Event> event) {
    //     if (event.hasData) {
    //       Map<String, dynamic> data =
    //           Map<String, dynamic>.from(event.data!.snapshot.value);
    //
    //       print(data);
    //       petParkGame.otherPlayer!.petParkRoomUserMainPlayer!.userData = data;
    //
    //       petParkGame.otherPlayer!.direction = EnumToString.fromString(
    //           GameDirection.values,
    //           data['direction'] == null ? 'none' : data['direction']!)!;
    //
    //       petParkGame.otherPlayer!.position =
    //           Vector2(data['x'] * 1.0, data['y'] * 1.0);
    //     }
    //
    //   },
    // );
  }
}

Widget _chatBoxTextFormField() {
  const String _hintText = 'Chat with other Sen';
  return Align(
    alignment: Alignment.bottomCenter,
    child: Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(0), color: Colors.white),
      margin: EdgeInsets.only(bottom: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Consumer<ChatProvider>(
          builder: (_, chatProvider, __) {
            return Form(
              key: PetParkGameWidget._formKey,
              child: TextFormField(
                cursorColor: Colors.black,
                controller: chatProvider.textEditingController,
                onFieldSubmitted: (_) async {
                  await FirebaseDatabase.instance
                      .reference()
                      .child('pet_park')
                      .child(SignedAccount.instance.id!)
                      .set({
                    'message':
                        chatProvider.textEditingController.value.text.trim()
                  });

                  await chatProvider.onMessageSent();
                },
                style: TextStyle(
                    fontSize: 15, color: Colors.black.withOpacity(0.7)),
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(0),
                    hintStyle: TextStyle(fontSize: 15, color: Colors.black),
                    fillColor: Colors.white,
                    filled: true),
              ),
            );
          },
        ),
      ),
    ),
  );
}
