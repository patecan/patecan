import 'dart:ui';

import 'package:firebase_database/firebase_database.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:social_media/models/pet_park/game_direction.dart';
import 'package:social_media/models/pet_park/pet_ingame.dart';
import 'package:social_media/models/pet_park/pet_park_room.dart';
import 'package:social_media/models/pet_park/player.dart';
import 'package:social_media/models/pet_park/world.dart';
import 'package:social_media/models/pet_park/world_collidable.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/pet_park/map_loader.dart';

const TEXT_FIELD = "TEXT_FIELD";

class PetParkGame extends FlameGame
    with HasCollidables, KeyboardEvents, HasTappables {
  Player? otherPlayer;
  String message = '';
  bool runOnLoaded = false;
  Function tap;
  late Player mainPlayer;

  List<PetInGame> petInGameList = [];
  List<PetInGame> otherPetInGameList = [];

  DatabaseReference realtimeDB = FirebaseDatabase.instance.reference();

  World _world = World();

  PetParkGame(this.tap);

  @override
  Future<void> onLoad() async {
    runOnLoaded = true;
    print('onLoad');

    mainPlayer = Player(this.tap, SignedAccount.instance.id!);

    await add(_world);
    await add(mainPlayer);

    await mainPlayer.loadPetInGameList(petInGameList);

    camera.followComponent(mainPlayer,
        worldBounds: Rect.fromLTRB(0, 0, _world.size.x, _world.size.y));

    mainPlayer.petParkRoomUserMainPlayer = PetParkRoomUser(
      petParkRoomId: '123',
      petParkRoomName: 'Patecan',
      userId: mainPlayer.myUser.id!,
      timeJoined: DateTime.now(),
      userData: {},
    );

    addWorldCollision();

    DataSnapshot mainPlayerSnapshot = await realtimeDB
        .child('pet_park')
        .child(mainPlayer.petParkRoomUserMainPlayer!.userId)
        .once();

    if (mainPlayerSnapshot.value == null) {
      mainPlayer.position = _world.size / 2;

      mainPlayer.petParkRoomUserMainPlayer!.userData = {
        'x': mainPlayer.position.x,
        'y': mainPlayer.position.y,
      };

      await realtimeDB
          .child('pet_park')
          .child(mainPlayer.petParkRoomUserMainPlayer!.userId)
          .set(mainPlayer.petParkRoomUserMainPlayer!.userData);
    } else {
      print('MAIN PLAYER EVENT');

      Map<dynamic, dynamic> snapshotValues = mainPlayerSnapshot.value;

      mainPlayer.position =
          Vector2(snapshotValues['x'] * 1.0, snapshotValues['y'] * 1.0);

      if (snapshotValues['message'].toString().trim().isNotEmpty) {
        String newMessage = snapshotValues['message'].toString();

        await mainPlayer
            .showMessage(snapshotValues['message'].toString().trim());

        if (newMessage != message) {
          message = newMessage;
        }
      }
    }

    for (PetInGame _pet in petInGameList) {
      print("PetInGame: ${_pet.petId}");
      add(_pet);
      _pet.position = new Vector2(
          (petInGameList.indexOf(_pet) == 0
                  ? mainPlayer.position.x - 10
                  : mainPlayer.position.x - 5) -
              (30 * (petInGameList.indexOf(_pet) + 1)),
          mainPlayer.position.y);

      print(
          "position pet ${petInGameList.indexOf(_pet) + 1}: ${_pet.position}");
    }

    if (SignedAccount.instance.id! != 'testerid') {
      await add(this.otherPlayer!);

      await otherPlayer!.loadPetInGameList(otherPetInGameList);

      for (PetInGame _otherPet in otherPetInGameList) {
        add(_otherPet);
        _otherPet.position = new Vector2(
            (otherPetInGameList.indexOf(_otherPet) == 0
                    ? this.otherPlayer!.position.x - 10
                    : this.otherPlayer!.position.x - 5) -
                (30 * (otherPetInGameList.indexOf(_otherPet) + 1)),
            this.otherPlayer!.position.y);
      }
    }

    realtimeDB.child('pet_park').child(this.mainPlayer.userId).onValue.listen(
      (Event event) async {
        Map data = event.snapshot.value;

        if (data['message'].toString().trim().isNotEmpty) {
          String newMessage = data['message'].toString();
          if (newMessage != message) {
            message = newMessage;
            mainPlayer.showMessage(data['message'].toString().trim());
            print("MESSAGE: ${message}");
          }
        }
      },
    );

    // if (otherPlayerSnapshot.value == null) {
    //   otherPlayer!.position = _world.size / 2;
    //
    //   otherPlayer!.petParkRoomUserMainPlayer!.userData = {
    //     'x': otherPlayer!.position.x,
    //     'y': otherPlayer!.position.y,
    //   };
    //
    //   await realtimeDB
    //       .child('pet_park')
    //       .child('testerid')
    //       .set(otherPlayer!.petParkRoomUserMainPlayer!.userData);
    // } else {
    //   print('OTHER PLAYER EVENT');
    //   Map<dynamic, dynamic> _snapshotValues = otherPlayerSnapshot.value;
    //
    //   otherPlayer!.position =
    //       Vector2(_snapshotValues['x'] * 1.0, _snapshotValues['y'] * 1.0);
    // }

    // for (PetInGame _pet in otherPetInGameList) {
    //   add(_pet);
    //   _pet.position = new Vector2(
    //       (otherPetInGameList.indexOf(_pet) == 0
    //               ? this.otherPlayer!.position.x - 10
    //               : this.otherPlayer!.position.x - 5) -
    //           (30 * (otherPetInGameList.indexOf(_pet) + 1)),
    //       this.otherPlayer!.position.y);
    // }

    super.onLoad();
  }

  @override
  void onMount() {
    super.onMount();
  }

  void addWorldCollision() async =>
      (await MapLoader.readRayWorldCollisionMap()).forEach((rect) {
        add(WorldCollidable()
          ..position = Vector2(rect.left, rect.top)
          ..width = rect.width
          ..height = rect.height);
      });

  void onJoyPadDirectionChanged(GameDirection direction) {
    mainPlayer.direction = direction;

    petInGameList.forEach((pet) {
      pet.direction = direction;
    });
  }

  WorldCollidable createWorldCollidable(Rect rect) {
    final collidable = WorldCollidable();
    collidable.position = Vector2(rect.left, rect.top);
    collidable.width = rect.width;
    collidable.height = rect.height;
    return collidable;
  }

  @override
  KeyEventResult onKeyEvent(
      RawKeyEvent event, Set<LogicalKeyboardKey> keysPressed) {
    final isKeyDown = event is RawKeyDownEvent;
    GameDirection? keyDirection = null;

    if (event.logicalKey == LogicalKeyboardKey.keyA) {
      keyDirection = GameDirection.left;
    } else if (event.logicalKey == LogicalKeyboardKey.keyD) {
      keyDirection = GameDirection.right;
    } else if (event.logicalKey == LogicalKeyboardKey.keyW) {
      keyDirection = GameDirection.up;
    } else if (event.logicalKey == LogicalKeyboardKey.keyS) {
      keyDirection = GameDirection.down;
    }

    if (isKeyDown && keyDirection != null) {
      mainPlayer.direction = keyDirection;
    } else if (mainPlayer.direction == keyDirection) {
      mainPlayer.direction = GameDirection.none;
    }

    return super.onKeyEvent(event, keysPressed);
  }
}
