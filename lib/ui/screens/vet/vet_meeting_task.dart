import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/services/vet/time_utils.dart';
import 'package:social_media/ui/screens/vet/vet_meeting.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_datasource.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_provider.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_viewing_screen.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class VetMeetingTasks extends StatefulWidget {
  const VetMeetingTasks({Key? key}) : super(key: key);

  @override
  State<VetMeetingTasks> createState() => _VetMeetingTasksState();
}

class _VetMeetingTasksState extends State<VetMeetingTasks> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<VetMeetingProvider>(context);
    final selectedVetMeeting = provider.vetMeetingList;

    if (selectedVetMeeting.isEmpty) {
      return Center(
        child: Text("No meeting found!"),
      );
    }

    return SfCalendarTheme(
      data: SfCalendarThemeData(
          timeTextStyle: TextStyle(fontSize: 10, color: Colors.black)),
      child: SfCalendar(
        view: CalendarView.day,
        dataSource: VetMeetingDatasource(provider.vetMeetingList),
        initialDisplayDate: provider.selectedDate,
        headerHeight: 0,
        appointmentBuilder: (context, CalendarAppointmentDetails details) {
          VetMeeting meeting = details.appointments.first;
          return Container(
            width: details.bounds.width,
            height: details.bounds.height,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Color(int.parse(meeting.color)),
                borderRadius: BorderRadius.circular(12.5)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    meeting.content,
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      TimeUtils.toTime(meeting.from),
                      style: TextStyle(fontSize: 13, color: Colors.white),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5, right: 5),
                      child: Text(
                        '-',
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Text(
                      TimeUtils.toTime(meeting.from),
                      style: TextStyle(fontSize: 13, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
        onTap: (details) {
          if (details.appointments == null) return;

          final meeting = details.appointments!.first;
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => VetMeetingViewingScreen(
                vetMeeting: meeting,
              ),
            ),
          );
        },
      ),
    );
  }
}
