import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:group_button/group_button.dart';
import 'package:provider/provider.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/vet/vet_meeting_service.dart';
import 'package:social_media/ui/screens/vet/vet_meeting.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_provider.dart';
import 'package:uuid/uuid.dart';

import '../../../../services/vet/time_utils.dart';
import '../../../models/users/my_user.dart';
import '../../widgets/messages/contact_user_item.dart';

class CreateVetMeetingScreen extends StatefulWidget {
  VetMeeting? vetMeeting;

  CreateVetMeetingScreen({this.vetMeeting, Key? key}) : super(key: key);

  @override
  State<CreateVetMeetingScreen> createState() => _CreateVetMeetingScreenState();
}

class _CreateVetMeetingScreenState extends State<CreateVetMeetingScreen> {
  bool isEditing = false;
  VetMeetingService _vetMeetingService = VetMeetingService();
  late String _vetMeetingId;

  GlobalKey<FormState> _addVetMeetingForm = GlobalKey<FormState>();
  TextEditingController _meetingTitleController = TextEditingController();
  TextEditingController _participantsController = TextEditingController();
  List<MyUser> _meetingMembersList = [
    MyUser.fromSignedInAccount(SignedAccount.instance)
  ];

  DateTime? fromDate;
  DateTime? toDate;

  @override
  void initState() {
    if (widget.vetMeeting == null) {
      _vetMeetingId = new Uuid().v4();

      fromDate = DateTime.now();
      toDate = DateTime.now().add(Duration(hours: 2));
    } else {
      isEditing = true;

      _vetMeetingId = widget.vetMeeting!.id;

      fromDate = widget.vetMeeting!.from;
      toDate = widget.vetMeeting!.to;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: FaIcon(FontAwesomeIcons.chevronLeft),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[Color(0xffFAC0D1), Color(0xffFE97B5)],
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: FaIcon(FontAwesomeIcons.check),
            onPressed: () {
              if (isEditing == false) {
                createVetMeeting();
              } else {
                editVetMeeting();
              }
            },
          )
        ],
      ),
      body: Form(
        key: _addVetMeetingForm,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 10),
                child: TextFormField(
                  controller: _meetingTitleController,
                  style: TextStyle(color: Colors.indigo),
                  decoration: new InputDecoration(
                    filled: true,
                    fillColor: Theme.of(context).backgroundColor,
                    hintText: 'Meeting title',
                    contentPadding: EdgeInsets.only(top: 17, left: 17.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: new BorderSide(
                          color: AppColors.blueWithPink, width: 2),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(33)),
                      borderSide: BorderSide(
                        color: AppColors.blueWithPink,
                        width: 2.0,
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter meeting title';
                    }
                    return null;
                  },
                  onSaved: (value) {},
                  cursorColor: Colors.indigo,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'From',
                  style: TextStyle(
                      color: AppColors.veryFreshBlue,
                      fontSize: 17.5,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: ListTile(
                      title: Text(TimeUtils.toDate(fromDate!)),
                      trailing: Icon(Icons.arrow_drop_down),
                      onTap: () async {
                        DateTime? date = await pickDate(initialDate: fromDate!);
                        if (date != null) {
                          setState(() {
                            fromDate = date;

                            if (date.isAfter(toDate!)) {
                              toDate = fromDate;
                            }
                          });
                        }
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: ListTile(
                      title: Text(TimeUtils.toTime(fromDate!)),
                      trailing: Icon(Icons.arrow_drop_down),
                      onTap: () async {
                        print(fromDate);
                        DateTime? time = await pickTime(initialDate: fromDate!);

                        if (time != null) {
                          setState(() {
                            fromDate = time;

                            if (time.isAfter(toDate!)) {
                              toDate = fromDate;
                            }
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'To',
                  style: TextStyle(
                      color: AppColors.veryFreshBlue,
                      fontSize: 17.5,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: ListTile(
                      title: Text(TimeUtils.toDate(toDate!)),
                      trailing: Icon(Icons.arrow_drop_down),
                      onTap: () async {
                        DateTime? date = await pickDate(
                            initialDate: toDate!, firstDate: fromDate);
                        if (date != null) {
                          setState(() {
                            toDate = date;

                            if (date.isBefore(fromDate!)) {
                              fromDate = toDate;
                            }
                          });
                        }
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: ListTile(
                      title: Text(TimeUtils.toTime(toDate!)),
                      trailing: Icon(Icons.arrow_drop_down),
                      onTap: () async {
                        DateTime? time = await pickTime(initialDate: toDate!);
                        if (time != null) {
                          setState(() {
                            toDate = time;

                            if (time.isBefore(fromDate!)) {
                              fromDate = toDate;
                            }
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  GroupButton(
                    controller: GroupButtonController(),
                    buttons: ['Everyday', 'Every Week'],
                    borderRadius: BorderRadius.circular(5.0),
                    buttonHeight: 50,
                    buttonWidth: 150,
                    onSelected: (i, selected) =>
                        debugPrint('Button #$i $selected'),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Theme.of(context).backgroundColor,
                        hintText: 'Participant username',
                        contentPadding:
                            EdgeInsets.only(left: 17.0, top: 8, bottom: 8),
                        isDense: true,
                        focusedBorder: OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.blue),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.blue,
                            width: 1.0,
                          ),
                        ),
                      ),
                      controller: _participantsController,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      FirebaseFirestore.instance
                          .collection('users')
                          .where('userRole', isEqualTo: 'vet')
                          .where('username',
                              isEqualTo: _participantsController.text)
                          .get()
                          .then((querySnapshot) {
                        querySnapshot.docs.forEach((document) {
                          setState(() {
                            _meetingMembersList
                                .add(MyUser.fromDocumentSnapshot(document));
                          });
                        });
                        _participantsController.clear();
                      });
                    },
                    child: Text('Add'),
                  ),
                ],
              ),
              if (_meetingMembersList.length > 0)
                SingleChildScrollView(
                  child: Container(
                    height: 200,
                    child: ListView.builder(
                      itemCount: _meetingMembersList.length,
                      itemBuilder: (context, index) {
                        return ContactUserItem(
                          user: _meetingMembersList[index],
                          from: 'none',
                        );
                      },
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Future<DateTime?> pickDate(
      {required DateTime initialDate, DateTime? firstDate}) async {
    final date = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: firstDate ?? DateTime(2022, 1),
        lastDate: DateTime(2024));

    if (date == null) return null;

    final time = Duration(hours: initialDate.hour, minutes: initialDate.minute);

    return date.add(time);
  }

  Future<DateTime?> pickTime(
      {required DateTime initialDate, DateTime? firstDate}) async {
    final timeOfDate = await showTimePicker(
        context: context, initialTime: TimeOfDay.fromDateTime(initialDate));

    if (timeOfDate == null) return null;

    final date = DateTime(initialDate.year, initialDate.month, initialDate.day);

    final time = Duration(hours: timeOfDate.hour, minutes: timeOfDate.minute);

    return date.add(time);
  }

  Future<void> createVetMeeting() async {
    if (_addVetMeetingForm.currentState!.validate()) {
      final vetMeeting = VetMeeting(
          id: _vetMeetingId,
          content: '',
          from: fromDate!,
          to: toDate!,
          members: _meetingMembersList.map((member) => member.id!).toList());

      var provider = Provider.of<VetMeetingProvider>(context, listen: false);
      provider.addVetMeeting(vetMeeting);

      await _vetMeetingService.sendVetMeetingInvite(
          member: (_meetingMembersList.firstWhere(
              (member) => member.id != SignedAccount.instance.id)).id!,
          vetMeeting: vetMeeting);

      Navigator.of(context).pop();
    }
  }

  void editVetMeeting() {
    // if (_addVetMeetingForm.currentState!.validate()) {
    //   final newVetMeeting = VetMeeting(
    //       id: widget.vetMeeting!.id, content: '', from: fromDate!, to: toDate!);
    //
    //   var provider = Provider.of<VetMeetingProvider>(context, listen: false);
    //   provider.editVetMeeting(newVetMeeting, widget.vetMeeting!);
    //
    //   Navigator.of(context).pop();
    // }
  }
}
