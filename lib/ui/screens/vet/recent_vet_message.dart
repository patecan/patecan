import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/messages/conversation.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/messages/message_service.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

import '../../widgets/vet/vet_message_item.dart';

class RecentVetMessageScreen extends StatefulWidget {
  RecentVetMessageScreen({Key? key}) : super(key: key);

  @override
  _RecentVetMessageScreenState createState() => _RecentVetMessageScreenState();
}

class _RecentVetMessageScreenState extends State<RecentVetMessageScreen> {
  MessageService messageService = new MessageService();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      child: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('vet_messages')
            .where('members', arrayContains: SignedAccount.instance.id)
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshotConversations) {
          if (snapshotConversations.hasData) {
            QuerySnapshot conversationSnapshot = snapshotConversations.data;

            print("CURRENT CONVERSATIONS: ${conversationSnapshot.docs}");

            List<Map<String, dynamic>> listMessage =
                conversationSnapshot.docs.map((document) {
              Map<String, dynamic> data =
                  document.data() as Map<String, dynamic>;

              Conversation conversation = Conversation.fromDocument(document);

              Message lastMessage = conversation.messages.last;

              return {
                'member': data['members']
                    .where((memberId) => memberId != SignedAccount.instance.id!)
                    .toList()
                    .first,
                'message': lastMessage
              };
            }).toList();

            return Container(
              child: ListView.builder(
                itemCount: listMessage.length,
                itemBuilder: (context, index) {
                  return Container(
                    child: VetMessageItem(listMessage[index]['member'],
                        listMessage[index]['message']),
                  );
                },
              ),
            );
          }
          return LinearProgress();
        },
      ),
    );
  }
}
