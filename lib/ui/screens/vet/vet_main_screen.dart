import 'package:flutter/material.dart';
import 'package:social_media/constant/user_roles.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/vet/vet_user/vet_user_homepage.dart';
import 'package:social_media/ui/screens/vet/vet_veterinarian/vet_veterinarian_homepage.dart';

class VetMainScreen extends StatefulWidget {
  const VetMainScreen({Key? key}) : super(key: key);

  @override
  State<VetMainScreen> createState() => _VetMainScreenState();
}

class _VetMainScreenState extends State<VetMainScreen> {
  @override
  Widget build(BuildContext context) {
    if (SignedAccount.instance.userRoles == UserRoles.vet) {
      return VetVeterinarianHomepage();
    } else {
      return VetUserHomepage();
    }
  }
}
