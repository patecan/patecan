import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_datasource.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_provider.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_task.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class CalendarWidget extends StatefulWidget {
  const CalendarWidget({Key? key}) : super(key: key);

  @override
  State<CalendarWidget> createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget> {
  @override
  Widget build(BuildContext context) {
    final vetMeetingList =
        Provider.of<VetMeetingProvider>(context).vetMeetingList;

    return SfCalendar(
      view: CalendarView.month,
      backgroundColor: Color(0xffFFADAD),
      appointmentTextStyle: TextStyle(color: Colors.white),
      headerStyle: CalendarHeaderStyle(
          textStyle:
              TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
      viewHeaderStyle: ViewHeaderStyle(
          dayTextStyle:
              TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          dateTextStyle:
              TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
      monthViewSettings: MonthViewSettings(
          monthCellStyle: MonthCellStyle(
              textStyle:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              leadingDatesTextStyle:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              trailingDatesTextStyle:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
          agendaStyle: AgendaStyle(
              dayTextStyle:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              dateTextStyle:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              appointmentTextStyle:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
      dataSource: VetMeetingDatasource(vetMeetingList),
      initialSelectedDate: DateTime.now(),
      cellBorderColor: Colors.transparent,
      onTap: (details) {
        final provider =
            Provider.of<VetMeetingProvider>(context, listen: false);

        provider.selectedDate = details.date!;
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          builder: (context) => Container(
            height: MediaQuery.of(context).size.height * 0.85,
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 0),
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(40.0),
                topRight: const Radius.circular(40.0),
              ),
            ),
            child: VetMeetingTasks(),
          ),
        );
      },
    );
  }
}
