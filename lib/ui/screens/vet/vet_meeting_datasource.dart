import 'package:social_media/ui/screens/vet/vet_meeting.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class VetMeetingDatasource extends CalendarDataSource {
  VetMeetingDatasource(List<VetMeeting> appointments) {
    this.appointments = appointments;
  }

  VetMeeting getVetMeeting(int index) {
    return appointments![index] as VetMeeting;
  }

  @override
  DateTime getStartTime(int index) {
    return (appointments![index] as VetMeeting).from;
  }

  @override
  DateTime getEndTime(int index) {
    return (appointments![index] as VetMeeting).to;
  }

  @override
  String getSubject(int index) {
    return (appointments![index] as VetMeeting).content;
  }
}
