import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/ui/screens/vet/vet_veterinarian/add_free_time_screen.dart';
import 'package:social_media/ui/screens/vet/vet_veterinarian/vet_meeting_notification_screen.dart';

import '../../../../constant/app_colors.dart';
import '../recent_vet_message.dart';
import '../vet_meeting_room/vet_meeting_room_main_screen.dart';

class VetVeterinarianHomepage extends StatefulWidget {
  const VetVeterinarianHomepage({Key? key}) : super(key: key);

  @override
  State<VetVeterinarianHomepage> createState() =>
      _VetVeterinarianHomepageState();
}

class _VetVeterinarianHomepageState extends State<VetVeterinarianHomepage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this, initialIndex: 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    AppBar appBar = AppBar(
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[Color(0xffFAC0D1), Color(0xffFE97B5)],
          ),
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      title: Text(
        'Veterinarian',
        style:
            TextStyle(color: Colors.white, fontFamily: "Nunito", fontSize: 20),
      ),
      centerTitle: true,
      bottom: TabBar(
        controller: _tabController,
        unselectedLabelColor: Colors.white,
        indicatorColor: Color(0xffA1DEFA),
        labelColor: Color(0xffA1DEFA),
        indicatorWeight: 3.0,
        tabs: [
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.commentMedical,
            ),
          ),
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.solidEnvelope,
            ),
          ),
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.solidCalendarCheck,
            ),
          ),
        ],
      ),
      actions: [
        GestureDetector(
          onTap: () {},
          child: Container(
            margin: EdgeInsets.only(right: 3),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                topLeft: Radius.circular(15),
                bottomRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurStyle: BlurStyle.outer,
                  blurRadius: 5,
                  offset: Offset(0.5, 0.5),
                ),
              ],
            ),
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: GestureDetector(
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => AddFreeTimeScreen())),
                    child: FaIcon(FontAwesomeIcons.businessTime,
                        size: 25, color: AppColors.blueWithPink),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: Row(
                    children: [
                      FaIcon(FontAwesomeIcons.solidCalendar,
                          size: 25, color: AppColors.blueWithPink),
                      FaIcon(FontAwesomeIcons.plus,
                          size: 15, color: AppColors.blueWithPink),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: SafeArea(
        child: TabBarView(
          controller: _tabController,
          children: [
            RecentVetMessageScreen(),
            VetMeetingNotificationScreen(),
            VetMeetingRoomMainScreen(),
          ],
        ),
      ),
    );
  }
}
