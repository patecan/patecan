import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/notifications/store_notification_service.dart';

import '../../../../models/notification/my_vet_meeting_notification.dart';
import '../../../widgets/notifications/vet_meeting_notification_item.dart';
import '../../../widgets/social/progress.dart';

class VetMeetingNotificationScreen extends StatefulWidget {
  @override
  _VetMeetingNotificationScreenState createState() =>
      _VetMeetingNotificationScreenState();
}

class _VetMeetingNotificationScreenState
    extends State<VetMeetingNotificationScreen> {
  @override
  void initState() {
    super.initState();
  }

  NotificationService storeNotificationService = new NotificationService();

  @override
  Widget build(context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.skinBackground,
      ),
      child: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection("vet_meeting_invites")
            .where('userInvited', isEqualTo: SignedAccount.instance.id)
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          List<Widget> notificationList = [];
          if (snapshot.hasData) {
            QuerySnapshot querySnapshot = snapshot.data;
            List<DocumentSnapshot> documentList = querySnapshot.docs;

            documentList.forEach(
              (document) {
                print(document.data());

                MyVetMeetingNotification _notification =
                    MyVetMeetingNotification.fromQueryDocument(document);

                notificationList.add(VetMeetingNotificationItem(_notification));
              },
            );

            return ListView(children: notificationList);
          }
          print("snapshot data: ${snapshot.data}");
          return CircularProgress();
        },
      ),
    );
  }
}
