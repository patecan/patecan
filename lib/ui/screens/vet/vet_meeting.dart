class VetMeeting {
  String id;
  String content;
  List<String> members;
  DateTime from;
  DateTime to;
  String color;

  VetMeeting(
      {required this.id,
      required this.content,
      required this.members,
      required this.from,
      required this.to,
      this.color = '0xff50ACEA'});

  @override
  String toString() {
    return "{id: $id, content: $content, members: $members, from: $from, to: $to, color: $color}";
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      "content": content,
      "members": members,
      "from": from,
      "to": to,
      "color": color.toString()
    };
  }

  factory VetMeeting.fromJson(Map<String, dynamic> json) {
    return VetMeeting(
      id: json['id'],
      content: json['content'],
      members: List<String>.from(json["members"]),
      from: json["from"].toDate(),
      to: json["to"].toDate(),
      color: json["color"],
    );
  }
}
