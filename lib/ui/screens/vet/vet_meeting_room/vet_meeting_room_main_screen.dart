import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/messages/meeting_service.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_room/vet_meeting_room_detail_screen.dart';

import '../../../widgets/social/progress.dart';
import '../../../widgets/vet/vet_meeting_room_item.dart';

class VetMeetingRoomMainScreen extends StatefulWidget {
  VetMeetingRoomMainScreen({Key? key}) : super(key: key);

  @override
  _VetMeetingRoomMainScreenState createState() =>
      _VetMeetingRoomMainScreenState();
}

class _VetMeetingRoomMainScreenState extends State<VetMeetingRoomMainScreen> {
  GlobalKey<FormState> inviteFormKey = new GlobalKey<FormState>();
  MeetingRoomService inviteService = new MeetingRoomService();

  bool isLoading = false;

  TextEditingController inviteController = new TextEditingController();
  TextEditingController totalInviteController = new TextEditingController();

  dispose() {
    inviteController.clear();
    inviteController.dispose();
    super.dispose();
  }

  void submit() async {
    if (inviteFormKey.currentState!.validate()) {
      inviteFormKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      await inviteService.inviteUser(inviteController.text, 1);
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double _deviceHeight = MediaQuery.of(context).size.height;
    double _deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: isLoading == true
          ? CircularProgress()
          : Container(
              width: _deviceWidth,
              height: _deviceHeight,
              padding: EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: AppColors.skinBackground,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: Text(
                                "Ongoing meeting room",
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 16.5,
                                ),
                              ),
                            ),
                          ),
                          StreamBuilder(
                            stream: FirebaseFirestore.instance
                                .collection('vet_meeting_rooms')
                                .where('status', isEqualTo: 'on going')
                                .where('members',
                                    arrayContains: SignedAccount.instance.id!)
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.hasData) {
                                QuerySnapshot querySnapshot = snapshot.data;
                                List<DocumentSnapshot> _meetingRoomList =
                                    querySnapshot.docs;

                                print(_meetingRoomList);
                                return Container(
                                  height: _meetingRoomList.length * 200,
                                  child: ListView.builder(
                                    itemCount: _meetingRoomList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return GestureDetector(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: VetMeetingRoomItem(
                                            document: _meetingRoomList[index],
                                          ),
                                        ),
                                        onTap: () async {
                                          await getPermissions();
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  VetMeetingRoomDetailScreen(
                                                meetingId:
                                                    _meetingRoomList[index].id,
                                                meetingStatus:
                                                    _meetingRoomList[index]
                                                        ['status'],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  ),
                                );
                              } else {
                                return Text('');
                              }
                            },
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: Text(
                                "Upcoming meeting room",
                                style: TextStyle(
                                  color: Colors.orange,
                                  fontSize: 16.5,
                                ),
                              ),
                            ),
                          ),
                          StreamBuilder(
                            stream: FirebaseFirestore.instance
                                .collection('vet_meeting_rooms')
                                .where('status', isEqualTo: 'not started')
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.hasData) {
                                QuerySnapshot querySnapshot = snapshot.data;
                                List<DocumentSnapshot> meetingRoomList =
                                    querySnapshot.docs;
                                return Container(
                                  height: meetingRoomList.length * 200,
                                  child: ListView.builder(
                                    itemCount: meetingRoomList.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: VetMeetingRoomItem(
                                            document: meetingRoomList[index],
                                          ),
                                        ),
                                        onTap: () async {
                                          await getPermissions();
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  VetMeetingRoomDetailScreen(
                                                meetingId:
                                                    meetingRoomList[index].id,
                                                meetingStatus:
                                                    meetingRoomList[index]
                                                        ['status'],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  ),
                                );
                              } else {
                                return Text('');
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  Future<void> getPermissions() async {
    PermissionStatus cameraPermissionStatus = await Permission.camera.status;
    PermissionStatus microphonePermissionStatus =
        await Permission.microphone.status;

    if (cameraPermissionStatus != Permission.camera.isGranted) {
      await Permission.camera.request();
    }
    if (microphonePermissionStatus != Permission.microphone.isGranted) {
      await Permission.camera.request();
    }
  }
}
