import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/messages/meeting_service.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_room/vet_signaling.dart';
import 'package:social_media/ui/widgets/rounded_loading_button.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

import '../../../widgets/messages/meeting/in_meeting_room_user_item.dart';

class VetMeetingRoomDetailScreen extends StatefulWidget {
  String meetingId;
  String meetingStatus;

  VetMeetingRoomDetailScreen(
      {required this.meetingId, required this.meetingStatus, Key? key})
      : super(key: key);

  @override
  _VetMeetingRoomDetailScreenState createState() =>
      _VetMeetingRoomDetailScreenState();
}

class _VetMeetingRoomDetailScreenState
    extends State<VetMeetingRoomDetailScreen> {
  FirebaseUserHelper userService = FirebaseUserHelper();
  MeetingRoomService meetingRoomService = MeetingRoomService();

  RoundedLoadingButtonController roundedLoadingButtonController =
      RoundedLoadingButtonController();

  VetSignaling signaling = VetSignaling();
  RTCVideoRenderer _localRenderer = RTCVideoRenderer();
  RTCVideoRenderer _remoteRenderer = RTCVideoRenderer();
  String? roomId;
  bool isOfferedOnce = false;
  bool isAnsweredOnce = false;
  TextEditingController textEditingController = TextEditingController(text: '');

  //RtcEngine? rtcEngine;

  bool muted = false;
  bool isRoomJoined = false;
  List<dynamic> currentUsersInThisRoom = <String>[];

  final StopWatchTimer _stopWatchTimer = StopWatchTimer();

  @override
  void initState() {
    _localRenderer.initialize();
    _remoteRenderer.initialize();

    signaling.openUserMedia(
        localVideo: _localRenderer, remoteVideo: _remoteRenderer);
    signaling.onAddRemoteStream = ((stream) {
      _remoteRenderer.srcObject = stream;
      setState(() {});
    });
    listenMeetingDatabase();

    meetingRoomService.joinVetMeetingRoom(
        SignedAccount.instance.id!, widget.meetingId);
    _stopWatchTimer.onExecute.add(StopWatchExecute.start);
    super.initState();
  }

  @override
  void dispose() async {
    super.dispose();
    isRoomJoined = false;
    await _stopWatchTimer.dispose();
    _remoteRenderer.srcObject = null;
    _remoteRenderer.dispose();
    _localRenderer.dispose();

    await signaling.hangUp(_localRenderer);
    await meetingRoomService.leaveVetMeetingRoom(
        SignedAccount.instance.id!, widget.meetingId);
  }

  Future<void> listenMeetingDatabase() async {
    FirebaseFirestore.instance
        .collection('vet_meeting_rooms')
        .doc(widget.meetingId)
        .snapshots()
        .listen(
      (event) async {
        var data = event.data();
        if (data != null) {
          if (data['status'] != widget.meetingStatus) {
            widget.meetingStatus = data['status'];
          }

          currentUsersInThisRoom = data['currentUsersInThisRoom'];

          if (currentUsersInThisRoom.length == 0) {
            signaling.clearWebRTCInfo(meetingId: widget.meetingId);
            await signaling.changeRoomStatus(meetingId: widget.meetingId);
          } else if (currentUsersInThisRoom.length > 0 &&
              currentUsersInThisRoom.first['id'] == SignedAccount.instance.id) {
            if (data['offer'] == null && isOfferedOnce == false) {
              print("WEB_RTC: ${currentUsersInThisRoom.first} Send an Offer");
              await signaling.createRoom(meetingId: widget.meetingId);
              isOfferedOnce = true;
            }

            // if (data['answer'] == null && isAnsweredOnce == false) {
            //   print("WEB_RTC: Send an Answer");
            //   await signaling.joinRoom(widget.meetingId, _remoteRenderer);
            //   isAnsweredOnce = true;
            // }
          }
        }
      },
    );
  }

  Size? size;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: FutureBuilder(
          future: FirebaseFirestore.instance
              .collection('vet_meeting_rooms')
              .doc(widget.meetingId)
              .get(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              DocumentSnapshot documentSnapshot = snapshot.data;
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Room: ${documentSnapshot['content']}",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  StreamBuilder<int>(
                    stream: _stopWatchTimer.rawTime,
                    initialData: 0,
                    builder: (context, snap) {
                      final value = snap.data;
                      final displayTime = StopWatchTimer.getDisplayTime(value!);
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              FaIcon(FontAwesomeIcons.recordVinyl,
                                  color: Colors.red, size: 13),
                              SizedBox(width: 5),
                              Text(
                                displayTime,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Helvetica',
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  ),
                ],
              );
            } else {
              return Text("Meeting Room");
            }
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: size!.width,
          color: Colors.black87,
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 5),
          child: Column(
            children: [
              StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('vet_meeting_rooms')
                    .doc(widget.meetingId)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<dynamic> streamSnapshot) {
                  if (streamSnapshot.hasData) {
                    DocumentSnapshot documentSnapshot = streamSnapshot.data;

                    return Container(
                      height: size!.height,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size!.width,
                            padding: EdgeInsets.all(7),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: currentUsersInThisRoom.map<Widget>(
                                  (userMap) {
                                    return FutureBuilder(
                                      future: userService
                                          .getUserById(userMap['id']),
                                      builder: (BuildContext context,
                                          AsyncSnapshot<dynamic> snapshot) {
                                        if (snapshot.hasData) {
                                          MyUser myUser =
                                              MyUser.fromDocumentSnapshot(
                                                  snapshot.data);

                                          return Padding(
                                            padding: const EdgeInsets.only(
                                                right: 3.0),
                                            child: InMeetingRoomUserItem(
                                                myUser: myUser, size: size!),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      },
                                    );
                                  },
                                ).toList(),
                              ),
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    signaling.openUserMedia(
                                        localVideo: _localRenderer,
                                        remoteVideo: _remoteRenderer);
                                  },
                                  child: Text("Open cam/mic"),
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    signaling.joinRoom(
                                      widget.meetingId,
                                      _remoteRenderer,
                                    );
                                  },
                                  child: Text("Join room"),
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    signaling.hangUp(_localRenderer);
                                    dispose();
                                  },
                                  child: Text("Hangup"),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              color: Colors.black87,
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    left: 0.0,
                                    right: 0.0,
                                    top: 0.0,
                                    bottom: 65.0,
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(
                                          0.0, 0.0, 0.0, 0.0),
                                      child: RTCVideoView(_remoteRenderer,
                                          objectFit: RTCVideoViewObjectFit
                                              .RTCVideoViewObjectFitCover),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(7.5),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 2,
                                    bottom: 2,
                                    child: Container(
                                      width: 120.0,
                                      height: 170.0,
                                      child: RTCVideoView(_localRenderer,
                                          objectFit: RTCVideoViewObjectFit
                                              .RTCVideoViewObjectFitCover,
                                          mirror: true),
                                      decoration: BoxDecoration(
                                        color: Colors.blueGrey,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(7.5),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
        padding: EdgeInsets.only(right: 130),
        child: SizedBox(
          width: size!.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                heroTag: null,
                onPressed: () {
                  signaling.joinRoom(
                    widget.meetingId,
                    _remoteRenderer,
                  );
                },
                tooltip: 'Join Room',
                child: Icon(Icons.call, color: Colors.lightGreen),
                backgroundColor: Colors.white,
                mini: true,
              ),
              FloatingActionButton(
                heroTag: null,
                onPressed: () {
                  signaling.hangUp(_localRenderer);
                },
                tooltip: 'Hangup',
                child: Icon(Icons.call_end, color: Colors.red),
                backgroundColor: Colors.white,
                mini: true,
              ),
              FloatingActionButton(
                heroTag: null,
                child: const Icon(Icons.switch_camera),
                onPressed: () {
                  signaling.switchCamera();
                },
                backgroundColor: Colors.white,
                mini: true,
              ),
              FloatingActionButton(
                heroTag: null,
                child: const Icon(Icons.mic_off),
                onPressed: () {
                  signaling.muteMic();
                },
                backgroundColor: Colors.white,
                mini: true,
              ),
            ],
          ),
        ),
      ),
      //bottomSheet: buildBottomSheetWhenJoinedRoom(),
    );
  }

  Widget buildBottomSheetWhenNotJoinedRoom() {
    return Container(
      height: size!.height * 0.07,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), topRight: Radius.circular(15)),
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(0.5, 0.5),
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: size!.width * 0.3,
            child: RoundedLoadingButton(
              width: size!.width * 0.3,
              height: size!.height * 0.06,
              valueColor: Colors.black,
              color: Colors.red,
              controller: roundedLoadingButtonController,
              borderColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FaIcon(FontAwesomeIcons.doorOpen),
                    SizedBox(width: 15),
                    Text("Leave")
                  ],
                ),
              ),
              onPressed: () async {
                roundedLoadingButtonController.start();

                await meetingRoomService.leaveMeetingRoom(
                    SignedAccount.instance.id!, widget.meetingId);

                setState(() {
                  currentUsersInThisRoom.removeWhere(
                      (myUser) => myUser == SignedAccount.instance.id);

                  isRoomJoined = false;
                });
                roundedLoadingButtonController.reset();
              },
            ),
          ),
          Container(
            width: size!.width * 0.7,
            child: RoundedLoadingButton(
              height: size!.height * 0.06,
              width: size!.width * 0.7,
              valueColor: Colors.black,
              color: Colors.lightGreen,
              controller: roundedLoadingButtonController,
              borderColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  FaIcon(FontAwesomeIcons.handPointUp),
                  SizedBox(width: 15),
                  Text("Join Meeting"),
                ]),
              ),
              onPressed: () async {
                roundedLoadingButtonController.start();

                if (currentUsersInThisRoom
                    .any((user) => user == SignedAccount.instance.id)) {
                  roundedLoadingButtonController.reset();
                  return;
                } else {
                  await meetingRoomService.joinMeetingRoom(
                      SignedAccount.instance.id!, widget.meetingId);

                  setState(() {
                    //currentUsersInThisRoom.add(MyUser.fromSignedInAccount(SignedAccount.instance));

                    isRoomJoined = true;
                  });

                  if (currentUsersInThisRoom.length > 0) {}
                }

                roundedLoadingButtonController.reset();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildBottomSheetWhenJoinedRoom() {
    return Container(
      height: size!.height * 0.07,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), topRight: Radius.circular(15)),
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(0.5, 0.5),
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              ElevatedButton(
                onPressed: () {
                  signaling.openUserMedia(
                      localVideo: _localRenderer, remoteVideo: _remoteRenderer);
                },
                child: Text("Open cam & mic"),
              ),
              SizedBox(
                width: 8,
              ),
              ElevatedButton(
                onPressed: () async {
                  roomId =
                      await signaling.createRoom(meetingId: widget.meetingId);
                  textEditingController.text = roomId!;
                  setState(() {});
                },
                child: Text("Create room"),
              ),
            ],
          ),
          Column(
            children: [
              ElevatedButton(
                onPressed: () {
                  // Add roomId
                  signaling.joinRoom(
                    textEditingController.text,
                    _remoteRenderer,
                  );
                },
                child: Text("Join room"),
              ),
              SizedBox(
                width: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  signaling.hangUp(_localRenderer);
                },
                child: Text("Hangup"),
              ),
            ],
          ),
        ],
      ),
    );
  }

// Future<void> joinRoom() async {
//   await [Permission.microphone].request();
//   // Create RTC client instance
//   RtcEngineContext context = RtcEngineContext(Credentials.AGORA_PROJECT_ID);
//   RtcEngine rtcEngine = await RtcEngine.createWithContext(context);
//
//   rtcEngine.setEventHandler(
//     RtcEngineEventHandler(
//       joinChannelSuccess: (String channel, int uid, int elapsed) {
//         setState(
//           () {
//             isRoomJoined = true;
//           },
//         );
//       },
//       userJoined: (int uid, int elapsed) {
//         setState(
//           () {
//             _remoteUid = uid;
//           },
//         );
//       },
//       userOffline: (int uid, UserOfflineReason reason) {
//         setState(
//           () {
//             _remoteUid = 0;
//           },
//         );
//       },
//     ),
//   );
//
//   await rtcEngine.setChannelProfile(ChannelProfile.LiveBroadcasting);
//   await rtcEngine.setClientRole(ClientRole.Audience);
//   await rtcEngine.joinChannel(Credentials.AGORA_TEMP_TOKEN, 'test', null, 0);
// }

// Future<void> initPlatformState() async {
//   await [Permission.microphone].request();
//   // Create RTC client instance
//   RtcEngineContext context = RtcEngineContext(Credentials.AGORA_PROJECT_ID);
//   RtcEngine rtcEngine = await RtcEngine.createWithContext(context);
//
//   rtcEngine.setEventHandler(
//     RtcEngineEventHandler(
//       joinChannelSuccess: (String channel, int uid, int elapsed) {
//         setState(
//           () {
//             isRoomJoined = true;
//           },
//         );
//       },
//       userJoined: (int uid, int elapsed) {
//         setState(
//           () {
//             _remoteUid = uid;
//           },
//         );
//       },
//       userOffline: (int uid, UserOfflineReason reason) {
//         setState(
//           () {
//             _remoteUid = 0;
//           },
//         );
//       },
//     ),
//   );
//
//   await rtcEngine.setChannelProfile(ChannelProfile.LiveBroadcasting);
//   await rtcEngine.setClientRole(ClientRole.Broadcaster);
//   await rtcEngine.joinChannel(Credentials.AGORA_TEMP_TOKEN, 'test', null, 0);
// }
}
