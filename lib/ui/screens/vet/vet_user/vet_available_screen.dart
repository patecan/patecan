import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/widgets/messages/contact_user_item.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

class VetAvailableScreen extends StatefulWidget {
  double deviceHeight;

  VetAvailableScreen(this.deviceHeight);

  @override
  _VetAvailableScreenState createState() => _VetAvailableScreenState();
}

class _VetAvailableScreenState extends State<VetAvailableScreen> {
  String? searchName;
  TextEditingController searchFieldController = new TextEditingController();
  FirebaseUserHelper userService = new FirebaseUserHelper();

  bool isSelectedContactBook = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(context) {
    return Container(
      child: Column(
        children: [
          buildContactSearchBar(),
          (searchName == null || searchName!.isEmpty)
              ? StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('users')
                      .where('userRole', isEqualTo: 'vet')
                      .snapshots(),
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      QuerySnapshot querySnapshot = snapshot.data;
                      List<MyUser> allUserList = querySnapshot.docs
                          .map((document) =>
                              MyUser.fromDocumentSnapshot(document))
                          .toList();
                      return buildUsersContactListView(allUserList);
                    } else {
                      return CircularProgress();
                    }
                  },
                )
              : StreamBuilder(
                  stream: userService.getUserByDisplayName(searchName!),
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      QuerySnapshot querySnapshot = snapshot.data;
                      List<MyUser> searchedUserList = querySnapshot.docs
                          .map((document) =>
                              MyUser.fromDocumentSnapshot(document))
                          .toList();
                      return buildUsersContactListView(searchedUserList);
                    } else {
                      return CircularProgress();
                    }
                  },
                ),
        ],
      ),
    );
  }

  Widget buildContactSearchBar() {
    return Container(
      height: widget.deviceHeight * 0.08,
      padding: EdgeInsets.symmetric(vertical: widget.deviceHeight * 0.02),
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.8,
            child: TextField(
              controller: searchFieldController,
              autocorrect: false,
              style: TextStyle(color: Colors.blue),
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.blue,
                ),
                labelText: "Search",
                border: OutlineInputBorder(borderSide: BorderSide.none),
              ),
              onSubmitted: (value) {
                setState(() {
                  searchName = value;
                  print(searchName);
                });
              },
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FaIcon(FontAwesomeIcons.users,
                    size: 18,
                    color:
                        isSelectedContactBook ? Colors.black54 : Colors.blue),
                IconButton(
                  icon: FaIcon(FontAwesomeIcons.addressBook,
                      color:
                          isSelectedContactBook ? Colors.blue : Colors.black54),
                  onPressed: () async {
                    await openAddressBook();
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildUsersContactListView(List<MyUser> userList) {
    return Container(
      height: widget.deviceHeight - (widget.deviceHeight * 0.25),
      child: ListView.builder(
        itemCount: userList.length,
        itemBuilder: (context, index) {
          return ContactUserItem(from: 'vet', user: userList[index]);
        },
      ),
    );
  }

  Future openAddressBook() async {
    if (await FlutterContacts.requestPermission()) {
      // Get all contacts (lightly fetched)
      final contact = await FlutterContacts.openExternalPick();

      if (contact != null) {
        searchFieldController.text = contact.phones.single.normalizedNumber;
      }
    }
  }
}
