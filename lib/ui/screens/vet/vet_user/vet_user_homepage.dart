import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import 'package:social_media/ui/screens/vet/create_vet_meeting_screen.dart';
import 'package:social_media/ui/screens/vet/recent_vet_message.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_room/vet_meeting_room_main_screen.dart';
import 'package:social_media/ui/screens/vet/vet_user/vet_available_screen.dart';

import '../../../../constant/app_colors.dart';

class VetUserHomepage extends StatefulWidget {
  const VetUserHomepage({Key? key}) : super(key: key);

  @override
  State<VetUserHomepage> createState() => _VetUserHomepageState();
}

class _VetUserHomepageState extends State<VetUserHomepage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this, initialIndex: 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    AppBar appBar = AppBar(
      leading: Center(
        child: IconButton(
          icon:
              FaIcon(FontAwesomeIcons.angleLeft, color: AppColors.darkBlueText),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      backgroundColor: Colors.transparent,
      toolbarHeight: 60,
      title: GradientText(
        'Vet',
        style: TextStyle(
          fontSize: 17.5,
          fontWeight: FontWeight.bold,
        ),
        colors: [
          AppColors.darkRedText,
          AppColors.darkRedText,
        ],
      ),
      centerTitle: false,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[Color(0xffFAC0D1), Color(0xffFE97B5)],
          ),
        ),
      ),
      bottom: TabBar(
        controller: _tabController,
        unselectedLabelColor: Colors.white,
        indicatorColor: AppColors.blueWithPink,
        labelColor: AppColors.blueWithPink,
        indicatorWeight: 3.0,
        tabs: [
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.userNurse,
            ),
          ),
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.commentMedical,
            ),
          ),
          Tab(
            icon: FaIcon(
              FontAwesomeIcons.solidCalendarCheck,
            ),
          ),
        ],
      ),
      actions: [
        GestureDetector(
          onTap: () {},
          child: Container(
            margin: EdgeInsets.only(right: 3),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                topLeft: Radius.circular(15),
                bottomRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurStyle: BlurStyle.outer,
                  blurRadius: 5,
                  offset: Offset(0.5, 0.5),
                ),
              ],
            ),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => CreateVetMeetingScreen())),
                  child: Container(
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                    ),
                    child: Row(
                      children: [
                        FaIcon(FontAwesomeIcons.solidCalendar,
                            size: 25, color: AppColors.blueWithPink),
                        FaIcon(FontAwesomeIcons.plus,
                            size: 15, color: AppColors.blueWithPink),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: Container(
        height: size.height,
        width: size.width,
        child: TabBarView(
          controller: _tabController,
          children: [
            VetAvailableScreen(size.height),
            RecentVetMessageScreen(),
            VetMeetingRoomMainScreen(),
          ],
        ),
      ),
    );
  }
}
