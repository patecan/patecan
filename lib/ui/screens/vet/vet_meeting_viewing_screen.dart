import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/ui/screens/vet/create_vet_meeting_screen.dart';
import 'package:social_media/ui/screens/vet/vet_meeting.dart';

class VetMeetingViewingScreen extends StatefulWidget {
  VetMeeting vetMeeting;

  VetMeetingViewingScreen({required this.vetMeeting, Key? key})
      : super(key: key);

  @override
  State<VetMeetingViewingScreen> createState() =>
      _VetMeetingViewingScreenState();
}

class _VetMeetingViewingScreenState extends State<VetMeetingViewingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.vetMeeting.from} - ${widget.vetMeeting.to}'),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        CreateVetMeetingScreen(vetMeeting: widget.vetMeeting),
                  ),
                );
              },
              icon: FaIcon(FontAwesomeIcons.edit))
        ],
      ),
      body: Text(widget.vetMeeting.toString()),
    );
  }
}
