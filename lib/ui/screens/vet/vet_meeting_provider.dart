import 'package:flutter/cupertino.dart';
import 'package:social_media/ui/screens/vet/vet_meeting.dart';

class VetMeetingProvider extends ChangeNotifier {
  List<VetMeeting> _vetMeetingList = [];

  void addVetMeeting(VetMeeting vetMeeting) {
    _vetMeetingList.add(vetMeeting);
    notifyListeners();
  }

  void editVetMeeting(VetMeeting newMeeting, VetMeeting oldMeeting) {
    final index = _vetMeetingList.indexOf(oldMeeting);
    _vetMeetingList[index] = newMeeting;

    notifyListeners();
  }

  DateTime _selectedDate = DateTime.now();

  DateTime get selectedDate => _selectedDate;

  set selectedDate(DateTime value) {
    _selectedDate = value;
  }

  List<VetMeeting> get vetMeetingList => _vetMeetingList;

  set vetMeetingList(List<VetMeeting> value) {
    _vetMeetingList = value;
  }
}
