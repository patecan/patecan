import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/messages/conversation.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/singned_account.dart';

import '../../../models/users/my_user.dart';
import '../../../services/vet/vet_service.dart';
import '../../widgets/messages/message_items/image_message_item.dart';
import '../../widgets/pet_adopt/pet_adopt_message_item.dart';
import '../../widgets/social/progress.dart';

class VetMessageDetailScreen extends StatefulWidget {
  Message message;
  MyUser user;
  String veterinarianId;

  VetMessageDetailScreen(this.user, this.veterinarianId, this.message,
      {Key? key})
      : super(key: key);

  @override
  _VetMessageDetailScreenState createState() => _VetMessageDetailScreenState();
}

class _VetMessageDetailScreenState extends State<VetMessageDetailScreen> {
  /* Responsive Design */
  double deviceWidth = 0;
  double deviceHeight = 0;

  void setNewConversation(String id) {
    setState(() {
      widget.message.conversationID = id;
    });
  }

  late VetService vetService;
  late TextEditingController messageTextController;
  late GlobalKey<FormState> inputMessageFormKey;
  late ScrollController scrollController;
  String? messageContent;
  File? imageFile;
  String? imageFileUrl;
  late ImagePicker _imagePicker;
  double requestPoint = 0.0;

  @override
  void initState() {
    super.initState();
    messageTextController = new TextEditingController();
    vetService = new VetService();
    inputMessageFormKey = GlobalKey<FormState>();
    messageContent = '';
    _imagePicker = ImagePicker();
    scrollController = ScrollController();
  }

  @override
  void dispose() {
    messageTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    AppBar appBar = AppBar(
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
      backgroundColor: Colors.transparent,
      title: Text(
        this.widget.message.displayName!,
        style: TextStyle(color: Colors.black),
      ),
      actions: [],
    );

    deviceHeight = size.height - appBar.preferredSize.height;
    deviceWidth = size.width;

    return Scaffold(
      appBar: appBar,
      body: StreamBuilder<Object>(
        stream: FirebaseFirestore.instance
            .collection('vet_messages')
            .doc(widget.message.conversationID)
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            DocumentSnapshot documentSnapshot = snapshot.data;

            return SizedBox(
              height: deviceHeight,
              width: deviceWidth,
              child: Column(
                children: [
                  if (documentSnapshot.data() != null)
                    Flexible(
                        flex: 10,
                        fit: FlexFit.tight,
                        child: buildMessageList(documentSnapshot))
                  else
                    Flexible(
                        flex: 10, fit: FlexFit.tight, child: Text('Say Hi')),
                  Flexible(
                      flex: 1,
                      fit: FlexFit.loose,
                      child: buildMessageInputField(context)),
                  if (imageFile != null)
                    Flexible(
                      flex: 1,
                      fit: FlexFit.loose,
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          width: deviceWidth * 0.4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                                image: FileImage(imageFile!),
                                fit: BoxFit.contain),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            );
          }
          return LinearProgress();
        },
      ),
    );
  }

  Widget buildMessageList(DocumentSnapshot documentSnapshot) {
    Conversation conversation = Conversation.fromDocument(documentSnapshot);
    List messagesList = conversation.messages;
    bool isSender = widget.message.senderID == SignedAccount.instance.id;

    messagesList.forEach((element) {
      print("$element\n");
    });

    Timer(
      Duration(milliseconds: 50),
      () {
        scrollController.jumpTo(scrollController.position.maxScrollExtent);
      },
    );

    return Container(
      color: AppColors.lightPinkBackground,
      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
      child: ListView.builder(
        controller: scrollController,
        itemCount: messagesList.length,
        itemBuilder: (context, index) {
          if (messagesList[index].messageType == MessageType.Image)
            return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: ImageMessageItem(messagesList[index]),
            );
          else if (messagesList[index].messageType == MessageType.Text)
            return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: PetAdoptTextMessageItem(messagesList[index]),
            );
          else
            return Container();
        },
      ),
    );
  }

  Widget buildMessageInputField(BuildContext context) {
    return Container(
      height: deviceHeight * 0.08,
      child: Form(
        key: inputMessageFormKey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            SizedBox(
              width: deviceWidth * 0.1,
              child: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.image,
                  color: Colors.lightBlue,
                ),
              ),
            ),
            SizedBox(
              width: deviceWidth * 0.1,
              child: IconButton(
                onPressed: () async {
                  XFile? file =
                      await _imagePicker.pickImage(source: ImageSource.gallery);
                  if (file != null) {
                    setState(() {
                      imageFile = File(file.path);
                    });
                  }
                },
                icon: Icon(
                  Icons.camera_alt_rounded,
                  color: Colors.lightBlue,
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 234, 235, 1.0),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 15.0),
                        child: TextFormField(
                          controller: messageTextController,
                          onChanged: (value) {
                            inputMessageFormKey.currentState!.save();
                          },
                          onSaved: (value) {
                            setState(() {
                              messageContent = value!;
                            });
                          },
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Type message"),
                          cursorColor: Colors.black,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.send, color: Colors.blue),
                      onPressed: () async {
                        if (inputMessageFormKey.currentState!.validate()) {
                          if (widget.message.conversationID == null) {
                            if (imageFile == null) {
                              vetService.sendNewMessage(
                                receiverID: widget.veterinarianId,
                                setNewConversation: setNewConversation,
                                imageURL: null,
                                messageContent: messageTextController.text,
                              );
                            } else {
                              imageFileUrl = await vetService.uploadImage(
                                  SignedAccount.instance.id!, imageFile!);

                              vetService.sendNewMessage(
                                receiverID: widget.veterinarianId,
                                setNewConversation: setNewConversation,
                                imageURL: imageFileUrl,
                                messageContent: messageTextController.text,
                              );
                            }
                          } else {
                            late Message newMessage;
                            if (imageFile == null) {
                              newMessage = new Message(
                                messageType: MessageType.Text,
                                lastMessageContent: messageTextController.text,
                                senderID: SignedAccount.instance.id,
                                sentTime: DateTime.now(),
                              );
                            } else if (imageFile != null) {
                              imageFileUrl = await vetService.uploadImage(
                                  SignedAccount.instance.id!, imageFile!);
                              newMessage = new Message(
                                messageType: MessageType.Image,
                                lastMessageContent: imageFileUrl,
                                senderID: SignedAccount.instance.id,
                                sentTime: DateTime.now(),
                              );
                            }
                            vetService.sendMessage(
                              widget.message.conversationID!,
                              newMessage,
                            );
                          }

                          setState(() {
                            messageTextController.clear();
                            FocusScope.of(context).unfocus();
                            imageFile = null;
                            imageFileUrl = null;
                          });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
