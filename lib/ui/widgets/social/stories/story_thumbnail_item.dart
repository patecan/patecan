import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/stories/story.dart';
import 'package:social_media/ui/screens/stories/story_screen.dart';

class StoryThumbnailItem extends StatefulWidget {
  Story story;

  StoryThumbnailItem({required this.story, Key? key}) : super(key: key);

  @override
  _StoryThumbnailItemState createState() => _StoryThumbnailItemState();
}

class _StoryThumbnailItemState extends State<StoryThumbnailItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                      spreadRadius: 0.005,
                    ),
                  ],
                  image: DecorationImage(
                      image: NetworkImage(widget.story.thumbnailURL!),
                      fit: BoxFit.cover),
                ),
              ),
              Positioned(
                top: 5,
                left: 5,
                child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('users')
                      .doc(widget.story.ownerID)
                      .snapshots(),
                  builder: (context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      DocumentSnapshot documentSnapshot = snapshot.data;
                      return Container(
                        width: 40,
                        height: 40,
                        child: CircularProfileAvatar(
                          documentSnapshot['photoUrl'],
                          radius: 25,
                          backgroundColor: Colors.white,
                          borderWidth: 3,
                          // sets initials text, set your own style, default Text('')
                          borderColor: Colors.blue,
                          // sets border color, default Colors.white
                          elevation: 0.0,
                          cacheImage: true,
                          // allow widget to cache image against provided url
                          imageFit: BoxFit.cover,
                          onTap: () {
                            print('adil');
                          },
                          showInitialTextAbovePicture: false,
                        ),
                      );
                    } else {
                      return CircularProfileAvatar(
                        'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
                        radius: 25,
                        backgroundColor: Colors.white,
                        borderWidth: 3,
                        borderColor: Colors.blue,
                        elevation: 0.0,
                        cacheImage: true,
                        // allow widget to cache image against provided url
                        imageFit: BoxFit.cover,
                      );
                    }
                  },
                ),
              )
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => StoryScreen(story: widget.story),
            ),
          );
        });
  }
}
