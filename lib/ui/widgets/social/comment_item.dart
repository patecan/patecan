import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/widgets/social/progress.dart';
import 'package:timeago/timeago.dart' as timeago;

class CommentItem extends StatelessWidget {
  String userID;
  MyUser? user;
  DateTime commentTime;
  String comment;
  FirebaseUserHelper firebaseUserHelper = new FirebaseUserHelper();

  int likesCount = 0;
  bool isLiked = false;
  bool isHearted = false;

  CommentItem(
      {required this.userID, required this.commentTime, required this.comment});

  factory CommentItem.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    return CommentItem(
      userID: documentSnapshot['userID'],
      comment: documentSnapshot['comment'],
      commentTime: documentSnapshot['commentTime'].toDate(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: firebaseUserHelper.getUserById(userID),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          MyUser user = MyUser.fromDocumentSnapshot(snapshot.data);
          return Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: NetworkImage(user.photoUrl!),
                    ),
                    SizedBox(width: 7),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(user.displayName!,
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold)),
                          SizedBox(height: 1),
                          Text(comment, style: TextStyle(fontSize: 15)),
                          SizedBox(height: 2),
                          Text(timeago.format(commentTime),
                              style: TextStyle(fontSize: 12)),
                        ],
                      ),
                    ),
                    GestureDetector(
                      child: isLiked == true
                          ? Icon(Icons.favorite, size: 20, color: Colors.pink)
                          : Icon(Icons.favorite_border,
                              size: 20, color: Colors.black),
                      onTap: () async {},
                    ),
                  ],
                ),
              ),
            ],
          );
        } else {
          return CircularProgress();
        }
      },
    );
  }
}
