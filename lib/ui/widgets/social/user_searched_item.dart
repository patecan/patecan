import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/my_user.dart';

import '../../screens/social/profile_screen.dart';

class UserSearchedItem extends StatefulWidget {
  late MyUser user;

  UserSearchedItem(this.user);

  @override
  _UserSearchedItemState createState() => _UserSearchedItemState();
}

class _UserSearchedItemState extends State<UserSearchedItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProfileScreen(widget.user.id!)));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: Column(
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.grey,
                foregroundImage: NetworkImage(widget.user.photoUrl!),
              ),
              title: Text(
                widget.user.displayName!,
                style: TextStyle(
                    color: Colors.blueAccent, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                widget.user.username!,
                style: TextStyle(color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
