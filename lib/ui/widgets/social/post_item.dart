import 'dart:async';

import 'package:animator/animator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/social_media/post.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/notifications/firebase_social_notify_service.dart';
import 'package:social_media/services/social_media/firebase_comment_service.dart';
import 'package:social_media/services/social_media/firebase_post_service.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/screens/social/comment_screen.dart';
import 'package:social_media/ui/widgets/social/progress.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../screens/social/profile_screen.dart';

class PostItem extends StatefulWidget {
  Post post;

  PostItem(this.post);

  factory PostItem.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    return PostItem(Post.fromDocumentSnapshot(documentSnapshot));
  }

  int getLikeCount() {
    int count = 0;
    if (post.likes == null) {
      return 0;
    }
    post.likes!.values.forEach((element) {
      if (element == true) {
        count++;
      }
    });

    return count;
  }

  @override
  _PostItemState createState() => _PostItemState(
      postID: post.postID,
      ownerID: post.ownerID,
      username: post.username,
      mediaURL: post.mediaURL,
      location: post.location,
      caption: post.caption,
      likes: post.likes,
      likeCount: getLikeCount(),
      postTime: post.postTime);
}

class _PostItemState extends State<PostItem> {
  FirebaseUserHelper firebaseUser = new FirebaseUserHelper();
  FirebasePostHelper firebasePost = new FirebasePostHelper();
  FirebaseCommentHelper firebaseComment = new FirebaseCommentHelper();
  SocialMediaNotificationService firebaseNotification =
      new SocialMediaNotificationService();

  late String postID;
  late String ownerID;
  late String username;
  String? mediaURL;
  String? location;
  String? caption;
  late DateTime postTime;

  /* like feature */
  Map? likes;
  int likeCount;
  int? commentCount = 0;
  bool isLiked = false;
  bool isHearted = false;

  _PostItemState(
      {required this.postID,
      required this.ownerID,
      required this.username,
      this.mediaURL,
      this.location,
      this.caption,
      this.likes,
      this.likeCount = 0,
      required this.postTime});

  @override
  void initState() {
    if (widget.post.likes!.containsKey(SignedAccount.instance.id)) {
      isLiked = widget.post.likes![SignedAccount.instance.id];
    }
    super.initState();
  }

  late Size size;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.only(left: 5, right: 5, bottom: 10),
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      child: Column(
        children: [
          FutureBuilder(
            future: Future.wait([
              firebaseUser.getUserById(this.ownerID),
              FirebaseFirestore.instance
                  .collection('comments')
                  .doc(this.postID)
                  .collection('postComments')
                  .get()
            ]),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                commentCount = snapshot.data[1].docs.length;

                MyUser user = MyUser.fromDocumentSnapshot(snapshot.data[0]);
                return ListTile(
                  contentPadding: EdgeInsets.all(0),
                  leading: CircleAvatar(
                    maxRadius: 25,
                    backgroundColor: Colors.grey,
                    foregroundImage: NetworkImage(user.photoUrl!),
                  ),
                  title: GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileScreen(user.id!))),
                    child: Text(
                      user.displayName!,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  ),
                  subtitle: Text(
                    timeago.format(postTime).toString(),
                    style: TextStyle(color: Colors.black),
                  ),
                  trailing: IconButton(
                      onPressed: () {
                        if (SignedAccount.instance.id == this.ownerID) {
                          showDeleteDialog(context);
                        }
                      },
                      icon: Icon(Icons.more_vert)),
                );
              } else {
                return CircularProgress();
              }
            },
          ),
          GestureDetector(
            onDoubleTap: createHeartAnimation,
            child: Padding(
              padding: EdgeInsets.all(0),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    height: size.height * 0.5,
                    width: size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                      ),
                      child: GridTile(
                        child: Image.network(this.mediaURL!, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  if (isHearted == true)
                    Animator(
                      duration: Duration(milliseconds: 500),
                      tween: Tween(begin: 0.8, end: 1.5),
                      curve: Curves.elasticOut,
                      cycles: 0,
                      builder: (BuildContext context,
                          AnimatorState<dynamic> animatorState, Widget? child) {
                        return Transform.scale(
                          scale: animatorState.value,
                          child: Icon(Icons.favorite,
                              size: 80.0, color: Colors.red),
                        );
                      },
                    ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    "$caption",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 0, vertical: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: handleLike,
                      child: Container(
                        width: size.width * 0.27,
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                            color: AppColors.redHeartButtonBackground,
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                                isLiked == true
                                    ? Icons.favorite
                                    : Icons.favorite_border,
                                size: 30.0,
                                color: AppColors.redHeartButton),
                            Container(
                              child: Text(
                                (likeCount > 0)
                                    ? '${likeCount}'
                                    : '${likeCount}',
                                style: TextStyle(
                                    color: AppColors.redHeartButton,
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => showComment(
                          context: context,
                          postID: this.postID,
                          ownerID: this.ownerID),
                      child: Container(
                        width: size.width * 0.27,
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                            color: AppColors.blueHeartButtonBackground,
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.chat,
                                  size: 30.0, color: AppColors.blueHeartButton),
                              Text(
                                commentCount.toString(),
                                style: TextStyle(
                                    color: AppColors.blueHeartButton,
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              )
                            ]),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => {},
                      child: Container(
                        width: size.width * 0.27,
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                            color: AppColors.shareButtonBackground,
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FaIcon(FontAwesomeIcons.share,
                                  size: 27.0,
                                  color: AppColors.shareHeartButton),
                            ]),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  /* _________________________________ HELPER FUNCTION _________________________________ */

  void handleLike() async {
    bool isUserLikedBefore = likes![SignedAccount.instance.id] == true;
    if (isUserLikedBefore) {
      setState(() {
        likeCount--;
        this.isLiked = false;
        likes![SignedAccount.instance.id] = false;
      });
      await firebasePost.updateLike(this.postID, this.ownerID, false);
      await firebaseNotification.deleteNotification(
          postID: this.postID, ownerID: this.ownerID);
    } else if (!isUserLikedBefore) {
      setState(() {
        likeCount++;
        this.isLiked = true;
        likes![SignedAccount.instance.id] = true;
      });

      await firebasePost.updateLike(this.postID, this.ownerID, true);
      await firebaseNotification.insertPostNotification(
          postID: this.postID, ownerID: this.ownerID, notifyType: 'like');
    }
  }

  createHeartAnimation() {
    Animator(
      duration: Duration(milliseconds: 300),
      tween: Tween(begin: 0.8, end: 1.4),
      curve: Curves.elasticOut,
      cycles: 0,
      builder: (BuildContext context, AnimatorState<dynamic> animatorState,
          Widget? child) {
        return Transform.scale(
            scale: animatorState.value,
            child: Icon(Icons.favorite, size: 80.0, color: Colors.red));
      },
    );

    setState(() {
      isHearted = true;
      handleLike();
    });

    Timer(Duration(milliseconds: 500), () {
      setState(() {
        isHearted = false;
      });
    });
  }

  void showComment(
      {required BuildContext context,
      required String postID,
      required String ownerID}) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CommentScreen(postID, ownerID);
    }));
  }

  showDeleteDialog(BuildContext parentContext) {
    return showDialog(
        context: parentContext,
        builder: (context) {
          return SimpleDialog(
            title: Text("Remove this post?"),
            children: [
              SimpleDialogOption(
                  onPressed: () async {
                    await deletePost();
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Delete',
                    style: TextStyle(color: Colors.red),
                  )),
              SimpleDialogOption(
                  onPressed: () => Navigator.pop(context),
                  child: Text('Cancel')),
            ],
          );
        });
  }

  deletePost() async {
    await firebasePost.deletePost(this.postID, this.ownerID);
    await firebaseNotification.deleteNotification(
        postID: this.postID, ownerID: this.ownerID);
    await firebaseComment.deleteAllComment(this.postID);
  }
}
