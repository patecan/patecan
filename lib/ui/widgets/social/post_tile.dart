import 'package:flutter/material.dart';
import 'package:social_media/ui/widgets/social/network_custom_image.dart';
import 'package:social_media/ui/widgets/social/post_item.dart';

class PostTileView extends StatelessWidget {
  List<PostItem> postItemList;

  PostTileView(this.postItemList);

  @override
  Widget build(BuildContext context) {
    List<GridTile> gridTitle = [];
    postItemList.forEach((postItem) {
      gridTitle.add(
        GridTile(
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Scaffold(body: postItem)));
            },
            child: NetworkCustomImage(postItem.post.mediaURL!),
          ),
        ),
      );
    });

    return Container(
      padding: EdgeInsets.only(left: 8.0, right: 8.0),
      child: GridView.count(
        crossAxisCount: 3,
        childAspectRatio: 1,
        mainAxisSpacing: 1.5,
        crossAxisSpacing: 1.5,
        shrinkWrap: true,
        children: gridTitle,
      ),
    );
  }
}
