// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:social_media/services/social_media/firebase_user_service.dart';
//
// import '../../../constant/app_colors.dart';
//
// class SearchBar extends StatefulWidget implements PreferredSizeWidget {
//   FirebaseUserHelper firebaseHelper = new FirebaseUserHelper();
//   TextEditingController searchController = TextEditingController();
//
//   static ValueNotifier<QuerySnapshot?> searchResult =
//       ValueNotifier<QuerySnapshot?>(null);
//
//   @override
//   State<StatefulWidget> createState() {
//     return _SearchBarState();
//   }
//
//   SearchBar() : preferredSize = Size.fromHeight(60.0);
//   @override
//   final Size preferredSize;
// }
//
// class _SearchBarState extends State<SearchBar> {
//   void handleSearch(String value) async {
//     SearchBar.searchResult.value = null;
//     if (value == "") {
//       SearchBar.searchResult.value = null;
//     } else {
//       QuerySnapshot foundUser =
//           await widget.firebaseHelper.getUserSnapshotByDisplayName(value);
//       if (foundUser.docs.length > 0) {
//         SearchBar.searchResult.value = foundUser;
//       }
//     }
//   }
//
//   @override
//   Widget build(context) {
//     return AppBar(
//       backgroundColor: Colors.white,
//       leading: Center(
//         child: IconButton(
//           icon:
//               FaIcon(FontAwesomeIcons.angleLeft, color: AppColors.pinkGradient),
//           onPressed: () => Navigator.pop(context),
//         ),
//       ),
//       title: TextFormField(
//         decoration: InputDecoration(
//           hintText: 'Search',
//           filled: true,
//           prefixIcon: Icon(Icons.search, size: 28.0),
//           suffixIcon: IconButton(
//             icon: Icon(Icons.clear),
//             onPressed: () {},
//           ),
//         ),
//         onFieldSubmitted: handleSearch,
//       ),
//     );
//   }
// }
