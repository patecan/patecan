import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/pet_adopt/owner/create_proof_request_form_screen.dart';

import '../../screens/pet_adopt/create_response/proof_response_screen.dart';
import '../../screens/pet_adopt/response_review/proof_response_review_screen.dart';

class PetAdoptRequestItem extends StatefulWidget {
  Message message;

  PetAdoptRequestItem(this.message, {Key? key}) : super(key: key);

  @override
  _PetAdoptRequestItemState createState() => _PetAdoptRequestItemState();
}

class _PetAdoptRequestItemState extends State<PetAdoptRequestItem> {
  /* Responsive Design */
  double deviceWidth = 0;
  double deviceHeight = 0;

  @override
  Widget build(BuildContext context) {
    bool isSender = widget.message.senderID == SignedAccount.instance.id;

    Size size = MediaQuery.of(context).size;
    deviceHeight = size.height - MediaQuery.of(context).padding.top;
    deviceWidth = size.width;

    late IconData attachmentIcon;

    switch (widget.message.requestAttachment) {
      case 'location':
        attachmentIcon = FontAwesomeIcons.mapMarked;
        break;
      case 'film':
        attachmentIcon = FontAwesomeIcons.film;
        break;
      case 'sign':
        attachmentIcon = FontAwesomeIcons.signature;
        break;
      case 'images':
        attachmentIcon = FontAwesomeIcons.images;
        break;
      case 'file':
        attachmentIcon = FontAwesomeIcons.fileInvoice;
        break;
      default:
        attachmentIcon = FontAwesomeIcons.fileInvoice;
        break;
    }

    return StreamBuilder<Object>(
        stream: FirebaseFirestore.instance
            .collection('proof_you_are_deserved')
            .doc(widget.message.conversationID)
            .collection('proof_response')
            .snapshots(),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            QuerySnapshot querySnapshot = snapshot.data;
            if (querySnapshot.docs.length > 0) {
              int index = querySnapshot.docs
                  .indexWhere((element) => element.id == widget.message.id);

              if (index != -1) {
                Map<String, dynamic> _proofResponse =
                    querySnapshot.docs[index].data() as Map<String, dynamic>;

                return GestureDetector(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: isSender
                          ? MainAxisAlignment.end
                          : MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        if (!isSender)
                          CircleAvatar(
                            backgroundImage: NetworkImage(
                                'https://picsum.photos/seed/picsum/200/300'),
                          ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: deviceWidth * 0.02),
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(width: 0, color: Colors.white),
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: _proofResponse['accepted'] == false
                                    ? [Color(0xffFEC868), Color(0xffFEC868)]
                                    : [Color(0xff8D9F5E), Color(0xff8D9F5E)],
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  widget.message.requestContent!,
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Center(
                                  child: FaIcon(
                                    attachmentIcon,
                                    color: Colors.white,
                                    size: 45,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  padding: EdgeInsets.all(1),
                                  margin: EdgeInsets.only(bottom: 5),
                                  width: size.width * 0.9,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7.5),
                                      border: Border.all(
                                          width: 1, color: Colors.white)),
                                  child: Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.caretUp,
                                      color: Colors.white,
                                      size: 25,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    if (isSender) {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ProofResponseReview(
                              requestId: widget.message.id!,
                              message: widget.message,
                              requestAttachment: EnumToString.fromString(
                                  RequestAttachment.values,
                                  widget.message.requestAttachment!)!,
                              proofResponse: _proofResponse),
                        ),
                      );
                    }
                  },
                );
              }
            }
          }
          return GestureDetector(
            child: Container(
              child: Row(
                mainAxisAlignment:
                    isSender ? MainAxisAlignment.end : MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (!isSender)
                    CircleAvatar(
                      backgroundImage: NetworkImage(
                          'https://picsum.photos/seed/picsum/200/300'),
                    ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: deviceWidth * 0.02),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(width: 0, color: Colors.white),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [Color(0xff6EC2FB), Color(0xff6EC2FB)],
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            widget.message.requestContent!,
                            style: TextStyle(color: Colors.white),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Center(
                            child: FaIcon(
                              attachmentIcon,
                              color: Colors.white,
                              size: 45,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.all(1),
                            margin: EdgeInsets.only(bottom: 5),
                            width: size.width * 0.9,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.5),
                                border:
                                    Border.all(width: 1, color: Colors.white)),
                            child: Center(
                              child: FaIcon(
                                FontAwesomeIcons.caretUp,
                                color: Colors.white,
                                size: 25,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            onTap: () {
              if (!isSender) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ProofResponse(
                        requestId: widget.message.id!,
                        message: widget.message,
                        requestAttachment: EnumToString.fromString(
                            RequestAttachment.values,
                            widget.message.requestAttachment!)!),
                  ),
                );
              }
            },
          );
        });
  }
}
