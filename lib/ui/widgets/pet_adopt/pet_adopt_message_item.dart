import 'package:flutter/material.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/singned_account.dart';

class PetAdoptTextMessageItem extends StatefulWidget {
  Message message;

  PetAdoptTextMessageItem(this.message, {Key? key}) : super(key: key);

  @override
  _PetAdoptTextMessageItemState createState() =>
      _PetAdoptTextMessageItemState();
}

class _PetAdoptTextMessageItemState extends State<PetAdoptTextMessageItem> {
  /* Responsive Design */
  double deviceWidth = 0;
  double deviceHeight = 0;

  @override
  Widget build(BuildContext context) {
    bool isSender = widget.message.senderID == SignedAccount.instance.id;
    deviceHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    deviceWidth = MediaQuery.of(context).size.width;

    return Row(
      mainAxisAlignment:
          isSender ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (!isSender)
          CircleAvatar(
            backgroundImage: NetworkImage(widget.message.photoUrl!),
          ),
        Container(
          margin: EdgeInsets.only(left: deviceWidth * 0.02),
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: isSender
                  ? Border.all(width: 0, color: Colors.transparent)
                  : Border.all(width: 0.5, color: Colors.black),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: isSender
                    ? [Color(0xffA697F8), Color(0xff6EC2FB)]
                    : [Colors.white, Colors.white],
              )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.message.lastMessageContent!,
                  style:
                      TextStyle(color: isSender ? Colors.white : Colors.black)),
            ],
          ),
        ),
      ],
    );
  }
}
