import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/ui/screens/pet_adopt/pet_adopt_detail_screen.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../models/pet_adopt/pet_adopt.dart';

class PetAdoptItem extends StatefulWidget {
  late PetAdopt petAdopt;

  PetAdoptItem(this.petAdopt);

  @override
  State<PetAdoptItem> createState() => _PetAdoptItemState();
}

class _PetAdoptItemState extends State<PetAdoptItem> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    /* ___________________________ Provider __________________________ */

    return GestureDetector(
      child: Container(
        height: size.height * 0.2,
        width: size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            bottomRight: Radius.circular(20),
            bottomLeft: Radius.circular(20),
            topLeft: Radius.circular(20),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 7,
              spreadRadius: 0.1,
              offset: Offset(1, 2),
            ),
          ],
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              bottom: 0,
              top: 0,
              child: Container(
                width: size.width * 0.48,
                padding:
                    EdgeInsets.only(left: 15, right: 10, top: 10, bottom: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Column(
                  children: [
                    Container(
                      width: size.width,
                      margin: EdgeInsets.only(left: 4),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            widget.petAdopt.name!,
                            style: TextStyle(
                                fontSize: 17.5, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            timeago.format(widget.petAdopt.postTime!),
                            style: TextStyle(fontSize: 10),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: size.width,
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.petAdopt.desc!,
                        overflow: TextOverflow.fade,
                        maxLines: 3,
                        softWrap: true,
                      ),
                    ),
                    Container(
                      width: size.width,
                      margin: EdgeInsets.only(left: 4),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 2.5, horizontal: 10),
                            decoration: BoxDecoration(
                              color: Color(0xffFFEFEC),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                            ),
                            child: Text(
                              EnumToString.convertToString(
                                  widget.petAdopt.species),
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                          Text(
                            widget.petAdopt.condition!,
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              bottom: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                  bottomLeft: Radius.circular(20),
                ),
                child: Container(
                  width: size.width * 0.45,
                  decoration: BoxDecoration(
                    color: Color(0xffFFEFEC),
                  ),
                  child: Image.network(widget.petAdopt.imageUrl![0],
                      fit: BoxFit.contain),
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => PetAdoptDetailScreen(widget.petAdopt),
        ),
      ),
    );
  }
}
