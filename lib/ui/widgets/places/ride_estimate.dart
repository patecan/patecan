import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/places/direction.dart';
import 'package:social_media/ui/widgets/rounded_loading_button.dart';

import '../../../constant/app_colors.dart';

class RideEstimate extends StatefulWidget {
  double deviceWidth = 0.0;
  double deviceHeight = 0.0;

  Direction direction;
  Function createRideRequest;
  Function cancelRequest;

  RideEstimate(
      {required this.deviceHeight,
      required this.deviceWidth,
      required this.direction,
      required this.createRideRequest,
      required this.cancelRequest,
      Key? key})
      : super(key: key);

  @override
  State<RideEstimate> createState() => _RideEstimateState();
}

class _RideEstimateState extends State<RideEstimate> {
  late RoundedLoadingButtonController _requestController;
  bool isRequested = false;
  bool isSearching = false;

  @override
  void initState() {
    _requestController = RoundedLoadingButtonController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.deviceHeight * 0.33,
      width: widget.deviceWidth,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(0.5, 0.5),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 5,
            ),
            if (isSearching == false)
              Container(
                height: widget.deviceHeight * 0.08,
                child: ListTile(
                  leading: Image.asset('assets/images/places/taxi.png',
                      height: 70, width: 70),
                  minLeadingWidth: 10,
                  contentPadding:
                      EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
                  title: Text(
                    'Taxi',
                    style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                  ),
                  subtitle: Text(
                    (widget.direction.distanceValue! > 1000)
                        ? '${(widget.direction.distanceValue! / 1000).toStringAsFixed(2)} km'
                        : '${widget.direction.distanceValue!} m',
                    style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                  ),
                  trailing: Text(
                    '${widget.direction.estimateCost().toString()},000 vnđ',
                    style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                  ),
                ),
              )
            else
              Container(
                height: widget.deviceHeight * 0.08,
                alignment: Alignment.center,
                padding: EdgeInsets.only(top: 8),
                child: DefaultTextStyle(
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                  child: AnimatedTextKit(
                    animatedTexts: [
                      WavyAnimatedText('Requesting...'),
                    ],
                    isRepeatingAnimation: true,
                    onTap: () {
                      print("Tap Event");
                    },
                  ),
                ),
              ),
            SizedBox(
              height: 10,
            ),
            Divider(
              height: 1.0,
              color: AppColors.colorDimText,
              thickness: 1.0,
            ),
            SizedBox(
              height: 10,
            ),
            if (isSearching == false)
              Container(
                child: RoundedLoadingButton(
                  valueColor: Colors.white,
                  color: Colors.lightGreen,
                  borderColor: Colors.lightGreen,
                  successColor: Colors.lightGreen,
                  width: widget.deviceWidth,
                  height: widget.deviceHeight * 0.05,
                  onPressed: () async {
                    _requestController.start();
                    setState(() {
                      _requestController.success();
                      _requestController.stop();
                      isSearching = true;
                      widget.createRideRequest();
                    });
                  },
                  controller: _requestController,
                  child: Text('REQUEST'),
                ),
              )
            else
              Align(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    GestureDetector(
                        child: Container(
                          height: 50,
                          width: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                                width: 1,
                                color: AppColors.colorLightGrayFair),
                          ),
                          child: Icon(Icons.close, size: 25),
                        ),
                        onTap: () {
                          setState(() {
                            widget.cancelRequest();
                            isSearching = false;
                          });
                        }),
                    Text(
                      'Cancel searching',
                      style: TextStyle(fontSize: 10),
                    ),
                  ],
                ),
              ),

            // Container(
            //   child: RoundedLoadingButton(
            //     color: Colors.red,
            //     successColor: Colors.red,
            //     width: widget.deviceWidth,
            //     height: widget.deviceHeight * 0.05,
            //     onPressed: (){
            //         setState(() {
            //           _requestController.success();
            //           _requestController.stop();
            //           isRequested = false;
            //           widget.cancel();
            //         });
            //     },
            //     controller: _requestController,
            //     child: Text('CANCEL'),
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
