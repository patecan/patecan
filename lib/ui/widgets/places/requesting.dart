// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:social_media/constant/brand_colors.dart';
// import 'package:social_media/models/places/direction.dart';
// import 'package:social_media/widgets/rounded_loading_button.dart';
//
// class RideEstimate extends StatefulWidget {
//   double deviceWidth = 0.0;
//   double deviceHeight = 0.0;
//
//   Direction direction;
//   Function request;
//   Function cancel;
//
//   RideEstimate(
//       {required this.deviceHeight,
//       required this.deviceWidth,
//       required this.direction,
//       required this.request,
//       required this.cancel,
//       Key? key})
//       : super(key: key);
//
//   @override
//   State<RideEstimate> createState() => _RideEstimateState();
// }
//
// class _RideEstimateState extends State<RideEstimate> {
//   late RoundedLoadingButtonController _requestController;
//   bool isRequesting = false;
//
//   @override
//   void initState() {
//     _requestController = RoundedLoadingButtonController();
//     super.initState();
//   }
//
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: widget.deviceHeight * 0.33,
//       width: widget.deviceWidth,
//       decoration: BoxDecoration(
//         color: Theme.of(context).backgroundColor,
//         borderRadius: BorderRadius.only(
//           topLeft: Radius.circular(15),
//           topRight: Radius.circular(15),
//         ),
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black,
//             blurRadius: 5.0,
//             spreadRadius: 0.025,
//             offset: Offset(0.5, 0.5),
//           ),
//         ],
//       ),
//       child: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 24),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             SizedBox(
//               height: 5,
//             ),
//             Container(
//               height: widget.deviceHeight * 0.08,
//               child: ListTile(
//                 leading: Image.asset('assets/images/places/taxi.png',
//                     height: 70, width: 70),
//                 minLeadingWidth: 10,
//                 contentPadding:
//                     EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
//                 title: Text(
//                   'Taxi',
//                   style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
//                 ),
//                 subtitle: Text(
//                   (widget.direction.distanceValue! > 1000)
//                       ? '${(widget.direction.distanceValue! / 1000).toStringAsFixed(2)} km'
//                       : '${widget.direction.distanceValue!} m',
//                   style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
//                 ),
//                 trailing: Text(
//                   '${widget.direction.estimateCost().toString()},000 vnđ',
//                   style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
//                 ),
//               ),
//             ),
//             SizedBox(
//               height: 10,
//             ),
//             Container(
//               height: widget.deviceHeight * 0.08,
//               child: ListTile(
//                 leading: Icon(Icons.home, color: BrandColors.colorDimText),
//                 minLeadingWidth: 10,
//                 contentPadding:
//                     EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
//                 title: Text(
//                   "Home",
//                   style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
//                 ),
//                 subtitle: Text(
//                   'Your resident address_book',
//                   style: TextStyle(fontSize: 12, fontFamily: 'bolt'),
//                 ),
//               ),
//             ),
//             Divider(
//               height: 1.0,
//               color: BrandColors.colorDimText,
//               thickness: 1.0,
//             ),
//             if (isRequesting == false)
//               Container(
//                 child: RoundedLoadingButton(
//                   borderColor: Colors.lightGreen,
//                   color: Colors.lightGreen,
//                   valueColor: Colors.white,
//                   successColor: Colors.lightGreen,
//                   width: widget.deviceWidth,
//                   height: widget.deviceHeight * 0.05,
//                   onPressed: () async {
//                     _requestController.start();
//                     Timer(Duration(seconds: 1), () {
//                       setState(() {
//                         _requestController.success();
//                         _requestController.stop();
//                         isRequesting = true;
//                       });
//                     });
//                   },
//                   controller: _requestController,
//                   child: Text('REQUEST'),
//                 ),
//               )
//             else
//               Container(
//                 child: RoundedLoadingButton(
//                   color: Colors.red,
//                   borderColor: Colors.red,
//                   successColor: Colors.red,
//                   width: widget.deviceWidth,
//                   height: widget.deviceHeight * 0.05,
//                   valueColor: Colors.white,
//                   onPressed: () async {
//                     Timer(Duration(seconds: 1), () {
//                       setState(() {
//                         _requestController.success();
//                         _requestController.stop();
//                         isRequesting = false;
//                         widget.cancel();
//                       });
//                     });
//                   },
//                   controller: _requestController,
//                   child: Text('CANCEL'),
//                 ),
//               )
//           ],
//         ),
//       ),
//     );
//   }
// }
