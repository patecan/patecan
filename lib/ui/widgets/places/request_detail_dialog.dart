import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/places/trip.dart';
import 'package:social_media/providers/place_provider.dart';
import 'package:social_media/services/places/map_more_service.dart';

import '../../screens/places/trip/trip_screen.dart';

class RequestDetailDialog extends StatelessWidget {
  final TripDetail tripDetail;
  MapMoreService mapMoreService = MapMoreService();

  RequestDetailDialog({required this.tripDetail});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 5.0,
      backgroundColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.all(4.0),
        width: size.width * 0.95,
        height: size.height * 0.7,
        child: Container(
          margin: EdgeInsets.all(4.0),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(4),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Image.asset(
                "assets/gifs/cat_need_help.gif",
                height: 200.0,
                width: 200.0,
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                "NEED HELP REQUEST",
                style: TextStyle(
                    fontFamily: 'Nunito',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 18),
              ),
              Divider(),
              Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Container(
                      child: ListTile(
                        leading: Image.asset(
                          'assets/images/places/pickicon.png',
                          height: 16,
                          width: 16,
                        ),
                        title: Text(
                          "${tripDetail.pickupPlace.placeFormattedAddress}",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                        ),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        leading: Image.asset(
                          'assets/images/places/desticon.png',
                          height: 16,
                          width: 16,
                        ),
                        title: Text(
                            "${tripDetail.destPlace.placeFormattedAddress}",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(right: 5),
                              height: size.height * 0.06,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      side: BorderSide(color: Colors.black),
                                    ),
                                  ),
                                ),
                                child: Text("Decline",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.black)),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 5),
                              height: size.height * 0.06,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.lightGreen),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                  ),
                                ),
                                child: Text(
                                  "Accept",
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.white),
                                ),
                                onPressed: () async {
                                  await mapMoreService.acceptTrip(tripDetail);
                                  //await MapMoreService.disableLocationUpdate();

                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          ChangeNotifierProvider<PlaceProvider>(
                                        create: (_) => PlaceProvider(),
                                        child: TripDetailScreen(tripDetail),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
