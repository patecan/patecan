import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/singned_account.dart';

class PlacesDrawer extends StatefulWidget {
  double deviceHeight;
  double deviceWidth;

  PlacesDrawer(this.deviceHeight, this.deviceWidth, {Key? key})
      : super(key: key);

  @override
  _PlacesDrawerState createState() => _PlacesDrawerState();
}

class _PlacesDrawerState extends State<PlacesDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.deviceWidth * 0.8,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(
          left: widget.deviceWidth * 0.03, right: widget.deviceWidth * 0.02),
      color: Theme.of(context).backgroundColor,
      alignment: Alignment.topLeft,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(0),
            margin: EdgeInsets.all(0),
            height: widget.deviceHeight * 0.15,
            child: DrawerHeader(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              decoration: BoxDecoration(color: Colors.white),
              child: ListTile(
                contentPadding: EdgeInsets.all(0),
                leading: CircleAvatar(
                    child: Image.network(SignedAccount.instance.photoUrl!)),
                title: Text(SignedAccount.instance.displayName!),
                subtitle: Text(
                  EnumToString.convertToString(
                      SignedAccount.instance.userRoles),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: Icon(Icons.payment),
              title: Text('Payments'),
              subtitle: Text(
                EnumToString.convertToString(SignedAccount.instance.userRoles),
              ),
            ),
          ),
          Divider(height: 1.0, thickness: 1.0),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: Icon(Icons.history),
              title: Text('History'),
              subtitle: Text(
                EnumToString.convertToString(SignedAccount.instance.userRoles),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
