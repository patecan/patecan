import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:social_media/models/places/direction.dart';
import 'package:social_media/models/places/nearby_driver.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/places_configuration.dart';
import 'package:social_media/services/places/map_service.dart';
import 'package:social_media/ui/widgets/rounded_loading_button.dart';

import '../../../constant/app_colors.dart';

class UserPlacesDashboard extends StatefulWidget {
  double deviceWidth = 0.0;
  double deviceHeight = 0.0;

  Direction direction;
  Function createRideRequest;
  Function cancelRequest;

  UserPlacesDashboard(
      {required this.deviceHeight,
      required this.deviceWidth,
      required this.direction,
      required this.createRideRequest,
      required this.cancelRequest,
      Key? key})
      : super(key: key);

  @override
  State<UserPlacesDashboard> createState() => _UserPlacesDashboardState();
}

class _UserPlacesDashboardState extends State<UserPlacesDashboard> {
  late RoundedLoadingButtonController _requestController;
  MapService mapService = new MapService();
  String tripStatusDisplay = "";

  int count = 0;

  bool isDriverFounded = false;
  bool isSearching = false;

  @override
  void initState() {
    _requestController = RoundedLoadingButtonController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.deviceHeight * 0.33,
      width: widget.deviceWidth,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(0.5, 0.5),
          ),
        ],
      ),
      child: StreamBuilder(
        stream: FirebaseDatabase.instance
            .reference()
            .child('ride_request')
            .child(SignedAccount.instance.id!)
            .onValue,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            Map? data = snapshot.data!.snapshot.value;
            if (data != null) {
              if (data['status'] != null) {
                isDriverFounded = true;

                if (data['driver_location'] != null) {
                  double driverLat =
                      double.parse(data['driver_location']['latitude']);
                  double driverLong =
                      double.parse(data['driver_location']['longitude']);

                  nearbyDriverMap[data['driver_id']] = new NearbyDriver(
                    key: data['driver_id'],
                    latitude: driverLat,
                    longitude: driverLong,
                  );

                  if (data['status'] == 'accepted') {
                    print('mất tiền ngu $count');

                    // mapService
                    //     .getDirectionDetail(
                    //         new LatLng(currentPosition!.latitude,
                    //             currentPosition!.longitude),
                    //         new LatLng(driverLat, driverLong))
                    //     .then((directionDetail) {
                    //   if (directionDetail != null) {
                    //     setState(() {
                    //       tripStatusDisplay =
                    //           "${directionDetail.durationText!} ${directionDetail.distanceText!}";
                    //     });
                    //   }
                    // });
                  } else if (data['status'] == 'arrived') {
                    tripStatusDisplay = "Super Hero Arrived";
                  } else {
                    mapService
                        .getDirectionDetail(
                            new LatLng(currentPosition!.latitude,
                                currentPosition!.longitude),
                            new LatLng(driverLat, driverLong))
                        .then((directionDetail) {
                      if (directionDetail != null) {
                        setState(() {
                          tripStatusDisplay =
                              "${directionDetail.durationText!} ${directionDetail.distanceText!}";
                        });
                      }
                    });
                  }
                }
                return buildDriverDetail(
                  MyUser.fromSignedInAccount(SignedAccount.instance),
                );
              } else {
                return buildRequestingDashboard();
              }
            } else {
              return buildRequestingDashboard();
            }
          } else {
            return buildRequestingDashboard();
          }
        },
      ),
    );
  }

  Widget buildRequestingDashboard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 5,
          ),
          if (isSearching == false)
            Container(
              height: widget.deviceHeight * 0.08,
              child: ListTile(
                leading: Image.asset('assets/images/places/taxi.png',
                    height: 70, width: 70),
                minLeadingWidth: 10,
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
                title: Text(
                  'Taxi',
                  style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                ),
                subtitle: Text(
                  (widget.direction.distanceValue! > 1000)
                      ? '${(widget.direction.distanceValue! / 1000).toStringAsFixed(2)} km'
                      : '${widget.direction.distanceValue!} m',
                  style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                ),
                trailing: Text(
                  '${widget.direction.estimateCost().toString()},000 vnđ',
                  style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                ),
              ),
            )
          else
            Container(
              height: widget.deviceHeight * 0.08,
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: 8),
              child: DefaultTextStyle(
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                ),
                child: AnimatedTextKit(
                  animatedTexts: [
                    WavyAnimatedText('Requesting...'),
                  ],
                  isRepeatingAnimation: true,
                  onTap: () {
                    print("Tap Event");
                  },
                ),
              ),
            ),
          SizedBox(
            height: 10,
          ),
          Divider(
            height: 1.0,
            color: AppColors.colorDimText,
            thickness: 1.0,
          ),
          SizedBox(
            height: 10,
          ),
          if (isSearching == false)
            Container(
              child: RoundedLoadingButton(
                valueColor: Colors.white,
                color: Colors.lightGreen,
                borderColor: Colors.lightGreen,
                successColor: Colors.lightGreen,
                width: widget.deviceWidth,
                height: widget.deviceHeight * 0.05,
                onPressed: () async {
                  _requestController.start();
                  setState(() {
                    _requestController.success();
                    _requestController.stop();
                    isSearching = true;
                    widget.createRideRequest();
                  });
                },
                controller: _requestController,
                child: Text('REQUEST'),
              ),
            )
          else
            Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  GestureDetector(
                      child: Container(
                        height: 50,
                        width: 50,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(
                              width: 1, color: AppColors.colorLightGrayFair),
                        ),
                        child: Icon(Icons.close, size: 25),
                      ),
                      onTap: () {
                        setState(() {
                          widget.cancelRequest();
                          isSearching = false;
                        });
                      }),
                  Text(
                    'Cancel searching',
                    style: TextStyle(fontSize: 10),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget buildDriverDetail(MyUser driver) {
    isSearching = false;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 5,
          ),
          Container(
            height: widget.deviceHeight * 0.08,
            child: ListTile(
              leading: Image.network(driver.photoUrl!, height: 70, width: 70),
              minLeadingWidth: 10,
              contentPadding:
                  EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
              title: Text(
                "${driver.displayName!}",
                style: TextStyle(fontSize: 15, fontFamily: 'bolt'),
              ),
              subtitle: Text(
                "${driver.username!} - ${tripStatusDisplay}",
                style: TextStyle(fontSize: 15, fontFamily: 'bolt'),
              ),
              trailing: Text(
                EnumToString.convertToString(driver.userRoles!),
                style: TextStyle(fontSize: 15, fontFamily: 'bolt'),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            height: 1.0,
            color: AppColors.colorDimText,
            thickness: 1.0,
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(25),
                        ),
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      child: Icon(Icons.phone),
                    ),
                    SizedBox(height: 10),
                    Text('call'),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(25),
                        ),
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      child: Icon(Icons.list),
                    ),
                    SizedBox(height: 10),
                    Text('detail'),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                        child: Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(25),
                            ),
                            border: Border.all(width: 1, color: Colors.grey),
                          ),
                          child: Icon(Icons.cancel),
                        ),
                        onTap: () {
                          setState(() {
                            widget.cancelRequest();
                            isSearching = false;
                            isDriverFounded = false;
                          });
                        }),
                    SizedBox(height: 10),
                    Text('Cancel'),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
