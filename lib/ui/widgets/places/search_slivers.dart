import 'package:flutter/material.dart';
import 'package:social_media/ui/screens/places/regular_user/places_search_screen.dart';

import '../../../constant/app_colors.dart';

class SearchSlivers extends StatelessWidget {
  double deviceWidth = 0.0;
  double deviceHeight = 0.0;

  Function drawDirectionPolyline;

  SearchSlivers(
      {required this.deviceHeight,
      required this.deviceWidth,
      required this.drawDirectionPolyline,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: deviceHeight * 0.33,
      width: deviceWidth,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(0.5, 0.5),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 5,
            ),
            Text(
              'Where are you going?',
              style: TextStyle(fontSize: 18, fontFamily: 'bolt'),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.circular(4),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 5.0,
                    spreadRadius: 0.05,
                    offset: Offset(0.7, 0.7),
                  ),
                ],
              ),
              child: GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.search,
                        color: Colors.lightBlueAccent,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text('search destination'),
                    ],
                  ),
                ),
                onTap: () async {
                  dynamic result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PlacesSearchScreen(),
                    ),
                  );
                  switch (result) {
                    case 'getDirection':
                      await drawDirectionPolyline();
                      break;
                    default:
                      break;
                  }
                },
              ),
            ),
            Container(
              height: deviceHeight * 0.08,
              child: ListTile(
                leading: Icon(Icons.home, color: AppColors.colorDimText),
                minLeadingWidth: 10,
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
                title: Text(
                  "Home",
                  style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                ),
                subtitle: Text(
                  'Your resident store places',
                  style: TextStyle(fontSize: 12, fontFamily: 'bolt'),
                ),
              ),
            ),
            Divider(
              height: 1.0,
              color: AppColors.colorDimText,
              thickness: 1.0,
            ),
            Container(
              height: deviceHeight * 0.08,
              child: ListTile(
                leading:
                    Icon(Icons.emoji_people, color: AppColors.colorDimText),
                minLeadingWidth: 10,
                contentPadding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 6.0, right: 0.0),
                title: Text(
                  'Foster',
                  style: TextStyle(fontSize: 14, fontFamily: 'bolt'),
                ),
                subtitle: Text(
                  'Search for foster',
                  style: TextStyle(fontSize: 12, fontFamily: 'bolt'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
