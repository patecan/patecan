import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/services/users/firebase_user_service.dart';

import '../../../../services/vet/time_utils.dart';

class VetMeetingRoomItem extends StatefulWidget {
  DocumentSnapshot document;

  VetMeetingRoomItem({required this.document, Key? key}) : super(key: key);

  @override
  _VetMeetingRoomItemState createState() => _VetMeetingRoomItemState();
}

class _VetMeetingRoomItemState extends State<VetMeetingRoomItem> {
  FirebaseUserHelper userService = new FirebaseUserHelper();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      height: size.height * 0.20,
      width: size.width,
      margin: EdgeInsets.only(top: 5, bottom: 5, left: 0, right: 0),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade400,
            blurRadius: 5.0,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
        child: Stack(
          children: [
            Positioned(
              left: 0,
              child: Row(
                children: [
                  Container(
                    height: size.height * 0.25,
                    padding: EdgeInsets.only(left: 15, top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(widget.document['content'],
                              style: TextStyle(
                                  fontSize: 16.5, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(height: 7.5),
                        Container(
                          width: size.width,
                          child: Row(
                            children: [
                              FaIcon(FontAwesomeIcons.calendar, size: 15),
                              SizedBox(
                                width: 7,
                              ),
                              Text(
                                  "${TimeUtils.toDate(widget.document['from'].toDate())}\n${TimeUtils.toTime(widget.document['from'].toDate())} to ${TimeUtils.toTime(widget.document['to'].toDate())}",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: AppColors.veryFreshBlue)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          width: size.width,
                          child: Row(
                            children: [
                              Icon(Icons.people, size: 15),
                              SizedBox(
                                width: 7,
                              ),
                              Flexible(
                                child: Text(
                                  "${widget.document['members'].length.toString()} participants",
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.blueGrey),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 40,
                          width: size.width,
                          margin: EdgeInsets.only(left: 15),
                          child: Row(
                            children: widget.document['members'].map<Widget>(
                              (document) {
                                return FutureBuilder(
                                  future: userService.getImageOfUser(document),
                                  builder: (context,
                                      AsyncSnapshot<String> snapshot) {
                                    if (snapshot.hasData) {
                                      return CircleAvatar(
                                        backgroundImage:
                                            NetworkImage(snapshot.data!),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  },
                                );
                              },
                            ).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              right: 15,
              top: 5,
              child: Container(
                padding: const EdgeInsets.all(0.0),
                child: Text(
                  widget.document['status'],
                  style: TextStyle(
                      fontSize: 15,
                      color: widget.document['status'] == 'not started'
                          ? Colors.blueGrey
                          : Colors.green),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
