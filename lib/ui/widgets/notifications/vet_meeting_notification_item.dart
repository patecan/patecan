import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/notification/my_vet_meeting_notification.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/services/vet/time_utils.dart';
import 'package:social_media/services/vet/vet_meeting_service.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

import '../../../models/users/singned_account.dart';
import '../../screens/vet/vet_meeting.dart';
import '../gradient_button.dart';

class VetMeetingNotificationItem extends StatefulWidget {
  MyVetMeetingNotification notification;

  VetMeetingNotificationItem(this.notification);

  @override
  State<VetMeetingNotificationItem> createState() =>
      _VetMeetingNotificationItemState();
}

class _VetMeetingNotificationItemState
    extends State<VetMeetingNotificationItem> {
  VetMeetingService _vetMeetingService = new VetMeetingService();

  FirebaseUserHelper firebaseUserService = new FirebaseUserHelper();

  String? activity;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return FutureBuilder(
      future: firebaseUserService.getUserById(widget.notification.invitedBy),
      builder: (context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          MyUser _user = MyUser.fromDocumentSnapshot(snapshot.data);

          return GestureDetector(
            onTap: () {},
            child: Container(
              margin: EdgeInsets.only(top: 5, left: 5, right: 5),
              padding: EdgeInsets.only(bottom: 2.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurStyle: BlurStyle.outer,
                    blurRadius: 5,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              child: Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(_user.photoUrl!),
                    ),
                    title: GestureDetector(
                      onTap: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            text: TextSpan(
                                style: TextStyle(
                                    fontFamily: 'Nunito',
                                    fontSize: 17.5,
                                    color: Colors.black),
                                children: [
                                  TextSpan(
                                    text: _user.displayName,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  TextSpan(
                                    text: " had sent you a meeting invitation",
                                  ),
                                ]),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                              "${TimeUtils.toDate(widget.notification.meetingData['from'].toDate())}\n${TimeUtils.toTime(widget.notification.meetingData['from'].toDate())} to ${TimeUtils.toTime(widget.notification.meetingData['to'].toDate())}",
                              style: TextStyle(
                                  fontFamily: 'Nunito',
                                  fontSize: 15,
                                  color: Colors.black)),
                        ],
                      ),
                    ),
                  ),
                  if (!widget.notification.isAccepted)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 40,
                          width: size.width / 2.2,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          margin: EdgeInsets.only(bottom: 15),
                          child: GradientButton(
                            color1: Color(0xffFFADAD),
                            color2: Color(0xffFFADAD),
                            child: Text('Reject',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white)),
                            onPress: () async {
                              // await FirebaseFirestore.instance
                              //     .collection('proof_you_are_deserved')
                              //     .doc(widget.message.conversationID)
                              //     .collection('proof_response')
                              //     .doc(widget.requestId)
                              //     .delete();
                            },
                          ),
                        ),
                        Container(
                          height: 40,
                          width: size.width / 2.2,
                          padding: EdgeInsets.only(left: 10, right: 10),
                          margin: EdgeInsets.only(bottom: 15),
                          child: GradientButton(
                            color1: AppColors.successColor,
                            color2: AppColors.successColor,
                            child: Text(
                              'Accept',
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            onPress: () async {
                              VetMeeting _vetMeeting = VetMeeting.fromJson(
                                  widget.notification.meetingData);

                              await _vetMeetingService.createVetMeetingRoom(
                                  vetMeeting: _vetMeeting,
                                  createdBy: SignedAccount.instance.id!);

                              await FirebaseFirestore.instance
                                  .collection('vet_meeting_invites')
                                  .doc(widget.notification.id)
                                  .update({'isAccepted': true});

                              setState(() {
                                widget.notification.isAccepted = true;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  if (widget.notification.isAccepted)
                    Container(
                      height: 40,
                      width: size.width,
                      padding: EdgeInsets.only(left: 10, right: 10),
                      margin: EdgeInsets.only(bottom: 15),
                      child: GradientButton(
                        color1: AppColors.lightFreshBlue,
                        color2: AppColors.lightFreshBlue,
                        child: Text(
                          'Accepted',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        onPress: () async {},
                      ),
                    ),
                ],
              ),
            ),
          );
        } else {
          return LinearProgress();
        }
      },
    );
  }
}
