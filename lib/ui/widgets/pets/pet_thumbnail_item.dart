import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/ui/screens/pets/pet_profile_screen.dart';

class PetThumbnailItem extends StatefulWidget {
  Pet pet;

  PetThumbnailItem({required this.pet, Key? key}) : super(key: key);

  @override
  _PetThumbnailItemState createState() => _PetThumbnailItemState();
}

class _PetThumbnailItemState extends State<PetThumbnailItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(width: 1, color: Colors.grey),
                  image: DecorationImage(
                      image: NetworkImage(widget.pet.photoUrl!),
                      fit: BoxFit.cover),
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => PetProfileScreen(ownerId:widget.pet.ownerId! ,petId: widget.pet.id!),
            ),
          );
        });
  }
}
