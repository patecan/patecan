import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:social_media/models/pets/pet.dart';
import 'package:social_media/providers/dark_theme_provider.dart';

class MyPetItem extends StatefulWidget {
  Pet pet;

  MyPetItem(this.pet, {Key? key}) : super(key: key);

  @override
  _MyPetItemState createState() => _MyPetItemState();
}

class _MyPetItemState extends State<MyPetItem> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var themeChange = Provider.of<DarkThemeProvider>(context);
    bool confirmDismiss = false;
    double deviceHeight = (MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top);
    double deviceWidth = MediaQuery.of(context).size.width;

    return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.blue,
        alignment: Alignment.centerRight,
        child: Icon(Icons.delete, color: Colors.white, size: 40),
      ),
      direction: DismissDirection.startToEnd,
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: Text('Delete this product?'),
            actions: [
              ElevatedButton(
                child: Text('Yes'),
                onPressed: () {
                  confirmDismiss = true;
                  Navigator.of(context).pop(true);
                },
              ),
              ElevatedButton(
                child: Text('Cancel'),
                onPressed: () {
                  confirmDismiss = false;
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          ),
        );
      },
      onDismissed: (DismissDirection dismissDirection) async {
        if (confirmDismiss == true) {}
      },
      child: Container(
        height: deviceHeight * 0.15,
        width: deviceWidth,
        margin: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(15)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              spreadRadius: 0.025,
              offset: Offset(0.5, 0.5),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
          child: Stack(
            children: [
              Positioned(
                left: 0,
                top: 0,
                child: Row(
                  children: [
                    Container(
                      width: deviceWidth * 0.27,
                      height: deviceHeight * 0.15,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          bottomLeft: Radius.circular(15),
                        ),
                        child: GridTile(
                          child: GestureDetector(
                            onTap: () => {},
                            child: Image.network(widget.pet.photoUrl!,
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: deviceHeight * 0.15,
                      padding: EdgeInsets.only(left: 15, top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            widget.pet.displayName!,
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 10,
                top: 5,
                child: GestureDetector(
                  child: Row(
                    children: [
                      Text(
                        'managing',
                        style: TextStyle(fontSize: 15, color: Colors.blue),
                      ),
                      Container(
                        padding: const EdgeInsets.all(0.0),
                        width: 18.0,
                        child: Icon(Icons.keyboard_arrow_right,
                            color: Colors.blue),
                      ),
                    ],
                  ),
                  onTap: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
