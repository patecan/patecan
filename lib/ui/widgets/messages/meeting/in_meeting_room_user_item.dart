import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/my_user.dart';

class InMeetingRoomUserItem extends StatefulWidget {
  MyUser myUser;
  Size size;

  InMeetingRoomUserItem({required this.myUser, required this.size, Key? key})
      : super(key: key);

  @override
  _InMeetingRoomUserItemState createState() => _InMeetingRoomUserItemState();
}

class _InMeetingRoomUserItemState extends State<InMeetingRoomUserItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          height: widget.size.height * 0.13,
          width: widget.size.width * 0.21,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 3.5,
                      spreadRadius: 0.1,
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 50,
                      height: 60,
                      padding: EdgeInsets.only(top: 10),
                      child: CircularProfileAvatar(
                        widget.myUser.photoUrl!,
                        radius: 25,
                        backgroundColor: Colors.white,
                        borderWidth: 1.5,
                        borderColor: Colors.blue,
                        elevation: 0.0,
                        cacheImage: true,
                        imageFit: BoxFit.cover,
                        onTap: () {},
                        showInitialTextAbovePicture: false,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text(
                        widget.myUser.username!,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        onTap: () {});
  }
}
