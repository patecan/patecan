import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/services/users/firebase_user_service.dart';
import 'package:social_media/ui/widgets/social/progress.dart';

class OnGoingMeetingRoom extends StatefulWidget {
  String meetingRoomId;

  OnGoingMeetingRoom({required this.meetingRoomId, Key? key}) : super(key: key);

  @override
  _OnGoingMeetingRoomState createState() => _OnGoingMeetingRoomState();
}

class _OnGoingMeetingRoomState extends State<OnGoingMeetingRoom> {
  FirebaseUserHelper userService = new FirebaseUserHelper();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      height: size.height * 0.17,
      width: size.width,
      margin: EdgeInsets.only(top: 5, bottom: 5, left: 0, right: 0),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 5.0,
            spreadRadius: 0.025,
            offset: Offset(1, 3),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
        child: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('meeting_room')
              .doc(widget.meetingRoomId)
              .snapshots(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              DocumentSnapshot documentSnapshot = snapshot.data;

              return Stack(
                children: [
                  Positioned(
                    left: 0,
                    child: Row(
                      children: [
                        Container(
                          height: size.height * 0.17,
                          padding: EdgeInsets.only(left: 15, top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(documentSnapshot['title'],
                                  style: TextStyle(
                                      fontSize: 16.5,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 20,
                                width: size.width,
                                child: Row(
                                  children: [
                                    FaIcon(FontAwesomeIcons.calendar, size: 15),
                                    SizedBox(
                                      width: 7,
                                    ),
                                    Flexible(
                                      child: Text("On going",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.green)),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              SizedBox(
                                height: 20,
                                width: size.width,
                                child: Row(
                                  children: [
                                    Icon(Icons.people, size: 15),
                                    SizedBox(
                                      width: 7,
                                    ),
                                    Flexible(
                                      child: Text(
                                          "${documentSnapshot['invited'].length.toString()} participants",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.black)),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              SizedBox(
                                height: 40,
                                width: size.width,
                                child: Row(
                                  children: documentSnapshot['invited']
                                      .map<Widget>((doc) {
                                    return FutureBuilder(
                                      future:
                                          userService.getImageOfUser(doc['id']),
                                      builder: (context,
                                          AsyncSnapshot<String> snapshot) {
                                        if (snapshot.hasData) {
                                          return CircleAvatar(
                                            backgroundImage:
                                                NetworkImage(snapshot.data!),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      },
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 15,
                    top: 5,
                    child: GestureDetector(
                      child: Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(0.0),
                            width: 15.0,
                            child: Icon(Icons.lock, color: Colors.orange),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),
                ],
              );
            } else {
              return CircularProgress();
            }
          },
        ),
      ),
    );
  }
}
