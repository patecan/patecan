import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_media/models/users/my_user.dart';

class InvitedUserWidget extends StatefulWidget {
  late MyUser myUser;

  InvitedUserWidget({Key? key, required this.myUser}) : super(key: key);

  @override
  _InvitedUserWidgetState createState() => _InvitedUserWidgetState();
}

class _InvitedUserWidgetState extends State<InvitedUserWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(0),
      child: Column(
        children: [
          ListTile(
            dense: true,
            contentPadding: EdgeInsets.all(5),
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              foregroundImage: NetworkImage(widget.myUser.photoUrl!),
            ),
            title: Text(
              widget.myUser.displayName!,
              style: TextStyle(
                  color: Colors.blueAccent, fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              widget.myUser.username!,
              style: TextStyle(color: Colors.black),
            ),
            trailing: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: FaIcon(
                FontAwesomeIcons.microphone,
                color: Colors.lime,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
