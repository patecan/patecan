import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/constant/app_colors.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/messages/message_service.dart';
import 'package:social_media/ui/screens/messages/conversation_detail_screen.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../services/pet_adopt/proof_service.dart';
import '../../../services/vet/vet_service.dart';
import '../../screens/vet/vet_message_detail_screen.dart';

class ContactUserItem extends StatefulWidget {
  late MyUser user;
  String from;

  ContactUserItem({required this.from, required this.user});

  @override
  _ContactUserItemState createState() => _ContactUserItemState();
}

class _ContactUserItemState extends State<ContactUserItem> {
  MessageService messageService = new MessageService();
  ProofService proofYouAreDeservedService = ProofService();
  VetService vetService = VetService();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isOnline = widget.user.isOnline!;

    return GestureDetector(
      onTap: () async {
        if (widget.from == 'vet') {
          Message message = await vetService.getMessageIfNewConversation(
            you: MyUser.fromSignedInAccount(SignedAccount.instance),
            peerId: widget.user.id!,
          );

          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => VetMessageDetailScreen(
                  MyUser.fromSignedInAccount(SignedAccount.instance),
                  widget.user.id!,
                  message),
            ),
          );
        } else {
          Message message = await messageService.getMessageIfNewConversation(
              you: MyUser.fromSignedInAccount(SignedAccount.instance),
              peerId: widget.user.id!);

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  ConversationDetailScreen(widget.user, message),
            ),
          );
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: Column(
          children: [
            ListTile(
              leading: Container(
                width: 50,
                height: 50,
                child: CircleAvatar(
                  backgroundColor: Colors.grey,
                  foregroundImage: NetworkImage(widget.user.photoUrl!),
                ),
              ),
              title: Text(
                widget.user.displayName!,
                style: TextStyle(
                    color: AppColors.colorBlue, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                '@${widget.user.username!}',
                style: TextStyle(color: Colors.black),
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  FutureBuilder(
                    future: getLastTimeOnline(),
                    builder: (context, AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        if (isOnline)
                          return Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              color: Colors.lightGreenAccent,
                              borderRadius: BorderRadius.circular(100),
                            ),
                          );
                        else
                          return Text(
                            timeago.format(snapshot.data),
                            style: TextStyle(fontSize: 13),
                          );
                      } else {
                        return Text(
                          "Offline",
                          style: TextStyle(fontSize: 15),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future getLastTimeOnline() async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(widget.user.id)
        .get();
    return documentSnapshot['lastOnline'].toDate();
  }
}
