import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/messages/message_service.dart';
import 'package:timeago/timeago.dart' as timeago;

class UserItem extends StatefulWidget {
  late MyUser user;
  Function()? onClick;

  UserItem({required this.user, this.onClick});

  @override
  _UserItemState createState() => _UserItemState();
}

class _UserItemState extends State<UserItem> {
  MessageService messageService = new MessageService();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isOnline = widget.user.isOnline!;

    return GestureDetector(
      onTap: widget.onClick,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: Column(
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.grey,
                foregroundImage: NetworkImage(widget.user.photoUrl!),
              ),
              title: Text(
                widget.user.displayName!,
                style: TextStyle(
                    color: Colors.blueAccent, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                widget.user.username!,
                style: TextStyle(color: Colors.black),
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  FutureBuilder(
                    future: getLastTimeOnline(),
                    builder: (context, AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        if (isOnline)
                          return Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              color: Colors.lightGreenAccent,
                              borderRadius: BorderRadius.circular(100),
                            ),
                          );
                        else
                          return Text(
                            timeago.format(snapshot.data),
                            style: TextStyle(fontSize: 15),
                          );
                      } else {
                        return Text(
                          "Offline",
                          style: TextStyle(fontSize: 15),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future getLastTimeOnline() async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(widget.user.id)
        .get();
    return documentSnapshot['lastOnline'].toDate();
  }
}
