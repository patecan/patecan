import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_media/constant/app_colors.dart';

import '../../screens/shorts/upload_short_video_screen.dart';

class CreateYourShortThumbnailItem extends StatefulWidget {
  CreateYourShortThumbnailItem({Key? key}) : super(key: key);

  @override
  _CreateYourShortThumbnailItemState createState() =>
      _CreateYourShortThumbnailItemState();
}

class _CreateYourShortThumbnailItemState
    extends State<CreateYourShortThumbnailItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[
                      AppColors.pinkGradient,
                      AppColors.blueGradient
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                      spreadRadius: 0.005,
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Icon(
                  Icons.add_circle,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => UploadShortVideoScreen()));
        });
  }
}
