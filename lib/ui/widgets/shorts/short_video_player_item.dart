import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class ShortVideoPlayerItem extends StatefulWidget {
  String videoURL;
  ShortVideoPlayerItem({required this.videoURL, Key? key}) : super(key: key);

  @override
  _ShortVideoPlayerItemState createState() => _ShortVideoPlayerItemState();
}

class _ShortVideoPlayerItemState extends State<ShortVideoPlayerItem> {
  late VideoPlayerController videoPlayerController;

  @override
  void initState() {
    setState(() {
      videoPlayerController = VideoPlayerController.network(widget.videoURL);
    });

    videoPlayerController.initialize();
    videoPlayerController.play();
    videoPlayerController.setVolume(1);
    videoPlayerController.setLooping(true);
    super.initState();
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      child: Column(
        children: [
          Container(
              height: size.height * 0.93,
              child: VideoPlayer(videoPlayerController)),
          Container(
            height: size.height * (1 - 0.93),
            color: Colors.black,
          )
        ],
      ),
    );
  }
}
