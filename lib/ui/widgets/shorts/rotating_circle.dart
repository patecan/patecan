import 'package:flutter/material.dart';

class RotatingCircle extends StatefulWidget {
  Widget child;

  RotatingCircle({required this.child, Key? key}) : super(key: key);

  @override
  _RotatingCircleState createState() => _RotatingCircleState();
}

class _RotatingCircleState extends State<RotatingCircle>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 5000));
    animationController.reverse();
    animationController.repeat();
    super.initState();
  }

  @override
  void dispose(){
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: Tween(begin: 0.0, end: 10.0).animate(animationController),
      child: widget.child,
    );
  }
}
