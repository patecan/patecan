import 'package:flutter/material.dart';

class LoginButton extends StatefulWidget {
  const LoginButton({Key? key}) : super(key: key);

  @override
  _LoginButtonState createState() => _LoginButtonState();
}

class _LoginButtonState extends State<LoginButton> {


  @override
  Widget build(BuildContext context) {
    return Container(
      height: (MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top) * 0.05,
      width: MediaQuery.of(context).size.width,
      child: MaterialButton(
        onPressed: (){},
        color: Colors.lightBlue,
        child: Text("LOGIN", style: TextStyle(fontSize: 15,fontWeight: FontWeight.w700, color: Colors.white),),
      ),

    );
  }
}
