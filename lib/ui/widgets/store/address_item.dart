import 'package:flutter/material.dart';

import '../../../models/store/my_address.dart';

class AddressItem extends StatefulWidget {
  List<MyAddress> addressList;
  MyAddress address;
  MyAddress currentAddress;

  AddressItem(this.addressList, this.address, this.currentAddress, {Key? key})
      : super(key: key);

  @override
  _AddressItemState createState() => _AddressItemState();
}

class _AddressItemState extends State<AddressItem> {
  @override
  Widget build(BuildContext context) {
    return RadioListTile<MyAddress>(
      title: Text(widget.address.name!),
      subtitle: Text(widget.address.address!),
      value: widget.address,
      groupValue: widget.currentAddress,
      onChanged: (MyAddress? value) {
        setState(() {
          widget.currentAddress = value!;
        });
      },
    );
  }
}
