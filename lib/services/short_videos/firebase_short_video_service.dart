import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseShortVideoService {
  /* ____________________________________ LIKES ____________________________________ */
  addLike(String videoID, String likerID) async {
    await FirebaseFirestore.instance
        .collection('shorts')
        .doc(videoID)
        .update({
      'likes': FieldValue.arrayUnion([likerID]),
    });
  }

  removeLike(String videoID, String likerID) async {
    await FirebaseFirestore.instance
        .collection('shorts')
        .doc(videoID)
        .update({
      'likes': FieldValue.arrayRemove([likerID]),
    });
  }
}
