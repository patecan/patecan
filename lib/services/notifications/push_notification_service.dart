import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import 'package:social_media/models/users/singned_account.dart';

import '../../models/store/order.dart';
import '../../models/store/property_in_rent_list.dart';

enum NotificationTypes {
  RENT,
  COMMENT,
  MESSAGE,
  INVITE,
}

class PushNotificationService {
  static const String SERVER_KEY =
      'AAAA5hb5u34:APA91bEn4YkYgVvqNLrnkh4Rh-t6MFbuuKDI0MWcL4BGEeMVHoQOtajncTLy1B7Pnii6qFpslEk6w9bzJiZpPX7MasZBF15s2v79uUtnFuPmamk00LlcjPr1Mrkz8tsph3ZaoBC2p5cs';

  static const String SENDER_ID = '988227943294';
  static const String API_URL = 'https://fcm.googleapis.com/fcm/send';

  static const Map<String, String> HEADER = {
    'Authorization': 'key=$SERVER_KEY',
    'Content-Type': 'application/json',
  };

  FirebaseMessaging fcm = FirebaseMessaging.instance;

  Future initialize() async {
    NotificationSettings settings = await fcm.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    print('User granted permission: ${settings.authorizationStatus}');

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
      }
    });

    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  }

  Future<void> getToken() async {
    String? token = await FirebaseMessaging.instance.getToken();
    await FirebaseFirestore.instance
        .collection('users')
        .doc(SignedAccount.instance.id)
        .update({'androidNotificationToken': token!});
    print('Token $token');

    await fcm.subscribeToTopic('alldriders');
    await fcm.subscribeToTopic('allusers');
  }

  Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
    print("Handling a background message");
  }

  Future<void> sendPushNotification(
      {required String toUserId,
      required String notifyTitle,
      required String notifyBody,
      Map<String, dynamic>? data}) async {
    Uri apiUrl = Uri.parse(API_URL);
    late http.Response response;

    String? userToken;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(toUserId)
        .get()
        .then((document) {
      userToken = document['androidNotificationToken'];
    });

    try {
      if (userToken != null) {
        var body = jsonEncode(
          {
            "senderId": SENDER_ID,
            "category": "",
            "collapseKey": "",
            "data": data,
            "from": "",
            "messageId": "",
            "messageType": "",
            "sentTime": "",
            "to": userToken,
            "notification": {
              "body": notifyTitle,
              "title": notifyBody,
            }
          },
        );
        response = await http.post(
          apiUrl,
          headers: HEADER,
          body: body,
        );
      }
    } catch (error) {
      print('Error occurred : ${error.toString()}');
    }
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print('success');
    } else {
      print('something went wrong with');
    }
  }

  sendPushNotificationForCorrectUser(
      {required NotificationTypes notifyType,
      required String notifyTitle,
      Order? newOrder,
      required String notifyBody}) async {
    if (notifyType == NotificationTypes.RENT) {
      Set<String> differentLessorsId = new Set<String>();

      newOrder!.productsInRentListList.forEach(
        (product) {
          differentLessorsId.add(product.product.ownerId);
        },
      );

      Map<String, List<PropertyInRentList>> productsOfEachLessor =
          Map<String, List<PropertyInRentList>>();

      for (String lessorId in differentLessorsId) {
        List<PropertyInRentList> thisLessorProducts = newOrder
            .productsInRentListList
            .where((productsInRentList) =>
                productsInRentList.product.ownerId == lessorId)
            .toList();
        productsOfEachLessor.putIfAbsent(lessorId, () => thisLessorProducts);
      }

      productsOfEachLessor.forEach(
        (String lessorId,
            List<PropertyInRentList> productsInRentListList) async {
          productsInRentListList.forEach(
            (productsInRentList) {
              print("product in RentList: " +
                  productsInRentList.product.toString());

              sendStoreNotification(
                  toUserId: lessorId,
                  notifyTitle:
                      'New f*ucking rent request for ${productsInRentList.product.name}',
                  notifyBody:
                      '${productsInRentList.product.name} had been requested for renting ');
            },
          );
        },
      );
    }
  }

  Future sendStoreNotification(
      {required String toUserId,
      required String notifyTitle,
      required String notifyBody,
      Map<String, dynamic>? data}) async {
    Uri apiUrl = Uri.parse(API_URL);
    late http.Response response;

    String? userToken;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(toUserId)
        .get()
        .then((document) {
      userToken = document['androidNotificationToken'];
    });

    try {
      if (userToken != null) {
        var body = jsonEncode(
          {
            "senderId": SENDER_ID,
            "category": "",
            "collapseKey": "",
            "data": {},
            "from": "",
            "messageId": "",
            "messageType": "",
            "sentTime": "",
            "to": userToken,
            "notification": {
              "body": notifyTitle,
              "title": notifyBody,
            }
          },
        );

        response = await http.post(
          apiUrl,
          headers: HEADER,
          body: body,
        );
      }
    } catch (error) {
      print('Error occurred : ${error.toString()}');
    }
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print('send notification success');
    } else {
      print('send notification error');
    }
  }

  Future sendRideRequestNotification(
      {required String toUserId,
      required Map<String, dynamic> rideRequestMap,
      required String notifyTitle,
      required String notifyBody,
      Map<String, dynamic>? data}) async {
    print('sending notification...');
    Uri apiUrl = Uri.parse(API_URL);
    late http.Response response;

    String? userToken;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(toUserId)
        .get()
        .then((document) {
      userToken = document['androidNotificationToken'];
    });

    try {
      if (userToken != null) {
        var body = jsonEncode(
          {
            "senderId": SENDER_ID,
            "category": "",
            "collapseKey": "",
            "data": {
              "ride_request": rideRequestMap,
            },
            "from": "",
            "messageId": "",
            "messageType": "",
            "sentTime": "",
            "to": userToken,
            "notification": {
              "body": notifyTitle,
              "title": notifyBody,
            }
          },
        );

        response = await http.post(
          apiUrl,
          headers: HEADER,
          body: body,
        );
      }
    } catch (error) {
      print('Error occurred : ${error.toString()}');
    }
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print('send notification success');
    } else {
      print('send notification error');
    }
  }
}
