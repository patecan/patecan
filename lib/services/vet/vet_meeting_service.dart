import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/notifications/push_notification_service.dart';
import 'package:social_media/ui/screens/vet/vet_meeting.dart';
import 'package:uuid/uuid.dart';

class VetMeetingService {
  CollectionReference vetMeetingInviteRef =
      FirebaseFirestore.instance.collection('vet_meeting_invites');
  PushNotificationService pushNotificationService = PushNotificationService();

  Future<void> sendVetMeetingInvite(
      {required String member, required VetMeeting vetMeeting}) async {
    String inviteId = Uuid().v4();

    await vetMeetingInviteRef.doc(inviteId).set({
      'id': inviteId,
      'userInvited': member,
      'invitedBy': SignedAccount.instance.id,
      'isAccepted': false,
      'createdBy': SignedAccount.instance.id,
      'meetingData': vetMeeting.toJson(),
      'time': DateTime.now(),
    }).then((result) async {
      await pushNotificationService.sendPushNotification(
          toUserId: member,
          notifyTitle: "Invite",
          notifyBody: "${SignedAccount.instance.displayName} is inviting you",
          data: {'invite': vetMeeting.toString()});
    });
  }

  Future<void> createVetMeetingRoom(
      {required VetMeeting vetMeeting, required String createdBy}) async {
    String meetingId = Uuid().v4();

    await FirebaseFirestore.instance
        .collection('vet_meeting_rooms')
        .doc(meetingId)
        .set(
      {
        'id': meetingId,
        'content': vetMeeting.content,
        'createdBy': createdBy,
        'members': vetMeeting.members,
        'currentUsersInThisRoom': <String>[],
        'createdOn': DateTime.now(),
        'from': vetMeeting.from,
        'to': vetMeeting.to,
        'status': 'not started',
      },
    );
  }

  Future<void> joinMeetingRoom(String userId, String meetingId) async {
    await FirebaseFirestore.instance
        .collection('meeting_room')
        .doc(meetingId)
        .update({
      'currentUsersInThisRoom': FieldValue.arrayUnion([
        {
          'id': userId,
        }
      ]),
      'status': 'onGoing',
    });
  }

  Future<void> leaveMeetingRoom(String userId, String meetingId) async {
    await FirebaseFirestore.instance
        .collection('meeting_room')
        .doc(meetingId)
        .update({
      'currentUsersInThisRoom': FieldValue.arrayRemove([
        {
          'id': userId,
        }
      ]),
    });
  }
}
