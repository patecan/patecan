import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:social_media/models/messages/conversation.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:uuid/uuid.dart';

class VetService {
  Future<Message> getMessageIfNewConversation(
      {required MyUser you, required String peerId}) async {
    String? conversationId = await createOrGetConversationId(you.id!, peerId);

    late Message message;
    if (conversationId == null) {
      message = Message(
        displayName: you.displayName,
        photoUrl: you.photoUrl,
      );
    } else {
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('vet_messages')
          .where('conversationID', isEqualTo: conversationId)
          .get();

      Conversation existedConversation =
          Conversation.fromDocument(querySnapshot.docs.first);

      message = existedConversation.messages.last;
    }
    return message;
  }

  Future sendMessage(String conversationID, Message message) async {
    DocumentReference conversationRef = FirebaseFirestore.instance
        .collection('vet_messages')
        .doc(conversationID);

    Map<String, dynamic> newMessage = {};

    newMessage['_id'] = new Uuid().v4();
    newMessage['displayName'] = SignedAccount.instance.displayName;
    newMessage['photoUrl'] = message.photoUrl == null
        ? message.lastMessageContent
        : message.photoUrl;
    newMessage['message_content'] = message.lastMessageContent;
    newMessage['senderID'] = message.senderID;
    newMessage['sentTime'] = message.sentTime;

    switch (message.messageType) {
      case MessageType.Text:
        newMessage['message_type'] = 'text';
        break;
      case MessageType.Image:
        newMessage['message_type'] = 'image';
        break;
      case MessageType.Video:
        newMessage['message_type'] = 'video';
        break;
      case MessageType.GIF:
        newMessage['message_type'] = 'gif';
        break;
      case MessageType.Voice:
        newMessage['message_type'] = 'voice';
        break;
      default:
        newMessage['message_type'] = 'text';
        break;
    }
    await conversationRef.update({
      'messages': FieldValue.arrayUnion([newMessage])
    });
  }

  Future sendNewMessage(
      {required String receiverID,
      String? imageURL,
      String? messageContent,
      required Function setNewConversation}) async {
    String conversationID = Uuid().v4();
    var conversationRef = FirebaseFirestore.instance
        .collection('vet_messages')
        .doc(conversationID);

    await FirebaseFirestore.instance
        .collection('vet_messages')
        .doc(conversationID)
        .set(
      {
        'conversationID': conversationRef.id,
        'members': [
          SignedAccount.instance.id,
          receiverID,
        ],
        'ownerID': SignedAccount.instance.id,
        'requestPoint': 0.0,
        'messages': [
          {
            '_id': new Uuid().v4(),
            'displayName': SignedAccount.instance.displayName,
            'senderID': SignedAccount.instance.id,
            'message_content': imageURL == null ? messageContent : imageURL,
            'photoUrl': imageURL == null ? messageContent : imageURL,
            'sentTime': DateTime.now(),
            'message_type': imageURL == null ? 'text' : 'image',
          }
        ],
      },
    );

    //onCreateConversation(conversationID);
    setNewConversation(conversationID);
  }

  Future<String> uploadImage(String userID, File imageFile) async {
    Reference storageReferences = FirebaseStorage.instance.ref();
    String imageFileID = new Uuid().v4();
    late String imageURL;
    try {
      //imageFile = await compressImage(imageFile, imageFileID);
      UploadTask uploadTask = storageReferences
          .child('images')
          .child(userID)
          .child('message')
          .child("message_$imageFileID")
          .putFile(imageFile);
      TaskSnapshot taskSnapshot = await uploadTask;
      imageURL = await taskSnapshot.ref.getDownloadURL();
    } catch (e) {
      print(e.toString());
    }
    return imageURL;
  }

  Future<String?> createOrGetConversationId(
      String senderID, String receiverID) async {
    CollectionReference userConversation = FirebaseFirestore.instance
        .collection('users')
        .doc(senderID)
        .collection('vet_messages');

    QuerySnapshot querySnapshot = await FirebaseFirestore.instance
        .collection('vet_messages')
        .where('members', arrayContains: senderID)
        .get();

    if (querySnapshot.docs.length > 0) {
      DocumentSnapshot documentSnapshot = querySnapshot.docs
          .firstWhere((element) => element['members'].contains(receiverID));
      if (documentSnapshot.data() != null) {
        return documentSnapshot['conversationID'];
      }
    }
    return null;
  }
}
