import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseStoriesService {
  /* ____________________________________ LIKES ____________________________________ */
  addLike(String storyID, String likerID) async {
    await FirebaseFirestore.instance.collection('stories').doc(storyID).update({
      'likes': FieldValue.arrayUnion([likerID]),
    });
  }

  removeLike(String storyID, String likerID) async {
    await FirebaseFirestore.instance.collection('stories').doc(storyID).update({
      'likes': FieldValue.arrayRemove([likerID]),
    });
  }
}
