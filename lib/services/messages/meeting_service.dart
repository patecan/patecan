import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:social_media/models/clubhouse/meeting_room.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/notifications/push_notification_service.dart';
import 'package:social_media/ui/screens/vet/vet_meeting.dart';
import 'package:uuid/uuid.dart';

class MeetingRoomService {
  CollectionReference inviteRef =
      FirebaseFirestore.instance.collection('invites');
  PushNotificationService pushNotificationService = PushNotificationService();

  Future<void> inviteUser(String id, int totalInvite) async {
    await inviteRef.add({
      'userInvited': id,
      'invitedBy': SignedAccount.instance.id,
      'isAccepted': false,
      'time': DateTime.now(),
    }).then((result) async {
      int totalInviteLeft = totalInvite - 1;

      await pushNotificationService.sendPushNotification(
          toUserId: id,
          notifyTitle: "Invite",
          notifyBody: "${SignedAccount.instance.displayName} is calling you",
          data: {'invite': SignedAccount.instance.displayName});
    });
  }

  Future<void> createMeetingRoom(
      {required String title,
      required MeetingTypes meetingType,
      required List<MyUser> invitedParticipants,
      required DateTime meetingDateTime}) async {
    String meetingId = Uuid().v4();

    await FirebaseFirestore.instance
        .collection('meeting_room')
        .doc(meetingId)
        .set({
      'id': meetingId,
      'title': title,
      'meetingType': EnumToString.convertToString(meetingType),
      'createdBy': SignedAccount.instance.id,
      'invited': FieldValue.arrayUnion(invitedParticipants.map((user) async {
        inviteUser(user.id!, invitedParticipants.length);
        return {
          'id': user.id,
          'username': user.username,
        };
      }).toList()),
      'currentUsersInThisRoom': <String>[],
      'createdOn': DateTime.now(),
      'meetingDateTime': meetingDateTime,
      'status': 'new',
    });
  }

  Future<void> inviteVetMeeting(
      {required String memberId, required VetMeeting vetMeeting}) async {
    String inviteId = Uuid().v4();

    await inviteRef.doc(inviteId).set({
      'id': inviteId,
      'userInvited': memberId,
      'invitedBy': SignedAccount.instance.id,
      'isAccepted': false,
      'meetingData': vetMeeting.toString(),
      'time': DateTime.now(),
    }).then((result) async {
      await pushNotificationService.sendPushNotification(
          toUserId: memberId,
          notifyTitle: "Invite",
          notifyBody: "${SignedAccount.instance.displayName} is inviting you",
          data: {'invite': vetMeeting.toString()});
    });
  }

  Future<void> createVetMeetingRoom(
      {required String title,
      required String invitedParticipant,
      required DateTime meetingDateTime}) async {
    String meetingId = Uuid().v4();

    await FirebaseFirestore.instance
        .collection('vet_meeting_room')
        .doc(meetingId)
        .set(
      {
        'id': meetingId,
        'title': title,
        'createdBy': SignedAccount.instance.id,
        'members': [SignedAccount.instance.id, invitedParticipant],
        'currentUsersInThisRoom': <String>[],
        'createdOn': DateTime.now(),
        'meetingDateTime': meetingDateTime,
        'status': 'not started',
      },
    );
  }

  Future<void> joinVetMeetingRoom(String userId, String meetingId) async {
    await FirebaseFirestore.instance
        .collection('vet_meeting_rooms')
        .doc(meetingId)
        .update({
      'currentUsersInThisRoom': FieldValue.arrayUnion([
        {
          'id': userId,
        }
      ]),
      'status': 'on going',
    });
  }

  Future<void> leaveVetMeetingRoom(String userId, String meetingId) async {
    await FirebaseFirestore.instance
        .collection('vet_meeting_rooms')
        .doc(meetingId)
        .update({
      'currentUsersInThisRoom': FieldValue.arrayRemove([
        {
          'id': userId,
        }
      ]),
    });
  }

  Future<void> joinMeetingRoom(String userId, String meetingId) async {
    await FirebaseFirestore.instance
        .collection('meeting_room')
        .doc(meetingId)
        .update({
      'currentUsersInThisRoom': FieldValue.arrayUnion([
        {
          'id': userId,
        }
      ]),
      'status': 'onGoing',
    });
  }

  Future<void> leaveMeetingRoom(String userId, String meetingId) async {
    await FirebaseFirestore.instance
        .collection('meeting_room')
        .doc(meetingId)
        .update({
      'currentUsersInThisRoom': FieldValue.arrayRemove([
        {
          'id': userId,
        }
      ]),
    });
  }
}
