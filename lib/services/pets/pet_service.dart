import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;
import 'package:social_media/models/users/singned_account.dart';

import '../../constant/server_connection.dart';
import '../../models/pets/pet.dart';

class PetService {
  CollectionReference petsCollection =
      FirebaseFirestore.instance.collection('pets');

  Future<bool> transferPet(
      String senderID, String petID, String receiverID) async {
    DocumentSnapshot document = await petsCollection
        .doc(senderID)
        .collection('petOwned')
        .doc(petID)
        .get();

    List<DocumentSnapshot> eventHistoryDocumentsList = (await petsCollection
            .doc(senderID)
            .collection('petOwned')
            .doc(petID)
            .collection('EventsHistory')
            .get())
        .docs;

    Map<String, dynamic> petData = document.data() as Map<String, dynamic>;
    petData['ownerID'] = receiverID;

    await petsCollection
        .doc(receiverID)
        .collection('petOwned')
        .doc(petID)
        .set(petData);

    eventHistoryDocumentsList.forEach((document) async {
      await petsCollection
          .doc(receiverID)
          .collection('petOwned')
          .doc(petID)
          .collection("EventsHistory")
          .doc(document.reference.id)
          .set(document.data() as Map<String, dynamic>);
    });

    await petsCollection
        .doc(senderID)
        .collection('petOwned')
        .doc(petID)
        .delete();

    return true;
  }

  Future<List<Pet>?> getAllPetsFromSpring() async {
    http.Response response = await http.get(Endpoints.allPets,
        headers: SpringServerConnection.HEADER);
    print(response.body);
    if (response.statusCode == 200) {
      List<Pet> petsList = [];
      var result = json.decode(utf8.decode(response.bodyBytes));
      result.forEach((json) {
        Pet pet = Pet.fromJson(json);
        petsList.add(pet);
      });
      return petsList;
    } else {
      return null;
    }
  }

  Future<List<Pet>?> getAllPetsOfUser() async {
    List<Pet>? petsList;
    QuerySnapshot snapshot = await petsCollection
        .doc(SignedAccount.instance.id)
        .collection('petOwned')
        .get();

    List<DocumentSnapshot> documentList = snapshot.docs;

    if (documentList.length > 0) {
      petsList = documentList
          .map((document) =>
              Pet.fromJson(document.data() as Map<String, dynamic>))
          .toList();
    }
    return petsList;
  }
}
