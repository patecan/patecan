import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:social_media/services/social_media/firebase_post_service.dart';
import 'package:social_media/models/users/singned_account.dart';

class FirebaseCommentHelper {
  CollectionReference commentRef =
      FirebaseFirestore.instance.collection('comments');
  CollectionReference shortVideoRef =
      FirebaseFirestore.instance.collection('shorts');

  Reference storageReferences = FirebaseStorage.instance.ref();

  FirebasePostHelper firebasePostHelper = new FirebasePostHelper();

  /* ____________________________________ READ ____________________________________ */
  Future getPostComments(String postID) async{
    await commentRef.doc(postID).collection('postComments').get();
  }

/* ____________________________________ INSERT ____________________________________ */

  addPostComment(String postID, String comment) {
    commentRef.doc(postID).collection('postComments').add({
      'postID': postID,
      'username': SignedAccount.instance.username,
      'userID': SignedAccount.instance.id,
      'comment': comment,
      'commentTime': DateTime.now(),
    });
  }

  addShortVideoComment(String videoID, String comment) {
    shortVideoRef.doc(videoID).collection('comments').add({
      'shortVideoID': videoID,
      'username': SignedAccount.instance.username,
      'userID': SignedAccount.instance.id,
      'comment': comment,
      'commentTime': DateTime.now(),
    });
  }

/* ____________________________________ UPDATE ____________________________________ */

/* ____________________________________ DELETE ____________________________________ */

  deleteAllComment(String postID) async {
    QuerySnapshot querySnapshot =
        await commentRef.doc(postID).collection('postComments').get();
    querySnapshot.docs.forEach((document) {
      if (document.exists) {
        document.reference.delete();
      }
    });
  }
}
