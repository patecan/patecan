import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_geofire/flutter_geofire.dart';
import 'package:http/http.dart' as http;
import 'package:social_media/models/places/trip.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/notifications/push_notification_service.dart';

import '../../models/places/trip.dart';
import '../../models/users/my_user.dart';
import '../../places_configuration.dart';
import '../users/firebase_user_service.dart';

class MapMoreService {
  DatabaseReference placeRealTimeDB = FirebaseDatabase.instance.reference();

  CollectionReference usersCollection =
      FirebaseFirestore.instance.collection('users');

  PushNotificationService pushNotificationService =
      new PushNotificationService();

  FirebaseUserHelper userService = FirebaseUserHelper();

  Future<List<String>> getIdOfAvailableDrivers() async {
    List<String> idList = [];

    Uri url = Uri(
        scheme: 'https',
        host:
            'flutter-social-media-7fb3e-default-rtdb.asia-southeast1.firebasedatabase.app',
        path: 'driverAvailable.json');
    http.Response response = await http.get(url);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      Map<String, dynamic> responseMap =
          json.decode(response.body) as Map<String, dynamic>;

      responseMap.keys.forEach((availableDriverId) async {
        idList.add(availableDriverId);
      });
    }

    return idList;
  }

  Future<void> acceptTrip(TripDetail tripDetail) async {
    String rideID = tripDetail.rideID;
    MyUser foster = MyUser.fromSignedInAccount(SignedAccount.instance);
    await getCurrentPosition();

    await placeRealTimeDB
        .child('ride_request')
        .child('$rideID')
        .child('status')
        .set('accepted');

    await placeRealTimeDB
        .child('ride_request')
        .child('$rideID')
        .child('driver_id')
        .set("${foster.id}");

    await placeRealTimeDB
        .child('ride_request')
        .child('$rideID')
        .child('driver_name')
        .set("${foster.displayName}");

    await placeRealTimeDB
        .child('ride_request')
        .child('$rideID')
        .child('driver_location')
        .set({
      'latitude': currentPosition!.latitude.toString(),
      'longitude': currentPosition!.longitude.toString(),
    });
  }

  static Future<void> disableLocationUpdate() async {
    //positionStream!.pause();
    await Geofire.removeLocation(SignedAccount.instance.id!);
  }

  static Future<void> enableLocationUpdate() async {
    //positionStream!.resume();
    await Geofire.setLocation(
        SignedAccount.instance.id!,
        (await getCurrentPosition()).latitude,
        (await getCurrentPosition()).longitude);
  }
}
