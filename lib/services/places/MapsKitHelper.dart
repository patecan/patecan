import 'package:maps_toolkit/maps_toolkit.dart';

class MapsKitHelper {
  static double getMarkerRotation(
      double sourceLat, double sourceLong, double destLat, double destLong) {
    num rotation = SphericalUtil.computeHeading(
        LatLng(sourceLat, sourceLong), LatLng(destLat, destLong));

    return rotation as double;
  }
}
