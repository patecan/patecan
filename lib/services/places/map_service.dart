import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:social_media/constant/credentials.dart';
import 'package:social_media/constant/user_roles.dart';
import 'package:social_media/models/places/direction.dart';
import 'package:social_media/models/places/place.dart';
import 'package:social_media/models/places/predicted_place.dart';
import 'package:social_media/models/places/trip.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/place_provider.dart';

import '../../models/places/trip.dart';
import '../../models/users/my_user.dart';
import '../users/firebase_user_service.dart';

class MapService {
  /*______________________________ GET CURRENT POSITION ________________________________*/
  Future<Position> setupCurrentPositionLocation(
      Position currentPosition, BuildContext context) async {
    String? currentAddress =
        await getAddress(currentPosition.latitude, currentPosition.longitude);
    Place currentPlace = new Place(
      placeName: currentAddress,
      latitude: currentPosition.latitude,
      longitude: currentPosition.longitude,
      placeFormattedAddress: currentAddress,
    );
    if (SignedAccount.instance.userRoles == UserRoles.user) {
      Provider.of<PlaceProvider>(context, listen: false)
          .setNewPickupPlace(currentPlace);
    }

    return currentPosition;
  }

  Future<Position> setupCurrentStorePositionLocation(
      Position currentPosition, BuildContext context) async {
    Position currentPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.bestForNavigation);

    String? currentAddress =
        await getAddress(currentPosition.latitude, currentPosition.longitude);
    Place place = new Place(
      placeName: currentAddress,
      latitude: currentPosition.latitude,
      longitude: currentPosition.longitude,
      placeFormattedAddress: currentAddress,
    );
    Provider.of<PlaceProvider>(context, listen: false).setNewStorePlace(place);

    return currentPosition;
  }

  /*______________________________ GET ADDRESS ________________________________*/
  Future<String?> getAddress(double latitude, double longitude) async {
    try {
      Uri url = Uri.parse(
          'https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&key=${Credentials.GOOGLE_CLOUD_API_KEY}');

      http.Response response = await http.get(url);

      if (response.statusCode >= 200 && response.statusCode < 300) {
        String address =
            json.decode(response.body)['results'][0]['formatted_address'];
        return address;
      } else {
        return null;
      }
    } catch (e) {
      print(e.toString());
    }
  }

  /*______________________________ GET PREDICTED PLACE LIST ________________________________*/
  List getPredictedPlacesList(http.Response response) {
    Map valueMap = json.decode(response.body);
    List<dynamic> predictedPlaceList = valueMap['predictions'].map((item) {
      return PredictedPlace.fromJSON(item);
    }).toList();

    return predictedPlaceList;
  }

  /*================================ GET PLACE DETAIL ================================*/
  getPlaceDetail(String placeID, BuildContext context) async {
    Uri url = Uri.parse(
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeID&key=${Credentials.GOOGLE_CLOUD_API_KEY}');
    http.Response response = await http.get(url);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      Place destinationPlace = new Place();

      destinationPlace.placeName = json.decode(response.body)['result']['name'];
      destinationPlace.placeFormattedAddress =
          json.decode(response.body)['result']['formatted_address'];
      destinationPlace.placeID =
          json.decode(response.body)['result']['place_id'];
      destinationPlace.latitude =
          json.decode(response.body)['result']['geometry']['location']['lat'];
      destinationPlace.longitude =
          json.decode(response.body)['result']['geometry']['location']['lng'];

      Provider.of<PlaceProvider>(context, listen: false)
          .setNewDestinationPlace(destinationPlace);

      Navigator.pop(context, 'getDirection');
    } else {
      return null;
    }
  }

  Future getStorePlaceDetail(String placeID, BuildContext context) async {
    Uri url = Uri.parse(
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeID&key=${Credentials.GOOGLE_CLOUD_API_KEY}');

    http.Response response = await http.get(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      Place place = new Place();

      place.placeName = json.decode(response.body)['result']['name'];
      place.placeFormattedAddress =
          json.decode(response.body)['result']['formatted_address'];
      place.placeID = json.decode(response.body)['result']['place_id'];
      place.latitude =
          json.decode(response.body)['result']['geometry']['location']['lat'];
      place.longitude =
          json.decode(response.body)['result']['geometry']['location']['lng'];

      Provider.of<PlaceProvider>(context, listen: false)
          .setNewStorePlace(place);
      print(Provider.of<PlaceProvider>(context, listen: false).storePlace);
      Navigator.pop(context, 'getDirection');
    } else {
      return null;
    }
  }

/*================================ GET DIRECTION DETAIL ================================*/
  Future<Direction?> getDirectionDetail(
      LatLng startPosition, LatLng endPosition) async {
    Uri url = Uri.parse(
        "https://maps.googleapis.com/maps/api/directions/json?origin=${startPosition.latitude},${startPosition.longitude}&destination=${endPosition.latitude},${endPosition.longitude}&mode=driving&key=${Credentials.GOOGLE_CLOUD_API_KEY}");
    http.Response response = await http.get(url);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      Direction direction = new Direction();
      direction.durationText = json.decode(response.body)['routes'][0]['legs']
          [0]['duration']['text'];
      direction.durationValue = json.decode(response.body)['routes'][0]['legs']
          [0]['duration']['value'];
      direction.distanceText = json.decode(response.body)['routes'][0]['legs']
          [0]['distance']['text'];
      direction.distanceValue = json.decode(response.body)['routes'][0]['legs']
          [0]['distance']['value'];
      direction.encodedPoints = json.decode(response.body)['routes'][0]
          ['overview_polyline']['points'];

      return direction;
    } else {
      return null;
    }
  }

/*================================ GET REQUEST FOR HELP DETAIL ================================*/
  Future<TripDetail?> getRequestDetails(
      BuildContext context, Map<String, dynamic> rideRequestMap) async {
    TripDetail? trip;

    double pickupLat =
        double.parse(rideRequestMap['pickup_location']['latitude']);
    double pickupLng =
        double.parse(rideRequestMap['pickup_location']['longitude']);
    String pickupAddress =
        rideRequestMap['pickup_location']['address'].toString();

    Place pickupPlace = new Place(
        latitude: pickupLat,
        longitude: pickupLng,
        placeFormattedAddress: pickupAddress);

    double destLat =
        double.parse(rideRequestMap['destination_location']['latitude']);
    double destLng =
        double.parse(rideRequestMap['destination_location']['longitude']);
    String destAddress =
        rideRequestMap['destination_location']['address'].toString();

    Place destPlace = new Place(
        latitude: destLat,
        longitude: destLng,
        placeFormattedAddress: destAddress);

    MyUser requester = MyUser.fromDocumentSnapshot(
        await new FirebaseUserHelper()
            .getUserById(rideRequestMap['requester_id']));

    trip = TripDetail(
        rideID: rideRequestMap['ride_id'],
        requester: requester,
        foster: null,
        pickupPlace: pickupPlace,
        destPlace: destPlace);

    return trip;
  }
}
