import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/pets/pet.dart';

class NfcService {
  CollectionReference petRef = FirebaseFirestore.instance.collection('pets');

  Future<DocumentSnapshot?> searchPetByID(String petID) async {
    List<DocumentSnapshot> ownerDocsList = (await petRef.get()).docs;

    DocumentSnapshot? foundPet;

    for (DocumentSnapshot ownerDoc in ownerDocsList) {
      List<DocumentSnapshot> docsList =
          (await petRef.doc(ownerDoc.reference.id).collection('petOwned').get())
              .docs;

      if (docsList.where((doc) => doc.reference.id == petID).length > 0) {
        DocumentSnapshot foundPetDocument =
            docsList.firstWhere((doc) => doc.reference.id == petID);

        foundPet = foundPetDocument;
      }
    }

    return foundPet;
  }
}
