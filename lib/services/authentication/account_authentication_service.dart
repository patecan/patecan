import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:social_media/constant/user_roles.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/ui/screens/authentication/account_register_screen.dart';

import '../../ui/screens/homepage_screen.dart';
import '../users/firebase_user_service.dart';

class AccountAuthenticationService {
  late BuildContext context;
  late FirebaseUserHelper _userService;

  /*  Email Auth  */
  static late FirebaseAuth firebaseAuth;
  User? _currentEmailUser;

  /*  Google Auth  */
  static final GoogleSignIn googleSignIn = GoogleSignIn();
  GoogleSignInAccount? _currentGoogleUser;

  MyUser? _myUser;
  late SignedAccount signedAccount;

  /* ____________________________ Constructor ____________________________ */

  AccountAuthenticationService(this.context) {
    _userService = new FirebaseUserHelper();
    firebaseAuth = FirebaseAuth.instance;
    signedAccount = SignedAccount.instance;

    FirebaseAuth.instance.userChanges().listen(
      (User? user) {
        if (user != null) {
          setUpEmailAccount();
        }
      },
      onError: (error) => error.toString(),
    );
  }

  Future<void> loginWithEmailAndPassword(
      {required String email, required String password}) async {
    try {
      if (email == "admin@gmail.com" && password == "admin123") {
        setUpTestAccount();
      } else {
        if (await googleSignIn.isSignedIn()) {
          await googleSignIn.disconnect();
        }
        await firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password);
      }
    } on FirebaseAuthException catch (e) {
      String error = '';

      if (e.code == 'regular_user-not-found') {
        error = 'No regular user found for that email.';
      } else if (e.code == 'wrong-password') {
        error = 'Wrong password provided for that regular_user.';
      } else if (e.code == 'user-not-found') {
        error = 'No user found for that email.';
      }

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.blueGrey,
        content: Text(
          error,
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
      ));
    }
  }

  Future<void> loginWithGoogle() async {
    User? user;
    googleSignIn.disconnect();

    GoogleSignInAccount? googleAccount = await googleSignIn.signIn();

    _currentGoogleUser = googleSignIn.currentUser;

    if (googleAccount != null) {
      GoogleSignInAuthentication googleSignInAuthentication =
          await googleAccount.authentication;

      final OAuthCredential googleCredential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      try {
        final UserCredential googleUserCredential =
            await FirebaseAuth.instance.signInWithCredential(googleCredential);
        user = googleUserCredential.user;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'account-exists-with-different-credential') {
        } else if (e.code == 'invalid-credential') {}
      } catch (e) {}
    }
  }

  static Future<Null> signOutWithGoogle() async {
    await googleSignIn.signOut();
    // Sign out with firebase
    await firebaseAuth.signOut();
    // Sign out with google
  }

  void setUpEmailAccount() async {
    _currentEmailUser = firebaseAuth.currentUser;
    DocumentSnapshot? documentSnapshot =
        await _userService.getUserById(_currentEmailUser!.uid);

    if (documentSnapshot.data() == null) {
      final username = await Navigator.push(context,
          MaterialPageRoute(builder: (context) => AccountRegisterScreen()));

      FirebaseFirestore.instance
          .collection('users')
          .doc(_currentEmailUser!.uid)
          .set(
        {
          'id': _currentEmailUser!.uid,
          'username': username,
          'email': _currentEmailUser!.email,
          'photoUrl': _currentEmailUser!.photoURL == null
              ? 'https://firebasestorage.googleapis.com/v0/b/flutter-social-media-7fb3e.appspot.com/o/images%2Fimage_2021-09-03_154109.png?alt=media&token=3ec93a85-a926-483b-b722-588b371d58a0'
              : _currentEmailUser!.photoURL,
          'displayName': _currentEmailUser!.displayName == null
              ? username
              : _currentEmailUser!.displayName,
          'bio': '',
          'timeJoined': DateTime.now(),
          'isOnline': true,
          'userRole': EnumToString.convertToString(UserRoles.user),
          'balance': 0.0,
        },
      );

      FirebaseFirestore.instance
          .collection('users')
          .doc(_currentEmailUser!.uid)
          .collection('Conversations');

      FirebaseFirestore.instance
          .collection('stores')
          .doc(_currentEmailUser!.uid)
          .set({'addressBook': [], 'myOrders': []});

      _myUser = MyUser(
        id: _currentEmailUser!.uid,
        username: username,
        email: _currentEmailUser!.email,
        photoUrl: _currentEmailUser!.photoURL == null
            ? 'https://firebasestorage.googleapis.com/v0/b/flutter-social-media-7fb3e.appspot.com/o/images%2Fimage_2021-09-03_154109.png?alt=media&token=3ec93a85-a926-483b-b722-588b371d58a0'
            : _currentEmailUser!.photoURL,
        displayName: _currentEmailUser!.displayName == null
            ? username
            : _currentEmailUser!.displayName,
        bio: '',
        timeJoined: DateTime.now(),
        isOnline: true,
        balance: 0.0,
      );

      await _myUser!.getPetList();

      signedAccount.id = _myUser!.id;
      signedAccount.username = _myUser!.username;
      signedAccount.email = _myUser!.email;
      signedAccount.photoUrl = _myUser!.photoUrl;
      signedAccount.displayName = _myUser!.displayName;
      signedAccount.bio = _myUser!.bio;
      signedAccount.timeJoined = _myUser!.timeJoined;
      signedAccount.isOnline = _myUser!.isOnline;
      signedAccount.userRoles = _myUser!.userRoles!;
      signedAccount.petIdList = _myUser!.petIdList;
      signedAccount.petList = _myUser!.petList;
      signedAccount.balance = _myUser!.balance;
    } else {
      _myUser = MyUser.fromDocumentSnapshot(documentSnapshot);
      await _myUser!.getPetList();

      signedAccount.id = _myUser!.id;
      signedAccount.username = _myUser!.username;
      signedAccount.email = _myUser!.email;
      signedAccount.photoUrl = _myUser!.photoUrl;
      signedAccount.displayName = _myUser!.displayName;
      signedAccount.bio = _myUser!.bio;
      signedAccount.timeJoined = _myUser!.timeJoined;
      signedAccount.isOnline = _myUser!.isOnline;
      signedAccount.userRoles = _myUser!.userRoles!;
      signedAccount.petIdList = _myUser!.petIdList;
      signedAccount.petList = _myUser!.petList;
      signedAccount.balance = _myUser!.balance;
      print(_myUser);
    }

    HomepageScreen.isLoggedIn.value = true;
  }

  void setUpTestAccount() async {
    FirebaseFirestore.instance.collection('users').doc('testerid').set(
      {
        'id': 'testerid',
        'username': 'tester',
        'email': 'tester@gmail.com',
        'photoUrl':
            'https://firebasestorage.googleapis.com/v0/b/flutter-social-media-7fb3e.appspot.com/o/images%2Fimage_2021-09-03_154109.png?alt=media&token=3ec93a85-a926-483b-b722-588b371d58a0',
        'displayName': 'tester',
        'bio': '',
        'timeJoined': DateTime.now(),
        'isOnline': true,
        'userRole': EnumToString.convertToString(UserRoles.foster),
        'petIdList': ['testerpetid'],
        'balance': 0.0
      },
    );

    FirebaseFirestore.instance
        .collection('users')
        .doc('testerid')
        .collection('Conversations');

    FirebaseFirestore.instance
        .collection('stores')
        .doc('testerid')
        .set({'addressBook': [], 'myOrders': []});

    _myUser = MyUser(
      id: 'testerid',
      username: 'tester',
      email: 'tester@gmail.com',
      photoUrl:
          'https://firebasestorage.googleapis.com/v0/b/flutter-social-media-7fb3e.appspot.com/o/images%2Fimage_2021-09-03_154109.png?alt=media&token=3ec93a85-a926-483b-b722-588b371d58a0',
      displayName: 'tester',
      bio: '',
      timeJoined: DateTime.now(),
      isOnline: true,
      userRoles: UserRoles.foster,
      petIdList: ['testerpetid'],
      balance: 0.0,
    );

    await _myUser!.getPetList();

    signedAccount.id = _myUser!.id;
    signedAccount.username = _myUser!.username;
    signedAccount.email = _myUser!.email;
    signedAccount.photoUrl = _myUser!.photoUrl;
    signedAccount.displayName = _myUser!.displayName;
    signedAccount.bio = _myUser!.bio;
    signedAccount.timeJoined = _myUser!.timeJoined;
    signedAccount.isOnline = _myUser!.isOnline;
    signedAccount.userRoles = _myUser!.userRoles!;
    signedAccount.petIdList = _myUser!.petIdList;
    signedAccount.petList = _myUser!.petList;
    signedAccount.balance = _myUser!.balance;

    print(_myUser);

    HomepageScreen.isLoggedIn.value = true;
  }

  void setupSingletonAccount() {}
}
