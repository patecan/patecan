import 'dart:convert';

import 'package:advance_notification/advance_notification.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:http/http.dart' as http;
import 'package:social_media/constant/credentials.dart';

import '../../constant/app_colors.dart';

class TransactionResponse {
  String message;
  bool success;

  TransactionResponse({required this.message, required this.success});
}

class PaymentService {
  static String stripeEndpoint = 'https://api.stripe.com//v1';
  static String secretKey = Credentials.STRIPE_SECRET_KEY;

  static init() {}

  static Future<Map<String, dynamic>> payWithExistingCard(
      {required BuildContext context,
      required String amount,
      String currency = 'USD',
      required CardDetails cardDetails}) async {
    await Stripe.instance.dangerouslyUpdateCardDetails(cardDetails);

    final billingDetails = BillingDetails(
      email: 'email@stripe.com',
      phone: '+48888000888',
      address: Address(
        city: 'Houston',
        country: 'US',
        line1: '1459  Circle Drive',
        line2: '',
        state: 'Texas',
        postalCode: '77063',
      ),
    ); // mocked data for tests

    // 2. Create payment method
    final paymentMethod =
        await Stripe.instance.createPaymentMethod(PaymentMethodParams.card(
      billingDetails: billingDetails,
    ));

    int intAmount = double.parse(amount).round().toInt();

    Map<String, dynamic> body = {
      'amount': intAmount.toString(),
      'currency': currency,
      'payment_method_types[]': 'card'
    };

    var response = await http.post(
      Uri.parse('https://api.stripe.com/v1/payment_intents'),
      body: body,
      headers: {
        'Authorization': 'Bearer ${Credentials.STRIPE_SECRET_KEY}',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    );

    if (response.statusCode == 200) {
      AdvanceSnackBar(
        message: "TRANSACTION SUCCESS",
        type: sType.SUCCESS,
        mode: Mode.ADVANCE,
        bgColor: AppColors.successColor,
        iconColor: Colors.white,
        duration: Duration(seconds: 6),
      ).show(context);
    }

    return jsonDecode(response.body);
  }

  static Future<Map<String, dynamic>> payWithNewCard(
      {required String amount, String currency = 'USD'}) async {
    Map<String, dynamic> body = {
      'amount': amount,
      'currency': currency,
      'payment_method_types[]': 'card'
    };

    var response = await http.post(
      Uri.parse('https://api.stripe.com/v1/payment_intents'),
      body: body,
      headers: {
        'Authorization': 'Bearer ${Credentials.STRIPE_SECRET_KEY}',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    );

    if (response.statusCode == 200) {
      Map<String, dynamic> paymentIntentData =
          jsonDecode(response.body.toString());

      await Stripe.instance.initPaymentSheet(
        paymentSheetParameters: SetupPaymentSheetParameters(
            paymentIntentClientSecret: paymentIntentData['client_secret'],
            applePay: true,
            googlePay: true,
            merchantCountryCode: 'SG',
            merchantDisplayName: 'ASIF'),
      );

      await Stripe.instance.presentPaymentSheet().then((newValue) {
        print('payment intent' + paymentIntentData['id'].toString());
        print('payment intent' + paymentIntentData['client_secret'].toString());
        print('payment intent' + paymentIntentData['amount'].toString());
        print('payment intent' + paymentIntentData.toString());
      }).onError((error, stackTrace) {
        print('Exception ==> $error $stackTrace');
      });
    }
    return jsonDecode(response.body);
  }

  addMoneyToWalletFromNewCard(
      {required String amount, String currency = 'USD'}) {}

  addMoneyToWalletFromExistingCard(
      {required String amount,
      String currency = 'USD',
      required CardDetails cardDetails}) {}
}
