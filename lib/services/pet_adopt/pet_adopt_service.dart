import 'dart:io';

import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:path/path.dart' as p;
import 'package:social_media/models/pet_adopt/pet_adopt.dart';

class PetAdoptService {
  /* ======================================================= READ ALL ======================================================= */
  Future<Response> getAllPetAdopts() async {
    Dio dio = new Dio();
    Response response = await dio.get('http://192.168.2.3:8000/pet_adopts');

    return response;
  }

  Future<Response> getAllPetAdoptsOfUser(String ownerId) async {
    Dio dio = new Dio();
    Response response =
        await dio.get('http://192.168.2.3:8000/pet_adopts?owner=$ownerId');

    return response;
  }

  /* ======================================================= CREATE ======================================================= */
  Future<String> uploadImage(File imageFile) async {
    String fileName = p.basename(imageFile.path);

    var formData = FormData.fromMap({
      'image': await MultipartFile.fromFile(imageFile.path, filename: fileName),
    });

    var response = await new Dio().post(
        'http://192.168.2.3:8000/uploadS3?fileName=${fileName}',
        data: formData);

    print("uploadS3 image to S3 bucket response - ${response}");

    String imageURL = response.toString();
    return imageURL;
  }

  Future<Response> createPetAdopt(PetAdopt petAdopt) async {
    Dio dio = new Dio();
    dio.options.headers['content-Type'] = 'application/json';

    Map<String, dynamic> body = {
      '_id': petAdopt.id,
      'owner': petAdopt.owner,
      'name': petAdopt.name,
      'age': petAdopt.age,
      'species': EnumToString.convertToString(petAdopt.species),
      'desc': petAdopt.desc,
      'condition': petAdopt.condition,
      'images': petAdopt.imageUrl!.map((e) => {'imageURL': e}).toList(),
      'postTime': petAdopt.postTime.toString(),
      'location': {
        'nameAddress': petAdopt.address?.address,
        'latitude': petAdopt.address?.latitude,
        'longitude': petAdopt.address?.longitude
      },
      'candidate': [],
    };

    Response response =
        await dio.post('http://192.168.2.3:8000/pet_adopts', data: body);
    return response;
  }
}
