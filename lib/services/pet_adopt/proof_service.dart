import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:social_media/models/messages/conversation.dart';
import 'package:social_media/models/messages/message.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:uuid/uuid.dart';

class ProofService {
  Future<Message> getMessageIfNewConversation(
      {required MyUser you, required String peerId}) async {
    String? conversationId = await createOrGetConversationId(you.id!, peerId);

    late Message message;
    if (conversationId == null) {
      message = Message(
        displayName: you.displayName,
        photoUrl: you.photoUrl,
      );
    } else {
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('proof_you_are_deserved')
          .where('conversationID', isEqualTo: conversationId)
          .get();

      Conversation existedConversation =
          Conversation.fromDocument(querySnapshot.docs.first);

      message = existedConversation.messages.last;
    }
    return message;
  }

  Future sendMessage(String conversationID, Message message) async {
    DocumentReference conversationRef = FirebaseFirestore.instance
        .collection('proof_you_are_deserved')
        .doc(conversationID);

    Map<String, dynamic> newMessage = {};

    newMessage['_id'] = new Uuid().v4();
    newMessage['displayName'] = SignedAccount.instance.displayName;
    newMessage['photoUrl'] = message.photoUrl == null
        ? message.lastMessageContent
        : message.photoUrl;
    newMessage['message_content'] = message.lastMessageContent;
    newMessage['senderID'] = message.senderID;
    newMessage['sentTime'] = message.sentTime;

    switch (message.messageType) {
      case MessageType.Text:
        newMessage['message_type'] = 'text';
        break;
      case MessageType.Image:
        newMessage['message_type'] = 'image';
        break;
      case MessageType.Video:
        newMessage['message_type'] = 'video';
        break;
      case MessageType.GIF:
        newMessage['message_type'] = 'gif';
        break;
      case MessageType.Voice:
        newMessage['message_type'] = 'voice';
        break;
      default:
        newMessage['message_type'] = 'text';
        break;
    }
    await conversationRef.update({
      'messages': FieldValue.arrayUnion([newMessage])
    });
  }

  Future sendNewMessage(String receiverID, String? imageURL,
      String? messageContent, Function setNewConversation) async {
    String conversationID = Uuid().v4();
    var conversationRef = FirebaseFirestore.instance
        .collection('proof_you_are_deserved')
        .doc(conversationID);

    await FirebaseFirestore.instance
        .collection('proof_you_are_deserved')
        .doc(conversationID)
        .set(
      {
        'conversationID': conversationRef.id,
        'members': [
          SignedAccount.instance.id,
          receiverID,
        ],
        'ownerID': SignedAccount.instance.id,
        'requestPoint': 0.0,
        'messages': [
          {
            '_id': new Uuid().v4(),
            'displayName': SignedAccount.instance.displayName,
            'senderID': SignedAccount.instance.id,
            'message_content': imageURL == null ? messageContent : imageURL,
            'photoUrl': imageURL == null ? messageContent : imageURL,
            'sentTime': DateTime.now(),
            'message_type': imageURL == null ? 'text' : 'image',
          }
        ],
      },
    );

    //onCreateConversation(conversationID);
    setNewConversation(conversationID);
  }

  void onCreateConversation(String conversationID) async {
    DocumentSnapshot snapshot = await FirebaseFirestore.instance
        .collection('proof_you_are_deserved')
        .doc(conversationID)
        .get();

    List<String> memberIDsArray = List<String>.from(snapshot['members']);

    var messageList = snapshot['messages'];
    var lastMessage = messageList[messageList.length - 1];

    DocumentSnapshot senderDocument = await FirebaseFirestore.instance
        .collection("users")
        .doc(lastMessage['senderID'])
        .get();

    for (int i = 0; i < memberIDsArray.length; i++) {
      String currUserID = memberIDsArray[i];
      List<String> otherUserIDsArray =
          List<String>.from(memberIDsArray.where((otherID) {
        if (otherID != currUserID) {
          return true;
        } else {
          return false;
        }
      }));

      for (String otherUserID in otherUserIDsArray) {
        if (messageList.length > 2) {
          await FirebaseFirestore.instance
              .collection("users")
              .doc(currUserID.toString())
              .collection("GroupConversations")
              .doc(conversationID)
              .set({
            "conversationID": conversationID,
            "photoUrl": senderDocument['photoUrl'],
            "displayName": senderDocument['displayName'],
            "senderID": lastMessage.senderID,
            "lastMessage_content": lastMessage.message_content,
            "sentTime": lastMessage.sentTime,
            "unseenCount": FieldValue.increment(1),
            "message_type": lastMessage.message_type,
          });
        } else {
          await FirebaseFirestore.instance
              .collection("users")
              .doc(currUserID.toString())
              .collection("proof_you_are_deserved")
              .doc(otherUserID.toString())
              .set({
            "conversationID": conversationID,
            "photoUrl": senderDocument['photoUrl'],
            "displayName": senderDocument['displayName'],
            "unseenCount": 0,
            "senderID": lastMessage['senderID'],
            "lastMessage_content": lastMessage['message_content'],
            "sentTime": lastMessage['sentTime'],
            "message_type": lastMessage['message_type'],
          });
        }
      }
    }
  }

  Future<String> sendImage(String userID, File imageFile) async {
    Reference storageReferences = FirebaseStorage.instance.ref();
    String imageFileID = new Uuid().v4();
    late String imageURL;
    try {
      //imageFile = await compressImage(imageFile, imageFileID);
      UploadTask uploadTask = storageReferences
          .child('images')
          .child(userID)
          .child('message')
          .child("message_$imageFileID")
          .putFile(imageFile);
      TaskSnapshot taskSnapshot = await uploadTask;
      imageURL = await taskSnapshot.ref.getDownloadURL();
    } catch (e) {
      print(e.toString());
    }
    return imageURL;
  }

  Future<String?> createOrGetConversationId(
      String senderID, String receiverID) async {
    CollectionReference userConversation = FirebaseFirestore.instance
        .collection('users')
        .doc(senderID)
        .collection('proof_you_are_deserved');

    QuerySnapshot querySnapshot = await FirebaseFirestore.instance
        .collection('proof_you_are_deserved')
        .where('members', arrayContains: senderID)
        .get();

    if (querySnapshot.docs.length > 0) {
      DocumentSnapshot documentSnapshot = querySnapshot.docs
          .firstWhere((element) => element['members'].contains(receiverID));
      if (documentSnapshot.data() != null) {
        return documentSnapshot['conversationID'];
      }
    }
    return null;
  }

  void sendRequestMessage(
      {required String conversationId,
      required String requestContent,
      required String requestAttachment,
      required double requestPoint}) async {
    DocumentReference conversationRef = FirebaseFirestore.instance
        .collection('proof_you_are_deserved')
        .doc(conversationId);

    Map<String, dynamic> requestMessage = {};

    requestMessage['_id'] = new Uuid().v4();
    requestMessage['displayName'] = SignedAccount.instance.displayName;
    requestMessage['senderID'] = SignedAccount.instance.id;
    requestMessage['sentTime'] = DateTime.now();
    requestMessage['requestContent'] = requestContent;
    requestMessage['requestAttachment'] = requestAttachment;
    requestMessage['requestPoint'] = requestPoint;
    requestMessage['message_type'] = 'request';
    requestMessage['proofResponse'] = false;

    await conversationRef.update({
      'messages': FieldValue.arrayUnion([requestMessage])
    });
  }

  Future<String?> uploadVideo(String requestId, File videoFile) async {
    String? downloadUrl;

    try {
      final DateTime now = DateTime.now();

      Reference ref = FirebaseStorage.instance
          .ref()
          .child("video")
          .child('pet_adopt')
          .child('proof')
          .child('${requestId}_$now');

      SettableMetadata metadata = SettableMetadata(contentType: 'video/mp4');

      await ref.putFile(videoFile, metadata);

      downloadUrl = await ref.getDownloadURL();

      print(downloadUrl);
    } catch (error) {
      print(error);
    }

    return downloadUrl;
  }

  Future<String?> uploadSignature(String requestId, File videoFile) async {
    String? downloadUrl;

    try {
      final DateTime now = DateTime.now();

      Reference ref = FirebaseStorage.instance
          .ref()
          .child("video")
          .child('pet_adopt')
          .child('proof')
          .child('${requestId}_$now');

      SettableMetadata metadata = SettableMetadata(contentType: 'image/png');

      await ref.putFile(videoFile, metadata);

      downloadUrl = await ref.getDownloadURL();

      print(downloadUrl);
    } catch (error) {
      print(error);
    }

    return downloadUrl;
  }
}
