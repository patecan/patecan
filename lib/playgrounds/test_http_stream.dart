import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  StreamController<String> _controller = new StreamController.broadcast();

  List<String> _messages = <String>[];
  late StreamSubscription<String> _subscription;

  @override
  void initState() {
    _subscription = _controller.stream.listen((message) async => setState(() {
      _messages.add(message);
    }));
    super.initState();
  }

  Stream<String> getRandomNumberFact() async* {
    yield* Stream.periodic(Duration(seconds: 1), (_) {
      return http.get(Uri.parse("http://numbersapi.com/random/"));
    }).asyncMap((response) async {
      _messages.add((await response).body);
      return  (await response).body; });
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Sample App'),
      ),
      body: StreamBuilder<Object>(
        stream: getRandomNumberFact(),
        builder: (context, snapshot) {

          if(snapshot.hasData){


            return ListView(
              children: _messages.map((String message) {
                return new Card(
                  child: new Container(
                    height: 100.0,
                    child: new Center(
                      child: new Text(message),
                    ),
                  ),
                );
              }).toList(),
            );
          } else {
            return CircularProgressIndicator();
          }
        }
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          new FloatingActionButton(
            child: new Icon(Icons.account_circle_outlined),
            onPressed: () {
              // simulate a message arriving
              _controller.add('Hello World');
            },
          ),
          SizedBox(
            height: 20.0,
          ),
          new FloatingActionButton(
            child: new Icon(Icons.account_circle_rounded),
            onPressed: () {
              // simulate a message arriving
              _controller.add('Hi Flutter');
            },
          ),
        ],
      ),
    );
  }
}

class SampleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new HomeScreen(),
    );
  }
}

void main() {
  runApp(new SampleApp());
}