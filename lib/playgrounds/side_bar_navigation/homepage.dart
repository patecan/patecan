import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:social_media/playgrounds/side_bar_navigation/payment_page.dart';

import 'menu_page.dart';
import 'notification_page.dart';


void main() async {
  runApp(HomePage());
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Size size;
  MenuItem currentItem = MenuItems.payment;
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        body: ZoomDrawer(
            style: DrawerStyle.Style1,
            borderRadius: 40,
            angle: -10,
            slideWidth: 250,
            showShadow: true,
            backgroundColor: Colors.orangeAccent.shade100,
            menuScreen: Builder(
              builder: (context) {
                return MenuPage(
                  currentItem: currentItem,
                  onSelectedItem: (item){
                    setState(() => currentItem = item);
                    ZoomDrawer.of(context)!.close();
                  }

                );
              }
            ), mainScreen: getScreen()),
      ),
    );
  }


  Widget getScreen(){
      switch(currentItem){
        case MenuItems.payment:
          return PaymentPage();
        case MenuItems.notification:
          return NotificationPage();
        default:
          return PaymentPage();
      }
  }
}
