import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuItem {
  final Text title;
  final Icon icon;

  const MenuItem(this.title, this.icon);
}

class MenuItems {
  static const payment = MenuItem(Text('A'), Icon(Icons.payment));
  static const notification = MenuItem(Text('B'), Icon(Icons.notifications));

  static const all = [payment, notification];
}

class MenuPage extends StatelessWidget {
  final MenuItem currentItem;
  final ValueChanged<MenuItem> onSelectedItem;

  const MenuPage({Key? key, required this.currentItem, required this.onSelectedItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData.dark(),
      child: Scaffold(
        backgroundColor: Colors.indigo,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              ...MenuItems.all
                  .map((item) => ListTileTheme(
                        selectedColor: Colors.white,
                        child: ListTile(
                          selectedTileColor: Colors.black26,
                          minLeadingWidth: 20,
                          leading: item.icon,
                          title: item.title,
                          selected: currentItem == item,
                          onTap: () => onSelectedItem(item)),
                      ))
                  .toList(),
              Spacer(
                flex: 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
