import 'package:virtual_keyboard_multi_language/virtual_keyboard_multi_language.dart';

class CustomLayoutKeys extends VirtualKeyboardLayoutKeys {
  @override
  int getLanguagesCount() => 2;

  List<List> getLanguage(int index) {
    return defaultEnglishLayout;
  }
}

