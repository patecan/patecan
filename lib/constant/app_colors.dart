import 'package:flutter/material.dart';

class AppColors {
  static const Color niceBlack = Color(0xFF031637);

  static const Color menuBackground = Color(0xFFF0F7FF);
  static const Color appbarBackground = Color(0xFFFFFFFF);
  static const Color whiteScreenBackground = Color(0xFFFDFDFD);
  static const Color pinkBackground = Color(0xFFFDF3F4);
  static const Color pink2Background = Color(0xFFFFADAD);
  static const Color veryPink = Color(0xFFFABDCF);
  static const Color bottomNavbarButtonColor = Color(0xFFAAE2FC);
  static const Color tabViewButtonColor = Color(0xFFAAE2FC);
  static const Color bottomNavbarBackground = Color(0xFFECEDFF);
  static const Color blueScreenBackground = Color(0xFFE5F7FF);
  static const Color blueMenuButtonBackground = Color(0xFF4086FD);
  static const Color yellowMenuButtonBackground = Color(0xFFFFBB31);

  static const Color redHeartButtonBackground = Color(0xFFFFECEC);
  static const Color redHeartButton = Color(0xFFFE4545);

  static const Color blueHeartButtonBackground = Color(0xFFEDF4FF);
  static const Color blueHeartButton = Color(0xFF4C8DFB);

  static const Color shareButtonBackground = Color(0xFFFFF8EA);
  static const Color shareHeartButton = Color(0xFFFFBB31);

  static const Color pinkGradient = Color(0xFFEFAA97);
  static const Color lightPinkBackground = Color(0xffFFF3F5);
  static const Color skinBackground = Color.fromRGBO(255, 247, 224, 0.25);

  static const Color blueGradient = Color(0xFF80BBEE);
  static const Color blueWithPink = Color(0xffA1DEFA);
  static const Color blue2 = Color(0xFF56BDE6);
  static const Color veryFreshBlue = Color(0xFF53AFEE);
  static const Color lightFreshBlue = Color(0xFFADE3FC);

  static const Color veryLightBlue = Color(0xFFC9EDFF);

  static const Color redButtonBackground = Color(0xFFFFC5B6);
  static const Color darkRedText = Color(0xFF6E3522);
  static const Color darkBlueText = Color(0xff3A4276);

  static const Color colorBackground = Color(0xFFFBFAFF);
  static const Color lightGreyBackground = Color(0xFFFAFAFA);
  static const Color successColor = Color(0xFFABC270);

  static const Color colorPink = Color(0xFFE66C75);
  static const Color colorOrange = Color(0xFFE8913A);
  static const Color colorBlue = Color(0xFF2254A3);
  static const Color colorAccentPurple = Color(0xFF4f5cd1);

  static const Color colorText = Color(0xFF383635);
  static const Color colorTextLight = Color(0xFF918D8D);
  static const Color colorTextSemiLight = Color(0xFF737373);
  static const Color colorTextDark = Color(0xFF292828);

  static const Color colorGreen = Color(0xFF40cf89);
  static const Color colorLightGray = Color(0xFFe2e2e2);
  static const Color colorLightGrayFair = Color(0xFFe1e5e8);
  static const Color colorDimText = Color(0xFFadadad);
}
