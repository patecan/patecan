import 'package:enum_to_string/enum_to_string.dart';

enum UserRoles {
  foster,
  user,
  admin,
  vet,
  producer,
}
