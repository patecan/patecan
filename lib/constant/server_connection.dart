import 'package:dart_ipify/dart_ipify.dart';

class SpringServerConnection {
  static const Map<String, String> HEADER = {
    'Content-Type': 'application/json',
  };

  static const String IP_ADDRESS = '192.168.0.8';
  static const String SERVER_PORT = '8080';
  static const String SERVER_SOCKET = "http://$IP_ADDRESS:$SERVER_PORT";
}

class Endpoints {
  static Uri allPets =
      Uri.parse("${SpringServerConnection.SERVER_SOCKET}/pets/all");
}
