import 'package:flutter/material.dart';
import 'package:social_media/models/places/place.dart';

class PlaceProvider extends ChangeNotifier {
  Place? storePlace;

  //Place? currentPlace;
  Place? pickupPlace;
  Place? destinationPlace;

  // void setNewCurrentPlace(Place newPlace) {
  //   currentPlace = newPlace;
  //   notifyListeners();
  // }

  void setNewStorePlace(Place newPlace) {
    storePlace = newPlace;
    notifyListeners();
  }

  void setNewPickupPlace(Place newPlace) {
    pickupPlace = newPlace;
    notifyListeners();
  }

  void setNewDestinationPlace(Place newPlace) {
    destinationPlace = newPlace;
    notifyListeners();
  }
}
