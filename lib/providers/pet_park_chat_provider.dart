import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:social_media/models/users/singned_account.dart';

class ChatProvider extends ChangeNotifier {
  ChatProvider();

  DatabaseReference petParkRealtimeDB =
      FirebaseDatabase.instance.reference().child('pet_park');

  /// ChatBox TextEditingController defined in `HomePage`
  TextEditingController _textEditingController = TextEditingController();

  TextEditingController get textEditingController => _textEditingController;

  /// Called after sending the message in `Penguin` class
  Future<void> onMessageSent() async {
    final String _message = _textEditingController.value.text;
    if (_message.isNotEmpty) {
      //ChatBloc.addEventWithoutContext(ChatEvent.sendMessage(_message));
      await petParkRealtimeDB
          .child(SignedAccount.instance.id!)
          .set({'message': _message});
    }
    textEditingController.clear();
    notifyListeners();
  }

  void _listener() {}
}
