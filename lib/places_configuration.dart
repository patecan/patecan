import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'models/places/nearby_driver.dart';

bool? isLoading;

Position? currentPosition;

StreamSubscription<Position>? userPositionStream;

StreamSubscription<Position>? fosterPositionStream;

Future<Position> getCurrentPosition() async {
  currentPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.bestForNavigation);

  return currentPosition!;
}

/* Create custom marker */
BitmapDescriptor? userMarkerIcon;

Future<void> createUserMaker(BuildContext context) async {
  if (userMarkerIcon == null) {
    ImageConfiguration imageConfiguration =
        createLocalImageConfiguration(context, size: Size(0.05, 0.05));
    userMarkerIcon = await BitmapDescriptor.fromAssetImage(
        imageConfiguration, 'assets/images/places/user_marker.png');
  }
}

/* Create custom foster marker */
BitmapDescriptor? fosterMarkerIcon;

Future<void> createFosterMaker(BuildContext context) async {
  if (fosterMarkerIcon == null) {
    ImageConfiguration imageConfiguration =
        createLocalImageConfiguration(context, size: Size(0.05, 0.05));
    fosterMarkerIcon = await BitmapDescriptor.fromAssetImage(
        imageConfiguration, 'assets/images/places/car_marker.png');
  }
}

/* Create custom foster accepted marker */
BitmapDescriptor? fosterAcceptedMarkerIcon;

Future<void> createFosterAcceptedMaker(BuildContext context) async {
  if (fosterAcceptedMarkerIcon == null) {
    ImageConfiguration imageConfiguration =
        createLocalImageConfiguration(context, size: Size(0.05, 0.05));
    fosterAcceptedMarkerIcon = await BitmapDescriptor.fromAssetImage(
        imageConfiguration, 'assets/images/places/superhero_marker.png');
  }
}

Timer? timer;
int secondCount = 0;

void startTimer() {
  Duration oneSecond = Duration(seconds: 1);
  timer = Timer.periodic(oneSecond, (timer) {
    secondCount++;
  });
}

Map<String, NearbyDriver> nearbyDriverMap = new Map<String, NearbyDriver>();

// void removeFromList(String key) {
//   int index = nearbyDriverSet.indexWhere((element) => element.key == key);
//
//   if (nearbyDriverSet.length > 0) {
//     nearbyDriverSet.removeAt(index);
//   }
// }

// void updateNearbyLocation(NearbyDriver driver) {
//   int index =
//       nearbyDriverSet.indexWhere((element) => element.key == driver.key);
//
//   nearbyDriverSet[index].longitude = driver.longitude;
//   nearbyDriverSet[index].latitude = driver.latitude;
// }
