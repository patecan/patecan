import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:social_media/models/users/my_user.dart';

enum MeetingTypes {
  CASUAL,
  VET,
}

class MeetingRoom {
  String id;
  String title;
  String status;
  MeetingTypes meetingType;
  DateTime meetingDateTime;
  DateTime createdOn;
  String createdBy;
  List<MyUser> invited;

  MeetingRoom(
      {required this.id,
      required this.title,
      required this.status,
      required this.meetingType,
      required this.meetingDateTime,
      required this.createdOn,
      required this.createdBy,
      required this.invited});

  // factory MeetingRoom.fromJson(DocumentSnapshot snapshot) {
  //   return MeetingRoom(
  //     id: snapshot['id'],
  //     title: snapshot['title'],
  //     status: snapshot['status'],
  //     meetingType: EnumToString.fromString(
  //         MeetingTypes.values, snapshot['meetingType'])!,
  //     meetingDateTime: snapshot['meetingDateTime'].toDate(),
  //
  //   );
  // }
}
