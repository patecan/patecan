import 'package:cloud_firestore/cloud_firestore.dart';

class MyVetMeetingNotification {
  String id;
  String createdBy;
  String invitedBy;
  bool isAccepted;
  Map<String, dynamic> meetingData;
  DateTime time;
  String userInvited;

  MyVetMeetingNotification(
      {required this.id,
      required this.createdBy,
      required this.invitedBy,
      required this.isAccepted,
      required this.meetingData,
      required this.time,
      required this.userInvited});

  factory MyVetMeetingNotification.fromQueryDocument(
      DocumentSnapshot document) {
    return MyVetMeetingNotification(
      id: document['id'],
      createdBy: document['createdBy'],
      invitedBy: document['invitedBy'],
      isAccepted: document['isAccepted'],
      meetingData: Map<String, dynamic>.from(document['meetingData']),
      time: document['time'].toDate(),
      userInvited: document['userInvited'],
    );
  }

  @override
  String toString() {
    return 'MyVetMeetingNotification{id: $id, createdBy: $createdBy, invitedBy: $invitedBy, isAccepted: $isAccepted, meetingData: $meetingData, time: $time, userInvited: $userInvited}';
  }
}
