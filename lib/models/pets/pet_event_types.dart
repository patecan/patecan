import 'package:cloud_firestore/cloud_firestore.dart';

enum PetEventTypes {
  was_born,
  neutered,
  give_birth,
  need_help,
  injured,
  adopted,
  fostered,
  injected,
  owner_changed,
  get_more_owners,
  being_lost,
  pass_away
}

class PetEvent {
  String? desc;
  late PetEventTypes eventType;
  late String id;
  String? name;
  DateTime time = DateTime.now();

  PetEvent(
      {required this.id,
      required this.eventType,
      this.name,
      this.desc,
      required this.time}) {}

  factory PetEvent.fromDocumentSnapshot(DocumentSnapshot snapshot) {
    return PetEvent(
      id: snapshot['id'],
      eventType: getEventTypeEnum(snapshot['eventType'].toString())!,
      name: snapshot['name'],
      desc: snapshot['desc'],
      time: snapshot['time'].toDate(),
    );
  }

  static PetEventTypes? getEventTypeEnum(String event) {
    switch (event) {
      case "was born":
        return PetEventTypes.was_born;
      case "neutered":
        return PetEventTypes.neutered;
      case "give birth":
        return PetEventTypes.give_birth;
      case "need help":
        return PetEventTypes.need_help;
      case "injured":
        return PetEventTypes.injured;
      case "adopted":
        return PetEventTypes.adopted;
      case "fostered":
        return PetEventTypes.fostered;
      case "injected":
        return PetEventTypes.injected;
      case "owner changed":
        return PetEventTypes.owner_changed;
      case "get more owners":
        return PetEventTypes.get_more_owners;
      case "being lost":
        return PetEventTypes.being_lost;
      case "pass away":
        return PetEventTypes.pass_away;
    }
  }

  static String? getEventTypeString(PetEventTypes event) {
    switch (event) {
      case PetEventTypes.was_born:
        return "was born";
      case PetEventTypes.neutered:
        return "neutered";
      case PetEventTypes.give_birth:
        return "give birth";
      case PetEventTypes.need_help:
        return "need help";
      case PetEventTypes.injured:
        return "injured";
      case PetEventTypes.adopted:
        return "adopted";
      case PetEventTypes.fostered:
        return "fostered";
      case PetEventTypes.injected:
        return "injected";
      case PetEventTypes.owner_changed:
        return "owner changed";
      case PetEventTypes.get_more_owners:
        return "get more owners";
      case PetEventTypes.being_lost:
        return "being lost";
      case PetEventTypes.pass_away:
        return "pass away";
    }
  }

  static getEventTypeIcon() {}
}
