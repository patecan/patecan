import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class Pet {
  String? _id;
  String? _ownerId;
  String? _petUniqueName;
  String? _photoUrl;
  String? _displayName;
  String? _bio;
  DateTime? _timeJoined;

  Pet({
    required String? id,
    required String? ownerId,
    required String? petUniqueName,
    required String? photoUrl,
    required String? displayName,
    required String? bio,
    required DateTime? timeJoined,
  }) {
    this._id = id;
    this._ownerId = ownerId;
    this._petUniqueName = petUniqueName;
    this._photoUrl = photoUrl;
    this._displayName = displayName;
    this._bio = bio;
    this._timeJoined = timeJoined;
  }

  factory Pet.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    return Pet(
      id: documentSnapshot['id'],
      ownerId: documentSnapshot['ownerID'],
      petUniqueName: documentSnapshot['petUniqueName'],
      photoUrl: documentSnapshot['photoURL'],
      displayName: documentSnapshot['displayName'],
      bio: documentSnapshot['bio'],
      timeJoined: documentSnapshot['timeJoined'].toDate(),
    );
  }

  factory Pet.fromJson(Map<String, dynamic> json) {
    final dateTimeString = json['timeJoined'];
    final formatter = DateFormat('''yyyy-mm-dd'T'hh:mm:ss''');
    DateTime dateTimeFromString = formatter.parse(dateTimeString);

    return Pet(
      id: json['petId'],
      ownerId: json['ownerID'],
      petUniqueName: json['petUniqueName'],
      photoUrl: json['photoURL'],
      displayName: json['displayName'],
      bio: json['bio'],
      timeJoined: dateTimeFromString,
    );
  }

  String? get ownerId => _ownerId;

  set ownerId(String? value) {
    _ownerId = value;
  }

  DateTime? get timeJoined => _timeJoined;

  set timeJoined(DateTime? value) {
    _timeJoined = value;
  }

  String? get bio => _bio;

  set bio(String? value) {
    _bio = value;
  }

  String? get displayName => _displayName;

  set displayName(String? value) {
    _displayName = value;
  }

  String? get photoUrl => _photoUrl;

  set photoUrl(String? value) {
    _photoUrl = value;
  }

  String? get petUniqueName => _petUniqueName;

  set petUniqueName(String? value) {
    _petUniqueName = value;
  }

  String? get id => _id;

  set id(String? value) {
    _id = value;
  }

  @override
  String toString() {
    return 'Pet( id: ${_id}, petUniqueName: ${_petUniqueName}  \n '
        'photoUrl: ${photoUrl} displayName: ${displayName} bio: ${bio} \n'
        'timeJoined: ${timeJoined} )';
  }
}
