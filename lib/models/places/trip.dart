import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:social_media/models/places/place.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/services/places/map_service.dart';

import 'direction.dart';

class TripDetail {
  Place pickupPlace;
  Place destPlace;

  MyUser requester;
  MyUser? foster;

  String rideID;

  MapService mapService = new MapService();
  Direction? _direction;

  TripDetail(
      {required this.rideID,
      required this.requester,
      this.foster,
      required this.pickupPlace,
      required this.destPlace});

  Future<Direction?> getDirection() async {
    _direction = await mapService.getDirectionDetail(
        LatLng(pickupPlace.latitude!, pickupPlace.longitude!),
        LatLng(destPlace.latitude!, destPlace.longitude!));
    return _direction;
  }

  // void showDialog(BuildContext context) async {
  //   Direction? direction = await this.getDirection();
  //
  //   AwesomeDialog(
  //     context: context,
  //     dialogType: DialogType.WARNING,
  //     animType: AnimType.SCALE,
  //     title: 'New request',
  //     desc:
  //         "${this.pickupPlace.placeFormattedAddress}\n Distance: ${direction != null ? direction.distanceText! : 'have fun'}",
  //     btnCancelOnPress: () {},
  //     btnOkOnPress: () {
  //       Navigator.of(context).push(
  //           MaterialPageRoute(builder: (context) => TripDetailScreen(this)));
  //     },
  //   )..show();
  // }

  @override
  String toString() {
    return "Trip Detail {ride_id: $rideID \nuser: ${requester.displayName}\npickup: ${pickupPlace.placeFormattedAddress} dest: ${destPlace.placeFormattedAddress}";
  }

  void checkAvailability() {}
}
