class NearbyDriver {
  String? key;
  double? latitude;
  double? longitude;

  static List<NearbyDriver> nearbyDriverList = [];

  NearbyDriver(
      {required this.key, required this.latitude, required this.longitude});

  String toString() {
    return "Near Driver { key:$key, lat:$latitude, long:$longitude }";
  }
}
