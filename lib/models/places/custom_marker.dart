import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CustomMarker {

  static void createMarker(BuildContext context,BitmapDescriptor? nearbyIcon) {

    if (nearbyIcon == null) {
      ImageConfiguration imageConfiguration = createLocalImageConfiguration(context, size: Size(2, 2));
      BitmapDescriptor.fromAssetImage(imageConfiguration, (Platform.isIOS)
          ? 'assets/images/places/car.png'
          : 'assets/images/places/car.png').then((newIcon){
              nearbyIcon = newIcon;
      });
    }
  }



}