class Direction{

  String? distanceText;
  String? durationText;
  int? distanceValue;
  int? durationValue;
  String? encodedPoints;

  Direction({this.distanceText, this.durationText, this.distanceValue,
      this.durationValue, this.encodedPoints});


  int estimateCost(){
    // base cost = 20,000
    // cost per km  = 7,000
    // cost per minutes = 5,000

    double baseCost = 20;
    double distanceCost = (this.distanceValue!/1000) * 7;
    double durationCost = (this.durationValue!/60) * 5;

    double totalCost = baseCost + distanceCost + durationCost;
    return totalCost.floor();
  }

  @override
  String toString() {
    return " $distanceText $durationText $durationValue $encodedPoints";
  }
}