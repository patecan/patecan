import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';

import '../store/my_address.dart';
import '../users/singned_account.dart';

enum Species { UNDEFINED, CAT, DOG, HAMSTER }

class PetAdopt with ChangeNotifier {
  late String _id;
  late Map<String, dynamic> _owner;
  late String _name;
  Species? _species;
  String? _desc;
  double? _age;
  String? _condition;
  List<String>? _imageUrl;
  DateTime? _postTime;
  MyAddress? _address;
  List<dynamic>? candidates;

  PetAdopt({
    required id,
    required owner,
    name = 'Unknown',
    required species,
    required desc,
    required age,
    required condition,
    required imageUrl,
    required postTime,
    required address,
    this.candidates = const [],
  }) {
    this._id = id;
    this._owner = owner;
    this._name = name;
    this._species = species;
    this._desc = desc;
    this._age = age;
    this._condition = condition;
    this._imageUrl = imageUrl;
    this._postTime = postTime;
    this._address = address;
  }

  factory PetAdopt.fromDocument(Map<String, dynamic> document) {
    return PetAdopt(
      id: document['_id'],
      species: EnumToString.fromString(Species.values, document['species']),
      condition: document['condition'],
      owner: document['owner'],
      name: document['name'],
      desc: document['desc'],
      imageUrl: List<String>.from(
          document['images'].map((e) => e['imageURL']).toList()),
      candidates: document['candidates'].map((e) {
        return {
          '_id': e['_id'],
          'appliedDate': e['appliedDate'].toDate(),
          'objective': e['objective']
        };
      }).toList(),
      address: MyAddress(
          latitude: document['location.latitude'],
          longitude: document['location.longitude'],
          name: document['location.nameAddress']),
      age: double.parse(document['age'].toString()),
      postTime: DateTime.parse(document['postTime']),
    );
  }

  // factory PetAdopt.fromJson(Map<String, dynamic> json) {
  //   List<String> imageList = <String>[];
  //
  //   if (!(json['imageURL'] is List<dynamic>)) {
  //     imageList = <String>[json['imageURL']];
  //   } else {
  //     imageList = List<String>.from(json['imageURL']);
  //   }
  //   return PetAdopt();
  // }

  factory PetAdopt.empty({required String id}) {
    return PetAdopt(
      id: id,
      owner: {'_id': SignedAccount.instance.id},
      name: 'unknown',
      desc: '',
      condition: '',
      age: null,
      address: null,
      imageUrl: <String>[],
      postTime: DateTime.now(),
      species: Species.UNDEFINED,
      candidates: [],
    );
  }

  @override
  String toString() {
    return 'PetAdopt{_id: $_id, _owner: $_owner, _name: $_name, _species: $_species, _desc: $_desc, _age: $_age, _condition: $_condition, _imageUrl: $_imageUrl, _postTime: $_postTime, _address: $_address}';
  }

  Map<String, dynamic> get owner => _owner;

  set owner(Map<String, dynamic> value) {
    _owner = value;
  }

  DateTime? get postTime => _postTime;

  set postTime(DateTime? value) {
    _postTime = value;
  }

  Species? get species => _species;

  set species(Species? value) {
    _species = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String? get name => _name;

  set name(String? value) {
    _name = value!;
  }

  List<String>? get imageUrl => _imageUrl;

  set imageUrl(List<String>? value) {
    _imageUrl = value;
  }

  String? get desc => _desc;

  set desc(String? value) {
    _desc = value;
  }

  double? get age => _age;

  set age(double? value) {
    _age = value;
  }

  String? get condition => _condition;

  set condition(String? value) {
    _condition = value;
  }

  MyAddress? get address => _address;

  set address(MyAddress? value) {
    _address = value;
  }
}
