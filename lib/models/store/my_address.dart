import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:uuid/uuid.dart';

import '../users/singned_account.dart';

enum AddressTypes { HOME, OFFICE }

class MyAddress with ChangeNotifier {
  late String? id;
  late String? ownerId;
  late String? address;
  late String? name;
  late double? latitude;
  late double? longitude;
  AddressTypes? placeType;

  static MyAddress? currentAddress;

  setCurrentAddress(MyAddress? value) {
    MyAddress.currentAddress = value;
    notifyListeners();
  }

  static init() async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('stores')
        .doc(SignedAccount.instance.id!)
        .get();
    if (documentSnapshot['addressBook'].length > 0) {
      MyAddress.currentAddress = documentSnapshot['addressBook'][0];
    }
  }

  MyAddress(
      {this.id,
      this.ownerId,
      this.address,
      this.name,
      this.latitude,
      this.longitude,
      this.placeType});

  factory MyAddress.empty() {
    return MyAddress(
        id: Uuid().v4(),
        ownerId: SignedAccount.instance.id!,
        address: '',
        name: '',
        placeType: AddressTypes.HOME);
  }

  factory MyAddress.fromJson(Map<String, dynamic> json) {
    late AddressTypes placeType;
    switch (json['placeType']) {
      case 'home':
        placeType = AddressTypes.HOME;
        break;
      case 'office':
        placeType = AddressTypes.OFFICE;
        break;
      default:
        placeType = AddressTypes.OFFICE;
        break;
    }

    return MyAddress(
        id: json['id'],
        ownerId: json['ownerID'],
        address: json['address'],
        name: json['name'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        placeType: placeType);
  }

  @override
  String toString() {
    return 'My Address{id: $id, ownerId: $ownerId, address: $address, name: $name, latitude: $latitude, longitude: $longitude, placeType: $placeType}';
  }
}
