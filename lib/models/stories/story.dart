import 'package:cloud_firestore/cloud_firestore.dart';

class Story {
  String? id;
  String? caption;
  List<String>? comments;
  List<String>? likes;
  String? ownerID;
  String? songName;
  DateTime? timeCreated;
  String? thumbnailURL;
  String? videoURL;

  Story(
      {required this.id,
      required this.caption,
      required this.comments,
      required this.likes,
      required this.ownerID,
      required this.songName,
      required this.timeCreated,
      required this.thumbnailURL,
      required this.videoURL});

  factory Story.fromDocument(DocumentSnapshot document) {
    return Story(
      id: document['ID'],
      thumbnailURL: document['thumbnailURL'],
      ownerID: document['ownerID'],
      likes: List<String>.from(document['likes']),
      timeCreated: document['timeCreated'].toDate(),
      caption: document['caption'],
      songName: document['songName'],
      videoURL: document['videoURL'],
      comments: List<String>.from(document['comments']),
    );
  }

  @override
  String toString() {
    return "id: $id, songName: $songName";
  }
}
