import 'dart:async';
import 'dart:ui' as ui;

import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame/palette.dart';
import 'package:flutter/material.dart';

import '../../ui/screens/pet_park/pet_park_game.dart';
import '../pets/pet.dart';

const double MESSAGE_SHOW_TIME = 4;

final _tiny = TextPaint(style: TextStyle(fontSize: 15, color: Colors.black));

final _white = Paint()
  ..color = BasicPalette.white.color
  ..style = PaintingStyle.fill;

final style = TextStyle(color: BasicPalette.black.color);
final regular = TextPaint(style: style);

class PetInfoBox extends PositionComponent
    with HasGameRef<PetParkGame>, Tappable {
  Pet? pet;
  ui.Image? image;
  SpriteComponent? avatar;
  CancelButton cancelButton = CancelButton();
  TextComponent? userInfoText;
  PetProfileButton? profileButton;
  Function tap;

  PetInfoBox(this.tap, this.pet) : super(size: Vector2(375, 375));

  @override
  Future<void>? onLoad() async {
    /*--------------------- CANCEL BUTTON COMPONENT ---------------------*/
    cancelButton
      ..sprite = await Sprite.load('pet_park/cancel_btn.png')
      ..size = Vector2(50, 50)
      ..position = Vector2(size[0] - 50, size[1] - size[1] - 15);

    this.add(cancelButton);

    /*--------------------- PROFILE BUTTON COMPONENT ---------------------*/
    profileButton = PetProfileButton(this.tap, this.pet!);

    profileButton!
      ..sprite = await Sprite.load('pet_park/continue_button.png')
      ..size = Vector2(100, 50)
      ..position = Vector2(size[0] / 2 - 50, size[1]);

    this.add(profileButton!);

    /*--------------------- AVATAR COMPONENT ---------------------*/
    image = await getImage(pet!.photoUrl!);
    avatar = SpriteComponent.fromImage(image!, size: Vector2(150, 150));

    avatar!.position.x += size[0] / 2 - avatar!.width / 2;
    avatar!.position.y += size[1] / 3.5 - avatar!.height / 2;

    this.add(avatar!);

    /*--------------------- USER TEXT COMPONENT ---------------------*/
    userInfoText = TextComponent(text: pet.toString(), textRenderer: _tiny)
      ..size = Vector2(10, 10);

    userInfoText!.position.x += size[0] - size[0];
    userInfoText!.position.y += avatar!.position.y + avatar!.height + 10;

    this.add(userInfoText!);

    return super.onLoad();
  }

  @override
  void onGameResize(Vector2 gameSize) {
    super.onGameResize(gameSize);
  }

  @override
  void onMount() {
    super.onMount();
  }

  @override
  void onRemove() {
    super.onRemove();
  }

  @override
  bool onTapDown(TapDownInfo event) {
    print('TAP BOX ${event.eventPosition}');
    print('X ${x}');
    print('Y ${y}');

    return true;
  }

  Future<ui.Image> getImage(String url) async {
    Completer<ImageInfo> completer = Completer();
    var img = new NetworkImage(url);
    img
        .resolve(ImageConfiguration())
        .addListener(ImageStreamListener((ImageInfo info, bool _) {
      completer.complete(info);
    }));
    ImageInfo imageInfo = await completer.future;
    return imageInfo.image;
  }

  @override
  void render(Canvas canvas) async {
    final rect = Rect.fromLTWH(0, 0, size[0] / 1.025, size[1] * 1.2);
    canvas.drawRRect(
        RRect.fromRectAndRadius(rect, const Radius.circular(30)), _white);

    super.render(canvas);
  }

  @override
  void update(double dt) {
    super.update(dt);
  }
}

class CancelButton extends SpriteComponent with Tappable {
  @override
  Future<void>? onLoad() {
    return super.onLoad();
  }

  @override
  bool onTapDown(TapDownInfo info) {
    print('TAP CANCEL');
    return super.onTapDown(info);
  }
}

class PetProfileButton extends SpriteComponent with Tappable {
  Function tap;
  Pet pet;
  PetProfileButton(this.tap, this.pet);

  @override
  Future<void>? onLoad() {
    return super.onLoad();
  }

  @override
  bool onTapDown(TapDownInfo info) {
    tap(ownerId: this.pet.ownerId, petId: this.pet.id);
    print('TAP PROFILE');
    return super.onTapDown(info);
  }
}
