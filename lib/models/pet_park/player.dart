/** Copyright (c) 2021 Razeware LLC

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
    distribute, sublicense, create a derivative work, and/or sell copies of the
    Software in any work that is designed, intended, or marketed for pedagogical or
    instructional purposes related to programming, coding, application development,
    or information technology.  Permission for such use, copying, modification,
    merger, publication, distribution, sublicensing, creation of derivative works,
    or sale is expressly withheld.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE. **/
import 'package:bonfire/bonfire.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flame/components.dart';
import 'package:flame/geometry.dart';
import 'package:flame/input.dart';
import 'package:flame/sprite.dart';
import 'package:social_media/models/pet_park/info_box.dart';
import 'package:social_media/models/pet_park/pet_ingame.dart';
import 'package:social_media/models/pet_park/pet_park_room.dart';
import 'package:social_media/models/pet_park/text_box.dart';
import 'package:social_media/models/users/my_user.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/services/users/firebase_user_service.dart';

import '../../ui/screens/pet_park/pet_park_game.dart';
import 'game_direction.dart';
import 'world_collidable.dart';

class Player extends SpriteAnimationComponent
    with HasGameRef<PetParkGame>, HasHitboxes, Collidable, Tappable {
  late MyUser myUser;
  late String userId;
  String message = '';
  MyTextBox? _myTextBox;
  InfoBox? _infoBox;

  Function tap;

  late DatabaseReference realtimeDB = FirebaseDatabase.instance.reference();
  PetParkRoomUser? petParkRoomUserMainPlayer;

  List<PetInGame> petInGameList = [];

  final double _playerSpeed = 150.0;
  final double _animationSpeed = 0.2;
  SpriteSheet? spriteSheet;

  SpriteAnimation? _runDownAnimation;
  SpriteAnimation? _runLeftAnimation;
  SpriteAnimation? _runUpAnimation;
  SpriteAnimation? _runRightAnimation;
  SpriteAnimation? _standingAnimation;

  GameDirection direction = GameDirection.none;
  GameDirection _collisionDirection = GameDirection.none;
  bool _hasCollided = false;

  Player(this.tap, this.userId)
      : super(
          size: Vector2.all(50.0),
        ) {
    addHitbox(HitboxRectangle());
  }

  @override
  bool onTapDown(TapDownInfo event) {
    if (this.userId == SignedAccount.instance.id) {
      showMessage('Ounchhh... ');
    } else {
      print('TAP ${event.eventPosition}');
      print('X ${x}');
      print('Y ${y}');

      _infoBox = InfoBox(this.tap, myUser)
        ..x = x - 300
        ..y = y - 200;

      gameRef.add(_infoBox!);
    }
    return true;
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();
    this.myUser = MyUser.fromDocumentSnapshot(
        await FirebaseUserHelper().getUserById(this.userId));
    _loadAnimations(this.myUser.id!)
        .then((_) => {animation = _standingAnimation});
  }

  Future<void> loadPetInGameList(List<PetInGame> _petInGameList) async {
    print('PET LIST: ${myUser.petIdList}');

    myUser.petIdList.forEach((_petId) async {
      PetInGame _petInGame = PetInGame(this.tap, this.userId, _petId);
      print(_petInGame);
      _petInGameList.add(_petInGame);
    });
  }

  @override
  void update(double delta) {
    super.update(delta);
    movePlayer(delta);
    if (myUser.id == SignedAccount.instance.id) {
      this.petParkRoomUserMainPlayer!.userData = {
        'direction': EnumToString.convertToString(this.direction),
        'x': position.x,
        'y': position.y,
      };

      realtimeDB
          .child('pet_park')
          .child(this.petParkRoomUserMainPlayer!.userId)
          .update(this.petParkRoomUserMainPlayer!.userData);
    }

    if (_myTextBox != null) {
      if (gameRef.contains(_myTextBox!)) {
        _myTextBox!.x = x;
        _myTextBox!.y = y;
      }
    }

    if (_infoBox != null) {
      if (gameRef.contains(_infoBox!)) {
        _infoBox!.x = x - 175;
        _infoBox!.y = y - 250;
      }
    }
  }

  Future showMessage(String? message) async {
    if (message == null) {
      return;
    } else {
      if (_myTextBox == null) {
        _myTextBox = MyTextBox(message)
          ..x = x
          ..y = y;

        gameRef.add(_myTextBox!);
      } else {
        gameRef.remove(_myTextBox!);
        _myTextBox = MyTextBox(message)
          ..x = x
          ..y = y;
        gameRef.add(_myTextBox!);
      }
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, Collidable other) {
    super.onCollision(intersectionPoints, other);

    if (other is WorldCollidable) {
      if (!_hasCollided) {
        _hasCollided = true;
        _collisionDirection = direction;
      }
    }
  }

  @override
  void onCollisionEnd(Collidable other) {
    _hasCollided = false;
  }

  Future<void> _loadAnimations(String id) async {
    if (id == "testerid") {
      spriteSheet = SpriteSheet(
        image: await gameRef.images.load('pet_park/other_player_1.png'),
        srcSize: Vector2(30.0, 32.0),
      );
    } else {
      spriteSheet = SpriteSheet(
        image: await gameRef.images.load('pet_park/player_spritesheet.png'),
        srcSize: Vector2(29.0, 32.0),
      );
    }
    if (id == "testerid") {
      _runDownAnimation = spriteSheet!
          .createAnimation(row: 0, stepTime: _animationSpeed, to: 3);

      _runLeftAnimation = spriteSheet!
          .createAnimation(row: 1, stepTime: _animationSpeed, to: 3);

      _runRightAnimation = spriteSheet!
          .createAnimation(row: 2, stepTime: _animationSpeed, to: 3);

      _runUpAnimation = spriteSheet!
          .createAnimation(row: 3, stepTime: _animationSpeed, to: 3);

      _standingAnimation = spriteSheet!
          .createAnimation(row: 0, stepTime: _animationSpeed, to: 1);
    } else {
      _runDownAnimation = spriteSheet!
          .createAnimation(row: 0, stepTime: _animationSpeed, to: 4);

      _runLeftAnimation = spriteSheet!
          .createAnimation(row: 1, stepTime: _animationSpeed, to: 4);

      _runUpAnimation = spriteSheet!
          .createAnimation(row: 2, stepTime: _animationSpeed, to: 4);

      _runRightAnimation = spriteSheet!
          .createAnimation(row: 3, stepTime: _animationSpeed, to: 4);

      _standingAnimation = spriteSheet!
          .createAnimation(row: 0, stepTime: _animationSpeed, to: 1);
    }
  }

  void movePlayer(double delta) {
    switch (direction) {
      case GameDirection.up:
        if (canPlayerMoveUp()) {
          animation = _runUpAnimation;
          moveUp(delta);
        }
        break;
      case GameDirection.down:
        if (canPlayerMoveDown()) {
          animation = _runDownAnimation;
          moveDown(delta);
        }
        break;
      case GameDirection.left:
        if (canPlayerMoveLeft()) {
          animation = _runLeftAnimation;
          moveLeft(delta);
        }
        break;
      case GameDirection.right:
        if (canPlayerMoveRight()) {
          animation = _runRightAnimation;
          moveRight(delta);
        }
        break;
      case GameDirection.none:
        animation = _standingAnimation;
        break;
    }
  }

  bool canPlayerMoveUp() {
    if (_hasCollided && _collisionDirection == GameDirection.up) {
      return false;
    }
    return true;
  }

  bool canPlayerMoveDown() {
    if (_hasCollided && _collisionDirection == GameDirection.down) {
      return false;
    }
    return true;
  }

  bool canPlayerMoveLeft() {
    if (_hasCollided && _collisionDirection == GameDirection.left) {
      return false;
    }
    return true;
  }

  bool canPlayerMoveRight() {
    if (_hasCollided && _collisionDirection == GameDirection.right) {
      return false;
    }
    return true;
  }

  void moveUp(double delta) {
    position.add(Vector2(0, delta * -_playerSpeed));
  }

  void moveDown(double delta) {
    position.add(Vector2(0, delta * _playerSpeed));
  }

  void moveLeft(double delta) {
    position.add(Vector2(delta * -_playerSpeed, 0));
  }

  void moveRight(double delta) {
    position.add(Vector2(delta * _playerSpeed, 0));
  }
}
