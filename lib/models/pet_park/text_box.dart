import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame/palette.dart';
import 'package:flutter/material.dart';

import '../../ui/screens/pet_park/pet_park_game.dart';

const double MESSAGE_SHOW_TIME = 4;

final _tiny = TextPaint(style: TextStyle(fontSize: 15, color: Colors.black));

final _white = Paint()
  ..color = BasicPalette.white.color
  ..style = PaintingStyle.fill;

class MyTextBox extends TextBoxComponent with HasGameRef<PetParkGame> {
  late Timer _timer;
  MyTextBox(String text)
      : super(
          text: text,
          textRenderer: _tiny,
          boxConfig: TextBoxConfig(
              timePerChar: 0.01, maxWidth: text.length * 8.5), // T&E
        ) {
    _timer = Timer(MESSAGE_SHOW_TIME + text.length * 0.15, onTick: () {
      gameRef.remove(this);
    });
  }

  @override
  void onGameResize(Vector2 gameSize) {
    y = gameRef.mainPlayer.y;
    super.onGameResize(gameSize);
  }

  @override
  void onMount() {
    _timer.start();
    super.onMount();
  }

  @override
  void onRemove() {
    super.onRemove();
  }

  @override
  void drawBackground(Canvas c) {
    final rect = Rect.fromLTWH(0, 0, width, height);
    c.drawRRect(
        RRect.fromRectAndRadius(rect, const Radius.circular(3)), _white);
  }

  @override
  void update(double dt) {
    _timer.update(dt);
    super.update(dt);
  }
}
