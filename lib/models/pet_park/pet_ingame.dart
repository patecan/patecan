/** Copyright (c) 2021 Razeware LLC

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
    distribute, sublicense, create a derivative work, and/or sell copies of the
    Software in any work that is designed, intended, or marketed for pedagogical or
    instructional purposes related to programming, coding, application development,
    or information technology.  Permission for such use, copying, modification,
    merger, publication, distribution, sublicensing, creation of derivative works,
    or sale is expressly withheld.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE. **/
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flame/components.dart';
import 'package:flame/geometry.dart';
import 'package:flame/input.dart';
import 'package:flame/sprite.dart';
import 'package:social_media/models/pet_park/pet_info_box.dart';

import '../pets/pet.dart';
import 'game_direction.dart';
import 'world_collidable.dart';

class PetInGame extends SpriteAnimationComponent
    with HasGameRef, HasHitboxes, Collidable, Tappable {
  late Pet pet;
  late String petId;
  late String ownerId;
  SpriteSheet? spriteSheet;

  PetInfoBox? _infoBox;

  final double _playerSpeed = 150.0;
  final double _animationSpeed = 0.2;

  SpriteAnimation? _runDownAnimation;
  SpriteAnimation? _runLeftAnimation;
  SpriteAnimation? _runUpAnimation;
  SpriteAnimation? _runRightAnimation;
  SpriteAnimation? _standingAnimation;

  GameDirection direction = GameDirection.none;
  GameDirection _collisionDirection = GameDirection.none;
  bool _hasCollided = false;

  Function tap;

  PetInGame(this.tap, this.ownerId, this.petId)
      : super(
          size: Vector2.all(50.0),
        ) {
    addHitbox(HitboxRectangle());
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();
    DocumentSnapshot document = await FirebaseFirestore.instance
        .collection('pets')
        .doc(this.ownerId)
        .collection('petOwned')
        .doc(this.petId)
        .get();

    this.pet = Pet.fromDocumentSnapshot(document);
    _loadAnimations(this.petId).then((_) => {animation = _standingAnimation});
  }

  @override
  void update(double delta) {
    super.update(delta);
    movePet(delta);

    if (_infoBox != null) {
      if (gameRef.contains(_infoBox!)) {
        _infoBox!.x = x - 150;
        _infoBox!.y = y - 300;
      }
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, Collidable other) {
    super.onCollision(intersectionPoints, other);

    if (other is WorldCollidable) {
      if (!_hasCollided) {
        _hasCollided = true;
        _collisionDirection = direction;
      }
    }
  }

  @override
  void onCollisionEnd(Collidable other) {
    _hasCollided = false;
  }

  Future<void> _loadAnimations(String id) async {
    if (id == "4R4g5Axtaw9GUFOjEi2x") {
      spriteSheet = SpriteSheet(
        image: await gameRef.images.load('pet_park/cat_spritesheet.png'),
        srcSize: Vector2(48.0, 48.0),
      );
    } else if (id == "4xoqCRgj7zUsPz86bku9") {
      spriteSheet = SpriteSheet(
        image: await gameRef.images.load('pet_park/yellow_cat_spritesheet.png'),
        srcSize: Vector2(48.0, 48.0),
      );
    } else if (id == "testerpetid") {
      spriteSheet = SpriteSheet(
        image: await gameRef.images.load('pet_park/black_dog_spritesheet.png'),
        srcSize: Vector2(32.0, 32.0),
      );
    } else {
      spriteSheet = SpriteSheet(
        image: await gameRef.images.load('pet_park/yellow_cat_spritesheet.png'),
        srcSize: Vector2(48.0, 48.0),
      );
    }

    _standingAnimation =
        spriteSheet!.createAnimation(row: 0, stepTime: _animationSpeed, to: 1);

    _runDownAnimation =
        spriteSheet!.createAnimation(row: 0, stepTime: _animationSpeed, to: 3);

    _runLeftAnimation =
        spriteSheet!.createAnimation(row: 1, stepTime: _animationSpeed, to: 3);

    _runRightAnimation =
        spriteSheet!.createAnimation(row: 2, stepTime: _animationSpeed, to: 3);

    _runUpAnimation =
        spriteSheet!.createAnimation(row: 3, stepTime: _animationSpeed, to: 3);
  }

  void movePet(double delta) {
    switch (direction) {
      case GameDirection.up:
        if (canPlayerMoveUp()) {
          animation = _runUpAnimation;
          moveUp(delta);
        }
        break;
      case GameDirection.down:
        if (canPlayerMoveDown()) {
          animation = _runDownAnimation;
          moveDown(delta);
        }
        break;
      case GameDirection.left:
        if (canPlayerMoveLeft()) {
          animation = _runLeftAnimation;
          moveLeft(delta);
        }
        break;
      case GameDirection.right:
        if (canPlayerMoveRight()) {
          animation = _runRightAnimation;
          moveRight(delta);
        }
        break;
      case GameDirection.none:
        animation = _standingAnimation;
        break;
    }
  }

  bool canPlayerMoveUp() {
    if (_hasCollided && _collisionDirection == GameDirection.up) {
      return false;
    }
    return true;
  }

  bool canPlayerMoveDown() {
    if (_hasCollided && _collisionDirection == GameDirection.down) {
      return false;
    }
    return true;
  }

  bool canPlayerMoveLeft() {
    if (_hasCollided && _collisionDirection == GameDirection.left) {
      return false;
    }
    return true;
  }

  bool canPlayerMoveRight() {
    if (_hasCollided && _collisionDirection == GameDirection.right) {
      return false;
    }
    return true;
  }

  void moveUp(double delta) {
    position.add(Vector2(0, delta * -_playerSpeed));
  }

  void moveDown(double delta) {
    position.add(Vector2(0, delta * _playerSpeed));
  }

  void moveLeft(double delta) {
    position.add(Vector2(delta * -_playerSpeed, 0));
  }

  void moveRight(double delta) {
    position.add(Vector2(delta * _playerSpeed, 0));
  }

  @override
  bool onTapDown(TapDownInfo info) {
    if (_infoBox == null) {
      _infoBox = PetInfoBox(this.tap, this.pet)
        ..x = x - 150
        ..y = y - 300;

      gameRef.add(_infoBox!);
    }
    return super.onTapDown(info);
  }
}
