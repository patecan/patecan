class PetParkRoomUser {
  String petParkRoomId;
  String petParkRoomName;
  String userId;
  DateTime timeJoined;
  Map<String, dynamic> userData;

  PetParkRoomUser({
    required this.petParkRoomId,
    required this.petParkRoomName,
    required this.userId,
    required this.timeJoined,
    required this.userData,
  });
}
