import 'package:cloud_firestore/cloud_firestore.dart';

enum MessageType {
  Text,
  Image,
  Video,
  GIF,
  Voice,
  Request,
}

class Message {
  String? id;
  String? _conversationID;
  String? _displayName;
  String? _photoUrl;
  String? _senderID;
  String? _lastMessageContent;
  int? _unseenCount;
  DateTime? _sentTime;
  MessageType? messageType;

  /* REQUEST */
  String? requestAttachment;
  String? requestContent;
  double? requestPoint;
  bool proofResponse = false;

  Message({
    this.id,
    String? conversationID,
    String? senderID,
    String? lastMessageContent,
    int? unseenCount,
    DateTime? sentTime,
    String? displayName,
    String? photoUrl,
    MessageType? messageType,
    this.requestAttachment,
    this.requestContent,
    this.requestPoint,
    this.proofResponse = false,
  }) {
    this._senderID = senderID;
    this._lastMessageContent = lastMessageContent;
    this._unseenCount = unseenCount;
    this._sentTime = sentTime;
    this._displayName = displayName;
    this._photoUrl = photoUrl;
    this._conversationID = conversationID;
    this.messageType = messageType;
  }

  factory Message.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    late MessageType messageType;
    switch (documentSnapshot['message_type']) {
      case "text":
        messageType = MessageType.Text;
        break;
      case "gif":
        messageType = MessageType.GIF;
        break;
      case "video":
        messageType = MessageType.Video;
        break;
      case "image":
        messageType = MessageType.Image;
        break;
      case "voice":
        messageType = MessageType.Voice;
        break;
      case "request":
        messageType = MessageType.Request;
        break;
    }

    return Message(
      id: documentSnapshot['_id'],
      conversationID: documentSnapshot['conversationID'],
      senderID: documentSnapshot['senderID'],
      lastMessageContent: documentSnapshot['message_content'],
      unseenCount: documentSnapshot['unseenCount'],
      sentTime: documentSnapshot['sentTime'].toDate(),
      displayName: documentSnapshot['displayName'],
      photoUrl: documentSnapshot['photoUrl'],
      messageType: messageType,
      requestAttachment: documentSnapshot['requestAttachment'],
      requestContent: documentSnapshot['requestContent'],
      requestPoint: documentSnapshot['requestPoint'],
      proofResponse: documentSnapshot['proofResponse'],
    );
  }

  factory Message.fromJson(Map<String, dynamic> json) {
    late MessageType messageType;
    switch (json['message_type']) {
      case "text":
        messageType = MessageType.Text;
        break;
      case "gif":
        messageType = MessageType.GIF;
        break;
      case "video":
        messageType = MessageType.Video;
        break;
      case "image":
        messageType = MessageType.Image;
        break;
      case "voice":
        messageType = MessageType.Voice;
        break;
      case "request":
        messageType = MessageType.Request;
        break;
    }

    if (messageType != MessageType.Request) {
      return Message(
        id: json['_id'],
        displayName: json['displayName'],
        photoUrl: json['photoUrl'],
        conversationID: json['conversationID'],
        senderID: json['senderID'],
        lastMessageContent: json['message_content'],
        sentTime: json['sentTime'].toDate(),
        messageType: messageType,
      );
    } else {
      return Message(
        id: json['_id'],
        displayName: json['displayName'],
        photoUrl: json['photoUrl'],
        conversationID: json['conversationID'],
        senderID: json['senderID'],
        lastMessageContent: json['message_content'],
        sentTime: json['sentTime'].toDate(),
        messageType: messageType,
        requestAttachment: json['requestAttachment'],
        requestContent: json['requestContent'],
        requestPoint: json['requestPoint'],
        proofResponse: json['proofResponse'],
      );
    }
  }

  @override
  String toString() {
    return 'Message{id: $id, _conversationID: $_conversationID, _displayName: $_displayName, _photoUrl: $_photoUrl, _senderID: $_senderID, _lastMessageContent: $_lastMessageContent, _unseenCount: $_unseenCount, _sentTime: $_sentTime, messageType: $messageType, requestAttachment: $requestAttachment, requestContent: $requestContent, requestPoint: $requestPoint}';
  }

  DateTime? get sentTime => _sentTime;

  set sentTime(DateTime? value) {
    _sentTime = value;
  }

  int? get unseenCount => _unseenCount;

  set unseenCount(int? value) {
    _unseenCount = value;
  }

  String? get lastMessageContent => _lastMessageContent;

  set lastMessageContent(String? value) {
    _lastMessageContent = value;
  }

  String? get senderID => _senderID;

  set senderID(String? value) {
    _senderID = value;
  }

  String? get photoUrl => _photoUrl;

  set photoUrl(String? value) {
    _photoUrl = value;
  }

  String? get displayName => _displayName;

  set displayName(String? value) {
    _displayName = value;
  }

  String? get conversationID => _conversationID;

  set conversationID(String? value) {
    _conversationID = value;
  }
}
