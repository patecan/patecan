import 'package:cloud_firestore/cloud_firestore.dart';

import 'message.dart';

class Conversation {
  final String conversationID;
  final List members;
  final List messages;
  final String ownerID;
  double? requestPoint;

  Conversation(
      {required this.conversationID,
      required this.members,
      required this.messages,
      required this.ownerID,
      this.requestPoint});

  factory Conversation.fromDocument(DocumentSnapshot documentSnapshot) {
    List _messagesList = [];
    MessageType messageType;
    _messagesList = documentSnapshot['messages'].map((message) {
      switch (message['message_type']) {
        case 'text':
          messageType = MessageType.Text;
          break;
        case 'image':
          messageType = MessageType.Image;
          break;
        case 'video':
          messageType = MessageType.Video;
          break;
        case 'voice':
          messageType = MessageType.Voice;
          break;
        case 'gif':
          messageType = MessageType.GIF;
          break;
        case 'request':
          messageType = MessageType.Request;
          break;
        default:
          messageType = MessageType.Text;
          break;
      }

      if (message['message_type'] != 'request') {
        return Message(
          id: message['_id'],
          displayName: message['displayName'],
          photoUrl: message['photoUrl'],
          conversationID: documentSnapshot['conversationID'],
          senderID: message['senderID'],
          lastMessageContent: message['message_content'],
          sentTime: message['sentTime'].toDate(),
          messageType: messageType,
        );
      } else {
        return Message(
          id: message['_id'],
          displayName: message['displayName'],
          photoUrl: message['photoUrl'],
          conversationID: documentSnapshot['conversationID'],
          senderID: message['senderID'],
          lastMessageContent: message['message_content'],
          sentTime: message['sentTime'].toDate(),
          messageType: messageType,
          requestAttachment: message['requestAttachment'],
          requestContent: message['requestContent'],
          requestPoint: message['requestPoint'],
          proofResponse: message['proofResponse'],
        );
      }
    }).toList();

    return Conversation(
        conversationID: documentSnapshot['conversationID'],
        members: documentSnapshot['members'],
        messages: _messagesList,
        requestPoint: double.parse(documentSnapshot['requestPoint'].toString()),
        ownerID: documentSnapshot['ownerID']);
  }

  @override
  String toString() {
    return 'Conversation{conversationID: $conversationID, members: $members, messages: $messages, senderID: $ownerID}';
  }
}
