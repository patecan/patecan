import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:provider/provider.dart';
import 'package:social_media/constant/credentials.dart';
import 'package:social_media/models/store/my_address.dart';
import 'package:social_media/models/users/singned_account.dart';
import 'package:social_media/providers/dark_theme_provider.dart';
import 'package:social_media/providers/note_provider.dart';
import 'package:social_media/providers/pet_park_chat_provider.dart';
import 'package:social_media/providers/place_provider.dart';
import 'package:social_media/providers/property_list_provider.dart';
import 'package:social_media/providers/rentlist_provider.dart';
import 'package:social_media/providers/wishlist_provider.dart';
import 'package:social_media/ui/screens/address_book/address_book_screen.dart';
import 'package:social_media/ui/screens/authentication/recover_password_screen.dart';
import 'package:social_media/ui/screens/homepage_screen.dart';
import 'package:social_media/ui/screens/messages/messaging_main_screen.dart';
import 'package:social_media/ui/screens/notes/note_list_screen.dart';
import 'package:social_media/ui/screens/places/foster_places_main_screen.dart';
import 'package:social_media/ui/screens/places/trip/trip_screen.dart';
import 'package:social_media/ui/screens/places/user_places_main_screen.dart';
import 'package:social_media/ui/screens/shorts/short_video_screen.dart';
import 'package:social_media/ui/screens/stores/cart_screen.dart';
import 'package:social_media/ui/screens/stores/checkout_screen.dart';
import 'package:social_media/ui/screens/stores/search_screen.dart';
import 'package:social_media/ui/screens/stores/store_main_screen.dart';
import 'package:social_media/ui/screens/stores/store_notification_screen.dart';
import 'package:social_media/ui/screens/stores/wishlist_screen.dart';
import 'package:social_media/ui/screens/users/user_profile_screen.dart';
import 'package:social_media/ui/screens/vet/vet_meeting_provider.dart';

import 'constant/theme_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Stripe.publishableKey = Credentials.STRIPE_PUBLISH_KEY;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  DarkThemeProvider darkThemeProvider = new DarkThemeProvider();

  void getCurrentAppTheme() async {
    darkThemeProvider.setDarkTheme(await darkThemeProvider.getTheme());
  }

  @override
  void initState() {
    getCurrentAppTheme();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Nunito'),
      debugShowCheckedModeBanner: false,
      title: 'Patecan World',
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => darkThemeProvider),
          ChangeNotifierProvider(create: (context) => PlaceProvider()),
          ChangeNotifierProvider(create: (context) => PropertiesListProvider()),
          ChangeNotifierProvider(create: (context) => RentListProvider()),
          ChangeNotifierProvider(create: (context) => WishlistProvider()),
          ChangeNotifierProvider(create: (context) => MyAddress.empty()),
          ChangeNotifierProvider(create: (context) => NoteProvider()),
          ChangeNotifierProvider(create: (context) => ChatProvider()),
          ChangeNotifierProvider(create: (context) => VetMeetingProvider()),
        ],
        child: Consumer<DarkThemeProvider>(
          builder: (context, themeData, child) {
            return MaterialApp(
              theme: Styles.themeData(darkThemeProvider.isDarkTheme, context),
              title: "Social Media",
              home: HomepageScreen(),
              routes: {
                '/store_search_screen': (context) => StoreSearchScreen(),
                '/store_main_screen': (context) => StoreMainScreen(),
                '/user_profile': (context) =>
                    UserProfileScreen(SignedAccount.instance.id!),
                '/user_places_main_screen': (context) => UserPlacesMainScreen(),
                '/foster_places_main_screen': (context) =>
                    FosterPlacesMainScreen(),
                '/wishlist_screen': (context) => WishlistScreen(),
                '/recover_password_screen': (context) =>
                    RecoverPasswordScreen(),
                '/trip_detail_screen': (context) => TripDetailScreen.empty(),
                '/checkout_screen': (context) => CheckoutScreen(),
                '/address_book_screen': (context) => AddressBookScreen(),
                '/cart_screen': (context) => CartScreen(),
                '/short_video_screen': (context) => ShortVideoScreen(),
                '/notification_screen': (context) => StoreNotificationScreen(),
                '/messaging_main_screen': (context) => MessagingMainScreen(),
                '/note_list_screen': (context) => NoteListScreen(),
              },
            );
          },
        ),
      ),
    );
  }
}
