/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.julow.environment_sensors;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.julow.environment_sensors";
  public static final String BUILD_TYPE = "debug";
}
